from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from accwidgets.app_frame import ApplicationFrame
from accwidgets.qt import exec_app_interruptable

import cmmnbuild_dep_manager

import sys
import jpype
import qtawesome as qta

cern = jpype.JPackage("cern")

from functools import partial

# import pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.comm import ccda
from expert_gui_core.gui.widgets.combiner import metanxcalswidget

from expert_gui_core.gui.widgets.combiner import metapropertyfesawidget
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.timing import timingwidget
from expert_gui_core.gui.widgets.pyqt import systemmonitor
from expert_gui_core.gui.widgets.pyqt import convertorwidget


class BIApplicationFrame(ApplicationFrame):

    def __init__(self, app=None, title=None, use_timing_bar=True,
                 use_log_console=False,
                 use_rbac=True,
                 use_fesa=True,
                 use_nxcals=True):

        ApplicationFrame.__init__(self, use_timing_bar=False,
                                  use_log_console=use_log_console,
                                  use_rbac=use_rbac)

        qta.icon("fa5.clipboard")
        
        self._app = app
                
        # init frame

        self.title = title
        self.left = 100
        self.top = 100
        self.width = 1600
        self.height = 1000
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.default_palette = QGuiApplication.palette()
        
        # graphical mutex

        self._mutex_graph = False

        # FESA browser

        self._fesa_browser = None

        # RBAC

        cmmnbuild_dep_manager.Manager().jvm_required()

        rba_button = self.rba_widget
        if rba_button is not None:
            rba_button.loginSucceeded.connect(self.on_pyrbac_login)
            rba_button.loginFailed.connect(self.on_pyrbac_error)
            rba_button.logoutFinished.connect(self.on_pyrbac_logout)

        # NXCALS browser
        self._nxcals_browser = None

        # Timing panel
        domain = ["PSB", "LEI", "CPS", "SPS", "LNA", "ADE"]
        self._timing_panel = None
        if use_timing_bar:
            self._timing_panel = timingwidget.TimingPanel(domain=domain, listener=None)
            self._timing_panel.hide()

            # Menu
        menu_bar = self.menuBar()

        # tools menus

        self.tools_menu = menu_bar.addMenu('&Tools')
        if use_log_console:
            self.tools_menu.addAction('Show/Hide Log', self.show_hide_log)
        if use_timing_bar:
            self.tools_menu.addAction('Show/Hide Timing', self.show_hide_timing)
        if use_rbac:
            self.tools_menu.addAction('Show/Hide RBAC', self.show_hide_rbac)
        if use_fesa:
            self.tools_menu.addAction('Add FESA Browser', self.add_fesa_browser)
        if use_nxcals:
            self.tools_menu.addAction('Add NXCALS Browser', self.add_nxcals_browser)
        self.tools_menu.addAction('Show/Hide System Monitor', self.show_hide_sysmonitor)
        self.tools_menu.addAction('Show/Hide Data Convertor', self.show_hide_convertor)

        # system monitor

        self.system_monitor = systemmonitor.SystemMonitorWindow(parent=None, time_period=1 * 1000)
        self.system_monitor.resize(900, 200)
        self.system_monitor.hide()

        # convertor

        self.convertor = convertorwidget.ConvertorWidget(parent=None)
        self.convertor.resize(900, 100)
        self.convertor.hide()

        # theme menu

        theme_menu = menu_bar.addMenu('&Theme')
        theme_menu.addAction('Dark', self.dark_)
        theme_menu.addAction('Light', self.light_)

        # font size menu

        font_menu = menu_bar.addMenu('&Font')
        for i in range(5, 21):
            font_menu.addAction(str(i), partial(self.set_font_size, i))

        # tab panels

        self.tabpanels = []

        self._tabs = QTabWidget()
        self._tabs.setTabsClosable(True)
        self._tabs.tabCloseRequested.connect(self.closeTab)

        # center widget

        center_widget = QWidget()
        self._layout = QGridLayout(center_widget)
        self._layout.setContentsMargins(5, 5, 5, 0)
        self._layout.setSpacing(0)

        if self._timing_panel is not None:
            self._layout.addWidget(self._timing_panel, 0, 0)
            self._layout.setRowStretch(0, 0)

        self.setCentralWidget(center_widget)
        self._layout.addWidget(self._tabs, 2, 0)
        self._layout.setRowStretch(2, 1)

        # update time used by handle event

        self._update_time = 0

        # show

        self.show()

    def add_fesa_browser(self):
        """Add fesa browser in the tab panel"""
        if self._fesa_browser is None:
            self._fesa_browser = metapropertyfesawidget.MetaPropertyFesaWidget(self, self._timing_panel)
            if Colors.COLOR_LIGHT:
                self._fesa_browser.light_()
            else:
                self._fesa_browser.dark_()
            self._tabs.addTab(self._fesa_browser, "FESA Browser")
            self.tabpanels.append(self._fesa_browser)

    def add_fesa_class_menu(self, classes=None):

        if classes is None:
            return

        # FESA menus

        class_menu = self.menuBar().addMenu('&Class Fec Devices')
        class_menu_item = class_menu.addAction('ALL', self.open_devices_panel)
        fesa_db_fec_class_info = ccda.get_fecs_from_class(classes, operational=False)
        for fesa_class_name, fesa_class_fec_names in fesa_db_fec_class_info.items():
            dict_fesa_fec = ccda.get_devices_from_class_per_fec(fesa_class_name)
            class_menu_item = class_menu.addMenu(fesa_class_name)
            for fec_name in dict_fesa_fec.keys():
                devices = dict_fesa_fec[fec_name]
                fec_menu_item = class_menu_item.addMenu(fec_name)
                for device in devices:
                    fec_menu_item.addAction(device, partial(self.open_tab_panel, device))

    def open_devices_panel(self):
        pass

    def open_tab_panel(self, device=None):
        pass

    def on_pyrbac_login(self, pyrbac_token):
        """RBAC login action"""
        new_token = cern.rbac.common.RbaToken.parseAndValidate(jpype.java.nio.ByteBuffer.wrap(pyrbac_token.encode()))
        cern.rbac.util.holder.ClientTierTokenHolder.setRbaToken(new_token)

    def on_pyrbac_error(self, err):
        """handling of RBAC errors"""
        pass

    def on_pyrbac_logout(self):
        """RBAC logout action"""
        pass

    def show_hide_timing(self):
        """Show or hide timing panel"""
        if self._timing_panel is not None:
            if self._timing_panel.isVisible():
                self._timing_panel.hide()
            else:
                self._timing_panel.show()

    def show_hide_rbac(self):
        """Show or hide RBAC toolbar"""
        if self.main_toolbar() is not None:
            if self.main_toolbar().isVisible():
                self.main_toolbar().hide()
            else:
                self.main_toolbar().show()

    def show_hide_sysmonitor(self):
        """Show or hide Log toolbar"""
        if self.system_monitor is not None:
            if self.system_monitor.isVisible():
                self.system_monitor.hide()
            else:
                self.system_monitor.show()

    def show_hide_convertor(self):
        """Show or hide Log toolbar"""
        if self.convertor is not None:
            if self.convertor.isVisible():
                self.convertor.hide()
            else:
                self.convertor.show()

    def set_font_size(self, size=8):
        """Set font size in panels"""
        if self._app is not None:
            font_text_user = self._app.font()
            font_text_user.setPointSize(size)
            self._app.setFont(font_text_user)
        self.setStyleSheet("font-size:" + str(size) + ";")
        for tab in self.tabpanels:
            try:
                tab.set_font_size(size=size)
            except:
                pass
        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()

    def add_nxcals_browser(self):
        """Add edge browser in the tab panel"""
        if self._nxcals_browser is None:
            self._nxcals_browser = metanxcalswidget.MetaNXCALSWidget(self)
            self._tabs.addTab(self._nxcals_browser, "NXCALS Browser")
            self.tabpanels.append(self._nxcals_browser)
            if Colors.COLOR_LIGHT:
                self._nxcals_browser.light_()
            else:
                self._nxcals_browser.dark_()

    def show_hide_log(self):
        """Show or hide Log toolbar"""
        if self.log_console is not None:
            if self.log_console.isVisible():
                self.log_console.hide()
            else:
                self.log_console.show()

    def dark_(self):
        """Set dark theme"""
        self._mutex_graph = True
        darkpalette = QPalette()
        darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
        darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
        darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
        darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
        self._app.setPalette(darkpalette)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")
        self._tabs.setStyleSheet(
            "background-color: " + Colors.STR_COLOR_L2BLACK + ";color:" + Colors.STR_COLOR_WHITE + ";")

        qss = """
            QMenuBar::item {
                spacing: 2px;           
                padding: 2px 10px;
                background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
            }
            QMenuBar::item:selected {    
                background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }
            QMenuBar::item:pressed {
                background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
            }              
            QScrollArea {
                background-color: transparent;
            }
            QRadioButton::checked {
                color: """+Colors.STR_COLOR_L2BLUE+""";  
            }
            QRadioButton::indicator {
                border-radius: 6px;                            
            }
            QRadioButton::indicator::unchecked{ 
                border-radius: 6px;
                border:2px solid """+Colors.STR_COLOR_LIGHT1GRAY+""";
                background-color: """+Colors.STR_COLOR_L3BLACK+""";            
            }

            QRadioButton::indicator::checked{ 
                border: 2px solid; 
                border-color: """+Colors.STR_COLOR_L2BLUE+""";  
                border-radius: 6px;
                background-color: """+Colors.STR_COLOR_L2BLUE+""";   
            }
            QCheckBox {
                color: """ + Colors.STR_COLOR_WHITE + """;
            }
            QCheckBox::indicator {
                max-width:14;
                max-height:14;
                border:1px solid """+Colors.STR_COLOR_LIGHT1GRAY+""";
                background-color: """+Colors.STR_COLOR_L3BLACK+""";            
            }    
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """+Colors.STR_COLOR_L3BLACK+""";
                background-color: """+Colors.STR_COLOR_L2BLUE+""";            
            }             
            QLineEdit {
                background-color:"""+Colors.STR_COLOR_L2BLACK+""";
                color:"""+Colors.STR_COLOR_WHITE+""";
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """+Colors.STR_COLOR_LIGHT1GRAY+""";
            }         
            QComboBox {
                border:none;
                background-color:"""+Colors.STR_COLOR_L1BLACK+""";
                selection-color:"""+Colors.STR_COLOR_WHITE+""";
                color:"""+Colors.STR_COLOR_WHITE+""";
                selection-background-color:"""+Colors.STR_COLOR_LBLUE+""";
            }                
            QListView {
                background-color:"""+Colors.STR_COLOR_L2BLACK+""";
                selection-color:"""+Colors.STR_COLOR_WHITE+""";
                color:"""+Colors.STR_COLOR_WHITE+""";
                selection-background-color:"""+Colors.STR_COLOR_LBLUE+""";
            }
        """
        self._app.setStyleSheet(qss)
        Colors.COLOR_LIGHT = False
        if self._timing_panel is not None:
            self._timing_panel.dark_()
        if self._nxcals_browser is not None:
            self._nxcals_browser.dark_()
        if self._fesa_browser is not None:
            self._fesa_browser.dark_()
        self.system_monitor.dark_()
        self.convertor.dark_()
        self.dark_tabs()
        self._mutex_graph = False

    def light_(self):
        """Set light theme"""
        self._mutex_graph = True
        QGuiApplication.setPalette(self.default_palette)
        self.setStyleSheet("background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")
        self._tabs.setStyleSheet(
            "background-color: " + Colors.STR_COLOR_DWHITE + ";color:" + Colors.STR_COLOR_BLACK + ";")

        qss = """
            QScrollArea {
                background-color: transparent;
            }
             QCheckBox::indicator {
                max-width:14;
                max-height:14;
                border:1px solid """+Colors.STR_COLOR_LIGHT5GRAY+""";
                background-color: """+Colors.STR_COLOR_WHITE+""";            
            }       
            QCheckBox::indicator:checked {
                max-width:14;
                max-height:14;
                border:0px solid """+Colors.STR_COLOR_LIGHT4GRAY+""";
                background-color: """+Colors.STR_COLOR_L2BLUE+""";            
            }
            QCheckBox {            
                color: """+Colors.STR_COLOR_BLACK+""";                        
            }
            QRadioButton::checked {
                color: """+Colors.STR_COLOR_L2BLUE+""";  
            }
            QRadioButton::indicator {
                border-radius: 6px;                            
            }
            QRadioButton::indicator::unchecked{ 
                border-radius: 6px;
                border:2px solid """+Colors.STR_COLOR_LIGHT4GRAY+""";
                background-color: """+Colors.STR_COLOR_WHITE+""";            
            }

            QRadioButton::indicator::checked{ 
                border: 2px solid; 
                border-color: """+Colors.STR_COLOR_L2BLUE+""";  
                border-radius: 6px;
                background-color: """+Colors.STR_COLOR_L2BLUE+""";   
            }
            QLineEdit {
                color:"""+Colors.STR_COLOR_BLACK+""";
                text-align:left;
                padding-left:2px;
                padding-top:2px;
                padding-bottom:2px;
                border:0px solid """+Colors.STR_COLOR_LIGHT1GRAY+""";
                background-color:"""+Colors.STR_COLOR_WHITE+""";
            }
            QComboBox {
                border:0px solid """+Colors.STR_COLOR_LIGHT4GRAY+""";
                background-color:"""+Colors.STR_COLOR_WHITE+""";
                selection-color:"""+Colors.STR_COLOR_BLACK+""";
                color:"""+Colors.STR_COLOR_BLACK+""";
                selection-background-color:"""+Colors.STR_COLOR_DWHITE+""";
            }
            QListView {
                background-color:"""+Colors.STR_COLOR_DWHITE+""";
                selection-color:"""+Colors.STR_COLOR_WHITE+""";
                color:"""+Colors.STR_COLOR_BLACK+""";
                selection-background-color:"""+Colors.STR_COLOR_LBLUE+""";
            }
        """
        Colors.COLOR_LIGHT = True
        self._app.setStyleSheet(qss)
        if self._timing_panel is not None:
            self._timing_panel.light_()
        if self._nxcals_browser is not None:
            self._nxcals_browser.light_()
        if self._fesa_browser is not None:
            self._fesa_browser.light_()
        self.system_monitor.light_()
        self.convertor.light_()
        self.light_tabs()
        self._mutex_graph = False

    def light_tabs(self):
        """
        Light theme for all tab panels.
        """

        for tabpanel in self.tabpanels:
            try:
                tabpanel.light_()
            except:
                pass

    def dark_tabs(self):
        """
        Light theme for all tab panels.
        """

        for tabpanel in self.tabpanels:
            try:
                tabpanel.dark_()
            except:
                pass

    def closeTab(self, currentIndex):
        """Close tab"""
        currentQWidget = self._tabs.widget(currentIndex)
        currentQWidget.deleteLater()
        currentQWidget.close()
        if isinstance(currentQWidget, metanxcalswidget.MetaNXCALSWidget):
            self._nxcals_browser = None
        if isinstance(currentQWidget, metapropertyfesawidget.MetaPropertyFesaWidget):
            self._fesa_browser = None
        self._tabs.removeTab(currentIndex)


class _MyBIApplicationFrame(BIApplicationFrame):
    def __init__(self, app=None, title=None, use_timing_bar=False,
                 use_log_console=False,
                 use_rbac=True):
        BIApplicationFrame.__init__(self,
                                    app=app,
                                    title=title,
                                    use_timing_bar=use_timing_bar,
                                    use_log_console=use_log_console,
                                    use_rbac=use_rbac)

        mywidget = QWidget(self)
        # mywidget.setStyleSheet("background-color:black;")
        layout = QGridLayout(mywidget)
        layout.setContentsMargins(20, 10, 10, 2)
        layout.setSpacing(5)
        b = QRadioButton("TEST")
        layout.addWidget(b, 0,0)

        self._tabs.addTab(mywidget, "Main")


def main():
    #####
    # Create app

    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    #####
    # Init font icons

    # Colors.init_font()  

    #####
    # Create window

    window = _MyBIApplicationFrame(app=app, use_timing_bar=True)

    #####
    # Window show

    window.show()

    window.dark_()
    # window.light_()

    #####
    # End/close app
    sys.exit(exec_app_interruptable(app))


# Compatibility with acc-py app    
if __name__ == "__main__":
    main()
