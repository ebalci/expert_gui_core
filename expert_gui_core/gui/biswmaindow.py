from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from accwidgets.app_frame import ApplicationFrame
from accwidgets.qt import exec_app_interruptable
from accwidgets.timing_bar._model import TimingBarModel, TimingBarDomain

from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import numpy as np
import pandas as pd
import time
import jpype
import qtawesome as qta
import fontawesome as fa  

cern = jpype.JPackage("cern")

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.comm import ccda
from expert_gui_core.comm import fesacomm
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.timing import timingwidget
from expert_gui_core.gui.widgets.combiner import metapropertyfesawidget
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt import systemmonitor


class _BiswMainWindow(ApplicationFrame, fesacomm.FesaCommListener):

    """
    
    Template BI-SW GUI FESA Frame
    
    """
    
    def __init__(self,app=None):
        ApplicationFrame.__init__(self, use_timing_bar=False,
                                        use_log_console=True, 
                                        use_rbac=True)   
                                             
        
        qta.icon("fa5.clipboard")

        self._app = app
        
        # Init frame
        self.title = 'BI-SW FESA Frame'
        self.left =100
        self.top = 100
        self.width = 1200
        self.height = 900
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.default_palette = QGuiApplication.palette()

        rba_button = self.rba_widget
        if rba_button is not None:
            rba_button.loginSucceeded.connect(self.on_pyrbac_login)
            rba_button.loginFailed.connect(self.on_pyrbac_error)
            rba_button.logoutFinished.connect(self.on_pyrbac_logout)
        
        # Log 
        self.log_console.setFeatures(QDockWidget.NoDockWidgetFeatures)  
        self.log_console.console.expanded = False         

        # Menu
        menu_bar = self.menuBar()        
        tools_menu = menu_bar.addMenu('&Tools') 
        tools_menu.addAction('Show/Hide Timing', self.show_hide_timing)
        tools_menu.addAction('Show/Hide RBAC', self.show_hide_rbac)
        tools_menu.addAction('Show/Hide Log', self.show_hide_log)
        tools_menu.addAction('Show/Hide System Monitor', self.show_hide_sysmonitor)
        
        # System monitor
        self.system_monitor = systemmonitor.SystemMonitorWindow(parent = None, time_period = 1*1000)
        self.system_monitor.resize(900,200)
        self.system_monitor.hide()

        #FESA menus
        class_menu = menu_bar.addMenu('&Class Fec Devices')
        
        class_names = ["BSRALHC","BPMOPS","BSISO"]        
        
        fesa_db_fec_class_info = ccda.get_fecs_from_class(class_names)        
        for fesa_class_name,fesa_class_fec_names in fesa_db_fec_class_info.items():
            dict_fesa_fec = ccda.get_devices_from_class_per_fec(fesa_class_name)
            class_menu_item = class_menu.addMenu(fesa_class_name)
            for fec_name in dict_fesa_fec.keys():
                devices = dict_fesa_fec[fec_name]
                fec_menu_item = class_menu_item.addMenu(fec_name)
                for device in devices:
                    fec_menu_item.addAction(device, self.destroy)
        
        # Theme menu
        theme_menu = menu_bar.addMenu('&Theme') 
        theme_menu.addAction('Dark', self.dark_)
        theme_menu.addAction('Light', self.light_)
        
        # Timing panel
        domain = ["PSB","LEI","CPS","SPS","LNA","ADE"]
        self._listener = _Listener(self)
        self._timing_panel = timingwidget.TimingPanel(domain=domain, listener=self._listener, subscribe=True)    

        # Tab panel
        self.tabpanel_widget = _BiswTabPanelWidget(self)

        #Main widget
        main_widget = QWidget()
        self._layout = QGridLayout(main_widget)        
        self._layout.setContentsMargins(0,0,0,0)
        self._layout.setSpacing(0)        
        self.setCentralWidget(main_widget)  
        self._layout.addWidget(self._timing_panel,0,0)
        self._layout.addWidget(self.tabpanel_widget,1,0)
        self._layout.setRowStretch(0,0)
        self._layout.setRowStretch(1,1)

        # Show and change color theme
        self.show()
        
        #update time used by handle event
        self._update_time = 0
        
        # Status window
        self.statusBar().showMessage("Ready")

        #Hide RBAC and log
        self.main_toolbar().hide()
        self.log_console.hide()

    def show_hide_sysmonitor(self):
        """Show or hide Log toolbar"""
        if self.system_monitor is not None:
            if self.system_monitor.isVisible():
                self.system_monitor.hide()
            else:
                self.system_monitor.show()

    def on_pyrbac_login(self, pyrbac_token):
        """RBAC login action"""
        new_token = cern.rbac.common.RbaToken.parseAndValidate(jpype.java.nio.ByteBuffer.wrap(pyrbac_token.get_encoded()))
        cern.rbac.util.holder.ClientTierTokenHolder.setRbaToken(new_token)
        
    def on_pyrbac_error(self, err):
        """handling of RBAC errors"""
        self.auth_label.setText(f"RBAC error: {err!s}")

    def on_pyrbac_logout(self):
        """RBAC logout action"""
        self.japc.rbacLogout()

    def show_hide_timing(self):
        """Show or hide timing panel"""
        if self._timing_panel is not None:
            if self._timing_panel.isVisible():
                self._timing_panel.hide()
            else:
                self._timing_panel.show()

    def show_hide_rbac(self):
        """Show or hide RBAC toolbar"""
        if self.main_toolbar() is not None:
            if self.main_toolbar().isVisible():
                self.main_toolbar().hide()
            else:
                self.main_toolbar().show()

    def show_hide_log(self):
        """Show or hide Log toolbar"""
        if self.log_console is not None:
            if self.log_console.isVisible():
                self.log_console.hide()
            else:
                self.log_console.show()

    def handle_event(self,name,value):        
        """Handle event dispatcher"""
        if value['_time'] - self._update_time > 50:
            self.tabpanel_widget._data_widget.set_data(value['anArray'],name="anArray",name_parent="anArray")
            self._update_time = value['_time']        
        
    def dark_(self):
        """Set dark theme"""
        darkpalette = QPalette()
        darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
        darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
        darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
        darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
        darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
        darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
        self._app.setPalette(darkpalette)
        self.setStyleSheet("background-color: "+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        
        qss = """
            QMenuBar::item {
                spacing: 2px;           
                padding: 2px 10px;
                background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";
            }
            QMenuBar::item:selected {    
                background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";
            }
            QMenuBar::item:pressed {
                background: """+Colors.STR_COLOR_LIGHT1GRAY+""";
            }

            QMenu {
                background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";   
                margin: 2px;
            }
            QMenu::item {
                background-color: transparent;
            }
            QMenu::item:selected { 
                background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";                
            }         
            QLineEdit:{background-color: black;}            
        """ 
        self._app.setStyleSheet(qss)
        Colors.COLOR_LIGHT = False
        self.tabpanel_widget.dark_()
        self._timing_panel.dark_()
        self.system_monitor.dark_()        
        
    def light_(self):
        """Set light theme"""
        QGuiApplication.setPalette(self.default_palette)
        Colors.COLOR_LIGHT = True
        self.tabpanel_widget.light_()
        self._timing_panel.light_()
        self.system_monitor.light_()
                        
    def keyPressEvent(self, e):
        """Keypress event"""        
        if e.key() == Qt.Key_Escape:
            self.close()
    

class _Listener():     
    
    """
    
    Button start/stop listener
    
    """

    def __init__(self, parent):
        self._parent = parent

    def start(self, cycle):
        """Start subscription"""
        self._fesacomm = fesacomm.FesaComm("BISWRef2","Acquisition",listener=self._parent)
        self._fesacomm.subscribe("")

    def stop(self):
        """Stop subscription"""
        self._fesacomm.unsubscribe()


class _BiswTabPanelWidget(QWidget):

    """
    
    Panel widget with tab component
    
    """    

    def __init__(self, parent):        
        super(QWidget, self).__init__(parent)        
        # Layout
        self._layout = QGridLayout(self)        
        self._layout.setContentsMargins(5,5,5,5)
        self._layout.setSpacing(0) 
        # Initialize tab screen
        self._tabs = QTabWidget()
        self._tabs.setTabsClosable(True)
        # Timing panel
        # domain = ["PSB","LEI","CPS","SPS","LNA","ADE"]
        # domain = ["PSB"]
        # self._listener = Listener(parent)
        # self._timing_panel = timingwidget.TimingPanel(domain=domain, listener=self._listener)        
        # Create first tab 
        w = QScrollArea()
        self._layout_panel = QGridLayout(w)        
        self._layout_panel.setContentsMargins(0,0,0,0)
        self._layout_panel.setSpacing(2) 

        data_widget_opts = {
            "menubar":False, 
            "toolbar":False, 
            "auto":True, 
            "show_table":True,
            "show_image":True,
            "show_chart":True,
            "show_scalar":True,
            "show_bitenum":True,
            "show_chartgl":True,
            "show_chartgl_3d":True,
            "show_chartgl_surface":True,
            "toolbar_chart":False,            
            "history":True,
            "show_chart_value_axis":True,
            "show_image_value_axis":True,
            "size_max_table":10000,
            "statistics":True,
            "fitting":True,
            "color_amplitude":False
        }
        
        self._data_widget = datawidget.DataWidget(self, name=["anArray"], **data_widget_opts)        
        
        self._layout_panel.addWidget(self._data_widget,0,0)
        
        self.mpfw = metapropertyfesawidget.MetaPropertyFesaWidget(self,timing_panel=parent._timing_panel)
        self._tabs.addTab(self.mpfw, "FESA")    
        self._tabs.addTab(w, "TEST")    
        self._tabs.setStyleSheet("background-color: "+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")

        # # Add tabs to widget
        self._layout.addWidget(parent._timing_panel,0,0)
        self._layout.addWidget(self._tabs,1,0)
        self._layout.setRowStretch(0,0)
        self._layout.setRowStretch(1,1)
        
        # Skin theme
        # self.dark_()
        
    def dark_(self):
        """Set dark theme"""
        self._tabs.setStyleSheet("background-color: "+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        self._data_widget.dark_()
        self.mpfw.dark_()
        
    def light_(self):
        """Set light theme"""
        self._tabs.setStyleSheet("background-color: "+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")
        self._data_widget.light_()
        self.mpfw.light_()
        

class _MyGraphicsLayoutWidget(pg.GraphicsLayoutWidget):
    
    """
    
    Class inherit for GraphicsLayoutWidget
    
    """

    def __init__(self):
        super().__init__()

    def wheelEvent(self, event):
        """Wheel event"""
        super().wheelEvent(event)
        
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(9)
    app.setFont(font_text_user)    
    window = _BiswMainWindow(app=app)
    window.dark_()
    window.show()
    sys.exit(exec_app_interruptable(app))