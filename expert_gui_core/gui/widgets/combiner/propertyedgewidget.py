from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys
import time

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.tools import formatting
from expert_gui_core.comm import ccda
from expert_gui_core.comm import edgecomm

from expert_gui_core.gui.widgets.datapanels import boolpanelwidget
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.widgets.datapanels import bitenumpanelwidget
from expert_gui_core.gui.widgets.datapanels import enumpanelwidget
from expert_gui_core.gui.widgets.datapanels import arraypanelwidget
from expert_gui_core.gui.widgets.datapanels import stringpanelwidget
from expert_gui_core.gui.widgets.common import headerwidget

class PropertyEdgeWidget(QWidget, edgecomm.EdgeCommListener):
    
    """    
    Class widget to display automaic Edge module instance registers.
    
    :param parent: Parent object.
    :type parent: object
    :param title: Title to be shown in the top center.
    :type title: str, optional
    :param name_device: Device name.
    :type name_device: str
    :param name_property: Property name.
    :type name_property: str
    :param name_fields: Field names (filtering).
    :type name_fields: list, optional
    :param type_fields: Field types (change the GUI component or visualisation option).
    :type type_fields: list, optional
    :param max_row: Maximum number of rows to show, if the number exceeds then new columns are added (default=10).
    :type max_row: int, optional
    :param history: Keep historical data (default=False).
    :type history: bool, optional
    :param header: Show or not the header bar (default=True).
    :type header: bool, optional
    :param button: Show or not the comm actions panel (get/set/subscribe, default=True).
    :type button: bool, optional
    :param subscribe: Show or not subscribe button (default=True).
    :type subscribe: bool, optional
    """
    
    filter_changed = pyqtSignal()

    def __init__(self, parent, title=None, name_module = "", name_lun=0, name_blockinstance=None,
                name_fields=None, function_expression=None, max_row=10, history=False, header=True, button=True, subscribe=True):
        """
        Initialize class.
        """
        super(QWidget, self).__init__(parent)        

        qta.icon("fa5.clipboard")

        self.hide()

        self._parent = parent
        self._components = {}
        self._components_check = {}
        self._components_info = {}
        self._max_row = max_row
        self._light = True
        self._time_sub = 1000
        self._name_fields = name_fields
        self._function_expression = function_expression
        
        # Edge comm object

        self._edgecomm = edgecomm.EdgeComm(name_module,name_lun,listener=self)

        self._property_fields = self._edgecomm.get_hwdesc()[name_blockinstance]          

        if name_fields is None:
            name_fields = []
            for propertyField in self._property_fields:
                name_fields.append(propertyField['name'])
            self.name_fields = name_fields

        # layout construction

        self.layout = QGridLayout()
        self.layout.setContentsMargins(5,2,5,2)
        self.layout.setSpacing(0) 
        groupBox = QGroupBox()
        groupBox.setLayout(self.layout)

        # scroll area  

        self.scroll = QScrollArea()
        self.scroll.setWidget(groupBox)
        self.scroll.setWidgetResizable(True)
        
        layout = QVBoxLayout(self)

        # header

        if header:
            self._header = headerwidget.HeaderWidget()
            layout.addWidget(self._header) 
        else:
            self._header = None

        # title

        if title is not None:
            self._label_title = QLabel(title)
            self._label_title.setAlignment(Qt.AlignCenter)        
            layout.addWidget(self._label_title) 

        # add filter and all checkbox

        panel_filter = QWidget()
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(10, 5, 0, 0)
        layout_filter.setSpacing(5) 
        layout_filter.addWidget(QLabel("Filter : "), 0,2,alignment=Qt.AlignRight)
        self._text_filter = QLineEdit("")
        self._text_filter.installEventFilter(self)
        self.filter_changed.connect(self.filter_fields)
        layout_filter.addWidget(self._text_filter, 0,3,alignment=Qt.AlignLeft)
        layout_filter.addWidget(QLabel(" "), 0,1)
        self._checkbox_all = QCheckBox("")
        self._checkbox_all.setChecked(True)
        self._checkbox_all.setMaximumWidth(18)
        self._checkbox_all.toggled.connect(self.press_all)
        layout_filter.addWidget(self._checkbox_all, 0,0)        
        layout_filter.setColumnStretch(1,1)
        layout.addWidget(panel_filter) 

        layout.addWidget(self.scroll)

        # panel buttons get/set/poll_set/poll

        self.panel_buttons = QWidget()
        layout_buttons = QGridLayout(self.panel_buttons)
        layout_buttons.setContentsMargins(0, 10, 0, 0)
        layout_buttons.setSpacing(5) 
        layout_buttons.addWidget(QLabel(""), 0,0)
        self._get_button = QPushButton(" Get ")
        self._get_button.mousePressEvent = self.get
        self._set_button = QPushButton(" Set ")
        self._set_button.mousePressEvent = self.set
        self._clear_button = QPushButton(" Clear ")
        self._clear_button.mousePressEvent = self.clear_data

        self._checkbox_poll_set = QCheckBox("")
        self._checkbox_poll_set.setChecked(False)
        self._checkbox_poll_set.setMaximumWidth(16)
        
        self._subscribe_button = QPushButton(" Poll ")
        self._subscribe_button.mousePressEvent = self.subscribe
        self._le_time_sub = QLineEdit(str(self._time_sub))
        self._le_time_sub.setMaximumWidth(50)

        layout_buttons.addWidget(self._get_button, 0,1)
        layout_buttons.addWidget(self._set_button, 0,2)
        if subscribe:
            layout_buttons.addWidget(self._checkbox_poll_set, 0,4)    
            layout_buttons.addWidget(self._subscribe_button, 0,3)
        if history:
            layout_buttons.addWidget(self._clear_button, 0,5)
        layout_buttons.addWidget(self._le_time_sub, 0,6)
        
        layout_buttons.setColumnStretch(0,1)

        if button:
            layout.addWidget(self.panel_buttons)

        # create content panel
        
        self.content_pane_opts = {
            'title': title,
            'name_module': name_module,
            'name_lun': name_lun,
            'name_blockinstance': name_blockinstance,
            'name_fields': name_fields,
            'max_row' : max_row,
            'history' : history,
        }
        self.create_content_panel(**self.content_pane_opts)

        groupBox.setStyleSheet("border:none;")

        self._update_time = 0

        self.name_cycle = ""
        self.is_subscribing = False
        self.name_fields = name_fields    

        self.show() 
        
    def press_all(self,e):
        """
        Click on "all" checkbox.
        """
        for checkbox in self._components_check.values():
            checkbox.setChecked(self._checkbox_all.isChecked())

    def clear_data(self, e):
        """
        Clear all data buffer.
        """
        for name in self._components:            
            try:
                self._components[name]._data_widget.clear_data()                
            except Exception as e:
                pass   

    def subscribe(self,e):
        """
        Start/Stop subscription.
        """
        if self.is_subscribing == True:
            self.is_subscribing = False
            self._edgecomm.unsubscribe(self.content_pane_opts["name_blockinstance"])
            self._subscribe_button.setText(" Poll ")
            if self._light:
                self._subscribe_button.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")
            else:
                self._subscribe_button.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        else:
            self.is_subscribing = True
            name_fields_filtered = self.name_fields.copy()
            for name_field in self._components_check:
                if name_fields_filtered is not None:
                    if name_field not in name_fields_filtered:
                        continue
                if  name_field in name_fields_filtered:
                    if not self._components_check[name_field].isChecked():
                        try:
                            name_fields_filtered.remove(name_field)
                        except:
                            pass
            self._edgecomm.subscribe(self.content_pane_opts["name_blockinstance"], timer_period=int(self._le_time_sub.text()),registers=name_fields_filtered,mode_set=self._checkbox_poll_set.isChecked())
            self._subscribe_button.setText(" Stop ")
            self._subscribe_button.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHTRED+";")

    def unsubscribe(self):
        """
        Unsubscribe to Edge block module.
        """
        self._edgecomm.unsubscribe(self.content_pane_opts["name_blockinstance"])

    def get(self,e):
        """
        Automatic property Get action calling same handelevent from Listener and using selected cycle.        
        """
        name_fields_filtered = self.name_fields.copy()
        for name_field in self._components_check:
            if name_fields_filtered is not None:
                if name_field not in name_fields_filtered:
                    continue
            if  name_field in name_fields_filtered:
                if not self._components_check[name_field].isChecked():
                    try:
                        name_fields_filtered.remove(name_field)
                    except:
                        pass
        
        self._edgecomm.get_action(self.content_pane_opts["name_blockinstance"], name_fields=name_fields_filtered)

    def set(self,e):
        """
        Automatic property Set action calling using selected cycle.
        """
        result = self._edgecomm.get_action(self.content_pane_opts["name_blockinstance"], event=False, name_fields=self.name_fields)
        result.pop("_time")
        result.pop("_cycle")
        for name_field in self._components_check:
            if self.name_fields is not None:
                if name_field not in self.name_fields:
                    continue
            if  name_field in result:
                if self._components_check[name_field].isChecked():
                    data = self._components[name_field].get_data()
                    result[name_field] = formatting.type_value_to_java(data,type(result[name_field]))   
                else:      
                    result.pop(name_field)           
        self._edgecomm.set(self.content_pane_opts["name_blockinstance"],result)

    def get_expressions(self):
        """
        Get all function expressions.
        """
        self._function_expression = None
        for name in self._components:            
            try:
                expression = self._components[name].get_expression()
                if expression is not None:
                    if self._function_expression is None:
                        self._function_expression = {}
                    self._function_expression[name] = expression
            except:
                pass            
        return self._function_expression

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()        
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
                self._components_check[component_field].show()
                self._components_info[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                    self._components_check[component_field].show()
                    self._components_info[component_field].show()
                else:
                    self._components[component_field].hide()
                    self._components_check[component_field].hide()
                    self._components_info[component_field].hide()
            except:
                self._components[component_field].hide()
                self._components_check[component_field].hide()
                self._components_info[component_field].hide()

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()            
        return False 

    def create_content_panel(self,title=None,  name_module=None,name_lun=None,name_blockinstance=None, 
                                name_fields=None, type_fields=None, max_row=10, history=False):
        """
        Create automatic panel with all datamap property fields.
        
        :param title: Title to be shown on top center.
        :type title: str, optional
        :param name_fields: List of field names (filtering).
        :type name_fields: list, optional
        :param type_fields: Field types (change the GUI component or visualisation option).
        :type type_fields: list, optional
        :param max_row: Maximum number of rows to show, if the number exceeds then new columns are added (default=10)
        :type max_row: int, optional
        :param history: Keep historical data (default=False)
        :type history: bool, optional
        """        
        
        # get property fields

        property_fields = self._property_fields

        # calculate auto width

        i=0
        size_str_max = 0
        for propertyField in property_fields:
            if name_fields is not None:
                if propertyField['name'] not in name_fields:
                    continue
            if size_str_max < len(propertyField['name']):
                size_str_max = len(propertyField['name'])
        size_str_max = int(size_str_max * 8.2)

        # get property definition   

        edit = True
        index_definition = 3

        # add field widgets

        col=0
        row=1
        
        for propertyField in property_fields:
            if self._max_row != 0:
                if row == self._max_row:
                    col=col+3
                    row=0         
            if name_fields is not None:
                if propertyField['name'] not in name_fields:
                    continue
            
            if propertyField['name'] == "acqStamp" or propertyField['name'] == "cycleName" or propertyField['name'] == "cycleStamp" or propertyField['name'] == "updateFlags":   
                continue
            
            f_expression = None
            if self._function_expression is not None:
                try:
                    f_expression = self._function_expression[propertyField['name']]

                except:
                    f_expression = None

            #SCALAR

            if propertyField['type'] == "scalar":                    
                type_format="str"
                if type_fields is not None:
                    if propertyField['name'] in type_fields:
                        type_format = type_fields[propertyField['name']]
                if f_expression is not None and f_expression != "" and f_expression != "x":
                    type_format = "fun"
                number_opts = {
                    'title': None,
                    'label_name': propertyField['name'],
                    'edit' : edit,
                    'history' : history,
                    "type_format":type_format,
                    "function_expression":f_expression,
                    # 'statistics':True,
                    # 'fitting':True
                }
                self.number_widget = numberpanelwidget.NumberPanelWidget(self,**number_opts)       
                self.number_widget._label_name.setMinimumWidth(size_str_max)
                self.layout.addWidget(self.number_widget,row,col+1)
                checkbox = QCheckBox("")
                checkbox.setChecked(True)
                self._components_check[propertyField['name']] = checkbox
                self.layout.addWidget(checkbox,row,col) 
                self.layout.setRowStretch(row,0) 
                self._components[propertyField['name']] = self.number_widget    

            #ARRAY

            elif propertyField['type'] == "array": 
                type_format="raw"
                if f_expression is not None and f_expression != "" and f_expression != "x":
                    type_format = "fun"                   
                array_opts = {
                    'title': None,
                    'label_name': propertyField['name'],
                    'edit' : edit,
                    'history' : history,
                    'twod' : False,
                    "type_format":type_format,
                    "function_expression":f_expression,
                    # 'statistics':True,
                    # 'fitting':True
                }
                self.table_widget = arraypanelwidget.ArrayPanelWidget(self, **array_opts)  
                self.table_widget._label_name.setMinimumWidth(size_str_max)
                self.layout.addWidget(self.table_widget,row,col+1)  
                checkbox = QCheckBox("")
                checkbox.setChecked(True)
                self._components_check[propertyField['name']] = checkbox
                self.layout.addWidget(checkbox,row,col) 
                self.layout.setRowStretch(row,0) 
                self._components[propertyField['name']] = self.table_widget 

            #ARRAY2D

            elif propertyField['type'] == "array2D":                    
                array_opts = {
                    'title': None,
                    'label_name': propertyField['name'],
                    'edit' : edit,
                    'history' : False,
                    'twod': True                   
                }
                self.table_widget = arraypanelwidget.ArrayPanelWidget(self,**array_opts)  
                self.table_widget._label_name.setMinimumWidth(size_str_max)
                self.layout.addWidget(self.table_widget,row,col+1)   
                checkbox = QCheckBox("")
                checkbox.setChecked(True)
                self._components_check[propertyField['name']] = checkbox
                self.layout.addWidget(checkbox,row,col) 
                self.layout.setRowStretch(row,0) 
                self._components[propertyField['name']] = self.table_widget  
            info_button = QPushButton(fa.icons["info-circle"])
            self._components_info[propertyField['name']] = info_button
            self.layout.addWidget(info_button,row,col+2)                                  
            info_button.setMaximumWidth(20)
            info_button.setToolTip(str(propertyField))
            info_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")              
            i=i+1
            row=row+1        

        #add last label stretch 1

        self.bottomline = QLabel("")
        self.layout.addWidget(self.bottomline,row,col) 
        self.layout.setRowStretch(row,1) 

    def handle_event(self,name,value):        
        """
        Handle event dispatcher.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict   
        """
        try:
            self._checkbox_poll_set.isChecked()                
        except:
            return

        if self._checkbox_poll_set.isChecked():
            if self._header is not None:
                if self._header.is_pause():
                    return
                try:
                    self._header.recv(value['_cycle'])
                except:
                    return        
            self.set(None)        
            self._update_time = value['_time']      
        else:
            if value['_time'] - self._update_time > 50:
                if self._header is not None:
                    if self._header.is_pause():
                        return
                    try:
                        self._header.recv(value['_cycle'])
                    except:
                        return                
                for name_field in value.keys():
                    try:
                        if self.name_fields is not None:
                            if name_field not in self.name_fields:
                                continue
                        if name_field != "_time" and name_field != "_cycle":   
                            if len(value[name_field]) == 1:
                                self._components[name_field].set_data(value[name_field][0],cycle=value['_cycle'])              
                            else:
                                self._components[name_field].set_data(value[name_field],cycle=value['_cycle'])              
                    except:
                        print("error! " + name_field)                    
                self._update_time = value['_time']      

    def dark_(self):
        """
        Set dark theme.
        """        
        self._light = False
        self._get_button.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        self._set_button.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_BLUE+";")        
        self._checkbox_poll_set.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_BLUE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")        
        self._subscribe_button.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
        self._checkbox_all.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
        self._checkbox_all.setMaximumWidth(18)        
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for component in self._components.values():            
            component.dark_()
        for component in self._components_check.values():            
            component.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
            component.setMaximumWidth(18)
        if self._header is not None:
            self._header.dark_()
        
    def light_(self):
        """
        Set light theme.
        """
        self._light = True
        self._get_button.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")
        self._set_button.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_DBLUE+";")
        self._checkbox_poll_set.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_BLUE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")  
        self._subscribe_button.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";;color:"+Colors.STR_COLOR_BLACK+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")
        self._checkbox_all.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_WHITE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")  
        self._checkbox_all.setMaximumWidth(18)    
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for component in self._components.values():
            component.light_()
        for component in self._components_check.values():            
            component.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_WHITE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")     
            component.setMaximumWidth(18)
        if self._header is not None:
            self._header.light_()

    def set_font_size(self,val=8):
        """
        Change font size.

        :param val: Font size value (default=8).
        :type val: int, optional
        """
        font = self._get_button.font() 
        font.setPointSize(val)
        self._get_button.setFont(font)
        self._set_button.setFont(font)
        self._subscribe_button.setFont(font)
        self._clear_button.setFont(font)
        for component in self._components.values():
            component.set_font_size(val)


class _Example(QMainWindow):
    
    """    
    Example class to test.    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """
        Init user interface.
        """
        dark = True
        
        instances = []
        
        instances.append( ["vfc_hd_bsra",
                     5,
                     "crossbar"])
        
        central_widget = QWidget()         
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5) 
        ind = 0

        self._tabs = QTabWidget()
        self.layout.addWidget(self._tabs,0,0)            

        for instance in instances:
            property_fesa_widget = PropertyEdgeWidget(None,
                                                title="Title", 
                                                name_module = instance[0], 
                                                name_lun=instance[1], 
                                                name_blockinstance=instance[2],
                                                max_row=0,
                                                history=True) 
            
            property_fesa_widget.dark_()

            self._tabs.addTab(property_fesa_widget, str(instance[0]) + "/" + str(instance[1])+ "/" + str(instance[2]))   

            ind = ind + 1

        self.setCentralWidget(central_widget)

        self.resize(500,600)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)   

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    
    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";
        }
        QMenuBar::item:selected {    
            background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }
        QMenuBar::item:pressed {
            background: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }              
        QLineEdit:{background-color: black;}
    """ 
    app.setStyleSheet(qss)

    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())