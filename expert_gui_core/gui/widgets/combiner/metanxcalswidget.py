from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys
import time
import requests
import json

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.tools import formatting
from expert_gui_core.comm import ccda
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.common import togglepanelwidget
from expert_gui_core.comm import nxcalscomm
from expert_gui_core.gui.widgets.timing import timingwidget
from expert_gui_core.gui.widgets.combiner import nxcalswidget
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget


class MetaNXCALSWidget(QWidget):
    
    """
    
    Class widget to display automatic Multiple NXCALS variable) panels
    
    """    

    def __init__(self,parent):
        super(QWidget, self).__init__(parent)     

        qta.icon("fa5.clipboard")

        self._datawidgets = []
        self._nxcals = {}

        layout = QGridLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(10)     
        self.setLayout(layout)

        # search panel

        self._nxcals_search = _NXCALSSearch(self)
        self._nxcals_search.setMinimumWidth(700)

        self.toggle_panel_search = togglepanelwidget.TogglePanelWidget(self._nxcals_search,iconshow="sliders-h",align="topright")        
        layout.addWidget(self.toggle_panel_search,0,0)       
        
        self.mdi = _MdiArea(self)
        layout.addWidget(self.mdi, 0, 1)

        layout.setRowStretch(0,1)    
        
        layout.setColumnStretch(0,0)    
        layout.setColumnStretch(1,1)    

        self.set_font_size(9)

    def close(self):
        """
        Close the widget.
        """     
        for p in self._nxcals.values(): 
            try:
                p.deleteLater()
                p.subscribe(None)
            except:
                pass
        pass

    def set_font_size(self, size=8):
        """
        Change font size.
        """
        self._nxcals_search.set_font_size(size)       
    
    def dark_(self):
        """
        Set dark theme.
        """        
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";")
        self.mdi.dark_()        
        self._nxcals_search.dark_()
        self.toggle_panel_search.dark_()
        for dtw in self._datawidgets:
            dtw.dark_()

    def light_(self):
        """
        Set light theme.
        """        
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self.mdi.light_()
        self._nxcals_search.light_()
        self.toggle_panel_search.light_()
        for dtw in self._datawidgets:
            dtw.light_()

    def add_dtw(self,data_widget,name):
        """
        Add property data widget.

        :param property_fesa: Property FESA name.
        :type property_fesa: str
        :param name: Tab name.
        :type name: str
        """       
        self._datawidgets.append(data_widget)
        self._nxcals[name] = data_widget
        sub = _SubWindow(self,data_widget,self.mdi,name)
        sub.setWindowTitle(name)
        self.mdi.addWindow(sub,name)
        data_widget.show()
        sub.show()             


class _MdiArea(QMdiArea):

    def __init__(self, parent):
        super(_MdiArea, self).__init__(parent)
        
        self._subs ={}
        self._actions={}
        self._parent = parent
        self._selname = ""
        self.activated = True

        self.context_menu = QMenu(self)
        cascade = self.context_menu.addAction("Cascade")
        tiled = self.context_menu.addAction("Tiled")

        cascade.triggered.connect(self.cascade_triggered)
        tiled.triggered.connect(self.tiled_triggered)

    def addWindow(self,sub,name):
        self.addSubWindow(sub)
        self._subs[name] = sub
        sub.set_nxcals_search(self._parent._nxcals_search)
        show_sub = self.context_menu.addAction(name)
        show_sub.triggered.connect(lambda x:self.show_sub(name))
        self._actions[name] = show_sub

    def show_sub(self,name):
        self._subs[name].showMaximized()

    def delete_sub(self,name):
        try:
            self._parent._nxcals_search.close_dtw(name)          
            self._parent._nxcals_search._names.remove(name)
            self._subs.pop(name)
            self.context_menu.removeAction(self._actions[name])
            self._parent._nxcals.pop(name)            
        except Exception as e:
            print(e)
            pass

    def mousePressEvent(self,e):
        self.activated = not self.activated

    def contextMenuEvent(self, event):
        if self.activated:
            self.context_menu.exec(event.globalPos())

    def cascade_triggered(self):
        self.cascadeSubWindows()

    def tiled_triggered(self):
        self.tileSubWindows()

    def dark_(self):
        """
        Set dark theme.
        """        
        self.setBackground(Colors.COLOR_LIGHT0GRAY)
        qss = """ 
            QLabel:hover {
                    color: """+Colors.STR_COLOR_LBLUE+""";   
                    background-color: """+Colors.STR_COLOR_L3BLACK+""";   
                }
            QLabel {
                    background-color: """+Colors.STR_COLOR_L3BLACK+""";   
                }
            QMenu {
                background-color: """+Colors.STR_COLOR_L3BLACK+""";   
                margin:5px;
            }
        """ 

        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)

    def light_(self):
        """
        Set light theme.
        """       
        self.setBackground(Colors.COLOR_WHITE) 
        qss = """
                QLabel:hover {
                    color: """+Colors.STR_COLOR_LBLUE+""";   
                    background-color: """+Colors.STR_COLOR_LIGHT6GRAY+""";   
                }
                QLabel {
                    background-color: """+Colors.STR_COLOR_LIGHT6GRAY+""";   
                }
            QMenu {
                background-color: """+Colors.STR_COLOR_LIGHT6GRAY+""";   
                margin: 5px;
            }
        """ 
        if self.context_menu != None:
            self.context_menu.setStyleSheet(qss)


class _SubWindow(QMdiSubWindow):

    def __init__(self, parent, widget,mdi,name):
        super(_SubWindow, self).__init__(parent)
        self._nxcals_search = None
        self._widget = widget
        self._name = name
        self.mdi = mdi
        self.windowStateChanged.connect(self.delayActivated)
        self.setWidget(widget)
        self.setWindowIcon(self.create_icon_by_color(QColor("transparent")))
        self.resize(500,600)
    
    def set_nxcals_search(self,es):
        self._nxcals_search = es

    def closeEvent(self, event):
        """
        Close a sub window.
        """
        print("delete " + self._name)
        self.mdi.delete_sub(self._name)
        self._widget.deleteLater()
        event.accept()

    def create_icon_by_color(self,color):
        pixmap = QPixmap(512, 512)
        pixmap.fill(color)
        return QIcon(pixmap)

    def delayActivated(self,oldState,newState):
        self.mdi._selname = self._name        
        if newState & Qt.WindowActive:
            self.mdi.activated = False
        #     self._nxcals_search.load_registers(self._name,self._widget.name_fields)
        

class _PeriodSelection(QWidget):

    """

    Class widget panel to select either tfom tto or LHC fill number
    
    """  
    def __init__(self,parent,nxcals_comm=None):
        super(QWidget, self).__init__(parent)     

        last_lhcfill_number = 0
        if nxcals_comm is not None:
            last_lhcfill_number = nxcals_comm.get_last_fill_number()

        self._parent = parent
        self.layout = QGridLayout()
        self.layout.setSpacing(2)     
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        self.font_fa.setPointSize(12)
        
        self._now = QPushButton(fa.icons["chevron-circle-down"])
        self._now.setToolTip("Set time now")
        self._now.setMaximumWidth(20)
        self._now.setMaximumHeight(20)

        self._left = QPushButton(fa.icons["chevron-circle-left"])
        self._left.setToolTip("Move to previous time period")
        self._left.setFont(self.font_fa)      
        self._left.setMaximumWidth(25)
        self._left.setMaximumHeight(25)
        self._left.mousePressEvent = self.prev

        self._right = QPushButton(fa.icons["chevron-circle-right"])
        self._right.setToolTip("Move to next time period")
        self._right.setFont(self.font_fa)      
        self._right.setMaximumWidth(25)
        self._right.setMaximumHeight(25)
        self._right.mousePressEvent = self.next

        now = QDateTime.currentMSecsSinceEpoch()

        time_to = QDateTime()
        time_to.setMSecsSinceEpoch(now)

        time_from = QDateTime()
        time_from.setMSecsSinceEpoch(now-600000)        
        
        self.layout.addWidget(self._left, 0, 1, 2, 1)

        self.rb_time = QRadioButton()
        self.rb_time.setChecked(True)
        self.rb_time.setStyleSheet(''' 
                        QRadioButton {
                            color: #888888;
                        }
                        QRadioButton::checked {
                            color: rgb(84,190,230);
                        }
                        QRadioButton::indicator {
                            width: 8px;
                            height: 8px;
                            border-radius: 4px;                            
                        }
                        QRadioButton::indicator::unchecked{ 
                            border: 1px solid; 
                            border-color: #888888;
                            border-radius: 4px;
                            background-color: #888888; 
                            width: 8px; 
                            height: 8px; 
                        }

                        QRadioButton::indicator::checked{ 
                            border: 1px solid; 
                            border-color: rgb(84,190,230);
                            border-radius: 4px;
                            background-color: rgb(84,190,230); 
                            width: 8px; 
                            height: 8px; 
                        }
            ''')
        self.layout.addWidget(self.rb_time, 0, 0,2,1)

        self._label_from = QLabel("  From : ")
        self.layout.addWidget(self._label_from, 0, 2)
        self._datetime_from = QDateTimeEdit(self, calendarPopup=True)
        self._datetime_from.setDateTime(time_from)
        self._datetime_from.setDisplayFormat('yyyy-MM-dd hh:mm:ss.zzz')
        self.layout.addWidget(self._datetime_from, 0, 3)
        
        self.checkbox_rt = QCheckBox("")
        self.checkbox_rt.setChecked(False)
        self.checkbox_rt.setMaximumWidth(16)
        self.checkbox_rt.setToolTip("Live!")
        # self.layout.addWidget(self.checkbox_rt, 0, 4)

        self._label_to = QLabel("  To : ")
        self.layout.addWidget(self._label_to, 1, 2)
        self._datetime_to = QDateTimeEdit(self, calendarPopup=True)
        self._datetime_to.setDateTime(time_to)
        self._datetime_to.setDisplayFormat('yyyy-MM-dd hh:mm:ss.zzz')
        self.layout.addWidget(self._datetime_to, 1, 3)
        
        self.layout.addWidget(self._now,1,4)
        self._now.mousePressEvent = self.now

        self.layout.addWidget(self._right, 0, 5, 2, 3)

        self.layout.setColumnStretch(1,0)     
        self.layout.setColumnStretch(3,1)     

        self.lhcfill = numberpanelwidget.NumberPanelWidget(self,title=None,label_name=" LHC fill : ",type_format="spi",history=False,display_format="4.0")  
        self.lhcfill.set_data(last_lhcfill_number)

        self.rb_lhc = QRadioButton()
        self.rb_lhc.setStyleSheet(''' 
                        QRadioButton {
                            color: #888888;
                        }
                        QRadioButton::checked {
                            color: rgb(84,190,230);
                        }
                        QRadioButton::indicator {
                            width: 8px;
                            height: 8px;
                            border-radius: 4px;                            
                        }
                        QRadioButton::indicator::unchecked{ 
                            border: 1px solid; 
                            border-color: #888888;
                            border-radius: 4px;
                            background-color: #888888; 
                            width: 8px; 
                            height: 8px; 
                        }

                        QRadioButton::indicator::checked{ 
                            border: 1px solid; 
                            border-color: rgb(84,190,230);
                            border-radius: 4px;
                            background-color: rgb(84,190,230); 
                            width: 8px; 
                            height: 8px; 
                        }
            ''')
        self.rb_lhc.setChecked(False)
        self.layout.addWidget(self.rb_lhc, 2, 0,2,1)

        self.layout.addWidget(self.lhcfill, 2,2,1,2)
        self.lhcfill.setMaximumWidth(150)

        self._dark = False
        
    def now(self,e):
        time_to = QDateTime()
        now = QDateTime.currentMSecsSinceEpoch()
        time_to.setMSecsSinceEpoch(now)
        self._datetime_to.setDateTime(time_to)

    def prev(self,e):
        tfrom = self._datetime_from.dateTime()
        ltfrom = tfrom.toMSecsSinceEpoch()
        tto = self._datetime_to.dateTime()
        ltto = tto.toMSecsSinceEpoch()
        dt = ltto - ltfrom
        if dt > 0:
            ltto = ltto - dt
            ltfrom = ltfrom - dt
        tfrom.setMSecsSinceEpoch(ltfrom)
        tto.setMSecsSinceEpoch(ltto)
        self._datetime_from.setDateTime(tfrom)
        self._datetime_to.setDateTime(tto)

    def next(self,e):
        tfrom = self._datetime_from.dateTime()
        ltfrom = tfrom.toMSecsSinceEpoch()
        tto = self._datetime_to.dateTime()
        ltto = tto.toMSecsSinceEpoch()
        dt = ltto - ltfrom
        if dt > 0:
            ltto = ltto + dt
            ltfrom = ltfrom + dt
        tfrom.setMSecsSinceEpoch(ltfrom)
        tto.setMSecsSinceEpoch(ltto)
        self._datetime_from.setDateTime(tfrom)
        self._datetime_to.setDateTime(tto)

    def get_fill(self):
        return self.lhcfill.get_data()
    
    def is_fill(self):
        return self.rb_lhc.isChecked()

    def get_from(self):
        tfrom = self._datetime_from.dateTime()
        return tfrom.toString(self._datetime_from.displayFormat())

    def get_to(self):
        tto = self._datetime_to.dateTime()
        return tto.toString(self._datetime_to.displayFormat())

    def get_from_utc(self):
        tfrom = self._datetime_from.dateTime()
        tfromutc = tfrom.toUTC()
        return tfromutc.toString(self._datetime_from.displayFormat())

    def get_to_utc(self):
        tto = self._datetime_to.dateTime()
        ttoutc = tto.toUTC()
        return ttoutc.toString(self._datetime_to.displayFormat())

    def set_tfrom(self,tfrom):
        syl = tfrom.split(".")
        tfrom = syl[0]+"."+syl[-1][0:3]
        self._datetime_from.setDateTime(QDateTime.fromString(tfrom, 'yyyy-MM-dd hh:mm:ss.zzz'))

    def set_tto(self,tto):
        syl = tto.split(".")
        tto = syl[0]+"."+syl[-1][0:3]
        self._datetime_to.setDateTime(QDateTime.fromString(tto, 'yyyy-MM-dd hh:mm:ss.zzz'))

    def dark_(self):
        """Set dark theme"""     
        self._dark = True   
        self._label_from.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_to.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._now.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._left.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._right.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._datetime_from.setStyleSheet("QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";}")
        self._datetime_to.setStyleSheet("QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";}")
        self.lhcfill.dark_()
        self.checkbox_rt.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")

    def light_(self):
        """Set lght theme"""      
        self._dark = False     
        self._label_from.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_to.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._now.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._left.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._right.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._datetime_from.setStyleSheet("QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";}")
        self._datetime_to.setStyleSheet("QCalendarWidget,QCalendarWidget QMenu,QCalendarWidget QSpinBox,QCalendarWidget QWidget,QCalendarWidget QAbstractItemView,QDateEdit,QDateTimeEdit,QDateTimeEdit::up-button,QDateTimeEdit::down-button {background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";}")
        self.lhcfill.light_()  
        self.checkbox_rt.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_WHITE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")           


class _NXCALSSearch(QWidget):
    
    """

    Class widget panel to search NXCALS variables
    
    """    

    def __init__(self,parent):
        super(QWidget, self).__init__(parent)     

        self._parent = parent
        self.layout = QGridLayout()
        self.layout.setContentsMargins(10, 10, 10, 10)
        self.layout.setSpacing(10)     
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)

        self.nxcalscomm = nxcalscomm.NXCALSComm()

        self._variables = []
        self._properties = []
        self._names = []
        
        # Get all classes from CCDA
        classes = ccda.get_all_classes()        
        classes.sort()
 
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        
        self.font_text = QFont()
        self.font_text_small = QFont()

        # class
        self._label_class = QLabel("Class : ")
        self._le_classes = QLineEdit()
        # device
        self._get_devices = QPushButton(fa.icons["search"])
        self._get_devices.setToolTip("Get device list")
        self._label_device = QLabel("Device : ")
        self._enum_devices = QComboBox()
        # property
        self._label_property = QLabel("Property : ")
        self._get_properties = QPushButton(fa.icons["search"])
        self._get_properties.setToolTip("Get property list")
        self._enum_properties = QComboBox()
        # variable
        self._label_variable = QLabel("Variable : ")
        self._enum_variables = QComboBox()
        self._get_variables = QPushButton(fa.icons["search"])
        self._get_variables.setToolTip("Get variable list")
        # variable
        self._label_user = QLabel("User : ")
        self._enum_users = QComboBox()        
        # fields selcted
        self.filter_fields = _FilterFields(self)    
        # plus
        self._plus = QPushButton(fa.icons["plus"])
        self._plus.setToolTip("Add selected NXCALS setting")

        self._get_devices.setFont(self.font_fa)      
        self._get_devices.setMaximumWidth(25)
        self._get_devices.setMinimumWidth(25)
        self._get_devices.setMinimumHeight(25)
        self._get_devices.setMaximumHeight(25)
        self._get_devices.mousePressEvent = self.get_devices

        self._get_properties.setFont(self.font_fa)      
        self._get_properties.setMaximumWidth(25)
        self._get_properties.setMinimumWidth(25)
        self._get_properties.setMaximumHeight(25)
        self._get_properties.setMinimumHeight(25)
        self._get_properties.mousePressEvent = self.get_properties

        self._get_variables.setFont(self.font_fa)      
        self._get_variables.setMaximumWidth(25)
        self._get_variables.setMinimumWidth(25)
        self._get_variables.setMinimumHeight(25)
        self._get_variables.setMaximumHeight(25)
        self._get_variables.mousePressEvent = self.get_variables

        self._plus.setFont(self.font_fa)      
        self._plus.setMaximumWidth(30)
        self._plus.setMinimumHeight(30)
        self._plus.mousePressEvent = self.plus

        self._enum_devices = QComboBox()
        self._list_devices = QListView(self._enum_devices)
        self._enum_devices.setView(self._list_devices)
        self._enum_devices.setEditable(True)
        self._enum_devices.currentTextChanged.connect(self.get_properties)
        self._enum_devices.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_devices.setFont(self.font_text)

        self._enum_properties = QComboBox()
        self._list_properties = QListView(self._enum_properties)
        self._enum_properties.setView(self._list_properties)
        self._enum_properties.setEditable(True)
        self._enum_properties.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_properties.setFont(self.font_text)

        self._enum_variables = QComboBox()
        self._list_variables = QListView(self._enum_variables)
        self._enum_variables.setView(self._list_variables)
        self._enum_variables.setEditable(True)
        self._enum_variables.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_variables.setFont(self.font_text)

        self._enum_users = QComboBox()
        self._list_users = QListView(self._enum_users)
        self._enum_users.setView(self._list_users)
        self._enum_users.setEditable(True)
        self._enum_users.lineEdit().setAlignment(Qt.AlignRight)
        self._enum_users.setFont(self.font_text)
        
        self.layout.setSpacing(10)
        self.layout.addWidget(self._label_class, 0, 0)
        self.layout.addWidget(self._le_classes, 0, 1)
        self.layout.addWidget(self._get_devices, 0, 2)

        self.layout.addWidget(self._label_device, 1, 0)
        self.layout.addWidget(self._enum_devices, 1, 1)
        self.layout.addWidget(self._get_variables, 1, 2)

        self.layout.addWidget(self._label_variable, 2, 0)
        self.layout.addWidget(self._enum_variables, 2, 1)
        
        self.layout.addWidget(self._label_user, 3, 0)
        self.layout.addWidget(self._enum_users, 3, 1)
        
        self.layout.addWidget(self.filter_fields,5,0,1,2)
        
        self._users = []
        
        self._users.append("None")
        self._users.append("CPS.USER.ALL")
        listcps = timingwidget.TimingPanel.list_cps
        listcps.sort()
        for user in listcps:
            self._users.append("CPS.USER."+user)
        self._users.append("PSB.USER.ALL")
        listpsb = timingwidget.TimingPanel.list_psb
        listpsb.sort()
        for user in listpsb:
            self._users.append("PSB.USER."+user)
        self._users.append("SPS.USER.ALL")
        listsps = timingwidget.TimingPanel.list_sps
        listsps.sort()
        for user in listsps:
            self._users.append("SPS.USER."+user)
        self._users.append("LEI.USER.ALL")
        listlei = timingwidget.TimingPanel.list_lei
        listlei.sort()
        for user in listlei:
            self._users.append("LEI.USER."+user)
        self._users.append("ADE.USER.ALL")
        listade = timingwidget.TimingPanel.list_ade
        listade.sort()
        for user in listade:
            self._users.append("ADE.USER."+user)
        self._users.append("LNA.USER.ALL")
        listlna = timingwidget.TimingPanel.list_lna
        listlna.sort()
        for user in listlna:
            self._users.append("LNA.USER."+user)
        self._users.append("LHC.USER.ALL")
            
        for user in self._users:
            self._enum_users.addItem(user)                
        
        self._meta = _PeriodSelection(self,self.nxcalscomm)
        self._meta.setMaximumHeight(90)

        self.layout.addWidget(self._meta,4,0,1,2)        

        self.layout.addWidget(self._plus, 5, 2)

        self.layout.setRowStretch(0,0)     
        self.layout.setRowStretch(1,0)     
        self.layout.setRowStretch(2,0)    
        self.layout.setRowStretch(3,0)   
        self.layout.setRowStretch(4,0)   
        self.layout.setRowStretch(5,0)   
        self.layout.setRowStretch(6,1)   
                
        completer = QCompleter(classes)        
        self._le_classes.setCompleter(completer)
        if len(classes) > 1:
            self._le_classes.setText(classes[1])

        self._dark = False
      
    def set_font_size(self, size=8):
        """Change font size"""
        self._font_size = size
        self.font_text.setPointSize(size)
        self.font_text_small.setPointSize(size+1)
        self.font_fa.setPointSize(size) 
        
    def dark_(self):
        """Set dark theme"""     
        self._dark = True   
        self._label_class.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_device.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_property.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_variable.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_user.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._le_classes.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
        self._get_devices.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._get_variables.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._plus.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._get_properties.setStyleSheet("background-color:"+Colors.STR_COLOR_L3BLACK+";color:"+Colors.STR_COLOR_BLUE+";")
        self._enum_properties.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_L2BLACK+";")
        self._list_properties.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._enum_devices.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_L2BLACK+";")
        self._list_devices.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._enum_variables.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_L2BLACK+";")
        self._list_variables.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._enum_users.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_L2BLACK+";")
     
        self._meta.dark_()

    def light_(self):
        """Set lght theme"""      
        self._dark = False     
        
        self._meta.light_()

        self._label_class.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_device.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_property.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_variable.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_user.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._le_classes.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")
        self._get_devices.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._get_variables.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._plus.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._get_properties.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLUE+";")
        self._enum_properties.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_properties.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")        
        self._enum_devices.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_devices.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")        
        self._enum_variables.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_variables.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")        
        self._enum_users.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
    
    def plus(self,e):
        """Add property(ies) panels to parent tab"""
        
        QApplication.setOverrideCursor(Qt.WaitCursor)

        instances = []
        
        if self._enum_variables.currentText() == "*":
            for variable in self._variables:
                instances.append(variable)
        else:
            instances.append(self._enum_variables.currentText())
        
        for instance in instances:
            
            user = self._enum_users.currentText()

            if self._meta.is_fill():
                fillnumber = self._meta.get_fill()
                result = self.nxcalscomm.get_fill(instance,fillnumber)            
            else:
                stfrom = self._meta.get_from_utc() 
                stto = self._meta.get_to_utc()
                result = self.nxcalscomm.get(name=instance,tfrom=stfrom,tto=stto,selector=user)            
            
            if user == "None":
                user = None
                name = instance+"/"+result["split"][0] + "/" + result["split"][1]
            else:
                name = instance+"/"+result["split"][0] + "/" + result["split"][1] + "/" + user

            if name in self._names:
                self._parent.mdi.delete_sub(name)

            self._names.append(name)

            if result is not None and result["split"][2] > 1 and len(result["data"]) > 0:
                if (len(result["data"][1]) > 1):
                    # to
                    time_to = QDateTime()
                    time_to.setMSecsSinceEpoch(int(result["data"][1][-1]*1000))                    
                    strto = time_to.toString(self._meta._datetime_from.displayFormat())
                    # from
                    time_from = QDateTime()
                    time_from.setMSecsSinceEpoch(int(result["data"][1][0]*1000))                    
                    strfrom = time_from.toString(self._meta._datetime_from.displayFormat())
                    self._meta.set_tfrom(strfrom)
                    self._meta.set_tto(strto)
                else:
                    self._meta.set_tfrom(result["split"][0])
                    self._meta.set_tto(result["split"][1])
            else:
                if len(result["data"]) == 0:
                    QApplication.restoreOverrideCursor() 
                    return    
                        
            if result["data"][0] is None or len(result["data"][0]) == 0:
                QApplication.restoreOverrideCursor() 
                return
            
            if result["vector"]:
           
                nxcals_widget = nxcalswidget.NXCALSWidget(self, 
                                                        title="", 
                                                        name_device=self._enum_devices.currentText(),
                                                        name_variable=instance,
                                                        name_cycle=user,
                                                        tfrom=result["split"][0],
                                                        tto=result["split"][1],                                                        
                                                        vector=True)

                self._parent.add_dtw(nxcals_widget,name)                      
                nxcals_widget.set_data(result["data"][0],valuex=result["data"][1],name=instance, name_parent=instance)

            else:

                nxcals_widget = nxcalswidget.NXCALSWidget(self, 
                                                        title="", 
                                                        name_device=self._enum_devices.currentText(),
                                                        name_variable=instance,
                                                        name_cycle=user,
                                                        tfrom=result["split"][0],
                                                        tto=result["split"][1],                                                        
                                                        vector=False)

                self._parent.add_dtw(nxcals_widget,name)      
                nxcals_widget.set_data(result["data"][0],valuex=result["data"][1],name=instance, name_parent=instance)

            if self._dark:
                nxcals_widget.dark_()
            else:
                nxcals_widget.light_()

        QApplication.restoreOverrideCursor()  

    def get_devices(self,e):
        """Get list devices from CCDA"""
        QApplication.setOverrideCursor(Qt.WaitCursor)
        if self._le_classes.text() == "LTIM" or self._le_classes.text() == "AnalogueSignal":
            QApplication.restoreOverrideCursor()
            return
        devices = ccda.get_devices_from_class(self._le_classes.text())
        devices.sort()
        ind = 0
        self._enum_devices.clear()
        if len(devices) == 0:
            QApplication.restoreOverrideCursor()
            return
        for device in devices:
            if ind < 1000:
                self._enum_devices.addItem(device)
            ind = ind + 1
        QApplication.restoreOverrideCursor()
        
    def get_properties(self,e):
        """Get list properties from CCDA"""
        device = self._enum_devices.currentText()
        if device == "":
            QApplication.restoreOverrideCursor()
            return
        properties = self.nxcalscomm.get_properties(self._enum_devices.currentText())
        properties.sort()
        self._enum_properties.clear()
        # self._enum_properties.addItem("*")
        self._properties = []
        for propertyfesa in properties:
                self._enum_properties.addItem(propertyfesa)
                self._properties.append(propertyfesa)
        
        QApplication.restoreOverrideCursor()

    def get_variables(self,e):
        """Get list properties from CCDA"""
        QApplication.setOverrideCursor(Qt.WaitCursor)
        device_name = self._enum_devices.currentText()
        property_name = "*" #self._enum_properties.currentText()
        variables = self.nxcalscomm.get_variables(device_name=device_name,property_name=property_name,field_name="*")
        if variables is None:
            QApplication.restoreOverrideCursor()
            return
        variables.sort()
        self._enum_variables.clear()
        # self._enum_variables.addItem("*")
        self._variables = []
        for variable in variables:
                self._enum_variables.addItem(variable)
                self._properties.append(variable)
        QApplication.restoreOverrideCursor()

    def close_dtw(self, name):
        """
        One datawidget is being closed...
        
        :param name: Name of the dtw.
        :type parent: str    
        """
        pass
        # try:
        #     self._settings.pop(name)
        # except:
        #     return


class _FilterFields(QWidget):
    """ 
    Widget panel to filter field/registers

    :param parent: Parent object.
    :type parent: object    
    """    

    filter_changed = pyqtSignal()

    def __init__(self,parent):        
        super(QWidget, self).__init__(parent)     

        self._parent = parent

        self._field_entities = []

        self._components = {}
        self._text_filter = QLineEdit("")

        self._add_text_filter = False

        self.layout = QGridLayout(self)

        self.scroll = QScrollArea()         
        self.scroll.setStyleSheet("border:none;")     
        self.widget = QWidget()               
        self.vbox = QVBoxLayout()             

        self.widget.setLayout(self.vbox)

        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.widget)

        self.layout.addWidget(self.scroll,0,0)
        self.show()

    def clear_layout(self):
        for fe in self._field_entities:
            fe.deleteLater()
        self._field_entities = []

    def add_field(self,measname,t1,t2,classname="",devicename="",propertyname=""):
        
        # self.clear_layout()        
        height = (4+len(listfields))*30
        if height > 600:
            height=600
        self.setMinimumHeight(height)
        
        self._components = {}

        # add filter
        if self._add_text_filter == False:
            panel_filter = QWidget()
            layout_filter = QGridLayout(panel_filter)
            layout_filter.setContentsMargins(10, 5, 0, 0)
            layout_filter.setSpacing(5) 
            layout_filter.addWidget(QLabel("Filter : "), 0,2,alignment=Qt.AlignRight)
            self._text_filter.installEventFilter(self)
            self.filter_changed.connect(self.filter_fields)
            layout_filter.addWidget(self._text_filter, 0,3,alignment=Qt.AlignLeft)
            layout_filter.addWidget(QLabel(" "), 0,1)
            layout_filter.setColumnStretch(1,1)
            self.vbox.addWidget(panel_filter) 
            self._add_text_filter = True
        
        fe_all = _FieldEntity(self,"All")
        fe_all.checkbox_plot.toggled.connect(self.pressall)  
        self._field_entities.append(fe_all)
        for lf in listfields: 
            _field_entity = _FieldEntity(self,lf['name'],field=lf)
            self._components[lf['name']] = _field_entity
            self._field_entities.append(_field_entity)

        for fe in self._field_entities:            
            self.vbox.addWidget(fe)

        completer = QCompleter(self._components)        
        self._text_filter.setCompleter(completer)

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets.

        :param widget: Widget concerned.
        :type widget: object
        :return: Always False.
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()            
        return False 

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.
        """
        filtertxt = self._text_filter.text()        
        for component_field in self._components.keys():
            if filtertxt is None or filtertxt == "":
                self._components[component_field].show()
            try:
                if component_field.index(filtertxt) >= 0:
                    self._components[component_field].show()
                else:
                    self._components[component_field].hide()
            except:
                self._components[component_field].hide()

    def pressall(self,e):
        """
        Press one checkbox.
        """
        for fe in self._field_entities:
            fe.setChecked(self._field_entities[0].isChecked())

    def dark_(self):
        """
        Set dark theme.
        """  
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";")
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
        for fe in self._field_entities:
            fe.dark_()

    def light_(self):
        """
        Set light theme.
        """  
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";;color:"+Colors.STR_COLOR_BLACK+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")
        for fe in self._field_entities:
            fe.light_()

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        for fe in self._field_entities:
            fe.set_font_size(size)


class _FieldEntity(QWidget):
    """
    Field widget panel

    :param parent: Parent object.
    :type parent: object    
    """    

    def __init__(self,parent,name,field=None):
        """
        Initialize Class.
        """

        super(QWidget, self).__init__(parent)     

        self._parent = parent
        self.name = name
        self.setMaximumHeight(30)
        self.layout = QGridLayout()
        self.layout.setContentsMargins(5, 0, 5, 5)
        self.layout.setSpacing(5)             
        self.setAutoFillBackground(True)
        self.setLayout(self.layout)
        # name
        label_ = QLabel(name+"  ")
        label_.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        label_.setAlignment(Qt.AlignRight)    
        label_.setStyleSheet("font-weight:bold;")
        self.layout.addWidget(label_,0,0)

        self._field = None
        self.label_rwmode = None
        self.label_type = None
        self.label_dwith = None
        self.label_size = None

        if field is not None:

            self._field = field

            # rwmode
            self.label_rwmode = QLabel(" "+field['rwmode']+" ")
            self.label_rwmode.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
            self.label_rwmode.setAlignment(Qt.AlignCenter)    
            self.layout.addWidget(self.label_rwmode,0,1)
            # type
            self.label_type = QLabel(field['type'])
            self.label_type.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
            self.label_type.setAlignment(Qt.AlignCenter)    
            self.layout.addWidget(self.label_type,0,2)
            # dwidth
            self.label_dwith = QLabel(str(field['dwith']))
            self.label_dwith.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
            self.label_dwith.setAlignment(Qt.AlignCenter)    
            self.layout.addWidget(self.label_dwith,0,3)
            # size            
            if field['depth'] > 1:
                self.label_size = QLabel(" "+str(field['depth'])+" ")
                self.label_size.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
                self.label_size.setAlignment(Qt.AlignCenter)    
                self.layout.addWidget(self.label_size,0,4)
        # checkbox
        self.checkbox_plot = QCheckBox("")
        self.checkbox_plot.setChecked(False)
        self.checkbox_plot.setMaximumWidth(16)
        self.layout.addWidget(self.checkbox_plot,0,5)
        self.layout.setRowStretch(0,0)
        self.layout.setColumnStretch(0,1)
        self.layout.setColumnStretch(1,0)
        self.layout.setColumnStretch(2,0)
        self.layout.setColumnStretch(3,0)
        self.layout.setColumnStretch(4,0)
        self.layout.setColumnStretch(5,0)

    def isChecked(self):
        return self.checkbox_plot.isChecked()

    def setChecked(self,val):
        self.checkbox_plot.setChecked(val)

    def dark_(self):
        """
        Set dark theme.
        """  
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHTRED+";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLUE+";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHT0GREEN+";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHT3GRAY+";color:white;")

        self.checkbox_plot.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT0GRAY+";")
        
    def light_(self):
        """
        Set light theme.
        """  
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")

        if self._field is not None:
            if self._field["rwmode"] == "RO":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHTRED+";color:white;")
            elif self._field["rwmode"] == "RW":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLUE+";color:white;")
            elif self._field["rwmode"] == "WO":
                self.label_rwmode.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHT0GREEN+";color:white;")
            if self.label_size is not None:
                self.label_size.setStyleSheet("background-color:"+Colors.STR_COLOR_LIGHT3GRAY+";color:white;")

        self.checkbox_plot.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";background-color:"+Colors.STR_COLOR_WHITE+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")  

    def set_font_size(self, size=8):
        """
        Change font size.

        :param size: Font size (default=8).
        :type size: int, optional
        # """
        pass


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """
        Init user interface.
        """
        dark = True        

        central_widget = QWidget() 

        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5) 
        
        ind = 0

        self._meta = MetaNXCALSWidget(central_widget)
        # nxcals_comm = nxcalscomm.NXCALSComm(self)
        # self._meta = _PeriodSelection(central_widget,nxcals_comm)
        # self._meta.setMaximumHeight(90)
        self.layout.addWidget(self._meta,0,0)            
        
        self.setCentralWidget(central_widget)

        if dark:
            central_widget.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_WHITE+";")
        else:
            central_widget.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")

        self.resize(1300,900)
        self.move(100,100)

        if dark:
            self._meta .dark_()
        else:
            self._meta .light_()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
        
    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";
        }
        QMenuBar::item:selected {    
            background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }
        QMenuBar::item:pressed {
            background: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }

        QMenu {
            background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";   
            margin: 2px;
        }
        QMenu::item {
            background-color: transparent;
        }
        QMenu::item:selected { 
            background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";                
        }         
        QLineEdit:{background-color: black;}            
    """ 
    app.setStyleSheet(qss)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())