from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

from functools import partial
import sys
import time

# import os, pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import ccda

class DevicesFesaWidget(QWidget):    

    """    
    Class widget to display automaic FESA devices with name, color and text.

    :param parent: Parent object
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param class_names: List of classes to be processed.
    :type class_names: list
    :param max_col: Number of columns to be shown (default=5).
    :type max_col: int
    :param sorted: If "tab" then fecs will be splitted in tabs else all devices will be shown in the same panels.
    :type sorted: str, optional
    """

    filter_changed = pyqtSignal()
    
    def __init__(self, parent, title=None, class_names = [], fec = None, max_col=5, sorted=None, devices_to_hide=None):
        """
        Initialize class.
        """

        super(QWidget, self).__init__(parent)        

        qta.icon("fa5.clipboard")

        self._parent = parent
        self._signal = None
        self._max_col = max_col
        self._sorted = sorted # options : None or tab

        # find devices

        self._dbw = {}
        
        self._devices = {}
        self._device_class = {}
        self._name_devices = []    

        # sort first classes names

        class_names.sort()

        # then find fecs and their devices

        for fesa_class_name in class_names:
            
            dict_fesa_fec = ccda.get_devices_from_class_per_fec(fesa_class_name)
            
            fnames = [*dict_fesa_fec]
            fnames.sort()
            
            for fec_name in fnames:                
                if fec is not None and fec_name not in fec:
                    continue
                devices = [*dict_fesa_fec[fec_name]]
                if devices_to_hide is not None:
                    for dev_to_hide in devices_to_hide:
                        if dev_to_hide in devices:
                            devices.remove(dev_to_hide)
                devices.sort()
                for device in devices:              
                    if fesa_class_name not in self._devices:
                        self._devices[fesa_class_name] = {}
                    if fec_name not in self._devices[fesa_class_name]:
                        self._devices[fesa_class_name][fec_name] = []
                    self._devices[fesa_class_name][fec_name].append(device)
                    self._device_class[device] = fesa_class_name
                    self._name_devices.append(device)
                    
        self._panel_class_devices = {}
        show_title = True
        if sorted == "tab":
            show_title = False
        for class_device in self._devices:
            self._panel_class_devices[class_device] = DeviceClassPanelWidget(self, 
                                                                             class_name=class_device, 
                                                                             fecs=self._devices[class_device], 
                                                                             max_col=max_col, 
                                                                             sorted=sorted, 
                                                                             title=show_title)
        
        # layout and scroll

        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)

        # title

        self._label_title = None
        if title is not None:
            self._label_title = QLabel(title)
            self._label_title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self._label_title.setAlignment(Qt.AlignCenter)        
            self.gridLayout.addWidget(self._label_title, 0, 0)             
            self.gridLayout.setRowStretch(0, 0)

        # filter

        panel_filter = QWidget()
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(10, 5, 0, 0)
        layout_filter.setSpacing(5) 
        layout_filter.addWidget(QLabel("Filter : "), 0, 2, alignment=Qt.AlignRight)
        self._text_filter = QLineEdit("")
        self._text_filter.installEventFilter(self)
        self.filter_changed.connect(self.filter_fields)
        layout_filter.addWidget(self._text_filter, 0, 3, alignment=Qt.AlignLeft)
        layout_filter.addWidget(QLabel(" "), 0, 1)
        layout_filter.setColumnStretch(1, 1)
        self.gridLayout.addWidget(panel_filter, 1, 0) 
        self.gridLayout.setRowStretch(1, 0)
        
        # devices   

        ind = 2
        if sorted == "tab":
            self._tabs = QTabWidget()
            self._tabs.setTabsClosable(False)
            self.gridLayout.addWidget(self._tabs, ind, 0)
            self.gridLayout.setRowStretch(ind, 1)
        
        for class_device in self._devices:            
            if sorted is None:
                self.gridLayout.addWidget(self._panel_class_devices[class_device], ind, 0)
                self.gridLayout.setRowStretch(ind, 1)
                ind=ind+1
            elif sorted == "tab":
                self._tabs.addTab(self._panel_class_devices[class_device], class_device)

    def eventFilter(self, widget, event):
        """
        If keyboard event on filter -> refresh field widgets
        
        :param widget: Widget where the components are.
        :type widget: object
        :param event: Keyboard event.
        :type event: QEvent
        :return: Always False
        :rtype: bool
        """
        if event.type() == QEvent.KeyPress and widget is self._text_filter and event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.filter_changed.emit()            
        return False 

    @pyqtSlot()
    def filter_fields(self):
        """
        Filter visible field panel using their names.        
        """
        filtertxt = self._text_filter.text()    
        for panel in self._panel_class_devices.values():
            panel.filter_fields(filtertxt)
       
    def get_devices(self):
        """
        Get devices list.
        
        :return: List of devices.
        :rtype: list
        """
        return self._name_devices
    
    def get_device_buttons(self, class_name):
        """
        Get devices button list.
        """
        return self._panel_class_devices[class_name].get_device_buttons()
    
    def get_device_button(self, class_name, device):
        """
        Get devices button.
        """
        return self._panel_class_devices[class_name].get_device_button(device)

    def set_title(self, device, title):
        """
        Change title in a device button.

        :param device: Device name.
        :type device: str
        :param title: Title for a specific device
        :type title: str        
        """
        if device in self._dbw:
            self._dbw[device].set_title(title)

    def set_text(self, device, text):
        """
        Change text in a device button

        :param device: Device name.
        :type device: str
        :param text: Text to be shown for a specific device
        :type text: str        
        """
        if device in self._dbw:
            self._dbw[device].set_text(text)

    def set_color(self, device, color):
        """
        Change color in a device button

        :param device: Device name.
        :type device: str
        :param color: Background color a specific device
        :type color: str        
        """
        if device in self._dbw:
            self._dbw[device].set_color(color)

    def set_text_color(self, device, color):
        """
        Change color in a device button

        :param device: Device name.
        :type device: str
        :param color: Background color a specific device
        :type color: str        
        """
        if device in self._dbw:
            self._dbw[device].set_text_color(color)

    def addSignal(self, signal):
        """
        Add pyqtsignal to be triggered if device button pressed.
        
        :param signal: Signal to be added/connected.
        :type signal: PyQtSignal        
        """
        for panel in self._panel_class_devices.values():
            panel.addSignal(signal)

    def dark_(self):
        """
        Set to dark theme.
        """
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for panel in self._panel_class_devices.values():
            panel.dark_()

    def light_(self):
        """
        Set to light theme.
        """
        self._text_filter.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";;color:"+Colors.STR_COLOR_BLACK+";text-align:right;border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";")
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for panel in self._panel_class_devices.values():
            panel.light_()


class DeviceClassPanelWidget(QWidget):

    """    
    Class widget panel to display all devices for one specific class.

    :param parent: Parent object
    :type parent: object
    :param title: Show or not the class_name as a title (default=True).
    :type title: bool, optional
    :param class_name: Class name.
    :type class_name: str, optional
    :param fecs: List of fec names
    :type fecs: list
    :param max_col: Number of columns to be shown (default=5).    
    :type max_col: int
    :param sorted: If "tab" then fecs will be splitted in tabs else all devices will be shown in the same panels.
    :type sorted: str, optional
    """
    
    def __init__(self, parent, title=True, class_name="", fecs={}, max_col=5, sorted=None):
        super(QWidget, self).__init__(parent)        

        self._parent = parent
        self._signal = None
        self._max_col = max_col
        self._sorted = sorted 

        self._fecs = fecs
        self._name_devices = []
        self._class_name = class_name

        for fec in self._fecs:
            for device in self._fecs[fec]:        
                self._name_devices.append(device)
        
        if len(self._name_devices) < self._max_col:
            self._max_col = len(self._name_devices)

        # layout and scroll

        self.layout = QHBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.layout.addWidget(self.scrollArea)
        
        # title

        self._label_title = None
        if title:
            self._label_title = QLabel(class_name)
            self._label_title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.gridLayout.addWidget(self._label_title, 0, 0)             
            self.gridLayout.setRowStretch(0, 0)

        # devices  

        ind = 0
        row = 2
        col = 0

        self._dbw = {}
        self._dbw_show = {}
        self._label_fecs = {}

        for fec in self._fecs:

            ind = 0
            col = 0            
            
            row=row+1    
            
            self._label_fecs[fec] = QLabel(fec)
            self.gridLayout.addWidget(self._label_fecs[fec], row, 0)
            
            row=row+1    
            
            for device in self._fecs[fec]:            
                
                self._dbw[device] = DeviceButtonWidget_(title=device, text="") 
                parent._dbw[device] = self._dbw[device]
                self._dbw[device].mousePressEvent = partial(self.press, device)
                self._dbw[device].setMinimumHeight(40)
                self._dbw[device].set_title(device)
                self._dbw_show[device] = True
                
                if col >= max_col:
                    col = 0
                    row = row+1

                self.gridLayout.addWidget(self._dbw[device], row, col)                
                self.gridLayout.setColumnStretch(col, 1)
                self.gridLayout.setRowStretch(row, 1)
                
                ind=ind+1
                col=col+1

        self.gridLayout.setRowStretch(row+1, 1)
        
    def update_panel_devices(self):
        """
        Redraw device(s) panel.
        """
        
        row=2    

        for lab in self._label_fecs.values():
            self.gridLayout.removeWidget(lab)

        for fec in self._fecs:

            ind = 0
            col = 0           
            row=row+1                
            
            self.gridLayout.addWidget(self._label_fecs[fec], row, 0)            
            
            row=row+1                
            
            for device in self._fecs[fec]:            
                self.gridLayout.removeWidget(self._dbw[device])
                if self._dbw_show[device]: 
                    if col >= self._max_col:
                        col = 0
                        row = row+1
                    self.gridLayout.addWidget(self._dbw[device], row, col)
                    self.gridLayout.setColumnStretch(col, 1)
                    self.gridLayout.setRowStretch(row, 1)
                    ind=ind+1
                    col=col+1

        self.gridLayout.setRowStretch(row+1, 1)

    def filter_fields(self, filter):
        """
        Filter visible field panel using their names.

        :param filter: Filter string.
        :type filter: str
        """
        try:
            if filter.index("fec=") >= 0:
                filter_fec = filter.replace("fec=", "")    
                try:
                    for lab in self._label_fecs.values():
                        lab.hide()
                    self._label_fecs[filter_fec].show()
                except:
                    pass
            else:
                for lab in self._label_fecs.values():
                    lab.show()          
                for device in self._dbw.keys():
                    self._dbw[device].show()           
        except:
            for lab in self._label_fecs.values():
                lab.show()                    
            for device in self._dbw.keys():
                    self._dbw[device].show()           

        for device in self._dbw.keys():

            if filter is None or filter == "":
                self._dbw[device].show()                
                self._dbw_show[device] = True
            try:
                try:
                    if filter.index("fec=") >= 0:
                        filter_fec = filter.replace("fec=", "")
                        try:
                            if device in self._fecs[filter_fec]:
                                self._dbw[device].show()  
                                self._dbw_show[device] = True                  
                            else:
                                self._dbw[device].hide() 
                                self._dbw_show[device] = False                   
                        except:
                            self._dbw[device].hide()                
                            self._dbw_show[device] = False
                    else:
                        if device.index(filter) >= 0:
                            self._dbw[device].show()  
                            self._dbw_show[device] = True                  
                        else:
                            self._dbw[device].hide() 
                            self._dbw_show[device] = False                   
                except:
                    try:
                        if device.index(filter) >= 0:
                            self._dbw[device].show()  
                            self._dbw_show[device] = True                  
                        else:
                            self._dbw[device].hide() 
                            self._dbw_show[device] = False              
                    except:
                        self._dbw[device].hide() 
                        self._dbw_show[device] = False              
            except:
                self._dbw[device].hide()                
                self._dbw_show[device] = False
        self.update_panel_devices()

    def get_devices(self):
        """
        Get devices list.
        """
        return self._dbw.keys()
        
    def get_device_buttons(self):
        """
        Get devices button list.
        """
        return self._dbw.values()

    def get_device_button(self, device):
        """
        Get devices button list.
        """
        return self._dbw[device]

    def get_title_label(self, device):
        """
        Return the title label.
        """
        return self._dbw[device].get_title_label()
        
    def get_desc_label(self, device):
        """
        Return the desc label.
        """
        return self._dbw[device].get_desc_label()

    def set_title(self, device, title):
        """
        Change title in a device button.

        :param device: Device name.
        :type device: str
        :param title: Title to be shown per device.
        :type title: str
        """
        self._dbw[device].set_title(title)

    def set_text(self, device, text):
        """
        Change text in a device button.

        :param device: Device name.
        :type device: str
        :param text: Text to be shown per device.
        :type text: str
        """
        self._dbw[device].set_text(text)

    def set_color(self, device, color):
        """
        Change color in a device button.
        
        :param device: Device name.
        :type device: str
        :param color: Background color to be used per device button.
        :type color: QColor
        """
        if device in self._dbw:
            self._dbw[device].set_color(color)

    def addSignal(self, signal):
        """
        Add pyqtsignal to be triggered if device button pressed.
        
        :param signal: Signal to be added/connected.
        :type signal: PyQtSignal  
        """
        self._signal = signal

    def press(self, device, e):
        """
        Press one device button

        :param device: Device name.
        :type device: str
        """
        if self._signal is not None:
            self._signal.emit(device)

    def dark_(self):
        """
        Set to dark theme.
        """
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for label in self._label_fecs.values():
            label.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:italic;")

    def light_(self):
        """
        Set to light theme.
        """
        if self._label_title is not None:
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        for label in self._label_fecs.values():
            label.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:italic;")


class DeviceButtonWidget_(QPushButton):

    """    
    Device Button (QPushButton)    

    :param title: Title to be shown.
    :type title: str, optional
    :param text: Text to be shown.
    :type text: str, optional
    :param color: Background color.
    :type color: QColor
    """    
    
    update_signal = pyqtSignal()

    def __init__(self, title="", text="", color=None):
        """
        Initialize class.
        """
        QPushButton.__init__(self)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding);        
        self._color = color    
        self._title = title
        self._text = text  
        self._titletext = QLabel(self)      
        self._titletext.setStyleSheet("font-weight:bold;")
        self._titletext.setTextFormat(Qt.RichText)
        self._titletext.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self._titletext.setAttribute(Qt.WA_TranslucentBackground)
        self._titletext.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._titletext.setSizePolicy(
            QSizePolicy.Expanding, 
            QSizePolicy.Expanding, 
        )
        self._desctext = QLabel(self)      
        self._desctext.setTextFormat(Qt.RichText)
        self._desctext.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self._desctext.setAttribute(Qt.WA_TranslucentBackground)
        self._desctext.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._desctext.setSizePolicy(
            QSizePolicy.Expanding, 
            QSizePolicy.Expanding, 
        )
        self._layout = QGridLayout()
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.setSpacing(0)
        self.setLayout(self._layout)
        self._layout.addWidget(self._titletext, 0, 0)
        self._layout.addWidget(self._desctext, 1, 0)
        self.update_signal.connect(self.update_button)
        self.update_signal.emit()    
        
    def set_title(self, title):
        """
        Set title.
        
        :param title: Title
        :type title: str
        """
        self._title = title
        self.update_signal.emit()        

    def set_text(self, text):
        """
        Set text.
        
        :param text: Title
        :type text: str
        """
        self._text = text
        self.update_signal.emit()

    def set_text_color(self, color):
        """
        Set txt color.

        :param color: Color
        :type color: QColor
        """
        self._titletext.setStyleSheet("color:"+color+";")
        self._desctext.setStyleSheet("color:"+color+";")
        
    def set_color(self, color):
        """
        Set color.

        :param color: Color
        :type color: QColor
        """
        self._color = color
        self.update_signal.emit()
    
    def set_color_button(self, color):
        """
        Define color inside LED.

        :param color: Color
        :type color: QColor
        """
        self.setStyleSheet("background:"+self._color)

    def get_title_label(self):
        """
        Return the title label.
        """
        return self._titletext

    def get_desc_label(self):
        """
        Return the desc label.
        """
        return self._desctext

    def get_layout(self):
        """
        Return the layout.
        """
        return self._layout

    @pyqtSlot()
    def update_button(self):
        """
        Update text in button.
        """
        self._titletext.setText(self._title)
        self._desctext.setText(self._text)
        if self._color is not None:     
            self.set_color_button(self._color)
        self.updateGeometry()


class _Example(QMainWindow):
    
    """    
    Example class to test.    
    """
    
    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()
        
    def init_ui(self):        
        """
        Init user interface.
        """
        central_widget = QWidget()         
        self.layout = QGridLayout(central_widget)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(5) 
        class_names=["BSISO"]
        dfw = DevicesFesaWidget(self, class_names=class_names, sorted=None, devices_to_hide=["XIHS.BFC0100", "XTRAP.BFC2000"])
        dfw.dark_()
        self.layout.addWidget(dfw, 0, 0)            
        self.setCentralWidget(central_widget)
        self.layout.setRowStretch(0, 1)                
        self.resize(800, 800)

        ind = 0
        for device in dfw.get_devices():
            if ind == 1 or ind == 6:
                dfw.set_color(device, Colors.STR_COLOR_LBLUE)
            if ind == 0 or ind == 10:
                dfw.set_color(device, Colors.STR_COLOR_LIGHTRED)
                dfw.set_text(device, "This is a test")
                dfw.set_text_color(device, Colors.STR_COLOR_YELLOW)
            ind = ind + 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)   

    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    
    qss = """
        QMenuBar::item {
            spacing: 2px;           
            padding: 2px 10px;
            background-color: """+Colors.STR_COLOR_LIGHT0GRAY+""";
        }
        QMenuBar::item:selected {    
            background-color: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }
        QMenuBar::item:pressed {
            background: """+Colors.STR_COLOR_LIGHT1GRAY+""";
        }              
        QLineEdit:{background-color: black;}
    """ 
    app.setStyleSheet(qss)

    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())