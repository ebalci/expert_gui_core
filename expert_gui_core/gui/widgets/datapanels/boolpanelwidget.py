from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import ledwidget
from expert_gui_core.gui.widgets.common import togglebutton
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.common import expander

from expert_gui_core.tools import formatting


class BoolPanelWidget(QWidget):

    """    
    Class low-level boolean widget setting/displaying (only) boolean.

    :param parent: Parent object.
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param edit: Edit or not (not used, default=True).
    :type edit: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional
    :param size_history: Size history (default=600)
    :type size_history: int, optional
    :param type_boolean: Type of bool component to show ("checkbox","led","toggle","checkboxcol",default="checkbox").
    :type type_boolean: str, optional
    :param statistics: Statistics option (default=False).
    :type statistics: bool, optional
    :param fitting: Fitting option (default=False).
    :type fitting: bool, optional    
    """
    
    update_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    _signal = pyqtSignal()

    def __init__(self,
                 parent,
                 title=None,
                 name=True,
                 label_name="",
                 edit=True,
                 history=True,
                 size_history=600,
                 type_boolean="checkbox",
                 statistics=False,
                 fitting=False):
        """Init UI"""
        super(QWidget, self).__init__(parent)        

        qta.icon("fa5.clipboard")

        self._name = label_name
        self._edit = edit        
        self.default_palette = QGuiApplication.palette()
        self.type = type_boolean
        self._function_expression = None
        
        maxheight = 30

        self.data = False
        self.data_type = None
        self.cycle = None
        self.time = 0
        
        self.font_text = QFont()
        self.font_textbig = QFont()
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        
        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        self.mainlayout.setContentsMargins(0,0,0,0)
        self.mainlayout.setSpacing(0) 

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)                

        self.mainlayout.addWidget(self._main_widget,0,0)

        self._label_title = QLabel(title)
        self._label_name = QLabel(label_name+" ")
        self._toggle = togglebutton.ToggleButton(
            self,
            text=["",""],
            width=18
            )        
        self._checkboxcol_value = QCheckBox()
        if self.type == "checkbox":
            self._checkboxcol_value.setMaximumWidth(18)
        self._led_value = ledwidget.LedWidget()
        self._history_button = QPushButton(fa.icons["chart-line"])

        self._color_led_active = Colors.COLOR_BLUE       
        self._color_led_inactive = Colors.COLOR_LIGHT4GRAY       
            
        # title 

        self.font = self._label_title.font( )        
        self._label_title.setAlignment(Qt.AlignCenter)        
        
        if title is not None:
            self.layout.addWidget(self._label_title,0,0,1,5)
            maxheight = maxheight + 18

        self.setMinimumHeight(maxheight)        
        
        # history

        if history:
            self.layout.addWidget(self._history_button,1,0)
            self._history_button.setFont(self.font_fa)
            self._history_button.setMaximumWidth(20)
            self._history_button.mousePressEvent = self.show_history

        # name 

        if name and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name,1,1)
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        
        # boolean component depending on type

        self._toggle.hide()
        if self.type == "toggle":
            self.layout.addWidget(self._toggle,1,2,alignment=Qt.AlignLeft)
            self._toggle.show()
        elif self.type == "checkboxcol" or self.type == "checkbox":
            self.layout.addWidget(self._checkboxcol_value,1,2,alignment=Qt.AlignLeft)                
        elif self.type == "led":
            self.layout.addWidget(self._led_value,1,2,alignment=Qt.AlignLeft)
        
        # mouse event

        if edit:
            if self.type == "toggle":
                self._toggle.mousePressEvent = self.push
            elif self.type == "checkboxcol" or self.type == "checkbox":
                self._checkboxcol_value.stateChanged.connect(self.push)
            elif self.type == "led":
                self._led_value.mousePressEvent = self.push

        # stretching

        lab = QLabel(" ")     
        self.layout.addWidget(lab,1,5)

        if edit:
            self.layout.setColumnStretch(2,1)
            self.layout.setRowStretch(1,1)

        # datawidget

        data_widget_opts = {
            "auto":True, 
            "show_chart":True,
            "show_table":True,
            "history":True,
            "show_chart_value_axis":True,
            "statistics":False,
            "fitting":False,
            "statistics":statistics,
            "fitting":fitting
        }
        if history:
            self._data_widget = datawidget.DataWidget(None, 
                                      title=None, 
                                      name=[self._name],
                                      header=True,
                                      max=size_history,
                                      **data_widget_opts)
        else:
            self._data_widget = None

        self._frame = expander._DataWidgetExpander(self,name=label_name)

        # signals

        self.update_signal.connect(self.update_data)
        self.update_signal_chart.connect(self.update_chart)  
        
        if self.type == "checkboxcol":
            self._checkboxcol_value.setStyleSheet("color:white;border:none;")

    def get_label_component(self):
        return self._label_name

    def get_main_component(self):
        if self.type == "toggle":
            return [self._toggle]
        elif self.type == "checkboxcol" or self.type == "checkbox":
            return [self._checkboxcol_value]
        elif self.type == "led":
            return [self._led_value]

    def set_minimum_width_name(self, width):
        """
        Set minimum text label name width.
        
        :param width: Minimum width of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def show_history(self, e):
        """
        Show chart component.
        """
        if self._frame.isVisible():
            self._frame.hide()            
        else:
            self._frame.show()   
            if self._data_widget is not None:
                self._data_widget.update_all_components()

    def get_toggle(self):
        """
        Get the toggle button.
        """
        return self._toggle

    def set_font_size(self, size=8):
        """
        Change component font size.
        
        :param size: Size font (default=8)
        :type size: int, optional
        """
        pass
      
    def dark_(self):
        """
        Set dark theme.
        """        
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        self._toggle.dark_()
        if self.type == "checkboxcol":
            self._checkboxcol_value.setStyleSheet("QCheckBox::indicator:checked{background-color:"+Colors.STR_COLOR_BLACK+";} QCheckBox::indicator:unchecked{background-color:"+Colors.STR_COLOR_LIGHT4GRAY+";}")
        else:
            self._checkboxcol_value.setStyleSheet(
                "max-width:14;max-height:14;color: " + Colors.STR_COLOR_WHITE + ";border:1px solid " + Colors.STR_COLOR_L3BLACK + ";background-color: " + Colors.STR_COLOR_L2BLACK + ";")
        if self._data_widget is not None:            
            self._data_widget.dark_()        

    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        self._toggle.light_()
        if self.type == "checkboxcol":
            self._checkboxcol_value.setStyleSheet("QCheckBox::indicator:checked{background-color:"+Colors.STR_COLOR_WHITE+";} QCheckBox::indicator:unchecked{background-color:"+Colors.STR_COLOR_LIGHT4GRAY+";}")
        else:
            qss =  """
                QCheckBox::indicator {
                    border:1px solid """+Colors.STR_COLOR_LIGHT5GRAY+""";            
                }       
                QCheckBox::indicator:checked {
                    max-width:14;
                    max-height:14;
                    border:0px solid """+Colors.STR_COLOR_LIGHT5GRAY+""";
                    background-color: """+Colors.STR_COLOR_L2BLUE+""";            
                }             
                QCheckBox {
                    max-width:14;
                    max-height:14;
                    color: """+Colors.STR_COLOR_BLACK+""";
                    background-color: """+Colors.STR_COLOR_WHITE+""";            
                }
                """
            self._checkboxcol_value.setStyleSheet(qss)

        if self._data_widget is not None:
            self._data_widget.light_()

    def set_data(self, data, cycle=None):
        """
        Set data in a plot item.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param cycle: Cycle name (information).
        :type cycle: str, optional
        """
        self.data = data
        try:
            self.data_type = type(data)
        except:
            self.data_type = 'bool'
        self.cycle = cycle
        self.update_signal.emit()
        self.update_signal_chart.emit()

    @pyqtSlot()
    def update_chart(self):
        """
        Update chart component.
        """        
        if self._data_widget is not None:
            self._data_widget.set_data(self.data, cycle=self.cycle, name=self._name, name_parent=self._name)

    def set_color_led(self, true_color=Colors.COLOR_BLUE, false_color=Colors.COLOR_LIGHT4GRAY):
        """
        Change LED colors true_color/false_color.

        :param true_color: True color (default=COLOR_BLUE)
        :type true_color: QColor, optional
        :param false_color: False color (default=COLOR_LIGHT4GRAY)
        :type false_color: QColor, optional
        """
        self._color_led_active = true_color       
        self._color_led_inactive = false_color       

    @pyqtSlot()
    def update_data(self):
        """
        Update data.
        """
        if self.data:
            self._led_value.set_color_inside(self._color_led_active)            
            self._toggle.setChecked(True)
            self._checkboxcol_value.setChecked(True)
        else:
            self._led_value.set_color_inside(self._color_led_inactive)
            self._toggle.setChecked(False)
            self._checkboxcol_value.setChecked(False)

    def push(self,e):
        """
        Push button emit event.
        """
        self.data = not self.data
        self.set_data(self.data)
        if self._signal is not None:
            self._signal.emit()
    
    def get_data(self):
        """
        Get data.
        
        :return: Data value
        :rtype: bool
        """
        data = formatting.type_value(self.data,self.data_type)
        return data

    def addActionsignal(self, signal):
        """
        Add action signal.

        :param signal: Input signal.
        :type signal: PyQtSignal
        """
        self._signal = signal

    def changeValue(self):
        """
        Value changed!.
        """
        if self._signal is not None:
            self._signal.emit()

    def addSignal(self,signal):
        """
        Add pyqtsignal to be triggered if data changed.

        :param signal: Input signal.
        :type signal: PyQtSignal
        """
        self._signal = signal

    def get_signal(self):
        return self._signal

    def set_colors_toggle(self, colors=[[Colors.COLOR_L4BLUE,Colors.COLOR_LIGHT1GRAY],[Colors.COLOR_L4BLUE,Colors.COLOR_LIGHT3GRAY]]):
        """
        Change colors toogle.
        
        :param colors: 4 colors on/off light/dark (default: L4BLUE,LIGHT4GRAY | L4BLUE,LIGHT4GRAY)
        :type colors: list
        """
        self._toggle.set_colors(colors)

    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression


class _Example(QMainWindow):

    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """Init user interface"""
        central_widget = QWidget() 

        scroll = QScrollArea()        
        scroll.setWidgetResizable(True)        
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)
        
        type_format = [
			"checkbox",
			"led",
			"toggle",
			"checkboxcol"
		]

        b=True
        for i in range(8):
            bool_widget = BoolPanelWidget(self,title=None,label_name="name",type_boolean=type_format[i%4],edit=True)       
            # if i==0:
            #     bool_widget.setStyleSheet("background:red;")
            self.layout.addWidget(bool_widget,i,0)
            b=not b
            bool_widget.set_data(b)
            bool_widget.set_data(not b)
            bool_widget.set_data(b)
            # if i!=2 and i!=5:
            #     bool_widget.light_() 
            # else:
            bool_widget.dark_() 
            # self.layout.setRowStretch(i,1)

        self.setCentralWidget(scroll)
        self.resize(400,400)
        
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 
    default_palette = QGuiApplication.palette()
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    app.setPalette(darkpalette)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())