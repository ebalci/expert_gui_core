from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import expander
from expert_gui_core.tools import formatting
from expert_gui_core.gui.widgets.pyqt import datawidget


class EnumPanelWidget(QWidget):

    """    
    Class low-level enum widget setting/displaying enum.

    :param parent: Parent object.
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param labels: List of labels to be used in bit enum.
    :type labels: list, optional
    :param edit: Edit or not (not used, default=True).
    :type edit: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional
    :param size_history: Size history (default=600)
    :type size_history: int, optional
    :param temp: Show or not thermometer.
    :type temp: bool, optional   
    """
    
    update_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    _signal = pyqtSignal()

    def __init__(self, parent, title=None, name=True, label_name="", labels=None, edit=True,
                    history=True, size_history=600, temp=False, statistics=False, fitting=False):
        super(QWidget, self).__init__(parent)        
        
        qta.icon("fa5.clipboard")

        self.default_palette = QGuiApplication.palette()
        self._edit = edit
        self.data = 0
        self.data_type = None
        self.cycle = None
        self._name = label_name
        self._function_expression = None

        maxheight = 30
        
        self.font_text = QFont()
        self.font_textbig = QFont()
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        self.mainlayout.setContentsMargins(0,0,0,0)
        self.mainlayout.setSpacing(0) 

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)                

        self.mainlayout.addWidget(self._main_widget,0,0)
        
        self._label_title = QLabel(title)
        self._label_name = QLabel(label_name+" ")
        self._label_jauge_normal = QLabel(fa.icons["thermometer-half"])       
        self._label_jauge_alert_full = QLabel(fa.icons["thermometer-full"])       
        self._label_jauge_alert_empty = QLabel(fa.icons["thermometer-empty"])       
        self._label_jauge_none = QLabel(fa.icons["thermometer-half"]) 
        
        self._enum_value_set = QComboBox()        
        self._list_value_set = QListView(self._enum_value_set)
        self._enum_value_set.setView(self._list_value_set)
        self._enum_value_set.setEditable(True)
        self._enum_value_set.lineEdit().setAlignment(Qt.AlignLeft)   
        self._enum_value_set.lineEdit().setDisabled(True)
        self._enum_value_set.lineEdit().setReadOnly(True)
        self._enum_value_set.currentTextChanged.connect(self.changeValue)

        self._labels = labels
        if labels is not None:
            listlabels = []
            for label in labels.values():
                listlabels.append(label)
            listlabels.sort()
            for label in listlabels:
                self._enum_value_set.addItem(label)
            
        self.font = self._label_title.font()        
        self._label_title.setAlignment(Qt.AlignCenter)        
        
        if title is not None:
            self.layout.addWidget(self._label_title,0,0,1,5)
            maxheight = maxheight + 18

        self.setMinimumHeight(maxheight)

        self._history_button = QPushButton(fa.icons["chart-line"])            
        if history == True:
            self.layout.addWidget(self._history_button,1,0)
            self._history_button.setFont(self.font_fa)
            self._history_button.setMaximumWidth(20)
            self._history_button.mousePressEvent = self.show_history
        
        if name == True and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name,1,1)
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        
        edit=True
        if edit == True:            
            self.layout.addWidget(self._enum_value_set,1,2)

        self._label_jauge_normal.setStyleSheet("color:rgb(0,210,0);")
        self._label_jauge_alert_full.setStyleSheet("color:rgb(255,60,60);")
        self._label_jauge_alert_empty.setStyleSheet("color:rgb(255,60,60);")
        self._label_jauge_none.setStyleSheet("color:lightgray;")        
        self.layout.addWidget(self._label_jauge_none,1,3)
        self.layout.addWidget(self._label_jauge_alert_full,1,3)
        self.layout.addWidget(self._label_jauge_normal,1,3)
        self.layout.addWidget(self._label_jauge_alert_empty,1,3)
        if temp == False:
            self._label_jauge_none.hide()
        self._label_jauge_normal.hide()
        self._label_jauge_alert_full.hide()
        self._label_jauge_alert_empty.hide()
    
        if edit == True:            
            self.layout.setColumnStretch(2,1)
        
        data_widget_opts = {
            "auto":True, 
            "show_chart":True,
            "show_table":True,
            "time_in_table":True,
            "history":True,
            "show_chart_value_axis":True,
            "statistics":statistics,
            "fitting":fitting        
        }
        if history:
            self._data_widget = datawidget.DataWidget(None, 
                                      title=None, 
                                      name=[self._name],
                                      header=True,
                                      max=size_history,
                                      **data_widget_opts)
        else:
            self._data_widget = None
        self._frame = expander._DataWidgetExpander(self,name=label_name)

        self.update_signal.connect(self.update_data)
        self.update_signal_chart.connect(self.update_chart) 

    def get_label_component(self):
        return self._label_name

    def get_main_component(self):
        return [self._enum_value_set]

    def set_minimum_width_name(self,width):
        """
        Set minimum text label name width.
        
        :param width: Minimum width of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def show_history(self,e):
        """
        Show/hide chart component.
        """
        if self._frame.isVisible():
            self._frame.hide()            
        else:
            self._frame.show()   
            if self._data_widget is not None:
                self._data_widget.update_all_components()

    def set_font_size(self, size=8):
        """
        Change component font size.
        
        :param size: Size font (default=8)
        :type size: int, optional
        """
        pass

    def dark_(self):
        """
        Set dark theme.
        """        
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._enum_value_set.setStyleSheet("border:1px solid "+Colors.STR_COLOR_L3BLACK+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._list_value_set.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.dark_()
        
    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._enum_value_set.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT6GRAY+";background-color:"+Colors.STR_COLOR_WHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_value_set.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.light_()

    def set_data(self, data, cycle=None):
        """
        Set data in a plot item.
        
        :param data: Data input
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param cycle: Cycle name (information).
        :type cycle: str, optional
        """
        self.data = data
        try:
            self.data_type = type(data)
        except:
            self.data_type = 'int'
        if self.data == []:
            self.data = 0
        else:
            try:
                if type(self.data) is tuple:
                    self.data = self.data[0]
                elif type(self.data) is list:
                    if type(self.data[0]) is list:
                        self.data = self.data[0][0]
                    elif type(self.data[0]) is tuple:
                        valsum = 0                
                        for val in self.data:
                            valsum = valsum + val[0]
                        self.data = valsum
                    else:
                        self.data = self.data[0]            
            except:
                pass
        self.cycle = cycle
        self.update_signal.emit()   
        self.update_signal_chart.emit()           

    @pyqtSlot()
    def update_chart(self):
        """
        Update chart component.
        """
        if self._data_widget is not None:
            self._data_widget.set_data(self.data, cycle=self.cycle,name=self._name, name_parent=self._name)

    @pyqtSlot()
    def update_data(self):
        """
        Update data.
        """ 
        if type(self.data) is tuple:
            self._enum_value_set.setCurrentText(self._labels[str(self.data[0])])
        else:
            self._enum_value_set.setCurrentText(self._labels[str(self.data)])

    def get_data(self):
        """
        Get data.
        
        :return: Data value
        :rtype: int
        """
        data = None        
        label = self._enum_value_set.currentText()
        for key, value in self._labels.items():
            if value == label:
                data = int(formatting.type_value(key,self.data_type))
        return data
        
    def changeValue(self):
        """
        Trigger signal action.
        """
        if self._signal is not None:
            self._signal.emit()

    def addSignal(self,signal):
        """
        Add pyqtsignal to be triggered if data changed.
        
        :param signal: Input signal.
        :type signal: PyQtSignal
        """
        self._signal = signal   

    def connect_to_signal(self,f):
        self._signal.connect(f)

    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression

    def get_signal(self):
        return self._signal


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """Init user interface"""
        central_widget = QWidget() 

        scroll = QScrollArea()        
        scroll.setWidgetResizable(True)        
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)

        labels =  {
            "1":"A",
            "5":"B",
            "6":"C",
            } 
        for i in range(1):
            self.number_widget = EnumPanelWidget(self,title=None,label_name="name",labels=labels)       
            self.layout.addWidget(self.number_widget,i,0)
            self.number_widget.set_data(5)

            if i!=0:
                self.number_widget.light_()
            else:
                self.number_widget.dark_()

        self.setCentralWidget(scroll)
        self.resize(400,400)
  
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 
    default_palette = QGuiApplication.palette()
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    app.setPalette(darkpalette)

    qssblack = """
               QMenuBar::item {
                   spacing: 2px;           
                   padding: 2px 10px;
                   background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
               }
               QMenuBar::item:selected {    
                   background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
               }
               QMenuBar::item:pressed {
                   background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
               }              
               QScrollArea {
                   background-color: transparent;
               }
               QCheckBox {
                   max-width:14;
                   max-height:14;
                   color: """ + Colors.STR_COLOR_WHITE + """;
                   background-color: """ + Colors.STR_COLOR_L3BLACK + """;
               }
               QCheckBox::indicator:checked {
                   max-width:14;
                   max-height:14;
                   border:0px solid """ + Colors.STR_COLOR_L1BLACK + """;
                   background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
               }       

               QLineEdit {
                   background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                   color:""" + Colors.STR_COLOR_WHITE + """;
                   text-align:left;
                   padding-left:2px;
                   padding-top:2px;
                   padding-bottom:2px;
                   border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
               }
           """

    qss = """
               QScrollArea {
                   background-color: transparent;
               }
               QMenuBar::item {
                   spacing: 2px;           
                   padding: 2px 10px;
                   background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
               }
               QMenuBar::item:selected {    
                   background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
               }
               QMenuBar::item:pressed {
                   background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
               }
               QCheckBox::indicator {
                   border:0px solid """ + Colors.STR_COLOR_LIGHT6GRAY + """;            
               }       
               QCheckBox::indicator:checked {
                   max-width:14;
                   max-height:14;
                   border:0px solid """ + Colors.STR_COLOR_LIGHT6GRAY + """;
                   background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
               }             
               QCheckBox {
                   max-width:14;
                   max-height:14;
                   color: """ + Colors.STR_COLOR_BLACK + """;
                   background-color: """ + Colors.STR_COLOR_WHITE + """;            
               }
               QLineEdit {
                   color:""" + Colors.STR_COLOR_BLACK + """;
                   text-align:left;
                   padding-left:2px;
                   border: 0px solid """ + Colors.STR_COLOR_RED + """;
                   background-color:""" + Colors.STR_COLOR_WHITE + """;
               }
           """
    dark = False
    # if dark:
    #     app.setStyleSheet(qssblack)
    # else:
    #     app.setStyleSheet(qss)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())