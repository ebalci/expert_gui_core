from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.widgets.common import expander
from expert_gui_core.gui.widgets.common import spinboxwidget
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.tools import formatting
from expert_gui_core.gui.widgets.pyqt import datawidget


class NumberPanelWidget(QWidget):

    """    
    Low-level boolean widget setting/displaying (only) float/int/double data.Ka

    :param parent: Parent object
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param label_unit: Label unit (default="").
    :type label_unit: str, optional    
    :param min_value: Not used.
    :type min_value: float, optional
    :param max_value: Not used.
    :type max_value: float, optional    
    :param edit: Edit or not (not used, default=True).
    :type edit: bool, optional
    :param unit: Unit (default=True).
    :type unit: bool, optional
    :param temp: Show or not thermometer.
    :type temp: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional
    :param size_history: Size history (default=600)
    :type size_history: int, optional
    :param precision: Not used.
    :type precision: str, optional
    :param type_format: Type data to be shown ("str" standard value format, "sci" scientifc format .8e, "hex" hexadecimal format, "bin" binary format, "date" date format, "fxp131" format signed float 31 bit, default="str").
    :type type_format: str, optional
    :param display_format: Format display data (default="5.5").
    :type display_format: str, optional
    :param statistics: Statistics option (default=False).
    :type statistics: bool, optional
    :param fitting: Fitting option (default=False).
    :type fitting: bool, optional    
    """
    
    update_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    valueChanged = pyqtSignal(object)
    _signal = pyqtSignal()

    def __init__(self, parent, title=None, name=True, label_name="", label_unit="", min_value=None,
                    max_value=None, edit=True, unit=True, temp=False, history=True, size_history=600,
                    precision=-1, type_format="str", display_format="5.5", statistics=False, 
                    fitting=False, function_expression=None):
        super(QWidget, self).__init__(parent)        
        
        qta.icon("fa5.clipboard")

        self.default_palette = QGuiApplication.palette()
        
        self._contextMenu = None

        self._edit = edit
        self._type_format = type_format
        self._min = min_value
        self._max = max_value
        self._precision = precision
        self._name = label_name

        self.data = 0
        self.data_type = None
        self.cycle = None
        self.time_data = 0

        self._function_expression = function_expression
        
        maxheight = 32

        self.font_text = QFont()
        self.font_textbig = QFont()
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")
        
        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        self.mainlayout.setContentsMargins(0,0,0,0)
        self.mainlayout.setSpacing(0) 

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)                

        self.mainlayout.addWidget(self._main_widget,0,0)
        
        self._label_title = QLabel(title)
        self._label_name = QLabel(label_name+" ")
        self._name = label_name
        self._textedit_value = QLineEdit("")
        self._label_unit = QLabel(label_unit)
        
        self._label_type = QPushButton()
        self._label_type.setFont(self.font_fa)
        self._label_type.setText(fa.icons["circle"])
        self._label_type.setMaximumWidth(20)
        self._label_type.setStyleSheet("padding:0;margin:0;border:none;color:transparent;")

        self._te_value = "" 

        spin_opts = {
            # 'min_val': 0,
            # 'max_val': 1000,
            'display_format': display_format,
            'fixed_format': False,
            'shift_arrows': False
        }

        self._spinbox = spinboxwidget.SpinboxWidget(self, **spin_opts)   
        self._spinbox.hide()

        self._label_jauge_normal = QLabel(fa.icons["thermometer-half"])
        self._label_jauge_alert_full = QLabel(fa.icons["thermometer-full"])       
        self._label_jauge_alert_empty = QLabel(fa.icons["thermometer-empty"])       
        self._label_jauge_none = QLabel(fa.icons["thermometer-half"])

        self._history_button = QPushButton(fa.icons["chart-line"])

        self.font = self._label_title.font()        
        self._label_title.setAlignment(Qt.AlignCenter)        
        
        if title is not None:
            self.layout.addWidget(self._label_title,0,0,1,6)
            maxheight = maxheight + 18

        self.setMinimumHeight(maxheight)
        
        if history == True:
            self.layout.addWidget(self._history_button,1,0)        
            self._history_button.setFont(self.font_fa)
            self._history_button.setMaximumWidth(20)
            self._history_button.mousePressEvent = self.show_history

        if name == True and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name,1,1)
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.layout.addWidget(self._textedit_value,1,2)
        self._textedit_value.installEventFilter(self)
        self.layout.addWidget(self._spinbox,1,2)        
        self._textedit_value.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        if temp:
            self._label_jauge_normal.setStyleSheet("color:rgb(0,210,0);")
            self._label_jauge_alert_full.setStyleSheet("color:rgb(255,60,60);")
            self._label_jauge_alert_empty.setStyleSheet("color:rgb(255,60,60);")
            self._label_jauge_none.setStyleSheet("color:lightgray;")        
            self.layout.addWidget(self._label_jauge_none,1,3)
            self.layout.addWidget(self._label_jauge_alert_full,1,3)
            self.layout.addWidget(self._label_jauge_normal,1,3)
            self.layout.addWidget(self._label_jauge_alert_empty,1,3)
            self._label_jauge_normal.hide()
            self._label_jauge_alert_full.hide()
            self._label_jauge_alert_empty.hide()

        if unit == True and label_unit != "":
            self.layout.addWidget(self._label_unit,1,5)
        self._label_unit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        
        self.layout.addWidget(self._label_type,1,6)
        self.layout.setColumnStretch(6,0)
        
        if edit == True:
            self.layout.setColumnStretch(2,1)

        if type_format == "spi":
            self._spinbox.show()
            self._textedit_value.hide()

        data_widget_opts = {
            "auto":True, 
            "show_chart":True,
            "show_table":True,
            "show_scalar":True,
            "history":True,
            "show_chart_value_axis":True,
            "statistics":statistics,
            "fitting":fitting
        }

        if history:
            self._data_widget = datawidget.DataWidget(None, 
                                      title=None, 
                                      name=[self._name],
                                      header=True,
                                      max=size_history,
                                      **data_widget_opts)
        else:
            self._data_widget = None
                                      
        self._frame = expander._DataWidgetExpander(self,name=label_name)

        self.update_signal.connect(self.update_data)
        self._spinbox.set_handler(self.update_data_spin)
        self.update_signal_chart.connect(self.update_chart)   

        self.set_led_type(self._type_format)

    def get_label_component(self):
        return self._label_name

    def get_main_component(self):
        return [self._textedit_value, self._spinbox]

    def set_minimum_width_name(self,width):
        """
        Set minimum text label name width.
        
        :param width: Minimum width of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def show_history(self,e):
        """
        Show/hide chart component.
        """
        if self._frame.isVisible():
            self._frame.hide()            
        else:
            self._frame.show()         
            if self._data_widget is not None:
                self._data_widget.update_all_components()                           

    def set_font_size(self, size=8):
        """
        Change component font size.
        
        :param size: Size font (default=8)
        :type size: int, optional
        """
        self.font_fa.setPointSize(size-1) 
        self._label_jauge_normal.setFont(self.font_fa)
        self._label_jauge_alert_full.setFont(self.font_fa)
        self._label_jauge_alert_empty.setFont(self.font_fa)
        self._label_jauge_none.setFont(self.font_fa)
        
    def contextMenuEvent(self, event):
        """
        Click right menu options to format the string.
        """
        self._contextMenu = QMenu(self)

        self.l_str = QLabel('None')
        self.l_str.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_str.setAlignment(Qt.AlignLeft)
        strAct = QWidgetAction(self._contextMenu)
        strAct.setDefaultWidget(self.l_str)
        self._contextMenu.addAction(strAct)

        self.l_spi = QLabel('Spin')
        self.l_spi.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_spi.setAlignment(Qt.AlignLeft)
        spinAct = QWidgetAction(self._contextMenu)
        spinAct.setDefaultWidget(self.l_spi)
        self._contextMenu.addAction(spinAct)

        self.l_sci = QLabel('Scientific')
        self.l_sci.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_sci.setAlignment(Qt.AlignLeft)
        sciAct = QWidgetAction(self._contextMenu)
        sciAct.setDefaultWidget(self.l_sci)
        self._contextMenu.addAction(sciAct)
        
        self.l_hex = QLabel('HexaDecimal')
        self.l_hex.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_hex.setAlignment(Qt.AlignLeft)
        hexAct = QWidgetAction(self._contextMenu)
        hexAct.setDefaultWidget(self.l_hex)
        self._contextMenu.addAction(hexAct)

        self.l_bin = QLabel('Binary')
        self.l_bin.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_bin.setAlignment(Qt.AlignLeft)
        binAct = QWidgetAction(self._contextMenu)
        binAct.setDefaultWidget(self.l_bin)
        self._contextMenu.addAction(binAct)

        self.l_date = QLabel('Time')
        self.l_date.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_date.setAlignment(Qt.AlignLeft)
        dateAct = QWidgetAction(self._contextMenu)
        dateAct.setDefaultWidget(self.l_date)
        self._contextMenu.addAction(dateAct)

        self.l_fxp = QLabel('fxp131')
        self.l_fxp.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_fxp.setAlignment(Qt.AlignLeft)
        fxp131Act = QWidgetAction(self._contextMenu)
        fxp131Act.setDefaultWidget(self.l_fxp)
        self._contextMenu.addAction(fxp131Act)

        self.l_fun = QLabel('function')
        self.l_fun.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.l_fun.setAlignment(Qt.AlignLeft)
        funAct = QWidgetAction(self._contextMenu)
        funAct.setDefaultWidget(self.l_fun)
        self._contextMenu.addAction(funAct)

        self.l_str.setStyleSheet("padding:3 10 3 10;")
        self.l_spi.setStyleSheet("padding:3 10 3 10;")
        self.l_sci.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_BLUE+";")
        self.l_hex.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTGREEN+";")
        self.l_bin.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTRED+";")
        self.l_date.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LPINK+";")
        self.l_fxp.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTORANGE+";")
        self.l_fun.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHT3GRAY+";")

        self.style_menu()

        action = self._contextMenu.exec_(self.mapToGlobal(event.pos()))
        if action == spinAct:
            self._spinbox.show()
            self._textedit_value.hide()
        else:
            self._spinbox.hide()
            self._textedit_value.show()    
        self.update_textedit_value()
        if action == strAct:
            self._type_format = "str"        
        elif action == spinAct:
            self._type_format = "spi"  
        elif action == sciAct:
            self._type_format = "sci"
        elif action == hexAct:
            self._type_format = "hex"
        elif action == binAct:
            self._type_format = "bin"
        elif action == dateAct:
            self._type_format = "date"            
        elif action == fxp131Act:
            self._type_format = "fxp131"            
        elif action == funAct:
            self._type_format = "fun"            
            expression = self._function_expression
            if expression is None:
                expression = "x"
            text, ok = QInputDialog.getMultiLineText(self, 'Function Editor', ' Only use x as variable\n ex:\n np.array(x,dtype=np.int32)\n x*2.3+52\n x != xprev\n cycle == "CPS.USER.TOF"\n', expression)
            if ok:
                self._function_expression = text
        self.update_signal.emit()     

    def set_led_type(self, type):
        """
        Set led color per type.

        :param type: Change format data display.
        :type type: str
        """        
        if self._type_format == "str":
            self._label_type.setStyleSheet("border:0;color:transparent;")    
        elif self._type_format == "spi":
            self._label_type.setStyleSheet("border:0;color:transparent;")    
        elif self._type_format == "sci":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_BLUE+";")
        elif self._type_format == "hex":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_LIGHTGREEN+";")
        elif self._type_format == "bin":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_LIGHTRED+";")
        elif self._type_format == "date":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_LPINK+";")
        elif self._type_format == "fxp131":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_LIGHTORANGE+";")
        elif self._type_format == "fun":
            self._label_type.setStyleSheet("border:0;color:"+Colors.STR_COLOR_LIGHT3GRAY+";")

    def style_menu(self):
        """
        Change style menu.
        """
        if self._contextMenu is not None:
            self.l_str.setStyleSheet("padding:3 10 3 10;")
            self.l_spi.setStyleSheet("padding:3 10 3 10;")
            self.l_sci.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_BLUE+";")
            self.l_hex.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTGREEN+";")
            self.l_bin.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTRED+";")
            self.l_date.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LPINK+";")
            self.l_fxp.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHTORANGE+";")
            self.l_fun.setStyleSheet("padding:3 10 3 10;color:"+Colors.STR_COLOR_LIGHT3GRAY+";")

    def dark_(self):
        """
        Set dark theme.
        """        
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        self._textedit_value.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:left;padding-left:2px;border:1px solid "+Colors.STR_COLOR_L3BLACK+";")
        self._label_unit.setStyleSheet("color:"+Colors.STR_COLOR_LIGHT3GRAY+";")
        self.setStyleSheet("QInputDialog {background-color:"+Colors.STR_COLOR_LBLACK+";}")
        self._spinbox.dark_()
        if self._data_widget is not None:
            self._data_widget.dark_()
        self.style_menu()
        
    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        self._textedit_value.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";text-align:left;border:1px solid "+Colors.STR_COLOR_LIGHT6GRAY+";")
        self._label_unit.setStyleSheet("color:"+Colors.STR_COLOR_LIGHT2GRAY+";")
        self.setStyleSheet("QInputDialog {background-color:"+Colors.STR_COLOR_LIGHT5GRAY+";}")
        self._spinbox.light_()
        if self._data_widget is not None:
            self._data_widget.light_()
        self.style_menu()
    
    def set_data(self, data,cycle=None,time_data=0,doevent=True):
        """
        Set data in a plot item.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param cycle: Cycle name (information).
        :type cycle: str, optional
        :param time_data: not used.
        :type time_data: int, optional
        :param doevent: Internal flag to update widget.
        :type doevent:bool, optional
        """
        if self._type_format == "fun" and self._function_expression != None: 
            try:
                xprev = self.data
                x = data                
                functions = self._function_expression.split("\n")                
                for function in functions:
                    xold = x
                    x = eval(function)
                    try:
                        if type(x) == type(True):
                            if x == False:
                                x = None
                            elif x == True:
                                x = xold
                            if x is None:
                                return
                    except:
                        pass
                data=x
                if x is None:
                    return
            except Exception as e2:
                self._function_expression = None
        if data is None:
            return
        self.data = data        
        try:
            self.data_type = type(data)        
        except:
            self.data_type = 'float'
        self.cycle = cycle
        self.time_data = time_data
        if doevent:
            self.update_signal.emit()       
            self.update_signal_chart.emit()         

    @pyqtSlot()
    def update_chart(self):
        """
        Update chart function.
        """
        if self._data_widget is not None:
            self._data_widget.set_data(self.data,cycle=self.cycle,time_value=self.time_data,name=self._name, name_parent=self._name)      
    
    @pyqtSlot()
    def update_data(self):
        """
        Update data.
        """
        self._te_value = formatting.format_value_to_string(self.data,type_format=self._type_format)
        self._textedit_value.setText(self._te_value)        
        self.set_led_type(self._type_format)
        self._spinbox.set_data(self.data)
        
    @pyqtSlot()
    def update_data_spin(self):
        """
        Update data.
        """
        self.set_data(self.get_data(),doevent=False)
        if self._signal is not None:
            self._signal.emit()

    def update_textedit_value(self):
        """
        Update the text edit field if the value is requested or the type format change.
        """
        if self._type_format == "spi":
            data = self._spinbox.get_data()
        else:
            data = self._textedit_value.text()
            if self._type_format != "str" and self._type_format != "fun":
                data = formatting.format_value_to_string(data,"i"+self._type_format)
        self.data = formatting.type_value(data,self.data_type)
        if self._type_format == "spi":
            self._te_value = formatting.format_value_to_string(self.data,type_format="str")
        else:
            self._te_value = formatting.format_value_to_string(self.data,type_format=self._type_format)        
        self._textedit_value.setText(self._te_value)
        
    def get_data(self):
        """
        Get data.
        
        :return: Data value
        :rtype: str
        """
        self.update_textedit_value()        
        data = self._te_value
        if self._type_format == "fxp131":
             data = formatting.format_value_to_string(data,"ifxp131")
        data = formatting.type_value(data,self.data_type)
        return data        

    def changeValue(self):
        """
        If any change is detected then trigger signal.
        """
        if self._signal is not None:
            self._signal.emit()

    def eventFilter(self, widget, event):
        """
        Filter the text written as it should only be digit.
        """
        if event.type() == QEvent.KeyPress and widget is self._textedit_value:
            if event.key() in (Qt.Key_Enter, Qt.Key_Return):
                if self._signal is not None:
                    self.set_data(self.get_data())
                    self._signal.emit()
                return True
            if self._type_format == "str":
                if event.key() not in (Qt.Key_0, Qt.Key_1, Qt.Key_2, Qt.Key_3, Qt.Key_4, Qt.Key_5, Qt.Key_6, Qt.Key_7, Qt.Key_8, Qt.Key_9, Qt.Key_Minus,Qt.Key_Period, 46, 16777219, 16777234, 16777236):
                    return True
            else:
                return False
        return False

    def addSignal(self,signal):
        """
        Add pyqtsignal to be triggered if data changed.
        
        :param signal: Input signal.
        :type signal: PyQtSignal
        """
        self._signal = signal

    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression

    def get_signal(self):
        return self._signal


class Style_(QProxyStyle):
    
    """
    Class styling.

    :param style: not used.
    :type style: str: optional
    """
    
    def __init__(self, style = None):
        super().__init__(style)
        self._colors = dict()

    def setColor(self, text, color):
        """
        Set color.

        :param text: Text to colorize.
        :type text: str
        :param color: Color to use.
        :type color: QColor
        """
        self._colors[text] = color

    def drawControl(self, element, option, painter, widget):
        if element == QStyle.CE_MenuItem:
            text = option.text
            option_ = QStyleOptionMenuItem(option)
            if text in self._colors:
                color = self._colors[text]
            else:
                color = option_.palette.color(QPalette.Text) 
            option_.palette.setColor(QPalette.Text, color)
            return self.baseStyle().drawControl(element, option_, painter, widget)
        return self.baseStyle().drawControl(element, option, painter, widget)


class MenuStyler_():

    """
    Class menu styler

    :param menu: Menu to change style
    :type menu: QMenuItem
    """

    def __init__(self, menu):
        style = Style_()
        style.setBaseStyle(menu.style())
        menu.setStyle(style)
        self._style = style
        self._menu = menu

    def setColor(self, key, color):
        """
        Set color.

        :param key: Specific QProxyStyle key to change.
        :type key: int or str (convert to str anyway)
        """
        if isinstance(key, str):
            self._style.setColor(key, color)
        elif isinstance(key, int):
            text = self._menu.actions()[key].text()
            self._style.setColor(text, color)
        else:
            raise ValueError("Key must be either int or string")


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """Init user interface"""
        central_widget = QWidget() 

        central_widget.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_WHITE+";")

        scroll = QScrollArea()        
        scroll.setWidgetResizable(True)        
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)

        for i in range(8):
            self.number_widget = NumberPanelWidget(self,title=None,label_name="name",label_unit="mV",type_format="spi",statistics=True,display_format="5.0")
            self.number_widget._label_name.setMinimumWidth(100)
            self.layout.addWidget(self.number_widget,i,0)
            # self.number_widget.set_data(int(2700*i))
            # self.number_widget.get_data()
            # if i<=1:
            self.number_widget.dark_()
            # else:
            #     self.number_widget.light_()
            
        push = QPushButton(self)
        # self.layout.addWidget(push,1,0)
        push.mousePressEvent = self.getdata

        self.setCentralWidget(scroll)
        self.resize(400,400)

    def getdata(self,e):          
        val = self.number_widget.get_data()
        
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 

    # default_palette = QGuiApplication.palette()
    # darkpalette = QPalette()
    # darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    # darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    # darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    # darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    # darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    # darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    # app.setPalette(darkpalette)

    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())