from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt import bitenumwidget
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.common import expander
from expert_gui_core.tools import formatting


class BitEnumPanelWidget(QWidget):

    """    
    Low-level bitenum widget setting/displaying bitenum.
    
    :param parent: Parent object
    :type parent: object
    :param title: Title shown top center (default=None).
    :type title: str
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param labels: List of labels to be used in bit enum.
    :type labels: list, optional
    :param edit: Edit or not (not used, default=True).
    :type edit: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional
    :param size_history: Size history (default=600)
    :type size_history: int, optional
    :param precision: Not used.
    :type precision: str, optional
    :param type_format: Type data to be shown ("bit" bit enum value, "str" standard value format, "sci" scientifc format .8e, "hex" hexadecimal format, "bin" binary format, "date" date format, default="bit").
    :type type_format: str, optional
    :param size_bit: Size bit enum (8 16 32 64), default=16.
    :type size_bit: int, optional
    :param statistics: Statistics option (default=False).
    :type statistics: bool, optional
    :param fitting: Fitting option (default=False).
    :type fitting: bool, optional    
    """

    update_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    _signal = pyqtSignal()

    def __init__(self, parent, title=None, name=True, label_name="", labels=None, edit=True, history=True, size_history=600, precision=-1, type_format="bit",
                        size_bit=16,statistics=False,fitting=False,border=True):
        super(QWidget, self).__init__(parent)        
        
        qta.icon("fa5.clipboard")

        self._edit = edit
        self.default_palette = QGuiApplication.palette()
        self.type_format = type_format

        self.data = 0
        self.data_type = None
        self.cycle = None
        self._function_expression = None

        maxheight = 52
        
        self.font_text = QFont()        
        self.font_textbig = QFont()        
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        self.mainlayout.setContentsMargins(0,0,0,0)
        self.mainlayout.setSpacing(0) 

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)                

        self.mainlayout.addWidget(self._main_widget,0,0)
        
        self._label_title = QLabel(title)
        self._label_name = QLabel(label_name+" ")
        self._name = label_name
        
        self._bit_value = bitenumwidget.BitEnumWidget(size_bit=size_bit,labels=labels,enabled=edit,border=border)
        
        self._label_status = QLabel("")
        self._label_status.hide()
        
        self._history_button = QPushButton(fa.icons["chart-line"])

        self.font = self._label_title.font()        
        self._label_title.setAlignment(Qt.AlignCenter)        
        
        if title is not None:
            self.layout.addWidget(self._label_title,0,0,1,5)
            maxheight = maxheight + 18

        self.setMinimumHeight(maxheight)
        
        if history == True:
            self.layout.addWidget(self._history_button,1,0)
            self._history_button.setFont(self.font_fa)
            self._history_button.setMaximumWidth(20)
            self._history_button.mousePressEvent = self.show_history

        if name == True and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name,1,1)   
        
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        
        self.layout.addWidget(self._bit_value,1,2)
        self.layout.addWidget(self._label_status,1,3)    

        if edit == True:            
            self.layout.setColumnStretch(2,1)

        self.layout.setColumnStretch(4,1)
        
        data_widget_opts = {
            "auto":True, 
            "show_chart":True,
            "show_table":True,
            "history":True,
            "show_chart_value_axis":True,
            "statistics":statistics,
            "fitting":fitting
        }
        if history:
            self._data_widget = datawidget.DataWidget(None, 
                                      title=None, 
                                      name=[self._name],
                                      header=True,
                                      max=size_history,
                                      **data_widget_opts)
        else:
            self._data_widget = None
        self._frame = expander._DataWidgetExpander(self,name=label_name)

        self.update_signal.connect(self.update_data)
        self.update_signal_chart.connect(self.update_chart)

    def get_label_component(self):
        return [self._label_name]

    def get_main_component(self):
        return [self._bit_value]

    def set_handler(self, handler, key_handler):
        """
        Set handler pushbutton
        :param handler:
        :param key_handler:
        :return:
        """
        self._bit_value.set_handler(handler, key_handler)

    @pyqtSlot()
    def update_data_bitenum(self):
        """
        Update data.
        """
        self.set_data(self.get_data(), doevent=False)
        if self._signal is not None:
            self._signal.emit()

    def set_minimum_width_name(self,width):
        """
        Set minimum text label name width.
        
        :param width: Minimum width of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def show_history(self,e):
        """
        Show/hide chart component.
        """
        if self._frame.isVisible():
            self._frame.hide()            
        else:
            self._frame.show()   
            if self._data_widget is not None:
                self._data_widget.update_all_components()

    def set_font_size(self, size=8):
        """
        Change component font size.
        
        :param size: Size font (default=8)
        :type size: int, optional
        """
        pass 
        
    def contextMenuEvent(self, event):
        """
        Menu event.
        """
        contextMenu = QMenu(self)
        bitAct = contextMenu.addAction("BitEnum")
        strAct = contextMenu.addAction("None")
        sciAct = contextMenu.addAction("Scientific")
        hexAct = contextMenu.addAction("HexaDecimal")
        binAct = contextMenu.addAction("Binary")
        dateAct = contextMenu.addAction("Time")
        action = contextMenu.exec_(self.mapToGlobal(event.pos()))
        if action == bitAct:
            self.type_format = "bit"
            self._label_status.hide()
            self._bit_value.show()            
        else:
            self._label_status.show()
            self._bit_value.hide()
            if action == strAct:
                self.type_format = "str"            
            elif action == sciAct:
                self.type_format = "sci"
            elif action == hexAct:
                self.type_format = "hex"
            elif action == binAct:
                self.type_format = "bin"
            elif action == dateAct:
                self.type_format = "date"
            self.update_label_status()
    
    def update_label_status(self):
        """
        Update label status shown with menu.
        """
        self._label_status.setText(formatting.format_value_to_string(self.data,type_format=self.type_format))

    def set_cell_colors(self, index, vallight, valdark):
        """
        Set cell colors.
        """
        self._bit_value.set_cell_colors(index, vallight, valdark)

    def set_bitenum_bg_colors(self, bglight=None, bgdark=None):
        """
        Set bitenum bg colors.
        """
        self._bit_value.sel_bgColors(bglight=bglight, bgdark=bgdark)

    def dark_(self):
        """
        Set dark theme.
        """        
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._bit_value.dark_()
        self._label_status.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.dark_()
        
    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+";")
        self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLUE+";font-weight:bold;")
        self._bit_value.light_()
        self._label_status.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.light_()
    
    def set_data(self, data,cycle=None):
        """
        Set data in a plot item.
        
        :param data: Data input
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param cycle: Cycle name (information).
        :type cycle: str, optional
        """
        self.data = data
        try:
            self.data_type = type(data)
        except:
            self.data_type = 'int'
        if self.data == []:
            self.data = 0
        else:
            try:
                if type(self.data) is tuple:
                    self.data = self.data[0]
                elif type(self.data) is list:
                    if type(self.data[0]) is list:
                        self.data = self.data[0][0]
                    elif type(self.data[0]) is tuple:
                        valsum = 0                
                        for val in self.data:
                            valsum = valsum + val[0]
                        self.data = valsum
                    else:
                        self.data = self.data[0]            
            except:
                pass
        self.cycle = cycle
        self.update_signal.emit()    
        self.update_signal_chart.emit()           

    @pyqtSlot()
    def update_chart(self):
        """
        Update chart component.
        """
        if self._data_widget is not None:
            self._data_widget.set_data(self.data,cycle=self.cycle,name=self._name, name_parent=self._name)

    @pyqtSlot()
    def update_data(self):
        """
        Update component bitenum.
        """
        self._bit_value.set_data(self.data) 
        self.update_label_status()

    def get_data(self):
        """
        Get data.
        
        :return: Data value
        :rtype: int
        """
        data = formatting.type_value(self._bit_value.get_data(),self.data_type)
        return data

    def changeValue(self):
        """
        Change data event.
        """
        if self._signal is not None:
            self._signal.emit()

    def addSignal(self,signal):
        """
        Add pyqtsignal to be triggered if data changed.
        
        :param signal: Input signal.
        :type signal: PyQtSignal
        """
        self._signal = signal
        
    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression

    def get_signal(self):
        return self._signal
        
class _Example(QMainWindow):

    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """Init UI"""
        central_widget = QWidget() 

        scroll = QScrollArea()        
        scroll.setWidgetResizable(True)        
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)

        labels_global_status = {
            "General power fail",
            "Or 8X power fail",
            "Or 8X step busy",
            "Or 8X ADC busy",
            "Presence ext. clock",
            "Presence ext. trig",
            "Presence cable con64",
            "Presence cable P2_0",
            "Presence cable P2_1"
        }
        labels = {}
        ind=0
        for lab in labels_global_status:
            labels[str(ind)] = str(lab)
            ind=ind+1
        for i in range(6):
            self.number_widget = BitEnumPanelWidget(self,title=None,label_name="name",size_bit=16,labels=labels)       
            self.number_widget.setMaximumWidth(560)
            self.number_widget.setMaximumHeight(30)
            self.layout.addWidget(self.number_widget,i,0)
            self.number_widget.set_data(9999)
            # if i != 2:
            #     self.number_widget.light_()
            # else:
            self.number_widget.dark_()

        self.setCentralWidget(scroll)
        self.resize(300,400)
        
        
if __name__ == '__main__':
    app = QApplication(sys.argv)

    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 

    default_palette = QGuiApplication.palette()
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    app.setPalette(darkpalette)

    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())