from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import fontawesome as fa
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import expander
from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.tools import formatting


class StringPanelWidget(QWidget):

    """    
    Low-level string widget setting/displaying (only) string.

    :param parent: Parent object
    :type parent: object
    :param title: Title (top center).
    :type title: str, optional
    :type name: bool, optional
    :param name: Show or not label name (default=True).
    :type name: bool, optional
    :param label_name: Label name (default="").
    :type label_name: str, optional
    :param edit: Edit or not (not used, default=True).
    :type edit: bool, optional
    :param history: Keep dataq history in datawidget component (default=False)
    :type history: bool, optional    
    """
    
    update_signal = pyqtSignal()
    update_signal_chart = pyqtSignal()
    _signal = pyqtSignal()

    def __init__(self, parent, title=None, name=True, label_name="", edit=True, history=True,temp=False,align=Qt.AlignLeft):
        super(QWidget, self).__init__(parent)        
        
        qta.icon("fa5.clipboard")

        self._name = label_name
        self._edit = edit
        self.default_palette = QGuiApplication.palette()
        self.data = ""
        self.data_type=None
        self.cycle = None
        self._function_expression = None

        maxheight = 30
        
        self.font_text = QFont()
        self.font_textbig = QFont()
        self.font_fa = QFont()
        self.font_fa.setFamily("FontAwesome")

        self._main_widget = QWidget()
        self.mainlayout = QGridLayout(self)

        self.mainlayout.setContentsMargins(0,0,0,0)
        self.mainlayout.setSpacing(0) 

        self.layout = QGridLayout(self._main_widget)
        self.layout.setContentsMargins(3, 2, 3, 2)
        self.layout.setSpacing(3)                

        self.mainlayout.addWidget(self._main_widget,0,0)
        
        self._label_title = QLabel(title)
        self._label_name = QLabel(label_name+" ")
        self._textedit_value = QLineEdit("")
        self._label_jauge_normal = QLabel(fa.icons["thermometer-half"])       
        self._label_jauge_alert_full = QLabel(fa.icons["thermometer-full"])       
        self._label_jauge_alert_empty = QLabel(fa.icons["thermometer-empty"])       
        self._label_jauge_none = QLabel(fa.icons["thermometer-half"])       
        self._history_button = QPushButton(fa.icons["list"])

        self.font = self._label_title.font()        
        self._label_title.setAlignment(Qt.AlignCenter)        
        self._label_title.setStyleSheet("color:rgb(84,190,230);font-weight:bold;")
        
        if title is not None:
            self.layout.addWidget(self._label_title,0,0,1,5)
            maxheight = maxheight + 18

        self.setMinimumHeight(maxheight)

        if history == True:
            self.layout.addWidget(self._history_button,1,0)
            self._history_button.setFont(self.font_fa)
            self._history_button.setMaximumWidth(20)
            self._history_button.mousePressEvent = self.show_history
        
        if name == True and label_name != "":
            self.layout.setContentsMargins(3, 2, 3, 2)
            self.layout.addWidget(self._label_name,1,1)
        self._label_name.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        
        edit=True
        if edit == True:
            self.layout.addWidget(self._textedit_value,1,2)
            self._textedit_value.installEventFilter(self)
        self._textedit_value.setAlignment(align | Qt.AlignVCenter)

        self._label_jauge_normal.setStyleSheet("color:rgb(0,210,0);")
        self._label_jauge_alert_full.setStyleSheet("color:rgb(255,60,60);")
        self._label_jauge_alert_empty.setStyleSheet("color:rgb(255,60,60);")
        self._label_jauge_none.setStyleSheet("color:lightgray;")        
        if temp:
            self.layout.addWidget(self._label_jauge_none,1,3)
            self.layout.addWidget(self._label_jauge_alert_full,1,3)
            self.layout.addWidget(self._label_jauge_normal,1,3)
            self.layout.addWidget(self._label_jauge_alert_empty,1,3)
        self._label_jauge_normal.hide()
        self._label_jauge_alert_full.hide()
        self._label_jauge_alert_empty.hide()

        if edit == True:
            self.layout.setColumnStretch(2,1)

        data_widget_opts = {
            "auto":True, 
            "show_table":True,
            "history":True            
        }
        if history:
            self._data_widget = datawidget.DataWidget(None, 
                                      title=None, 
                                      name=[self._name],
                                      header=True,
                                      max=100,
                                      **data_widget_opts)
        else:
            self._data_widget = None
        self._frame = expander._DataWidgetExpander(self,name=label_name)
        
        self.update_signal_chart.connect(self.update_chart)  
        self.update_signal.connect(self.update_data)

    def get_label_component(self):
        return self._label_name

    def get_main_component(self):
        return [self._textedit_value]

    def set_minimum_width_name(self,width):
        """
        Set minimum text label name width.
        
        :param width: Minimum width of the label name.
        :type width: int
        """
        self._label_name.setMinimumWidth(width)

    def set_font_size(self, size=8):
        """
        Change component font size.
        
        :param size: Size font (default=8)
        :type size: int, optional
        """
        pass

    def dark_(self):
        """
        Set dark theme.
        """        
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_WHITE+";")
        self._textedit_value.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";text-align:left;border:1px solid "+Colors.STR_COLOR_L3BLACK+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._label_name.setStyleSheet("font-weight:bold;color:"+Colors.STR_COLOR_BLACK+"")
        self._textedit_value.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";text-align:left;border:1px solid "+Colors.STR_COLOR_LIGHT6GRAY+";")
        self._history_button.setStyleSheet("padding-right:5px;border:none;color:"+Colors.STR_COLOR_BLUE+";")
        if self._data_widget is not None:
            self._data_widget.light_()

    def set_data(self,data,cycle=None):
        """
        Set data in a plot item.

        :param data: Data input.
        :type data: expert_gui_core.comm.data.DataObject, optional
        :param cycle: Cycle name (information).
        :type cycle: str, optional
        """
        self.data = data
        try:
            self.data_type = type(data)
        except:
            self.data_type = 'str'
        self.cycle = cycle
        self.update_signal.emit()        
        self.update_signal_chart.emit()             

    def show_history(self,e):
        """Show chart component"""
        if self._frame.isVisible():
            self._frame.hide()            
        else:
            self._frame.show()  
            if self._data_widget is not None:
                self._data_widget.update_all_components()

    @pyqtSlot()
    def update_chart(self):
        """Update chart component"""   
        if self._data_widget is not None:     
            self._data_widget.set_data(self.data, cycle=self.cycle,name=self._name, name_parent=self._name)

    @pyqtSlot()
    def update_data(self):
        """Update text edit value"""
        self._textedit_value.setText(self.data)

    def get_data(self):
        """Get data"""
        data = formatting.type_value(self._textedit_value.text(),self.data_type)
        return data        

    def changeValue(self):
        """Trigger is value changed"""
        if self._signal is not None:
            self._signal.emit()

    def eventFilter(self, widget, event):        
        """Event filtering"""
        if event.type() == QEvent.KeyPress and widget is self._textedit_value:
            if event.key() in (Qt.Key_Enter, Qt.Key_Return):
                if self._signal is not None:
                    self._signal.emit()
                return True         
        return False            
        
    def addSignal(self,signal):
        """Add pyqtsignal to be triggered if data changed"""
        self._signal = signal

    def get_expression(self):
        """
        Get the function expression.

        :return: Function expression.
        :rtype: str
        """
        return self._function_expression

    def get_signal(self):
        return self._signal

class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        """Init user interface"""
        central_widget = QWidget() 

        scroll = QScrollArea()        
        scroll.setWidgetResizable(True)        
        scroll.setWidget(central_widget)

        self.layout = QGridLayout(central_widget)

        for i in range(2):
            self.str_widget = StringPanelWidget(self,title=None,label_name="name")       
            self.layout.addWidget(self.str_widget,i,0)
            self.str_widget.set_data("Value to set")
            if i!=1:
                self.str_widget.light_() 
            else:
                self.str_widget.dark_()        

        self.setCentralWidget(scroll)
        self.resize(400,400)
        
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    qssblack = """
           QMenuBar::item {
               spacing: 2px;           
               padding: 2px 10px;
               background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
           }
           QMenuBar::item:selected {    
               background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
           }
           QMenuBar::item:pressed {
               background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
           }              
           QScrollArea {
               background-color: transparent;
           }
           QCheckBox {
               max-width:14;
               max-height:14;
               color: """ + Colors.STR_COLOR_WHITE + """;
               background-color: """ + Colors.STR_COLOR_L3BLACK + """;
           }
           QCheckBox::indicator:checked {
               max-width:14;
               max-height:14;
               border:0px solid """ + Colors.STR_COLOR_L1BLACK + """;
               background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
           }       

           QLineEdit {
               background-color:""" + Colors.STR_COLOR_L2BLACK + """;
               color:""" + Colors.STR_COLOR_WHITE + """;
               text-align:left;
               padding-left:2px;
               padding-top:2px;
               padding-bottom:2px;
               border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
           }
       """

    qss = """
           QScrollArea {
               background-color: transparent;
           }
           QMenuBar::item {
               spacing: 2px;           
               padding: 2px 10px;
               background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
           }
           QMenuBar::item:selected {    
               background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
           }
           QMenuBar::item:pressed {
               background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
           }
           QCheckBox::indicator {
               border:0px solid """ + Colors.STR_COLOR_LIGHT6GRAY + """;            
           }       
           QCheckBox::indicator:checked {
               max-width:14;
               max-height:14;
               border:0px solid """ + Colors.STR_COLOR_LIGHT6GRAY + """;
               background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
           }             
           QCheckBox {
               max-width:14;
               max-height:14;
               color: """ + Colors.STR_COLOR_BLACK + """;
               background-color: """ + Colors.STR_COLOR_WHITE + """;            
           }
           QLineEdit {
               color:""" + Colors.STR_COLOR_BLACK + """;
               text-align:left;
               padding-left:2px;
               border: 0px solid """ + Colors.STR_COLOR_RED + """;
               background-color:""" + Colors.STR_COLOR_WHITE + """;
           }
       """
    dark=True
    if dark:
        app.setStyleSheet(qssblack)
    else:
        app.setStyleSheet(qss)

    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())
