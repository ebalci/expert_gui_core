from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys
import pyqtgraph as pg
import qtawesome as qta
import pandas as pd
import numpy as np
import random
import threading

from scipy.optimize import curve_fit
from scipy import stats as st
from PIL import Image
import pyqtgraph.functions as fn
import scipy.ndimage as ndi

# import os, pathlib
#
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.comm import fesacomm, simucomm
from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common import togglepanelwidget, headerwidget, titledborderwidget
from expert_gui_core.gui.widgets.pyqt._imageitem import _ImageItem
from expert_gui_core.gui.widgets.pyqt._plotitem import _PlotItem

from expert_gui_core.gui.widgets.pyqt.tablewidget import TableWidget
from expert_gui_core.tools import formatting


def calculate_distance(pos1, pos2):
    return np.sqrt((pos2[0] - pos1[0]) ** 2 + (pos2[1] - pos1[1]) ** 2)


def fo_gauss(x, a, mu, sig, b, c):
    """
    Gauss formula.
    """
    gauss = a * np.exp(-(x - mu) ** 2 / (2 * sig ** 2)) + c
    return gauss


class MyRangeSlider(QSlider):

    sliderMoved = pyqtSignal(int, int)

    def __init__(self, *args):
        super(MyRangeSlider, self).__init__(*args)

        self._low = self.minimum()
        self._high = self.maximum()

        self.setStyleSheet("""
            MyRangeSlider {
                width:25px;
            }
        """)

        self.pressed_control = QStyle.SC_None
        self.tick_interval = 0
        self.tick_position = QSlider.NoTicks
        self.hover_control = QStyle.SC_None
        self.click_offset = 0

        self.active_slider = 0

    def low(self):
        return self._low

    def setLow(self, low: int):
        self._low = low
        self.update()

    def high(self):
        return self._high

    def setHigh(self, high):
        self._high = high
        self.update()

    def paintEvent(self, event):

        painter = QPainter(self)
        style = QApplication.style()

        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        opt.siderValue = 0
        opt.sliderPosition = 0
        opt.subControls = QStyle.SC_SliderGroove
        if self.tickPosition() != self.NoTicks:
            opt.subControls |= QStyle.SC_SliderTickmarks
        style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)
        groove = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderGroove, self)

        self.initStyleOption(opt)
        opt.subControls = QStyle.SC_SliderGroove

        opt.siderValue = 0

        opt.sliderPosition = self._low
        low_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        opt.sliderPosition = self._high

        high_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)
        low_pos = self.__pick(low_rect.center())
        high_pos = self.__pick(high_rect.center())

        min_pos = min(low_pos, high_pos)
        max_pos = max(low_pos, high_pos)

        c = QRect(low_rect.center(), high_rect.center()).center()

        if opt.orientation == Qt.Horizontal:
            span_rect = QRect(QPoint(min_pos, c.y() - 2), QPoint(max_pos, c.y() + 1))
        else:
            span_rect = QRect(QPoint(c.x() - 2, min_pos), QPoint(c.x() + 1, max_pos))

        if opt.orientation == Qt.Horizontal:
            groove.adjust(0, 0, -1, 0)
        else:
            groove.adjust(0, 0, 0, -1)

        if True:
            highlight = self.palette().color(QPalette.Highlight)
            painter.setBrush(QBrush(highlight))
            painter.setPen(QPen(highlight, 0))
            '''
            if opt.orientation == Qt.Horizontal:
                self.setupPainter(painter, opt.orientation, groove.center().x(), groove.top(), groove.center().x(), groove.bottom())
            else:
                self.setupPainter(painter, opt.orientation, groove.left(), groove.center().y(), groove.right(), groove.center().y())
            '''
            painter.drawRect(span_rect.intersected(groove))

        for i, value in enumerate([self._low, self._high]):

            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            if i == 0:
                opt.subControls = QStyle.SC_SliderHandle
            else:
                opt.subControls = QStyle.SC_SliderHandle

            if self.tickPosition() != self.NoTicks:
                opt.subControls |= QStyle.SC_SliderTickmarks

            if self.pressed_control:
                opt.activeSubControls = self.pressed_control
            else:
                opt.activeSubControls = self.hover_control

            opt.sliderPosition = value
            opt.sliderValue = value

            style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)

    def mousePressEvent(self, event):
        event.accept()

        style = QApplication.style()
        button = event.button()

        if button:
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            self.active_slider = -1

            if self._high - self._low <= 2:
                self._high = self._high + 2
            if self._high > self.maximum():
                self._high = self._high - 2
                self._low = self._low - 2

            for i, value in enumerate([self._low, self._high]):
                opt.sliderPosition = value

                hit = style.hitTestComplexControl(style.CC_Slider, opt, event.pos(), self)
                if hit == style.SC_SliderHandle:
                    self.active_slider = i
                    self.pressed_control = hit

                    self.triggerAction(self.SliderMove)
                    self.setRepeatAction(self.SliderNoAction)
                    self.setSliderDown(True)
                    break

            if self.active_slider < 0:
                self.pressed_control = QStyle.SC_SliderHandle
                self.click_offset = self.__pixelPosToRangeValue(self.__pick(event.pos()))
                self.triggerAction(self.SliderMove)
                self.setRepeatAction(self.SliderNoAction)
        else:
            event.ignore()

    def mouseMoveEvent(self, event):
        if self.pressed_control != QStyle.SC_SliderHandle:
            event.ignore()
            return

        event.accept()
        new_pos = self.__pixelPosToRangeValue(self.__pick(event.pos()))
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)

        if self.active_slider < 0:
            offset = new_pos - self.click_offset
            self._high += offset
            self._low += offset
            if self._low < self.minimum():
                diff = self.minimum() - self._low
                self._low += diff
                self._high += diff
            if self._high > self.maximum():
                diff = self.maximum() - self._high
                self._low += diff
                self._high += diff
            if new_pos >= self._high:
                new_pos = self._high - 1
            if new_pos <= self._low:
                new_pos = self._low + 1
        elif self.active_slider == 0:
            if new_pos >= self._high:
                new_pos = self._high - 1
            self._low = new_pos
        else:
            if new_pos <= self._low:
                new_pos = self._low + 1
            self._high = new_pos

        self.click_offset = new_pos

        self.update()

        self.sliderMoved.emit(self._low, self._high)

    def __pick(self, pt):
        if self.orientation() == Qt.Horizontal:
            return pt.x()
        else:
            return pt.y()

    def __pixelPosToRangeValue(self, pos):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        style = QApplication.style()

        gr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderGroove, self)
        sr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderHandle, self)

        if self.orientation() == Qt.Horizontal:
            slider_length = sr.width()
            slider_min = gr.x()
            slider_max = gr.right() - slider_length + 1
        else:
            slider_length = sr.height()
            slider_min = gr.y()
            slider_max = gr.bottom() - slider_length + 1

        return style.sliderValueFromPosition(self.minimum(), self.maximum(),
                                             pos - slider_min, slider_max - slider_min,
                                             opt.upsideDown)

    def light_(self):
        pass

    def dark_(self):
        pass


class MyRawImageWidget(QWidget):

    def __init__(self, parent=None, scaled=False):
        QWidget.__init__(self, parent=None)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        self._scaled = scaled
        self.opts = None
        self.image = None
        self._transpose = False
        self._max = 1
        self._min = 0
        self._pmin = 0.
        self._pmax = 1.
        self._blur = 0
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.right_menu)

    def right_menu(self, pos):

        menu = QMenu()

        transpose_option = menu.addAction('Transpose')
        scaled_option = menu.addAction('Scaled')

        raw_option = menu.addAction('Raw')

        blur_option = menu.addAction('Blur')
        blur2_option = menu.addAction('Blur dim/2')
        blur4_option = menu.addAction('Blur dim/4')

        transpose_option.triggered.connect(self.transpose)
        scaled_option.triggered.connect(self.scaled)
        raw_option.triggered.connect(self.raw)
        blur_option.triggered.connect(self.blur)
        blur2_option.triggered.connect(self.blur2)
        blur4_option.triggered.connect(self.blur4)

        menu.exec_(self.mapToGlobal(pos))

    def transpose(self):

        self._transpose = not self._transpose
        self.image = None
        self.update()

    def scaled(self):

        self._scaled = not self._scaled
        self.image = None
        self.update()

    def setPercent(self, min, max):

        if min >= max:
            self._pmin = 0.
            self._pmax = 1.
        else:
            self._pmax = max / 100.
            self._pmin = min / 100.
        self.image = None
        self.update()

    def blur(self):

        self._blur = 1
        self.image = None
        self.update()

    def blur2(self):

        self._blur = 2
        self.image = None
        self.update()

    def blur4(self):

        self._blur = 4
        self.image = None
        self.update()

    def raw(self):

        self._blur = 0
        self.image = None
        self.update()

    def setImage(self, img, *args, **kargs):

        if self._blur == 1:
            pass
        elif self._blur == 2:
            im = Image.fromarray(img)
            im = im.resize((im.width // 2, im.height // 2), resample=Image.NEAREST)
            img = np.asarray(im).copy()
        elif self._blur == 4:
            im = Image.fromarray(img)
            im = im.resize((im.width // 4, im.height // 4), resample=Image.NEAREST)
            img = np.asarray(im).copy()

        self._max = img.max()
        self._min = img.min()

        self.opts = (img, args, kargs)

        self.image = None

        self.update()

    def paintEvent(self, ev):

        if self.opts is None:
            return

        if self.image is None:

            try:
                if self._blur > 0:
                    if self._blur == 1:
                        data = ndi.uniform_filter(self.opts[0], size=11)
                    elif self._blur == 2:
                        data = ndi.uniform_filter(self.opts[0], size=7)
                    elif self._blur == 4:
                        data = ndi.uniform_filter(self.opts[0], size=5)
                    self._max = data.max()
                    hmed = 5. * np.median(data)
                    lmed = np.median(data) / 10.
                    if hmed < self._max:
                        self._max = hmed
                    self._min = data.min()
                    if lmed > self._min:
                        self._min = lmed / 10.
                else:
                    data = self.opts[0]

                self._level_min = int(self._min + (self._max - self._min) * self._pmin)
                self._level_max = int(self._min + (self._max - self._min) * self._pmax)

                argb, alpha = fn.makeARGB(data, levels=[self._level_min, self._level_max])

            except:
                return

            self.image = fn.makeQImage(argb,
                                       alpha=True,
                                       transpose=self._transpose)

        p = QPainter(self)

        if self._scaled:

            rect = self.rect()
            ar = rect.width() / float(rect.height())
            imar = self.image.width() / float(self.image.height())
            if ar > imar:
                rect.setWidth(int(rect.width() * imar / ar))
            else:
                rect.setHeight(int(rect.height() * ar / imar))

            p.drawImage(rect, self.image)
        else:
            rect = self.rect()
            p.drawImage(rect, self.image)

        p.end()

    def light_(self):
        pass

    def dark_(self):
        pass


class ProfileWidget(QWidget):
    supdate_signal = pyqtSignal()

    supdate_H = pyqtSignal()
    supdate_V = pyqtSignal()
    supdate_fit_H = pyqtSignal()
    supdate_fit_V = pyqtSignal()
    srefresh_plots = pyqtSignal()
    supdate_table = pyqtSignal()
    ssetdata = pyqtSignal(object, float)

    def __init__(self,
                 parent,
                 name="default",
                 positionH=Qt.AlignBottom,
                 positionV=Qt.AlignRight,
                 settings=False,
                 show_info=False,
                 ratio=False,
                 limit_time=20,
                 profile=True,
                 tv=True):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")

        self._parent = parent
        self._name = name

        self._positionH = positionH
        self._positionV = positionV

        self._minsize = 300
        self.show_info = show_info

        self._mutex = False
        self._time_image = 0

        self._tv = tv
        self._profile = profile

        self._led_flag = False

        if not self._tv:
            self._profile = True

        if not self._profile:
            self._tv = True

        self._data = {}

        self._ymax = 0
        self._absolute_scale = False
        self._maxmax = -999999999

        self._update_time = 0
        self.layouts = {}

        self._limit_time = limit_time

        self._old_reduction = 1

        self._active = True

        # layout

        self.setAttribute(Qt.WA_StyledBackground, True)

        self.image_widget = QWidget()

        self.layout = QGridLayout(self.image_widget)
        self.layout.setContentsMargins(5, 5, 5, 5)
        self.layout.setSpacing(0)

        self.layoutmain = QGridLayout(self)
        self.layoutmain.setContentsMargins(5, 5, 5, 5)
        self.layoutmain.setSpacing(0)

        self._tabs = QTabWidget()

        if self._profile:
            if self._tv:
                self.layoutmain.addWidget(self._tabs, 0, 0)
                self._tabs.addTab(self.image_widget, "Image")
            else:
                self.layoutmain.addWidget(self.image_widget, 0, 0)

        # TV
        if self._tv:

            self.tv_widget = QWidget()

            self.layouttv = QGridLayout(self.tv_widget)
            self.layouttv.setContentsMargins(0, 0, 0, 0)
            self.layouttv.setSpacing(2)

            self._tv = MyRawImageWidget(self.tv_widget, scaled=True)

            self._headerWidgettv = headerwidget.HeaderWidget(self,
                                                             layout=self.layouttv,
                                                             datawidget=self._tv)

            self.layouttv.addWidget(self._headerWidgettv, 0, 0)
            self.layouttv.setRowStretch(0, 0)
            self.layouttv.setRowStretch(1, 1)
            self.layouttv.addWidget(self._tv, 1, 0)

            slider = MyRangeSlider(Qt.Vertical)
            slider.setMinimumHeight(30)
            slider.setMinimum(0)
            slider.setMaximum(100)
            slider.setLow(0)
            slider.setHigh(100)
            slider.setTickPosition(QSlider.TicksBelow)
            slider.sliderMoved.connect(self.change_slider)

            self.layouttv.addWidget(slider, 1, 1)

            if self._profile:
                self._tabs.addTab(self.tv_widget, "TV")
            else:
                self.layoutmain.addWidget(self.tv_widget, 0, 0)

        if self._profile:

            self._roi2 = None

            # options

            self.w_options = QWidget()
            self.w_options_layout = QHBoxLayout(self.w_options)
            self.layouts["options"] = self.w_options_layout
            self.toggle_panel_options = togglepanelwidget.TogglePanelWidget(self.w_options,
                                                                            align="topleft",
                                                                            iconarrow="caret-up",
                                                                            iconshow="caret-down")
            # image

            self._selection_panel = titledborderwidget.TitledBorderWidget("Image")
            self._selection_panel.setMinimumHeight(70)

            self.selection_layout = QHBoxLayout(self._selection_panel)
            self.layouts["selection"] = self.selection_layout
            self.selection_layout.setContentsMargins(10, 20, 10, 10)

            self.w_options_layout.addWidget(self._selection_panel)

            self.b1 = QRadioButton("ROI")
            self.b1.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b1)

            self._checkboxroi_value = QCheckBox()
            self._checkboxroi_value.stateChanged.connect(self.push_roi)
            self._checkboxroi_value.setChecked(False)
            self.selection_layout.addWidget(self._checkboxroi_value)

            self.b2 = QRadioButton("Distance")
            self.b2.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b2)

            self.b3 = QRadioButton("None")
            self.b3.setChecked(True)
            self.b3.toggled.connect(lambda: self.change_option())
            self.selection_layout.addWidget(self.b3)

            from expert_gui_core.gui.widgets.datapanels.enumpanelwidget import EnumPanelWidget

            labels_reduction = {
                # "-1": "No Profile",
                # "0": "No Fit",
                "1": "Normal",
                "2": "Fast",
                "3": "Very Fast",
                # "4": "bits/2"
            }
            self.enum_reduction_widget = EnumPanelWidget(self,
                                                         title=None,
                                                         label_name="",
                                                         labels=labels_reduction,
                                                         history=False)
            self.selection_layout.addWidget(self.enum_reduction_widget)
            self.enum_reduction_widget.set_data(1)
            self.enum_reduction_widget.get_signal().connect(self.reduction_changed)

            # ratio

            self._ratio_panel = titledborderwidget.TitledBorderWidget("Ratio")
            self._ratio_panel.setMinimumHeight(70)

            self.ratio_layout = QHBoxLayout(self._ratio_panel)
            self.layouts["ratio"] = self.ratio_layout
            self.ratio_layout.setContentsMargins(10, 20, 10, 10)

            self.w_options_layout.addWidget(self._ratio_panel)

            from expert_gui_core.gui.widgets.datapanels.boolpanelwidget import BoolPanelWidget

            self._ratio = BoolPanelWidget(self,
                                          title=None,
                                          label_name="",
                                          type_boolean="toggle",
                                          edit=True,
                                          history=False)

            width = 15
            radius = width / 1.6
            self._ratio.get_main_component()[0]._radius = radius
            self._ratio.get_main_component()[0]._width = width

            self._ratio.get_signal().connect(self.ratio_changed)
            self.ratio_layout.addWidget(self._ratio)

            # cursor

            self._cursor_panel = titledborderwidget.TitledBorderWidget("Cursor")
            self._cursor_panel.setMinimumHeight(70)

            self.cursor_layout = QHBoxLayout(self._cursor_panel)
            self.layouts["cursor"] = self.cursor_layout
            self.cursor_layout.setContentsMargins(10, 20, 10, 10)

            self.w_options_layout.addWidget(self._cursor_panel)

            labels_cursor = {
                "1": "Mouse Click",
                "2": "Avg"
            }
            self.enum_cursor_widget = EnumPanelWidget(self,
                                                      title=None,
                                                      label_name="",
                                                      labels=labels_cursor,
                                                      history=False)
            self.cursor_layout.addWidget(self.enum_cursor_widget)
            self.enum_cursor_widget.set_data(2)

            # filter

            self._filter_panel = titledborderwidget.TitledBorderWidget("Filter")
            self._filter_panel.setMinimumHeight(70)

            self.filter_layout = QHBoxLayout(self._filter_panel)
            self.layouts["filter"] = self.filter_layout
            self.filter_layout.setContentsMargins(10, 20, 10, 10)

            self.w_options_layout.addWidget(self._filter_panel)

            labels_filter = {
                "1": "None",
                "2": "Any Signals",
                "3": "Good Signals"
            }

            self.enum_filter_widget = EnumPanelWidget(self,
                                                      title=None,
                                                      label_name="",
                                                      labels=labels_filter,
                                                      history=False)
            self.filter_layout.addWidget(self.enum_filter_widget)
            self.enum_filter_widget.set_data(1)

            # live

            self._live_panel = titledborderwidget.TitledBorderWidget("Live!")
            self._live_panel.setMinimumHeight(70)

            self.live_layout = QHBoxLayout(self._live_panel)
            self.layouts["live"] = self.live_layout
            self.live_layout.setContentsMargins(10, 20, 10, 10)

            self.w_options_layout.addWidget(self._live_panel)

            from expert_gui_core.gui.widgets.common import ledwidget

            self._led_live_widget = ledwidget.LedWidget()
            self._led_live_widget.set_color_inside(QColor("#666"))

            self.live_layout.addWidget(self._led_live_widget)

            # image

            from expert_gui_core.gui.widgets.pyqt.imagewidget import ImageWidget

            self.w_image = QWidget()
            self.w_image.setMinimumWidth(self._minsize)
            self.w_image.setMinimumHeight(self._minsize)
            self.im_widget = ImageWidget(self, roi=False)
            self.im_widget.add_plot(title="Image", lut=True, ratio=ratio)
            self.w_image_layout = QGridLayout(self.w_image)
            self.w_image_layout.setContentsMargins(0, 0, 0, 0)
            self.w_image_layout.setSpacing(0)
            self.w_image_layout.addWidget(self.im_widget, 1, 0)

            self.layouts["image"] = self.w_image_layout

            # header

            self._headerWidget = headerwidget.HeaderWidget(self.w_image,
                                                           layout=self.w_image_layout,
                                                           datawidget=self.im_widget)

            self.w_image_layout.addWidget(self._headerWidget, 0, 0)
            self.w_image_layout.setRowStretch(0, 0)
            self.w_image_layout.setRowStretch(1, 1)

            self.line = pg.LineSegmentROI([[0, 0],
                                           [0, 0]],
                                          pen=pg.mkPen({'color': Colors.COLOR_LIGHTRED, 'width': 2.}))

            self.im_widget.get_plot_item().addItem(self.line)

            # chart H

            from expert_gui_core.gui.widgets.pyqt.chartwidget import ChartWidget

            self.w_chart_H = QWidget()
            self.w_chart_H.setMinimumWidth(self._minsize)
            self.w_chart_H.setMinimumHeight(self._minsize)
            self.w_chart_H_layout = QGridLayout(self.w_chart_H)
            self.w_chart_H_layout.setContentsMargins(0, 0, 0, 0)
            self.w_chart_H_layout.setSpacing(0)
            self._chart_H = ChartWidget(self.w_chart_H)
            self._chart_H.add_plot(title="H",
                                   show_grid=False)

            self.w_chart_H_layout.addWidget(self._chart_H, 1, 0)
            self._headerWidgetH = headerwidget.HeaderWidget(self.w_chart_H,
                                                            layout=self.w_chart_H_layout,
                                                            datawidget=self._chart_H,
                                                            tick=False,
                                                            pause=False)
            self.w_chart_H_layout.addWidget(self._headerWidgetH, 0, 0)
            self.w_chart_H_layout.setRowStretch(0, 0)
            self.w_chart_H_layout.setRowStretch(1, 1)

            self.layouts["chartH"] = self.w_chart_H_layout

            # chart V

            self.w_chart_V = QWidget()
            self.w_chart_V.setMinimumWidth(self._minsize)
            self.w_chart_V.setMinimumHeight(self._minsize)
            self.w_chart_V_layout = QGridLayout(self.w_chart_V)
            self.w_chart_V_layout.setContentsMargins(0, 0, 0, 0)
            self.w_chart_V_layout.setSpacing(0)
            self._chart_V = ChartWidget(self.w_chart_V)

            self._chart_V.add_plot(title="V",
                                   swap=True,
                                   show_grid=False)

            self.w_chart_V_layout.addWidget(self._chart_V, 1, 0)
            self._headerWidgetV = headerwidget.HeaderWidget(self.w_chart_V,
                                                            layout=self.w_chart_V_layout,
                                                            datawidget=self._chart_V,
                                                            tick=False,
                                                            pause=False)
            self.w_chart_V_layout.addWidget(self._headerWidgetV, 0, 0)
            self.w_chart_V_layout.setRowStretch(0, 0)
            self.w_chart_V_layout.setRowStretch(1, 1)

            self.layouts["chartV"] = self.w_chart_V_layout

            # info

            self.w_info = QWidget()
            self.w_info.setMinimumWidth(self._minsize)
            self.w_info.setMinimumHeight(self._minsize)
            self.w_info.setMaximumWidth(self._minsize)

            self._values = {
                "distance": 0, "npts": 0, "amp": 0, "mu": 0, "sigma": 0, "offset": 0, "b": 0, "c": 0,
                "fit": False
            }

            data = {
                'Name': ["distance", "time", "sel x", "sel y", "amp H", "sigma H", "mu H", "offset H", "amp V",
                         "sigma V", "mu V", "offset V"],
                'Value': [0] * 12
            }

            self.df = pd.DataFrame(data)

            blankIndex = [''] * len(self.df)
            self.df.index = blankIndex

            status_colors = {
                "1": [QColor(255, 0, 0), QColor(255, 0, 0, 20)],
                "2": [QColor(0, 150, 0), QColor(0, 255, 0, 20)],
            }

            self.table = TableWidget(parent=self.w_info,
                                     status_colors=status_colors)

            self.table.set_data(self.df)

            self.table.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)

            self.w_info_layout = QGridLayout(self.w_info)
            self.w_info_layout.setContentsMargins(0, 0, 0, 0)
            self.w_info_layout.setSpacing(0)
            self.w_info_layout.addWidget(self.table, 0, 0)

            self.layouts["info"] = self.w_info_layout

            # settings

            if settings:
                self.w_settings = QWidget()
                self.w_settings.setMinimumWidth(self._minsize)
                self.w_settings.setMinimumHeight(self._minsize)
                self.toggle_panel_settings = togglepanelwidget.TogglePanelWidget(self.w_settings,
                                                                                 align="topright",
                                                                                 iconarrow="caret-left",
                                                                                 iconshow="caret-right")
                self.toggle_panel_settings.hide_(None)
            else:
                self.w_settings = None

            self.pos = QPoint(0, 0)

            # ROI

            self._roi = None
            self._roi2 = None
            self._roi_click = False

            self.rearrange()

        self.supdate_signal.connect(self.update_image)
        self.supdate_H.connect(self.update_data_H)
        self.supdate_V.connect(self.update_data_V)
        self.supdate_fit_H.connect(self.update_fit_H)
        self.supdate_fit_V.connect(self.update_fit_V)
        self.srefresh_plots.connect(self.refresh_plots)
        self.supdate_table.connect(self.update_table)

        self.line = None

    def change_slider(self, low_value, high_value):
        self._tv.setPercent(low_value, high_value)

    def get_layouts(self):
        return self.layouts

    def get_info(self):
        return self.df

    def push_roi(self, e):
        if not self.b1.isChecked():
            if self._roi2 is not None:
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()
                else:
                    self._roi2.hide()

    def set_roi_option(self):
        self.b1.setChecked(True)
        self.reduction_changed()
        self.change_option()

    def set_distance_option(self):
        self.b2.setChecked(True)

    def set_none_option(self):
        self.b3.setChecked(True)
        self.change_option()

    def change_option(self):

        # roi

        if self.b1.isChecked():
            if self._roi is not None:
                self._roi.show()
                self._roi2.hide()
            if self.line is not None:
                self.line.hide()
            self.im_widget.get_plot_item().set_draggable(False)
            self.im_widget.get_plot_item().set_hoverable(False)
            self.im_widget.get_plot_item().set_clickable(False)

        # distance

        elif self.b2.isChecked():
            if self._roi is not None:
                self._roi.hide()
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()
            if self.line is not None:
                self.line.show()
            self.im_widget.get_plot_item().set_draggable(False)
            self.im_widget.get_plot_item().set_hoverable(False)
            self.im_widget.get_plot_item().set_clickable(True)

        # none

        else:
            if self._roi is not None:
                self._roi.hide()
                if self._checkboxroi_value.isChecked():
                    self._roi2.show()
            if self.line is not None:
                self.line.hide()
            self.im_widget.get_plot_item().set_draggable(True)
            self.im_widget.get_plot_item().set_hoverable(True)
            self.im_widget.get_plot_item().set_clickable(True)

        self.srefresh_plots.emit()

    def set_image_ratio(self, ratio):
        if ratio:
            wx = np.shape(self._data["image"])[1]
            wy = np.shape(self._data["image"])[0]
            self.im_widget.get_plot_item().setAspectLocked(lock=True,
                                                           ratio=wx / wy)
        else:
            self.im_widget.get_plot_item().setAspectLocked(lock=False)

        xrange = self.im_widget.get_plot_item().viewRange()[0]
        yrange = self.im_widget.get_plot_item().viewRange()[1]

        self._chart_H.set_x_range(xmin=xrange[0], xmax=xrange[1])
        self._chart_V.set_y_range(ymin=yrange[0], ymax=yrange[1])

    def get_roi(self):
        return self._roi

    def get_signal_refresh(self):
        return self.supdate_fit_V

    @pyqtSlot()
    def update_roi(self):

        if self._roi is not None:

            visi_roi2 = self._roi2.isVisible()
            self._roi2.hide()

            x = int(self._roi.pos()[0])
            y = int(self._roi.pos()[1])
            w = int(self._roi.size()[0])
            h = int(self._roi.size()[1])

            self._roi2.setPos([x, y])
            self._roi2.setSize(size=[w, h])

            self._chart_H.set_x_range(xmin=x, xmax=x + w)
            self._chart_V.set_y_range(ymin=y, ymax=y + h)

            self.supdate_signal.emit()

            if visi_roi2:
                self._roi2.show()

    def set_roi_range(self, xmin, xmax, ymin, ymax):
        """
        Set ROI range.
        """

        if self._roi is None:
            return

        self._roi.setPos([xmin, ymin])
        self._roi.setSize(size=[xmax - xmin, ymax - ymin])

        self.update_roi()

    def reduction_changed(self):

        xmin = self.get_roi().pos()[0]
        ymin = self.get_roi().pos()[1]

        w = self.get_roi().size()[0]
        h = self.get_roi().size()[1]

        try:
            mapped_pos = QPoint(self.im_widget.get_plot_item()._vline_static.value(),
                                self.im_widget.get_plot_item()._hline_static.value())
        except:
            if self.pos is None:
                return
            mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(self.pos)

        alpha = 1.

        if self.enum_reduction_widget.get_data() == 2:
            if self._old_reduction == 1:
                alpha = 0.5
            elif self._old_reduction == 3:
                alpha = 2.
        elif self.enum_reduction_widget.get_data() == 3:
            if self._old_reduction == 1:
                alpha = 0.25
            elif self._old_reduction == 2:
                alpha = 0.5
        else:
            if self._old_reduction == 2:
                alpha = 2.
            elif self._old_reduction == 3:
                alpha = 4.

        self.pos = QPoint(int(mapped_pos.x() * alpha), int(mapped_pos.y() * alpha))

        self.im_widget.get_plot_item()._vline_static.setPos(self.pos.x())
        self.im_widget.get_plot_item()._hline_static.setPos(self.pos.y())

        self.set_roi_range(int(xmin * alpha), int((xmin + w) * alpha), int(ymin * alpha), int((ymin + h) * alpha))

        self._old_reduction = self.enum_reduction_widget.get_data()

    def fit(self, fo, x, y):
        """
        Function fit gaussian.
        """
        p_val = st.pearsonr(x, y)
        if p_val.pvalue < 0.03:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        y_used = np.array(list(y), dtype=float)
        x_used = np.array(list(x), dtype=float)

        try:
            imax = y_used.argmax()
        except:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        max0 = y_used[imax]
        mu0 = x_used[imax]
        y2 = y_used[y_used < max0 / 2]

        if y2 is None or len(y2) == 0:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        imax2 = y2.argmax()
        sigma0 = x_used[imax] - x_used[imax2]
        if sigma0 < 0:
            sigma0 = -sigma0
        if sigma0 == 0:
            sigma0 = 0.1
        c0 = y_used[:3].mean()
        if x_used[-1] != x_used[0]:
            b0 = (y_used[-1] - y_used[0]) / (x_used[-1] - x_used[0])
        else:
            b0 = 0

        try:
            popt, pcov = curve_fit(
                f=fo,
                xdata=x_used,
                ydata=y_used,
                p0=[float(max0), float(mu0), float(sigma0), float(b0), float(c0)],
                check_finite=False)
            # , method='lm')
        except:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        if popt[2] > 0:
            self._values["npts"] = len(x)
            self._values["amp"] = popt[0]
            self._values["mu"] = popt[1]
            self._values["sigma"] = popt[2]
            self._values["offset"] = popt[4]
            self._values["fit"] = True
        else:
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["fit"] = False
            return None

        return fo_gauss(x, *popt)

    def calculate(self, data_H, data_V):
        """
        Do the calculation and update widget.
        """

        data_x_V = range(0, len(data_V))
        data_x_H = range(0, len(data_H))

        fitH = self.fit(fo_gauss, data_x_H, data_H)

        self._values["ampH"] = self._values["amp"]
        self._values["muH"] = self._values["mu"]
        self._values["sigmaH"] = self._values["sigma"]
        self._values["offsetH"] = self._values["offset"]
        self._values["fitH"] = self._values["fit"]

        fitV = self.fit(fo_gauss, data_x_V, data_V)

        self._values["ampV"] = self._values["amp"]
        self._values["muV"] = self._values["mu"]
        self._values["sigmaV"] = self._values["sigma"]
        self._values["offsetV"] = self._values["offset"]
        self._values["fitV"] = self._values["fit"]

        return [fitH, fitV]

    def click_on_image(self, evt):
        if self.b2.isChecked():
            self.pos = QPoint(evt.pos().x(), evt.pos().y())
            self.srefresh_plots.emit()
        elif not self.b1.isChecked():
            try:
                self.pos = QPoint(self.im_widget.get_plot_item()._vline_static.value(),
                                  self.im_widget.get_plot_item()._hline_static.value())
            except:
                self.pos = QPoint(evt.pos().x(), evt.pos().y())
            self.srefresh_plots.emit()

    @pyqtSlot()
    def refresh_plots(self):

        if self.pos is None:
            return

        if self._roi is not None:
            try:
                x = int(self._roi.pos()[0])
                y = int(self._roi.pos()[1])
                w = int(self._roi.size()[0])
                h = int(self._roi.size()[1])
            except:
                return
        else:
            return

        # data

        data = self._data["image"]

        wimage = data.shape[0]
        himage = data.shape[1]

        if not self.b1.isChecked():
            x = 0
            y = 0
            w = wimage
            h = himage

        if x < 0:
            w = w + x
            x = 0

        if y < 0:
            h = h + y
            y = 0

        if h <= 0 or w <= 0:
            return

        if x >= wimage or y >= himage:
            return

        if x + w >= wimage:
            w = wimage - x

        if y + h >= himage:
            h = himage - y

        # map the image coordinates to data coordinates

        try:
            if not self.b2.isChecked():
                self.pos = QPoint(self.im_widget.get_plot_item()._vline_static.value(),
                                  self.im_widget.get_plot_item()._hline_static.value())
                mapped_pos = self.pos
            else:
                if self.pos is None:
                    return
                mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(self.pos)
        except:
            if self.pos is None:
                return
            mapped_pos = self.im_widget.get_plot_item().getViewBox().mapSceneToView(self.pos)

        # position of max

        max = np.max(data)
        if max > self._maxmax:
            self._maxmax = max
            self._chart_V.set_x_range(xmin=0, xmax=self._maxmax)
            self._chart_H.set_y_range(ymin=0, ymax=self._maxmax)

        imax = np.argmax(data)
        xm = int(imax / himage)
        ym = imax - himage * xm

        max_pos = (int(xm), int(ym))

        # extract the x and y indices

        self.x_index = int(mapped_pos.x())
        self.y_index = int(mapped_pos.y())

        if self.b1.isChecked():

            if self.x_index < x or self.x_index > w + x:
                return

            if self.y_index < y or self.y_index > h + y:
                return

        if 0 <= self.x_index < data.shape[0] and 0 <= self.y_index < data.shape[1]:

            # update H and V data

            if self.enum_reduction_widget.get_data() != -1:
                if self.enum_cursor_widget.get_data() == 1:

                    self.set_data_H(range(x, w + x), data[x:w + x, self.y_index])
                    self.set_data_V(range(y, h + y), data[self.x_index, y:h + y])
                else:
                    self.set_data_H(range(x, w + x), np.average(data, axis=1)[x:w + x])
                    self.set_data_V(range(y, h + y), np.average(data, axis=0)[y:h + y])

            # create a LineSegmentROI between max value and cursor click

            if self.b2.isChecked():

                self.im_widget.get_plot_item().removeItem(self.line)
                self.line = pg.LineSegmentROI([[max_pos[0], max_pos[1]], [self.x_index, self.y_index]],
                                              pen=pg.mkPen({'color': Colors.COLOR_LIGHTRED, 'width': 2.}))
                self.im_widget.get_plot_item().addItem(self.line)

                # calculate distance between max value and cursor click

                distance = calculate_distance((max_pos[0], max_pos[1]), (self.x_index, self.y_index))
            else:
                distance = -1

            if self.enum_filter_widget.get_data() != 0 and self.enum_reduction_widget.get_data() > 0:
                if self.enum_cursor_widget.get_data() == 1:
                    data_fit = self.calculate(data[x:w + x, self.y_index], data[self.x_index, y:h + y])
                else:
                    data_fit = self.calculate(np.average(data, axis=1)[x:w + x], np.average(data, axis=0)[y:h + y])

            if self.show_info:

                self.df["Value"][0] = formatting.format_value_to_string(distance, type_format="sci",
                                                                        format_sci="{:.3e}")  # distance
                self.df["Value"][1] = formatting.format_value_to_string(formatting.current_milli_time() / 1000,
                                                                        type_format="date")  # time
                self.df["Value"][2] = self.x_index  # sel x
                self.df["Value"][3] = self.y_index  # sel y
                if self.enum_reduction_widget.get_data() > 0:
                    self.df["Value"][4] = formatting.format_value_to_string(self._values["ampH"], type_format="sci",
                                                                            format_sci="{:.3e}")  # amp H
                    self.df["Value"][5] = formatting.format_value_to_string(self._values["sigmaH"], type_format="sci",
                                                                            format_sci="{:.3e}")  # sigma H
                    self.df["Value"][6] = formatting.format_value_to_string(self._values["muH"], type_format="sci",
                                                                            format_sci="{:.3e}")  # mu H
                    self.df["Value"][7] = formatting.format_value_to_string(self._values["offsetH"], type_format="sci",
                                                                            format_sci="{:.3e}")  # offset H
                    self.df["Value"][8] = formatting.format_value_to_string(self._values["ampV"], type_format="sci",
                                                                            format_sci="{:.3e}")  # amp V
                    self.df["Value"][9] = formatting.format_value_to_string(self._values["sigmaV"], type_format="sci",
                                                                            format_sci="{:.3e}")  # sigma V
                    self.df["Value"][10] = formatting.format_value_to_string(self._values["muV"], type_format="sci",
                                                                             format_sci="{:.3e}")  # mu V
                    self.df["Value"][11] = formatting.format_value_to_string(self._values["offsetV"], type_format="sci",
                                                                             format_sci="{:.3e}")  # offset V
                    if self._values["fitH"]:
                        self.df["status_list"][4] = 2
                    else:
                        self.df["status_list"][4] = 1

                    if self._values["fitV"]:
                        self.df["status_list"][8] = 2
                    else:
                        self.df["status_list"][8] = 1
                else:
                    self.df["Value"][4] = ""
                    self.df["Value"][5] = ""
                    self.df["Value"][6] = ""
                    self.df["Value"][7] = ""
                    self.df["Value"][8] = ""
                    self.df["Value"][9] = ""
                    self.df["Value"][10] = ""
                    self.df["Value"][11] = ""
                    self.df["status_list"][4] = 1
                    self.df["status_list"][8] = 1

                self.supdate_table.emit()

            if self.enum_reduction_widget.get_data() > 0:
                self.set_data_fit_H(range(x, w + x), data_fit[0])
                self.set_data_fit_V(range(y, h + y), data_fit[1])

        # if (formatting.current_milli_time() - t) > 10:
        #     print("@: " + str(formatting.current_milli_time() - t))

    def remove_singletons(self, data, time):

        t = formatting.current_milli_time()

        size_avg = 25

        acceptance = 3
        count_max = 2

        modw = size_avg * int(data.shape[0] / size_avg)
        modh = size_avg * int(data.shape[1] / size_avg)

        for i in range(0, int(data.shape[0] / size_avg)):
            for j in range(0, int(data.shape[1] / size_avg)):
                try:
                    datatmp = data[i * size_avg:(i + 1) * size_avg, j * size_avg:(j + 1) * size_avg]
                    avg = np.mean(datatmp)
                    maxs = np.argwhere(datatmp >= acceptance * avg)
                    if len(maxs) >= 1 and len(maxs) <= count_max:
                        datatmp[datatmp >= acceptance * avg] = avg
                except Exception as e:
                    print(e)
                    pass

        for i in range(0, int(data.shape[0] / size_avg)):
            try:
                datatmp = data[i * size_avg:(i + 1) * size_avg, modh:data.shape[1]]
                avg = np.mean(datatmp)
                maxs = np.argwhere(datatmp >= acceptance * avg)
                if len(maxs) >= 1 and len(maxs) <= count_max:
                    datatmp[datatmp >= acceptance * avg] = avg
            except Exception as e:
                print(e)
                pass

        for j in range(0, int(data.shape[1] / size_avg)):
            try:
                datatmp = data[modw:data.shape[0], j * size_avg:(j + 1) * size_avg]
                avg = np.mean(datatmp)
                maxs = np.argwhere(datatmp >= acceptance * avg)
                if len(maxs) >= 1 and len(maxs) <= count_max:
                    datatmp[datatmp >= acceptance * avg] = avg
            except Exception as e:
                print(e)
                pass

        datatmp = data[modw:data.shape[0],
                  modh:data.shape[1]]
        avg = np.mean(datatmp)
        maxs = np.argwhere(datatmp >= 3 * avg)
        if len(maxs) > 0 and len(maxs) >= 1 and len(maxs) <= count_max:
            datatmp[datatmp >= acceptance * avg] = avg

        print(formatting.current_milli_time() - t)

        return data

    def set_data(self, data, time=0):
        """
        Set data.
        :param data:
        :return:
        """

        if self._mutex:
            return

        self._mutex = True

        if (time != 0) and (time == self._time_image or (time - self._time_image) < 0):
            self.release_mutex()
            return

        self._time_image = time

        if self._tv:
            if self._profile:
                if self._tabs.currentIndex() == 1:
                    self._headerWidgettv.recv()
                    # data = self.remove_singletons(data)
                    self._tv.setImage(data)
                    self.release_mutex()
                    return
            else:
                self._headerWidgettv.recv()
                # data = self.remove_singletons(data)
                self._tv.setImage(data)
                self.release_mutex()
                return

        if not self._profile:
            self.release_mutex()
            return

        if (formatting.current_milli_time() - self._update_time) < self._limit_time:
            self.release_mutex()
            return

        if not self._active or self._headerWidget.is_pause():
            self.release_mutex()
            return

        type_signal = 1

        neighbour = 2
        neighbour_distance = 5

        if self.enum_reduction_widget.get_data() == 2:
            im = Image.fromarray(data)
            im = im.resize((im.width // 2, im.height // 2), resample=Image.NEAREST)
            data = np.asarray(im).copy()
            neighbour = 1
        elif self.enum_reduction_widget.get_data() == 3:
            im = Image.fromarray(data)
            im = im.resize((im.width // 4, im.height // 4), resample=Image.NEAREST)
            data = np.asarray(im).copy()
            neighbour = 1
        elif self.enum_reduction_widget.get_data() == 4:
            if "int16" in str(data.dtype):
                data = (data >> 8).astype(np.int8)
            elif "int32" in str(data.dtype):
                data = (data >> 16).astype(np.int16)

        max = np.max(data[::neighbour_distance - neighbour, ::neighbour_distance - neighbour])
        max2 = np.max(data[1::neighbour_distance - neighbour, 1::neighbour_distance - neighbour])
        max3 = 0
        max4 = 0

        # no beam saturated

        if max != max2:

            # remove singleton

            maxall = np.max(data)
            minall = np.min(data)
            data[data == maxall] = minall
            max3 = np.max(data[::neighbour_distance - neighbour, ::neighbour_distance - neighbour])
            max4 = np.max(data[1::neighbour_distance - neighbour, 1::neighbour_distance - neighbour])

        # beam + saturation

        if max == max2:
            self._data["image"] = data
            if self._absolute_scale:
                maxy = np.max(self._data["image"])
                if maxy > self._ymax:
                    self._ymax = maxy
                    self._chart_H.set_y_range(ymin=0, ymax=self._ymax)
                    self._chart_V.set_x_range(xmin=0, xmax=self._ymax)
            self._led_live_widget.set_color_inside(QColor("#FF2900"))
            type_signal = 2

        # beam

        elif max3 == max4:
            self._data["image"] = data
            if self._absolute_scale:
                maxy = np.max(self._data["image"])
                if maxy > self._ymax:
                    self._ymax = maxy
                    self._chart_H.set_y_range(ymin=0, ymax=self._ymax)
                    self._chart_V.set_x_range(xmin=0, xmax=self._ymax)
            self._led_live_widget.set_color_inside(QColor("#B6FF00"))
            type_signal = 3

        # none

        else:
            self._led_live_widget.set_color_inside(QColor("#666"))
            type_signal = 1

        self.reset_led()

        if (formatting.current_milli_time() - self._update_time) < 20:
            self.release_mutex()
            return

        self._update_time = formatting.current_milli_time()

        if self.enum_filter_widget.get_data() == 1 or (self.enum_filter_widget.get_data() == type_signal):
            self._data["image"] = data
            self.supdate_signal.emit()
        else:
            self.release_mutex()

    def release_mutex(self):
        try:
            self._mutex = False
        except:
            pass

    def reset_led(self):
        if self._led_flag:
            self._led_live_widget.set_color_inside(Colors.COLOR_TRANSPARENT)
        self._led_flag = not self._led_flag

    @pyqtSlot()
    def update_table(self):
        self.table.refresh_table()

    @pyqtSlot()
    def update_image(self):

        # t = formatting.current_milli_time()

        self._headerWidget.recv()

        self.srefresh_plots.emit()

        self.im_widget.set_data(self._data["image"], raw=True, low_res=False)

        if self._roi is None:
            self._roi = pg.ROI(
                [int(0.25 * self._data["image"].shape[0]), int(0.25 * self._data["image"].shape[1])],
                [int(0.50 * self._data["image"].shape[0]), int(0.50 * self._data["image"].shape[1])],
                rotatable=False,
                removable=False)

            self._roi.addScaleHandle([0.5, 1], [0.5, 0.5])
            self._roi.addScaleHandle([0, 0.5], [0.5, 0.5])
            self._roi.addScaleHandle([1, 0.5], [0.5, 0.5])
            self._roi.addScaleHandle([0.5, 0], [0.5, 0.5])
            self._roi.setZValue(10)
            self._roi.sigRegionChangeFinished.connect(self.update_roi)

            self._roi2 = pg.ROI([int(0.25 * self._data["image"].shape[0]), int(0.25 * self._data["image"].shape[1])],
                                [int(0.50 * self._data["image"].shape[0]), int(0.50 * self._data["image"].shape[1])],
                                movable=False,
                                rotatable=False,
                                resizable=False,
                                removable=False,
                                pen=pg.mkPen(color=Colors.COLOR_LIGHT0GREEN, width=2, dash=[2, 2]))
            self._roi2.setZValue(9)
            self._roi2.hide()

            self.change_option()

            self.im_widget.get_plot_item().addItem(self._roi)
            self.im_widget.get_plot_item().addItem(self._roi2)

            self.im_widget.get_plot_item().get_mousepressevent().connect(self.click_on_image)

        # print("@: " + str(formatting.current_milli_time() - t))

        self.release_mutex()

    @pyqtSlot()
    def update_data_H(self):
        self._headerWidgetH.recv()
        self._chart_H.set_data(self._data["H"],
                               datax=self._data["xH"],
                               skipFiniteCheck=True,
                               pen=pg.mkPen({'color': Colors.COLOR_LBLUE, 'width': 2.}), )

    def set_data_H(self, datax, data):
        if data is not None:
            self._data["xH"] = datax
            self._data["H"] = data
            self.supdate_H.emit()

    @pyqtSlot()
    def update_fit_H(self):
        self._chart_H.set_data(self._data["FitH"],
                               datax=self._data["xFitH"],
                               name="fitH",
                               skipFiniteCheck=True,
                               pen=pg.mkPen({'color': Colors.COLOR_LIGHT0GREEN, 'width': 2.}))

    def set_data_fit_H(self, datax, data):
        if data is not None:
            self._data["xFitH"] = datax
            self._data["FitH"] = data
            self.supdate_fit_H.emit()
        else:
            self._data["xFitH"] = [0, 0]
            self._data["FitH"] = [0, 0]
            self.supdate_fit_H.emit()

    @pyqtSlot()
    def update_data_V(self):
        self._headerWidgetV.recv()
        self._chart_V.set_data(self._data["V"],
                               datax=self._data["xV"],
                               skipFiniteCheck=True,
                               pen=pg.mkPen({'color': Colors.COLOR_PINK, 'width': 2.}))

    def set_data_V(self, datax, data):
        if data is not None:
            self._data["xV"] = datax
            self._data["V"] = data
            self.supdate_V.emit()

    @pyqtSlot()
    def update_fit_V(self):
        self._chart_V.set_data(self._data["FitV"],
                               datax=self._data["xFitV"],
                               name="fitH",
                               skipFiniteCheck=True,
                               pen=pg.mkPen({'color': Colors.COLOR_LIGHT0GREEN, 'width': 2.}))

    def set_data_fit_V(self, datax, data):
        if data is not None:
            self._data["xFitV"] = datax
            self._data["FitV"] = data
            self.supdate_fit_V.emit()
        else:
            self._data["xFitV"] = [0, 0]
            self._data["FitV"] = [0, 0]
            self.supdate_fit_V.emit()

    def light_(self):

        Colors.COLOR_LIGHT = True

        if self._profile:

            self._chart_V.light_()

            if self.w_settings is not None:
                self.toggle_panel_settings.light_()
            self._chart_H.light_()
            self.im_widget.light_()
            self.table.light_()

            self._selection_panel.light_()
            self.toggle_panel_options.light_()

            self._ratio.light_()
            self._ratio_panel.light_()

            self._cursor_panel.light_()
            self.enum_cursor_widget.light_()

            self._filter_panel.light_()
            self.enum_filter_widget.light_()

            self.enum_cursor_widget.light_()

            self._live_panel.light_()
            self._led_live_widget.light_()

    def dark_(self):

        Colors.COLOR_LIGHT = False

        if self._profile:

            if self.w_settings is not None:
                self.toggle_panel_settings.dark_()

            self._chart_V.dark_()
            self._chart_H.dark_()

            self.im_widget.dark_()

            self.table.dark_()

            self._selection_panel.dark_()
            self.toggle_panel_options.dark_()

            self._ratio.dark_()
            self._ratio_panel.dark_()

            self._cursor_panel.dark_()
            self.enum_cursor_widget.dark_()

            self._filter_panel.dark_()
            self.enum_filter_widget.dark_()

            self.enum_cursor_widget.dark_()

            self._live_panel.dark_()
            self._led_live_widget.dark_()

    def clear_layout(self):
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)

    def get_values(self):
        return self._values

    def get_w_options(self):
        return self.w_options

    def get_w_options_layout(self):
        return self.w_options_layout

    def rearrange(self):
        self.clear_layout()
        if self._positionV == Qt.AlignLeft:
            if self._positionH == Qt.AlignTop:
                self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                if self.w_settings is not None:
                    self.layout.addWidget(self.toggle_panel_settings, 1, 0, 2, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 1, 1, 1, 1)
                self.layout.addWidget(self.w_chart_V, 2, 1, 1, 1)
                self.layout.addWidget(self.w_chart_H, 1, 2, 1, 1)
                self.layout.addWidget(self.w_image, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 0)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 0)
                self.layout.setRowStretch(2, 1)
                self.w_chart_H.setMinimumHeight(self._minsize)
                self.w_chart_H.setMaximumHeight(self._minsize)
                self.w_chart_V.setMinimumWidth(self._minsize)
                self.w_chart_V.setMaximumWidth(self._minsize)
            elif self._positionH == Qt.AlignBottom:
                self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                if self.w_settings is not None:
                    self.layout.addWidget(self.toggle_panel_settings, 1, 0, 2, 1)
                self.layout.addWidget(self.w_chart_V, 1, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 2, 1, 1, 1)
                self.layout.addWidget(self.w_image, 1, 2, 1, 1)
                self.layout.addWidget(self.w_chart_H, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 0)
                self.layout.setColumnStretch(2, 1)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 0)
                self.w_chart_H.setMinimumHeight(self._minsize)
                self.w_chart_H.setMaximumHeight(self._minsize)
                self.w_chart_V.setMinimumWidth(self._minsize)
                self.w_chart_V.setMaximumWidth(self._minsize)
        elif self._positionV == Qt.AlignRight:
            if self._positionH == Qt.AlignTop:
                self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                if self.w_settings is not None:
                    self.layout.addWidget(self.toggle_panel_settings, 1, 0, 2, 1)
                self.layout.addWidget(self.w_chart_H, 1, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 1, 2, 1, 1)
                self.layout.addWidget(self.w_image, 2, 1, 1, 1)
                self.layout.addWidget(self.w_chart_V, 2, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 0)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 0)
                self.layout.setRowStretch(2, 1)
                self.w_chart_H.setMinimumHeight(self._minsize)
                self.w_chart_H.setMaximumHeight(self._minsize)
                self.w_chart_V.setMinimumWidth(self._minsize)
                self.w_chart_V.setMaximumWidth(self._minsize)
            elif self._positionH == Qt.AlignBottom:
                self.layout.addWidget(self.toggle_panel_options, 0, 1, 1, 2, Qt.AlignCenter)
                if self.w_settings is not None:
                    self.layout.addWidget(self.toggle_panel_settings, 1, 0, 2, 1)
                self.layout.addWidget(self.w_image, 1, 1, 1, 1)
                self.layout.addWidget(self.w_chart_H, 2, 1, 1, 1)
                if self.show_info:
                    self.layout.addWidget(self.w_info, 2, 2, 1, 1)
                self.layout.addWidget(self.w_chart_V, 1, 2, 1, 1)
                self.layout.setColumnStretch(0, 0)
                self.layout.setColumnStretch(1, 1)
                self.layout.setColumnStretch(2, 0)
                self.layout.setRowStretch(0, 0)
                self.layout.setRowStretch(1, 1)
                self.layout.setRowStretch(2, 0)
                self.w_chart_H.setMinimumHeight(self._minsize)
                self.w_chart_H.setMaximumHeight(self._minsize)
                self.w_chart_V.setMinimumWidth(self._minsize)
                self.w_chart_V.setMaximumWidth(self._minsize)

    def ratio_changed(self):
        self.set_image_ratio(self._ratio.get_data())

    def set_active(self, active=True):
        self._active = active


# EXAMPLE1

class Listener(simucomm.SimuCommListener):

    def __init__(self, parent):
        self._parent = parent

        self.simuDataObject = simucomm.SimuDataObject()

        self.change_data()

        self.simucomm = simucomm.SimuComm("",
                                          "",
                                          self.simuDataObject,
                                          listener=self,
                                          timer_period=20)

    def start(self, cycle=""):
        self.simucomm.subscribe()

    def stop(self):
        self.simucomm.unsubscribe()

    def get(self):
        data = self.simucomm.get()

        self.handle_event(self.simucomm._device + "/" + self.simucomm._property, data)

    def handle_event(self, name, value):
        """
        Handle event (notification).
        """

        self._parent.handle_event(name, value)

    def change_data(self):
        """
        Data change function to observe the change of data
        """
        pass


class _Example1(QMainWindow, fesacomm.FesaCommListener):
    update_signal = pyqtSignal(object)

    mutex = threading.Lock()

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.gauss = []
        for i in range(0, 10):
            size1 = 2000
            size2 = 1500
            sigma = 1. + random.random()
            muu = 0
            x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            dst = np.sqrt(x ** 2 + y ** 2)
            if i % 10 == 0:
                self.gauss.append(150 * np.exp(-((dst - muu) ** 2 / (0.2 * sigma ** 2))) + np.random.normal(50, 5,
                                                                                                            [size2,
                                                                                                             size1]) - 40)
            else:
                self.gauss.append(np.random.normal(50, 5, [size2, size1]) - 40)
        self._ind = 0

        self.profile = ProfileWidget(self,
                                     show_info=True,
                                     limit_time=20)
        self._update_time = 0

        self._listener = Listener(self)
        self._listener.start()

        self.update_signal.connect(self.update)

        self.layout.addWidget(self.profile)

        self._headerWidget = headerwidget.HeaderWidget(self,
                                                       layout=self.layout,
                                                       datawidget=None,
                                                       tick=True,
                                                       pause=True)

        # self.layout.addWidget(self._headerWidget)

        # self.imv = pg.ImageItem()
        glw = pg.GraphicsLayoutWidget()
        # self.plot = glw.addPlot(row=0, col=0)
        self.plot = _PlotItem()
        self.imv = _ImageItem(self.plot, name="AA")
        self.plot.addItem(self.imv)
        glw.addItem(self.plot, 0, 0)
        # self.layout.addWidget(glw)
        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

    def handle_event(self, name, value):

        dt = value['_time'] - self._update_time
        # print(dt)
        if dt > 20:
            self._ind = self._ind + 1
            if self._ind > 9:
                self._ind = 0
            self.profile.set_data(self.gauss[self._ind])
            # self.update_signal.emit(self.gauss[self._ind])
            self._update_time = value['_time']

    @pyqtSlot(object)
    def update(self, value):
        self.imv.setImage(value)

    def write_fit_values_on_logger(self):
        print(self.profile._values)

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


# EXAMPLE2

class _Example2(QMainWindow, fesacomm.FesaCommListener):
    update_signal = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.profile = ProfileWidget(self,
                                     show_info=True,
                                     limit_time=50)
        self._update_time = 0
        self._fesacomm = fesacomm.FesaComm("BTVDEV.DigiCam.CAM2",
                                           "LastImage",
                                           listener=self)

        self.gauss = []
        for i in range(0, 10):
            size1 = 1500
            size2 = 2000
            sigma = 1. + random.random()
            muu = 0
            # x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
            # dst = np.sqrt(x ** 2 + y ** 2)
            # self.gauss.append(np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2))))
            # self.gauss.append(np.random.normal(50, 5, [size1, size2]))
            self.gauss.append(pg.gaussianFilter(np.random.normal(size=(size1, size2)), (5, 5)))
        self._ind = 0

        self.update_signal.connect(self.update)

        # self.layout.addWidget(self.profile)

        self._headerWidget = headerwidget.HeaderWidget(self,
                                                       layout=self.layout,
                                                       datawidget=None,
                                                       tick=True,
                                                       pause=True)

        self.layout.addWidget(self._headerWidget)

        self.imv = pg.ImageItem()
        glw = pg.GraphicsLayoutWidget()
        self.plot = glw.addPlot(row=0, col=0)
        # self.plot = _PlotItem()
        # self.imv = _ImageItem(self.plot, name="AA")
        self.plot.addItem(self.imv)
        glw.addItem(self.plot, 0, 0)
        self.layout.addWidget(glw)

        self._fesacomm.subscribe("")
        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

    def handle_event(self, name, value):
        self._headerWidget.recv()
        if value['_time'] - self._update_time > 20:
            # print(str(value['_time'] - self._update_time))

            # self.profile.set_data(value['image2D'])

            self._ind = self._ind + 1
            if self._ind > 9:
                self._ind = 0
            # self.update_signal.emit(self.gauss[self._ind])
            if value['image2D'].dtype == "int16":
                values = (value['image2D'] >> 8).astype(np.int8)
                self.update_signal.emit(values)

            self._update_time = value['_time']

    @pyqtSlot(object)
    def update(self, value):
        self.imv.setImage(value)

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


# EXAMPLE 3

class _Example3(QMainWindow, fesacomm.FesaCommListener):

    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout(self)
        self.central_widget = QWidget(self)
        self.central_widget.setLayout(self.layout)

        self.profile = ProfileWidget(self,
                                     show_info=True,
                                     limit_time=20)
        self._update_time = 0
        self._fesacomm = fesacomm.FesaComm("BTVDEV.DigiCam.CAM2",
                                           "LastImage",
                                           listener=self)

        self.time = 0
        self.layout.addWidget(self.profile)

        self.setCentralWidget(self.central_widget)
        self.resize(900, 600)

        self._fesacomm.subscribe("")

    def handle_event(self, name, value):

        dt = value['_time'] - self._update_time

        # print(dt)

        if dt > -9999990:

            # t=formatting.current_milli_time()

            # if (float(value['imageTimeStamp']) <= self.time):
                # print("reject")
                # return

            # print((value['imageTimeStamp']-self.time)/1000000)

            # self.profile.set_data2(value['image2D'],time=float(value['imageTimeStamp']))


            if self.profile._mutex:
                print("Skipping image...", (float(value['imageTimeStamp']-self.time)/1000000))
                self.time = float(value['imageTimeStamp'])
                return;

            displayThread = threading.Thread(target=self.profile.set_data,
                                             args=(value['image2D'], float(value['imageTimeStamp'])))

            self.time = float(value['imageTimeStamp'])

            # print("#: " + str(formatting.current_milli_time() - t))

            self._update_time = value['_time']

            displayThread.start()
            displayThread.join()

    def light_(self):
        Colors.COLOR_LIGHT = True
        self.profile.light_()

    def dark_(self):
        Colors.COLOR_LIGHT = False
        self.profile.dark_()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)

    font_text_user = app.font()
    font_text_user.setPointSize(10)
    app.setFont(font_text_user)

    qss = """
                QMenuBar::item {
                    spacing: 2px;           
                    padding: 2px 10px;
                    background-color: """ + Colors.STR_COLOR_LIGHT0GRAY + """;
                }
                QMenuBar::item:selected {    
                    background-color: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }
                QMenuBar::item:pressed {
                    background: """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }              
                QScrollArea {
                    background-color: transparent;
                }
                QRadioButton::checked {
                    color: """ + Colors.STR_COLOR_L2BLUE + """;  
                }
                QRadioButton::indicator {
                    border-radius: 6px;                            
                }
                QRadioButton::indicator::unchecked{ 
                    border-radius: 6px;
                    border:2px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;            
                }

                QRadioButton::indicator::checked{ 
                    border: 2px solid; 
                    border-color: """ + Colors.STR_COLOR_L2BLUE + """;  
                    border-radius: 6px;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;   
                }
                QCheckBox {
                    color: """ + Colors.STR_COLOR_WHITE + """;
                }
                QCheckBox::indicator {
                    max-width:14;
                    max-height:14;
                    border:1px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                    background-color: """ + Colors.STR_COLOR_L3BLACK + """;            
                }    
                QCheckBox::indicator:checked {
                    max-width:14;
                    max-height:14;
                    border:0px solid """ + Colors.STR_COLOR_L3BLACK + """;
                    background-color: """ + Colors.STR_COLOR_L2BLUE + """;            
                }             
                QLineEdit {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    text-align:left;
                    padding-left:2px;
                    padding-top:2px;
                    padding-bottom:2px;
                    border:0px solid """ + Colors.STR_COLOR_LIGHT1GRAY + """;
                }         
                QComboBox {
                    border:none;
                    background-color:""" + Colors.STR_COLOR_L1BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }                
                QListView {
                    background-color:""" + Colors.STR_COLOR_L2BLACK + """;
                    selection-color:""" + Colors.STR_COLOR_WHITE + """;
                    color:""" + Colors.STR_COLOR_WHITE + """;
                    selection-background-color:""" + Colors.STR_COLOR_LBLUE + """;
                }
            """
    app.setStyleSheet(qss)
    ex = _Example3()
    ex.show()
    ex.dark_()
    # ex.light_()
    sys.exit(app.exec_())

#
# import numpy as np
# from PyQt5 import QtWidgets
# from PyQt5.QtCore import pyqtSignal, pyqtSlot
# import pyqtgraph as pg
# from pyqtgraph.Qt import QtCore, QtGui
# from pyqtgraph import GraphicsLayoutWidget
# from threading import Thread, Event
# import time
#
#
# # Routine to acquire and serve data
# # This might be a camera driver, notifying when a new frame is available
# def generate_data(callback, threadkill):
#     while not threadkill.is_set():
#         size1 = 2000
#         size2 = 1500
#         sigma = 1.+random.random()
#         muu = 0
#         x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
#         dst = np.sqrt(x ** 2 + y ** 2)
#         data = np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))
#         callback(data)
#         time.sleep(0.05)
#
#
# class PyQtGraphTest(QMainWindow):
#     # Signal to indicate new data acquisition
#     # Note: signals need to be defined inside a QObject class/subclass
#     data_acquired = pyqtSignal(np.ndarray)
#
#     def __init__(self):
#         super().__init__()
#
#         self.layout = QVBoxLayout(self)
#         self.central_widget = QWidget(self)
#         self.central_widget.setLayout(self.layout)
#
#         from expert_gui_core.gui.widgets.datapanels.boolpanelwidget import BoolPanelWidget
#
#         self.respect_ratio = BoolPanelWidget(self, title=None, label_name="Respect Ratio:", type_boolean="toggle",
#                                              edit=True, history=False, statistics=False, fitting=False)
#         # self.respect_ratio.get_main_componet().valueChanged.connect(self.ratio_changed)
#
#         self.profile = ProfileWidget(self, show_info=True)
#         self._update_time = 0
#         # self._fesacomm = fesacomm.FesaComm("BTVDC_865_BTVDIGITAL_1", "LastImage", listener=self)
#         # self._fesacomm = fesacomm.FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)
#         # self._fesacomm.subscribe("")
#
#         # self._listener = Listener(self)
#         # self._listener.start()
#         # self._listener.start()
#
#         self.layout.addWidget(self.respect_ratio)
#         self.layout.addWidget(self.profile)
#         self.setCentralWidget(self.central_widget)
#         self.resize(800, 600)
#
#
#         # self.plot = self.addPlot()
#         # self.spectrum = self.plot.plot()
#         # self.plot.enableAutoRange(pg.ViewBox.XYAxes)
#
#         # Connect the signal
#         self.data_acquired.connect(self.update_data)
#
#         # Make and start the background thread to acquire data
#         # Pass it the signal.emit as the callback function
#         self.threadkill = Event()
#         self.thread = Thread(target=generate_data, args=(self.data_acquired.emit, self.threadkill))
#         self.thread.start()
#
#     # Kill our data acquisition thread when shutting down
#     def closeEvent(self, close_event):
#         self.threadkill.set()
#
#     # Slot to receive acquired data and update plot
#     @pyqtSlot(np.ndarray)
#     def update_data(self, data):
#         # self.spectrum.setData(data)
#         self.profile.set_data(data)
#
#
# if __name__ == '__main__':
#     import sys
#
#     app = QApplication(sys.argv)
#     window = PyQtGraphTest()
#     window.show()
#     if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
#         sys.exit(app.exec_())
