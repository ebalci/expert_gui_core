from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import qtawesome as qta
import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.tools import formatting


class ScalarWidget(QWidget):

    """
    
    Class low-level chart widget displaying multi-arrays
    
    """

    update_signal = pyqtSignal()
    
    def __init__(self, parent, title="", color=None, background=None,unit="",type_format="str"):
        super(QWidget, self).__init__(parent)     

        qta.icon("fa5.clipboard")

        # Layout        
        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(10, 0, 10, 0)
        self.layout.setSpacing(0)   

        # Data & text
        self.type_format = "str"
        self.text = ""
        self.data = 0
        self.label = _CustomLabel(self.text)       

        # Unit and title
        self.labelunit = QLabel(unit)
        self.labeltitle = QLabel(title)
        self.layout.addWidget(self.labeltitle,0,0)
        self.layout.addWidget(self.label,1,0)
        self.layout.addWidget(self.labelunit,2,0)
        self.font = self.label.font()        
        self.label.setAlignment(Qt.AlignCenter)
        self.labelunit.setAlignment(Qt.AlignCenter)
        self.labeltitle.setAlignment(Qt.AlignCenter)
        self.layout.setRowStretch (0,1)
        self.layout.setRowStretch (1,3)
        self.layout.setRowStretch (2,1)        
        self.labeltitle.setStyleSheet("color:rgb(84,190,230);font-weight:bold;")
        self.labelunit.setStyleSheet("color:rgb(160,160,160);font-weight:italic;")
        if background is not None:
            if color is not None:
                self.label.setStyleSheet("background-color:"+background+";color:"+color+";")    
            else:
                self.label.setStyleSheet("background-color:"+background+";")
            self.labeltitle.setStyleSheet("background-color:"+background+";color:rgb(84,190,230);font-weight:bold;")
            self.labelunit.setStyleSheet("background-color:"+background+";color:rgb(160,160,160);font-weight:italic;")
        else:
            if color is not None:
                self.label.setStyleSheet("color:"+color+";")    
        self.update_signal.connect(self.update_data)

    def contextMenuEvent(self, event):
        """Menu event"""
        contextMenu = QMenu(self)
        strAct = contextMenu.addAction("None")
        sciAct = contextMenu.addAction("Scientific")
        hexAct = contextMenu.addAction("HexaDecimal")
        binAct = contextMenu.addAction("Binary")
        dateAct = contextMenu.addAction("Time")
        action = contextMenu.exec_(self.mapToGlobal(event.pos()))
        if action == strAct:
            self.type_format = "str"
        elif action == sciAct:
            self.type_format = "sci"
        elif action == hexAct:
            self.type_format = "hex"
        elif action == binAct:
            self.type_format = "bin"
        elif action == dateAct:
            self.type_format = "date"
        self.label.setText(formatting.format_value_to_string(self.data,type_format=self.type_format))
 
    def dark_(self):
        """Set dark theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")

    def light_(self):
        """Set light theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_WHITE+";")

    def set_data(self, data, update=True, name="default", name_parent="default", *args, **kwargs):
        """Set data in a plot item"""
        self.data = data        
        if update:
            self.update_signal.emit() 
    
    @pyqtSlot()
    def update_data(self):       
        """Update data (text)"""        
        self.label.setText(formatting.format_value_to_string(self.data,type_format=self.type_format))
    

class _CustomLabel(QLabel):

    """
    
    Custom QLabel adjusting font size automatically
    
    """

    def adjustFont(self):
        """Adjust font auto"""
        if self.parent():
            rect = self.parent().rect() & self.geometry()
            width = rect.width()
            height = rect.height()
        else:
            width = self.width()
            height = self.height()
        font = self.font()
        text = self.text()
        while True:
            textWidth = QFontMetrics(font).size(
                Qt.TextSingleLine, text).width()
            textHeight = QFontMetrics(font).size(
                Qt.TextSingleLine, text).height()
            if textHeight >= height or textWidth >= width or font.pointSize() >= 600:
                font.setPointSize(font.pointSize() - 1)
                break
            font.setPointSize(font.pointSize() + 1)
        self._font = font

    def setText(self, text):
        """Set text function"""
        super().setText(text)
        self.adjustFont()

    def resizeEvent(self, event):
        """resize widget"""
        self.adjustFont()

    def paintEvent(self, event):
        """Paint event"""
        if self.text():
            qp = QPainter(self)
            qp.setFont(self._font)
            qp.drawText(self.rect(), self.alignment(), self.text())
        else:
            super().paintEvent(event)


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):        
        """Init user interface"""
        w = QWidget()
        self.scalar = ScalarWidget(w)
        self.scalar.set_data(20000000)
        self.setWindowTitle('Scalar')
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self.scalar, 0, 0)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(300,300)
            
            
if __name__ == '__main__':
    app = QApplication(sys.argv)    
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
