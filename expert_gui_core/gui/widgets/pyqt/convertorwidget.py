from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.common.colors import Colors

class ConvertorWidget(QWidget):

    def __init__(self, parent):

        # inheritance
        super().__init__(parent=parent)

        # init layout
        self.layout = QGridLayout(self)

        self.setWindowTitle("Convertor")

        self.setContentsMargins(0, 0, 0, 0)
        self.setMinimumHeight(100)
        self.setMaximumHeight(100)
       
        self.info = QLabel("click right over text to use convertor options")
        self.info.setAlignment(Qt.AlignHCenter)
        self.layout.addWidget(self.info, 0, 0)
        self.number_widget = numberpanelwidget.NumberPanelWidget(self,title=None,label_name="value to convert",history=False)         
        self.layout.addWidget(self.number_widget, 1, 0)
        self.number_widget.set_data(0)
        f = self.number_widget._textedit_value.font()
        f.setPointSize(20)
        self.number_widget._textedit_value.setFont(f)
        

        self.layout.setRowStretch(0,0)
        self.layout.setRowStretch(1,1)

        # set layout
        self.setLayout(self.layout)

    def dark_(self):
        """
        Set dark theme.
        """
        self.info.setStyleSheet("font-style:italic;color:"+Colors.STR_COLOR_BLUE)
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_LIGHT5GRAY)
        self.number_widget.dark_()
    
    def light_(self):
        """
        Set light theme.
        """
        self.info.setStyleSheet("font-style:italic;color:"+Colors.STR_COLOR_BLUE)
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_LIGHT0GRAY)
        self.number_widget.light_()


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):        
        
        w = QWidget()        
        self.setWindowTitle('Convertor')
        mainLayout = QGridLayout() 
        w.setLayout(mainLayout)
        self.convertor = ConvertorWidget(parent = w)
        self.setCentralWidget(self.convertor)
        self.convertor.dark_()
        self.resize(900,100)
            
if __name__ == '__main__':
    app = QApplication(sys.argv)    
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    # Colors.init_font()
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())
