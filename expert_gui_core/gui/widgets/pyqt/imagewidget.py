from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import pyqtgraph as pg
import numpy as np
import pylab as plt
from functools import partial
import traceback
import qtawesome as qta
import time
import threading
from threading import Timer

from skimage.filters import sobel
from skimage.segmentation import slic
from skimage.measure import regionprops
from skimage.morphology import dilation, area_closing

import scipy.ndimage as ndi
from scipy.ndimage import label, sobel
from skimage.exposure import (histogram,cumulative_distribution, rescale_intensity)
from skimage import img_as_ubyte, img_as_float

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.comm import fesacomm
from expert_gui_core.comm.data import DataObject
from expert_gui_core.gui.widgets.pyqt._imageitem import _ImageItem
from expert_gui_core.gui.widgets.pyqt._plotitem import _PlotItem

from expert_gui_core.gui.widgets.pyqt import datawidget
from expert_gui_core.gui.widgets.pyqt.measurement import profilewidget
from expert_gui_core.gui.widgets.common import togglepanelwidget


class ImageWidget(QWidget):

    """
    
    Class low-level image widget displaying multi-arrays
    
    """
    def __init__(self, parent, name=["default"], palette="default", transpose=False,flip=False,low_res=False,absolute_scale=False,zmin=0,zmax=4096,roi=True):
        super(QWidget, self).__init__(parent)
        
        qta.icon("fa5.clipboard")
        
        self._parent = parent
        self.list_colors = Colors.LIST_COLORS_MENU_IMAGE

        # Layout
        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)  

        # Image
        self._image_layout_widget = {}
        max_col_show = 3
        if len(name) == 4:
            max_col_show = 2
        elif len(name) >= 16:
            max_col_show = 4            
        for i in range(len(name)):
            self._image_layout_widget[name[i]] = _ImageLayoutWidget(
                                                        parent=self,
                                                        transpose=transpose,
                                                        flip=flip,
                                                        low_res=low_res,
                                                        absolute_scale=absolute_scale,
                                                        zmin=zmin,
                                                        zmax=zmax,
                                                        palette=self.list_colors[palette],
                                                        roi=roi)
            self.layout.addWidget(self._image_layout_widget[name[i]],int(i/max_col_show),int(i%max_col_show))

    def add_plot(self, title="", name_parent="default", name='default', dyn_cursor=False, show_value_axis=True, ratio=False, lut=False):
        """Add plot image"""     
        return self._image_layout_widget[name_parent].add_plot(title=title, name=name, dyn_cursor=dyn_cursor, show_value_axis=show_value_axis, ratio=ratio, lut=lut)
        
    def get_plot(self, name_parent="default", name="default"):
        """Get image item using title key"""
        return self._image_layout_widget[name_parent].get_plot(name)        

    def get_image_data_item(self, name_parent="default", name="default"):
        """Get image item using title key"""
        return self._image_layout_widget[name_parent].get_image_data_item(name)

    def set_data(self, data, update=True, raw=False, low_res=False, name_parent="default", name="default", *args, **kwargs):
        """Set data in a plot item"""
        self._image_layout_widget[name_parent].set_data(data, update=update, name=name, raw=raw, low_res=low_res, *args, **kwargs)

    def set_x_range(self, name_parent="default", name="default", xmin=0, xmax=1):
        """Set x min max range"""
        self._image_layout_widget[name_parent].set_x_range(data, name, xmin, xmax)
        
    def set_y_range(self, name_parent="default", name="default", ymin=0, ymax=1):
        """Set y min max range"""
        self._image_layout_widget[name_parent].set_y_range(data, name, ymin, ymax)

    def set_labels(self, name_parent="default", name="default", left="", bottom="", right="", top=""):
        """Set labels around image"""
        self._image_layout_widget[name_parent].set_labels(name, left, bottom, right, top)

    def dark_(self):
        """Set dark theme"""
        for name in self._image_layout_widget.keys():
            self._image_layout_widget[name].dark_()

    def light_(self):
        """Set light theme"""
        for name in self._image_layout_widget.keys():
            self._image_layout_widget[name].light_()

    def set_colormap(self, name_parent="default",colormap="default"):
        """Set colormap for image"""
        color_pal = self.list_colors[colormap]
        self._image_layout_widget[name_parent].set_colormap(colormap=color_pal)

    def get_color_map_list(self):
        """Get list of colormap"""
        return self.list_colors  
    
    def set_filter(self, name_parent="default", name="default",filter=None):
        """Set filter for image"""
        self._image_layout_widget[name_parent].set_filter(filter=filter)
    
    def get_cursor(self, name_parent="default", name="default"):
        """Get the cursor (targetitem) positions"""
        return self._image_layout_widget[name_parent].get_cursor(name)

    def get_mousepressevent(self, name_parent="default", name="default"):
        """Get the cursor (targetitem) positions"""
        return self._image_layout_widget[name_parent].get_mousepressevent(name)

    def change_scale(self,name_parent="default",xscale=1,yscale=1,xoffset=0.,yoffset=0.):
        self._image_layout_widget[name_parent].change_scale(xscale=xscale,yscale=yscale,xoffset=xoffset,yoffset=yoffset)

    def show_lut(self, name_parent="default"):
        """Show/hide LUT"""
        self._image_layout_widget[name_parent].show_lut()

    def show_roi(self, name_parent="default"):
        self._image_layout_widget[name_parent].show_roi()

    def get_plot_item(self, name_parent="default"):
        return self._image_layout_widget[name_parent].get_plot_item()


class _ImageLayoutWidget(pg.GraphicsLayoutWidget):
    
    """
    
    Class low-level image layout widget displaying multi-arrays
    
    """
    
    update_signal = pyqtSignal(str, bool, bool, bool, tuple, dict)
    mutex = threading.Lock()

    def __init__(self,parent=None,transpose=False,flip=False,low_res=False,absolute_scale=False,zmin=0,zmax=4096,palette=["seismic","CET-D1A"],roi=True):
        super().__init__()
        self.ci.layout.setContentsMargins(5, 5, 5, 5)
        self._parent = parent
        self._image_items = {}
        self._plot_items = {}
        self._plot_roi_items = {}
        self._image_roi_items = {}
        self._image_data_items = {}
        self._data = DataObject()
        self._data_frozen = None
        self._data2 = None
        self.filter = None
        self.plt_palette = palette[0]
        self.palette = palette[1]        
        self.transpose = transpose
        self._flip = flip
        self._roi = roi
        self._show_roi = False
        self._show_lut = False
        self._counter = 0
        self._low_res = low_res
        self._raw = False
        self._absolute_scale = absolute_scale
        self._zmax = zmax
        self._zmin = zmin

        self._ratio = {}
        self._pressed = False
        # self.installEventFilter(self)
        self.update_signal.connect(self.update)

    # def eventFilter(self, object, event):
        # if event.type() == 2:
        #     if event.button() == Qt.LeftButton:
        #         self._pressed = not self._pressed
                # self.mouse_ReleaseEvent(event,self._pressed)
        # return False

    # def mouse_ReleaseEvent(self, e, pressed):
    #     for plot_item in self._plot_items.values():
    #         plot_item.mouseReleaseEvent(e,pressed)

    def wheelEvent(self, event):
        """Mouse wheel event"""
        super().wheelEvent(event)        

    def add_plot(self, 
                title=None, 
                name='default', 
                dyn_cursor=False, 
                show_value_axis=True,
                ratio=False,
                lut = False):
        """Add new image"""
        plot_item = _PlotItem(dyn_cursor=True,title=title,image=True,add_region=False)
        dyn_cursor = True

        # Lock or not ratio image
        self._ratio[name] = ratio

        # Default padding
        plot_item.setDefaultPadding(0.)

        # Axis values
        self._show_value_axis = show_value_axis
        plot_item.getAxis("left").setStyle(showValues=show_value_axis)
        plot_item.getAxis("top").setStyle(showValues=show_value_axis)
        plot_item.getAxis("right").setStyle(showValues=show_value_axis)        
        plot_item.getAxis("bottom").setStyle(showValues=show_value_axis)
        if title is not None:
            plot_item.setTitle("<span style='color:rgb(84,190,230);font-weight:bold;font-size:8pt'>"+title+"</span>")

        # Menu options
        menu_ = plot_item.vb.getMenu(None)
        actions_menu = menu_.addMenu('&Actions')
        filters_menu = menu_.addMenu('&Filters')
        palettes_menu = menu_.addMenu('&Palettes')
        for pal in self._parent.list_colors.keys():
            palettes_menu.addAction(pal, partial(self.set_colormap, colormap=Colors.LIST_COLORS_MENU_IMAGE[pal]))        
        filters_menu.addAction("None", partial(self.set_filter, filter=None))        
        filters_menu.addAction("Gauss [3]", partial(self.set_filter, filter="gauss3"))
        filters_menu.addAction("Gauss [5]", partial(self.set_filter, filter="gauss5"))
        filters_menu.addAction("Uniform", partial(self.set_filter, filter="uniform"))
        filters_menu.addAction("Normalize", partial(self.set_filter, filter="normalize"))
        filters_menu.addAction("Norm. & Gauss", partial(self.set_filter, filter="norm&gauss"))
        # filters_menu.addAction("Max Shape", partial(self.set_filter, filter="maxshape"))
        filters_menu.addAction("Median", partial(self.set_filter, filter="median3")) 
        # filters_menu.addAction("Edge", partial(self.set_filter, filter="edge"))
        # filters_menu.addAction("Sharp", partial(self.set_filter, filter="sharp"))
        # filters_menu.addAction("Shape", partial(self.set_filter, filter="shape"))
        # filters_menu.addAction("Segmentation", partial(self.set_filter, filter="segmentation"))
        actions_menu.addAction("Freeze/unfreeze", self.freeze)
        actions_menu.addAction("Transpose", self.set_transpose)
        actions_menu.addAction("Flip", self.set_flip)
        if lut:
            actions_menu.addAction("Show LUT", self.show_lut)
        actions_menu.addAction("Show axis", self.show_axis)
        if self._roi:
            actions_menu.addAction("Show ROI", self.show_roi)
        
        menu_.addAction("Show cursors", self.show_cursors)     

        # Image item
        image_item = _ImageItem(plot_item, name=name,
                                dyn_cursor=dyn_cursor,
                                palette=self.palette,
                                lut=lut)

        image_item.setAutoDownsample(True)

        self.addItem(plot_item, row=0, col=0)
        plot_item.addItem(image_item)

        if image_item._hist is not None:
            self.addItem(image_item._hist,row=0, col=2)

        self._plot_items[name] = plot_item
        self._image_items[name] = image_item

        #ROI
        plot_item_roi = _PlotItem(add_region = False)

        # Default padding
        plot_item_roi.setDefaultPadding(0.)

        # Axis values
        plot_item_roi.getAxis("left").setStyle(showValues=show_value_axis)
        plot_item_roi.getAxis("top").setStyle(showValues=show_value_axis)
        plot_item_roi.getAxis("right").setStyle(showValues=show_value_axis)
        plot_item_roi.getAxis("bottom").setStyle(showValues=show_value_axis)
        if title is not None:
            plot_item_roi.setTitle("<span style='color:rgb(84,190,230);font-weight:bold;font-size:8pt'>"+title+" ROI</span>")

        #Image item ROI
        image_item_roi = _ImageItem(plot_item_roi, name=name+"roi", palette=self.palette, roi=False)

        self._image_items[name].set_image_roi(image_item_roi)
        self.addItem(plot_item_roi, row=0, col=1)
        plot_item_roi.addItem(image_item_roi)
        self._plot_roi_items[name] = plot_item_roi
        self._image_roi_items[name] = image_item_roi
        plot_item_roi.hide()
        #
        self.set_colormap([self.plt_palette,self.palette])

        return image_item

    def freeze(self):
        """
        Freeze current data image and use it to do substraction.
        """
        if self._data_frozen is None:
            self._data_frozen = self._data
            self.setBackground(Colors.COLOR_BLUE)
        else:
            self._data_frozen = None
            if Colors.COLOR_LIGHT:
                self.setBackground(Colors.COLOR_DWHITE)
            else:
                self.setBackground(Colors.COLOR_LBLACK)

    def show_cursors(self):
        """
        Show cursor window.
        """

        for pi in self._plot_items.values():
            pi.show_cursors()
    
    def show_scaling(self):
        """
        Show scaling window.
        """

        for pi in self._plot_items.values():
            pi.show_scaling()

    def get_cursor(self,name='default'):
        """Get the cursor (targetitem) positions"""
        return self._plot_items[name].get_cursor()

    def get_mousepressevent(self,name="default"):
        """Get the mouse press event signal"""
        return self._plot_items[name].get_mousepressevent()

    def show_axis(self):
        """Show/hide axis"""
        self._show_value_axis = not self._show_value_axis
        for plot_item in self._plot_items.values():
            plot_item.getAxis("left").setStyle(showValues=self._show_value_axis)
            plot_item.getAxis("top").setStyle(showValues=self._show_value_axis)
            plot_item.getAxis("right").setStyle(showValues=self._show_value_axis)        
            plot_item.getAxis("bottom").setStyle(showValues=self._show_value_axis)

    def show_roi(self):
        """Show/hide ROI"""
        self._show_roi = not self._show_roi
        for image_item in self._image_items.values():
            image_item.show_roi(self._show_roi)
        for plot_item_roi in self._plot_roi_items.values():
            if self._show_roi:
                plot_item_roi.show()
            else:
                plot_item_roi.hide()
    
    def show_lut(self):
        """Show/hide LUT"""
        self._show_lut = not self._show_lut
        for image_item in self._image_items.values():
            image_item.show_lut(self._show_lut)
        
    def get_plot(self, name="default"):
        """Get image item using title key"""
        return self._image_items[name]

    def get_plot_roi(self, name="default"):
        """Get image roi item using title key"""
        return self._image_roi_items[name]

    def get_image_data_item(self, name="default"):
        """Get image item"""
        return self._image_data_items[name]

    def get_roi(self, name="default"):
        """Get image ROI"""
        return self._image_data_items[name].get_roi()

    def set_data(self, data, update=True, name="default", raw=False, low_res=False, *args, **kwargs):
        """
        Set data in a plot item
        """

        if not update:
            return

        # process data

        try:
            datanp = np.stack(data, axis=0)
            if self._data_frozen is None:
                self._data = datanp
            else:
                if datanp.shape == self._data_frozen.shape:
                    self._data = datanp - self._data_frozen
                else:
                    self._data = datanp
        except:
            self._data = data

        # update !

        if update:
            self.update_data(name, update, raw, low_res, args, kwargs)

    def update_data(self, name='default', update=True, raw=False, low_res=False, args=None, kwargs=None):
        """
        Update the chart component
        """
        self.mutex.acquire()

        self._low_res = low_res
        self._raw = raw

        # data

        check_3d = True
        try:
            le = len(self._data[0][0])
        except:
            check_3d = False

        if not raw:

            try:
                for i in range(1,len(self._data)):
                    if len(self._data[i]) != len(self._data[i-1]):
                        self.mutex.release()
                        return

                if self.transpose:
                    if check_3d:
                        data = np.array(self._data[0], dtype=np.float64)
                    else:
                        data = np.array(self._data, dtype=np.float64)
                else:
                    if check_3d:
                        data = np.array(self._data[0], dtype=np.float64).transpose()
                    else:
                        data = np.array(self._data, dtype=np.float64).transpose()

            except Exception as ex:
                traceback.print_exception(type(ex), ex, ex.__traceback__)
                self.mutex.release()
                return
        else:
            data = self._data

        try:

            # data reduction

            if self._low_res:
                maxline = 100
                nlines = len(data)
                inc = 1
                inc2 = 1
                if nlines > maxline:
                    inc = int(nlines/maxline)
                if len(data[0]) > maxline:
                    inc2 = int(len(data[0]) /maxline)
                if inc != 1 or inc2 != 1:
                    data = data[::inc,::inc2]

            # absolute scaling

            if self._absolute_scale:
                data[0][0] = self._zmax
                data[1][0] = self._zmin

        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)
            self.mutex.release()
            return

        # filtering

        data = self.filter_data(data)

        # plot it !

        try:
            if len(data) == 0:
                self.mutex.release()
                return
        except:
            pass

        self._data2 = data

        self.update_signal.emit(name, update, raw, low_res, args, kwargs)

    @pyqtSlot(str, bool, bool, bool, tuple, dict)
    def update(self, name='default', update=True, raw=False, low_res=False, args=None, kwargs=None):

        if self.get_plot(name=name).get_plot_item()._image:

            wx = np.shape(self._data2)[0]
            wy = np.shape(self._data2)[1]

            if name in self._ratio and self._ratio[name]:
                self.get_plot(name=name).get_plot_item().setAspectLocked(lock=True, ratio=wx / wy)
            self.get_plot(name=name).set_roi_range(xmin=int(wx / 3), ymin=int(wy / 3), xmax=int(2 * wx / 3),
                                                   ymax=int(2 * wy / 3))

        self.get_plot(name=name).setImage(image=self._data2, name=name, *args, **kwargs)

        if self.get_plot(name=name)._image_roi is not None:
            self.get_plot(name=name).update_roi()

        self.mutex.release()

    def change_scale(self,xscale=1,yscale=1,xoffset=0.,yoffset=0.):
        tr = QTransform() 
        tr.scale(xscale, yscale)
        xos = xoffset/xscale
        yos = yoffset/yscale
        tr.translate(xos, yos)
        for name in self._image_items:
            self.get_plot(name=name).setTransform(tr)

    def dilate(self,im,num):
        """
        Performs dilation morphology num times.
        """
        for i in range(num):
            im = dilation(im)
        return im

    def area_close(self,im, num):
        """
        Performs area_closing morphology num times.
        """
        for i in range(num):
            im = area_closing(im)
        return im

    def filter_data(self,data):
        """Filter data on-demand"""
        if self.filter is None:
            return data
        if self.filter == "uniform":
            return ndi.uniform_filter(data, size=11)
        elif self.filter == "gauss5":
            return ndi.gaussian_filter(data, sigma=5)
        elif self.filter == "gauss3":
            return ndi.gaussian_filter(data, sigma=3)
        elif self.filter == "normalize":
            try:
                freq, bins = cumulative_distribution(data)
                target_bins = np.arange(255)
                target_freq = np.linspace(0, 1, len(target_bins))
                ud_vals = np.interp(freq, target_freq, target_bins)
                TS_hc_ud = img_as_ubyte(ud_vals[data.astype(int)].astype(int))
                return TS_hc_ud
            except:
                return data
        elif self.filter == "norm&gauss":
            try:
                im = ndi.gaussian_filter(data, sigma=3)
                freq, bins = cumulative_distribution(im)
                target_bins = np.arange(255)
                target_freq = np.linspace(0, 1, len(target_bins))
                ud_vals = np.interp(freq, target_freq, target_bins)
                TS_hc_ud = img_as_ubyte(ud_vals[im.astype(int)].astype(int))
                return TS_hc_ud
            except:
                return data
        elif self.filter == "maxshape":
            im = ndi.median_filter(data, 3)
            im = ndi.median_filter(im, 3)
            min = np.min(im)
            th = min+0.95*(np.max(im)-min)
            im[im<th] = min
            return im
        elif self.filter == "median3":
            return ndi.median_filter(data, 3)
        elif self.filter == "sharp":
            im_cleaned = self.dilate(data,3)
            im_cleaned = self.area_close(im_cleaned, 25)
            return ndi.median_filter(im_cleaned, size=21)
        elif self.filter == "shape":
            self.filter = None
            data = ndi.gaussian_filter(data, sigma=3)
            mask = data > data.mean()
            label_im, nb_labels = label(mask)
            plt.figure(1)      
            plt.margins(0, 0)
            label_im = label_im*data
            cmap = self.plt_palette    
            if self.palette == "default":
                cmap = "jet"
            plt.imshow(label_im,interpolation='nearest', aspect='auto',cmap=cmap)
            plt.show()            
            return data
        elif self.filter == "edge":
            blurred_data = ndi.gaussian_filter(data, sigma=3)
            sx = sobel(data, axis = 0, mode = 'constant')
            sy = sobel(data, axis = 1, mode = 'constant')
            return np.hypot(sx, sy)
        elif self.filter == "segmentation":
            self.filter = None
            arr = np.zeros(( len(data), len(data[0]), 3 ))
            for i in range(len(data)):
                for j in range(len(data[i])):
                    arr[i][j][0] = data[i][j]
                    arr[i][j][1] = data[i][j]
                    arr[i][j][2] = data[i][j]
            segments = slic(arr,n_segments=100,sigma=4)
            for i in range(1):
                regions = regionprops(segments, intensity_image=arr[:,:,i])
                for r in regions:
                    self.paint_region_with_avg_intensity(arr,r.coords, int(r.mean_intensity), i)
            for i in range(len(arr)):
                for j in range(len(arr[i])):
                    data[i][j] = arr[i][j][0]     
            plt.figure(2)   
            cmap = self.plt_palette                   
            if self.palette == "default":
                cmap = "jet"
            plt.imshow(data,interpolation='nearest', aspect='auto',cmap=cmap)
            plt.show()  
            return data          
        return data

    def paint_region_with_avg_intensity(self,img,rp, mi, channel):
        """Replace image data by avg"""
        for i in range(rp.shape[0]):
            img[rp[i][0]][rp[i][1]][channel] = mi

    def set_x_range(self, name="default", xmin=0, xmax=1):
        """Set x min max range"""
        if xmin >= xmax:
            xmax = xmin + 1
        self._image_items[name].setXRange(xmin, xmax)

    def set_y_range(self, name="default", ymin=0, ymax=1):
        """Set y min max range"""
        if ymin >= ymax:
            ymax = ymin + 1
        self._image_items[name].setYRange(ymin, ymax)

    def set_labels(self, name="default", left="", bottom="", right="", top=""):
        """Set labels around image"""
        self._plot_items[name].setLabels(left=left, bottom=bottom, top=top, right=right)

    def dark_(self):
        """Set dark theme"""
        self.setBackground(Colors.COLOR_LBLACK)
        for image_item in self._image_items.values():
            image_item.dark_()

    def light_(self):
        """Set light theme"""
        self.setBackground(Colors.COLOR_DWHITE)
        for image_item in self._image_items.values():
            image_item.light_()

    def set_colormap(self,colormap=["default","default"]):
        """
        Set colormap.
        """
        
        self.plt_palette = colormap[0]
        self.palette = colormap[1]

        if colormap[1] == "default":
            cmap = Colors.CMAP_IM_DEFAULT
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap) 
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)
        elif colormap[1] == "dbgray":      
            cmap = Colors.CMAP_IM_DBGRAY
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap)
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)  
        elif colormap[1] == "expgray":      
            cmap = Colors.CMAP_IM_EXPGRAY
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap)
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)  
        elif colormap[1] == "dyngray":      
            cmap = Colors.CMAP_IM_DYNGRAY
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap)
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)  
        elif colormap[1] == "hawai":
            cmap = Colors.CMAP_IM_HAWAI
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap)
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)
        elif colormap[1] == "purple":
            cmap = Colors.CMAP_IM_PURPLE
            for image_item in self._image_items.values():
                image_item.setColorMap(cmap)
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(cmap)    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(cmap)
        else:
            for image_item in self._image_items.values():
                image_item.setColorMap(colorMap=colormap[1])
                if image_item._hist is not None:
                    image_item._hist.gradient.setColorMap(image_item.getColorMap())    
            for image_item in self._image_roi_items.values():
                image_item.setColorMap(colorMap=colormap[1])

    def get_image_item(self):
        for image_item in self._image_items.values():
            return image_item

    def get_plot_item(self):
        for plot_item in self._plot_items.values():
            return plot_item

    def set_filter(self, filter=None):
        """Change filter"""
        self.filter = filter
        for name in self._plot_items.keys():
            self.update_data(name, True, self._raw, self._low_res, (), {})

    def set_transpose(self):
        """Transpose image x<->y"""
        self.transpose =  not self.transpose
        for name in self._plot_items.keys():
            bottom = self._plot_items[name].getAxis("bottom").labelText
            left = self._plot_items[name].getAxis("left").labelText
            self._plot_items[name].setLabels(left=bottom, bottom=left) 
            self.update_data(name, True, self._raw, self._low_res, (), {})

    def set_flip(self):
        """Flip image y"""
        self._flip = not self._flip
        for name in self._plot_items.keys():
            self._plot_items[name].invertY(self._flip)
            self.update_data(name, True, self._raw, self._low_res, (), {})


class _Example2(QMainWindow,fesacomm.FesaCommListener):
    
    """
    
    Example class to test
    
    """

    update_signal1 = pyqtSignal(str)
    
    def __init__(self):
        super().__init__()

        self.names = [
            # "BTVDEV.DigiCam.CAM2",
            "TT41.BTV.412350.DigiCam"
        ]            

        self.init_ui()        
        self._update_time1 = 0        
        self._fesacomm1 = fesacomm.FesaComm(self.names[0],"LastImage",listener=self)        
        self._fesacomm1.subscribe("")       
        self.update_signal1.connect(self.update_plot1)
       
    def init_ui(self):                
        """Init user interface"""

        self.image_item = {}
        self.label = {}
        self.plot_data_item = {}

        self._show_left = True  

        self._type = 0x0
        
        central_widget = QWidget()      
        self.layout = QGridLayout(central_widget)
        
        central_widget.setAttribute(Qt.WA_StyledBackground, True)
        central_widget.setStyleSheet('background-color: '+Colors.STR_COLOR_LBLACK+";")
        self.setCentralWidget(central_widget)

        cmap = Colors.CMAP_IM_DEFAULT
        
        glw = pg.GraphicsLayoutWidget()
        # view = glw.addViewBox(enableMenu=True)
        
        self.label[str(1)] = QLabel()
        self.label[str(1)].setFont(QFont('Arial', 8))
        self.label[str(1)].setStyleSheet("padding:0;margin:0;border:none;color: rgb(255,255,255);")
        
        self.image_item[str(1)] = pg.ImageItem()   
        self.image_item[str(1)].setColorMap(cmap)

        self.hist = pg.HistogramLUTItem()
        self.hist.setImageItem(self.image_item[str(1)])

        # plot_item = pg.PlotItem()
        plot_item = glw.addPlot(row=0,col=0)
        plot_item.setDefaultPadding(0.)
        plot_item.getAxis("left").setStyle(showValues=False)
        plot_item.getAxis("top").setStyle(showValues=False)
        plot_item.getAxis("right").setStyle(showValues=False)        
        plot_item.getAxis("bottom").setStyle(showValues=False)
        plot_item.addItem(self.image_item[str(1)])     
        glw.addItem(self.hist)
        
        self.im_widget = ImageWidget(self, name=["NAMECL"])
        self.im_widget.add_plot(name_parent="NAMECL", name="NAMEPI",dyn_cursor=True)
        
        data_widget_opts = {
            "auto":True, 
            "show_image":True,
            "show_chart":True,
            "show_image_value_axis":True,
            "show_chart_value_axis":True,
            "show_chartgl":True,
            "show_chart_grid":True,
            "show_chart_waterfall":False,  
            "show_chartgl_3d":True,
            "show_chartgl_points":False,
            "show_chartgl_surface":True,            
            "show_chartgl_lines":False,     
            "color_amplitude":True
        }

        self.data_widget = datawidget.DataWidget(self,  header=True, **data_widget_opts)

        self.p_widget = profilewidget.ProfileWidget(self, name="TEST",orientation=Qt.AlignLeft, position=Qt.Vertical,ratio=False)

        self.left_widget = QWidget()      

        layoutleft = QGridLayout(self.left_widget)

        layoutleft.addWidget(glw,0,1)
        layoutleft.addWidget(self.im_widget,0,0)
        layoutleft.addWidget(self.data_widget,1,0,1,2)

        self.toggle_panel = togglepanelwidget.TogglePanelWidget(self.left_widget,align="topright",iconshow="chart-line")        
        
        self.toggle_panel.get_signal_show().connect(self.show_left)
        self.toggle_panel.get_signal_hide().connect(self.hide_left)

        self.layout.addWidget(self.toggle_panel,2,0)
        self.layout.addWidget(self.p_widget,2,1)

        layoutleft.setColumnStretch(0,1)
        layoutleft.setColumnStretch(1,1)
        layoutleft.setRowStretch(0,1)
        layoutleft.setRowStretch(1,1)
        
        self.layout.setColumnStretch(0,1)
        self.layout.setColumnStretch(1,1)
        self.layout.setRowStretch(0,0)
        self.layout.setRowStretch(1,1)
        
        panel_filter = QWidget()        
        layout_filter = QGridLayout(panel_filter)
        layout_filter.setContentsMargins(10, 5, 0, 0)
        layout_filter.setSpacing(5)         
                
        self._checkbox_1 = QCheckBox("1")
        self._checkbox_1.setChecked(False)
        self._checkbox_1.setMaximumWidth(18)
        self._checkbox_1.toggled.connect(self.press_cb)
        
        layout_filter.addWidget(self._checkbox_1, 0,0)        
        self._checkbox_2 = QCheckBox("2")
        self._checkbox_2.setChecked(False)
        self._checkbox_2.setMaximumWidth(18)
        self._checkbox_2.toggled.connect(self.press_cb)
        layout_filter.addWidget(self._checkbox_2, 0,1)        
        self._checkbox_3 = QCheckBox("3")
        self._checkbox_3.setChecked(False)
        self._checkbox_3.setMaximumWidth(18)
        self._checkbox_3.toggled.connect(self.press_cb)
        layout_filter.addWidget(self._checkbox_3, 0,2)        
        self._checkbox_4 = QCheckBox("4")
        self._checkbox_4.setChecked(False)
        self._checkbox_4.setMaximumWidth(18)
        self._checkbox_4.toggled.connect(self.press_cb)
        layout_filter.addWidget(self._checkbox_4, 0,3) 

        self._checkbox_1.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")       
        self._checkbox_2.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")       
        self._checkbox_3.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")       
        self._checkbox_4.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";background-color:"+Colors.STR_COLOR_L2BLACK+";"+"border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";")       

        Colors.COLOR_LIGHT = False

        self.data_widget.dark_()
        self.p_widget.dark_()
        self.toggle_panel.dark_()

        self.layout.addWidget(self.label[str(1)],0,0,1,2)         
        self.layout.addWidget(panel_filter,1,0,1,2) 

        self.resize(1600,600)

        self._data1=None

        self._mutex1=False

        self._t1 = 0

    def show_left(self):
        """
        Show image (main) component.
        """
        self._show_left = True  
        self.organize_layout()      
        
    def hide_left(self):
        """
        Hide image (main) component.
        """
        self._show_left = False
        self.organize_layout()      

    def organize_layout(self):
        """
        Re-organize layouts.
        """
        if self._show_left == False:
            self.layout.setColumnStretch(0,0)
        else:
            self.layout.setColumnStretch(0,1)

    def press_cb(self,e):

        ty = 0 

        if self._checkbox_1.isChecked():
            ty = ty+1
        if self._checkbox_2.isChecked():
            ty = ty+2
        if self._checkbox_3.isChecked():
            ty = ty+4
        if self._checkbox_4.isChecked():
            ty = ty+8
        self._type = ty    

    def handle_event(self,name,value):        
        """Handle event dispatcher"""        
        if value['_time'] - self._update_time1 > 50:    
            self._data1 = value["image2D"]                            
            if self._mutex1 == False:
                self._update_time1 = value['_time'] 
                self.update_signal1.emit(name)       
    
    @pyqtSlot(str)        
    def update_plot1(self, name='default'):
        """Update the chart component"""        
        self._mutex1 = True
        dti = time.time() * 1000
        if self._update_time1 != self._t1:
            freq = int(10000./ (self._update_time1-self._t1))/10.
        else:
            self._mutex1 = False
            return
        if (self._type & 0x2) == 0x2:
            self.image_item["1"].setImage(image=self._data1, name="1") 
        if (self._type & 0x1 == 0x1):
            self.im_widget.set_data(self._data1,name="NAMEPI", name_parent="NAMECL")
        if (self._type & 0x4 == 0x4):
            self.data_widget.set_data(self._data1)
        if (self._type & 0x8 == 0x8):
            self.p_widget.set_data(self._data1)
        
        self.label["1"].setText("[ "+self.names[0]+" ]   recv: "+str(int(self._update_time1-self._t1)) + "  " + str(freq) + "Hz    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t1 = self._update_time1
        self._mutex1 = False


class _Example1(QMainWindow,fesacomm.FesaCommListener):
    
    """
    
    Example class to test
    
    """

    update_signal1 = pyqtSignal(str)
    update_signal2 = pyqtSignal(str)
    update_signal3 = pyqtSignal(str)
    update_signal4 = pyqtSignal(str)
    update_signal5 = pyqtSignal(str)
    update_signal6 = pyqtSignal(str)
    update_signal7 = pyqtSignal(str)
    update_signal8 = pyqtSignal(str)
    update_signal9 = pyqtSignal(str)    

    def __init__(self):
        super().__init__()

        self.list_colors = {
            "seismic":["seismic","CET-D1A"],
            "bwr":["bwr","CET-D1"],
            "gray":["gray","CET-L1"],
            "jet":["jet","CET-R4"],
            "hot":["hot","CET-L3"],
            "cividis":["cividis","CET-CBL2"],
            "viridis":["viridis","viridis"],
            "inferno":["inferno","inferno"],
            "plasma":["plasma","plasma"],
            "blues":["Blues","CET-L6"],
            "greens":["Greens","CET-L5"],
            "greys":["Greys","CET-L2"],
            "ylorrd":["YlOrRd","CET-L18"],
        }

        self.names = [
            "TT41.BTV.412442.DigiCam",
            "TT41.BTV.412426.CORE.DigiCam",
            "TT41.BTV.412426.HALO.DigiCam",
            "TT41.BTV.412442.HALO.DigiCam",
            "TT41.VLC4.DigiCam",
            "TT41.VLC3.DigiCam",
            "TCC4.DMD1.DigiCam",
            "TCC4.DMD2.DigiCam",
            "TT41.VLC5.DigiCam"]

        self.init_ui()
        
        self._update_time1 = 0
        self._update_time2 = 0
        self._update_time3 = 0
        self._update_time4 = 0
        self._update_time5 = 0
        self._update_time6 = 0
        self._update_time7 = 0
        self._update_time8 = 0
        self._update_time9 = 0
        
        self._fesacomm1 = fesacomm.FesaComm(self.names[0],"LastImage",listener=self)
        self._fesacomm2 = fesacomm.FesaComm(self.names[1],"LastImage",listener=self)
        self._fesacomm3 = fesacomm.FesaComm(self.names[2],"LastImage",listener=self)
        self._fesacomm4 = fesacomm.FesaComm(self.names[3],"LastImage",listener=self)
        self._fesacomm5 = fesacomm.FesaComm(self.names[4],"LastImage",listener=self)
        self._fesacomm6 = fesacomm.FesaComm(self.names[5],"LastImage",listener=self)
        self._fesacomm7 = fesacomm.FesaComm(self.names[6],"LastImage",listener=self)
        self._fesacomm8 = fesacomm.FesaComm(self.names[7],"LastImage",listener=self)
        self._fesacomm9 = fesacomm.FesaComm(self.names[8],"LastImage",listener=self)
        
        self._fesacomm1.subscribe("")
        self._fesacomm2.subscribe("")
        self._fesacomm3.subscribe("")
        self._fesacomm4.subscribe("")
        self._fesacomm5.subscribe("")
        self._fesacomm6.subscribe("")
        self._fesacomm7.subscribe("")
        self._fesacomm8.subscribe("")
        self._fesacomm9.subscribe("")

        self.update_signal1.connect(self.update_plot1)
        self.update_signal2.connect(self.update_plot2)
        self.update_signal3.connect(self.update_plot3)
        self.update_signal4.connect(self.update_plot4)
        self.update_signal5.connect(self.update_plot5)
        self.update_signal6.connect(self.update_plot6)
        self.update_signal7.connect(self.update_plot7)
        self.update_signal8.connect(self.update_plot8)
        self.update_signal9.connect(self.update_plot9)

    def init_ui(self):        
        """Init user interface"""
        self.data_widget = QWidget()
        self.data_widget1 = QWidget()

        self.image_item = {}
        self.label = {}
        self.plot_data_item = {}
        
        layout = QGridLayout(self)
        
        central_widget = QWidget()      
        central_widget.setAttribute(Qt.WA_StyledBackground, True)
        central_widget.setStyleSheet('background-color: rgb(55, 58, 64);')
        self.setCentralWidget(central_widget)

        cmap = Colors.CMAP_IM_DEFAULT

        for i in range(9): 
            glw = pg.GraphicsLayoutWidget()
            view = glw.addViewBox(enableMenu=False)
            self.label[str(i+1)] = QLabel()
            self.label[str(i+1)].setFont(QFont('Arial', 8))
            self.label[str(i+1)].setStyleSheet("padding:0;margin:0;border:none;color: rgb(255,255,255);")
            self.image_item[str(i+1)] = pg.ImageItem()    
 
            self.image_item[str(i+1)].setColorMap(cmap)

            plot_item = pg.PlotItem()
            plot_item.setDefaultPadding(0.)
            plot_item.getAxis("left").setStyle(showValues=False)
            plot_item.getAxis("top").setStyle(showValues=False)
            plot_item.getAxis("right").setStyle(showValues=False)        
            plot_item.getAxis("bottom").setStyle(showValues=False)
            plot_item.addItem( self.image_item[str(i+1)])            
            view.addItem(plot_item)

            if i < 3:
                layout.addWidget(self.label[str(i+1)],0,i)
                layout.addWidget(glw,1,i)
            elif i < 6:
                layout.addWidget(self.label[str(i+1)],2,i-3)
                layout.addWidget(glw,3,i-3)
            else:
                layout.addWidget(self.label[str(i+1)],4,i-6)
                layout.addWidget(glw,5,i-6)

        central_widget.setLayout(layout)
        self.resize(800,600)
        
        self._data1=None
        self._data2=None
        self._data3=None
        self._data4=None
        self._data5=None
        self._data6=None
        self._data7=None
        self._data8=None
        self._data9=None

        self._mutex1=False
        self._mutex2=False
        self._mutex3=False
        self._mutex4=False
        self._mutex5=False
        self._mutex6=False
        self._mutex7=False
        self._mutex8=False
        self._mutex9=False

        self._t1 = 0
        self._t2 = 0
        self._t3 = 0
        self._t4 = 0
        self._t5 = 0
        self._t6 = 0
        self._t7 = 0
        self._t8 = 0
        self._t9 = 0
        
    def handle_event(self,name,value):        
        """Handle event dispatcher"""        
        if self.names[5] in name:
            if value['_time'] - self._update_time6 > 50:    
                self._data6 = value["image2D"]                            
                if self._mutex6 == False:
                    self._update_time6 = value['_time'] 
                    self.update_signal6.emit(name)
        elif self.names[0] in name:
            if value['_time'] - self._update_time1 > 50:    
                self._data1 = value["image2D"]
                if self._mutex1 == False:
                    self._update_time1 = value['_time'] 
                    self.update_signal1.emit(name)
        elif self.names[1] in name:
            if value['_time'] - self._update_time2 > 50:    
                self._data2 = value["image2D"]            
                if self._mutex2 == False:
                    self._update_time2 = value['_time'] 
                    self.update_signal2.emit(name)
        elif self.names[2] in name:
            if value['_time'] - self._update_time3 > 50:    
                self._data3 = value["image2D"]            
                if self._mutex3 == False:
                    self._update_time3 = value['_time'] 
                    self.update_signal3.emit(name)        
        elif self.names[3] in name:
            if value['_time'] - self._update_time4 > 50:    
                self._data4 = value["image2D"]            
                if self._mutex4 == False:
                    self._update_time4 = value['_time'] 
                    self.update_signal4.emit(name)
        elif self.names[4] in name:
            if value['_time'] - self._update_time5 > 50:    
                self._data5 = value["image2D"]            
                if self._mutex5 == False:
                    self._update_time5 = value['_time'] 
                    self.update_signal5.emit(name)
        elif self.names[6] in name:
            if value['_time'] - self._update_time7 > 50:    
                self._data7= value["image2D"]            
                if self._mutex7 == False:
                    self._update_time7 = value['_time'] 
                    self.update_signal7.emit(name)
        elif self.names[7] in name:
            if value['_time'] - self._update_time8 > 50:    
                self._data8 = value["image2D"]                            
                if self._mutex8 == False:
                    self._update_time8 = value['_time'] 
                    self.update_signal8.emit(name)
        elif self.names[8] in name:
            if value['_time'] - self._update_time9 > 50:    
                self._data9 = value["image2D"]            
                if self._mutex9 == False:
                    self._update_time9 = value['_time'] 
                    self.update_signal9.emit(name)
        
    @pyqtSlot(str)        
    def update_plot1(self, name='default'):
        """Update the chart component"""        
        self._mutex1 = True
        dti = time.time() * 1000
        data = np.array(self._data1, dtype=np.float64).transpose()
        self.image_item["1"].setImage(image=data, name="1")        
        self.label["1"].setText("[ "+self.names[0]+" ]   recv: "+str(int(self._update_time1-self._t1)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t1 = self._update_time1
        self._mutex1 = False

    @pyqtSlot(str)        
    def update_plot2(self, name='default'):
        """Update the chart component"""        
        self._mutex2 = True
        dti = time.time() * 1000
        data = np.array(self._data2, dtype=np.float64).transpose()
        self.image_item["2"].setImage(image=data, name="2")                
        self.label["2"].setText("[ "+self.names[1]+" ]   recv: "+str(int(self._update_time2-self._t2)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t2 = self._update_time2
        self._mutex2 = False

    @pyqtSlot(str)        
    def update_plot3(self, name='default'):
        """Update the chart component"""        
        self._mutex3 = True
        dti = time.time() * 1000
        data = np.array(self._data3, dtype=np.float64).transpose()
        self.image_item["3"].setImage(image=data, name="3")   
        self.label["3"].setText("[ "+self.names[2]+" ]   recv: "+str(int(self._update_time3-self._t3)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t3 = self._update_time3              
        self._mutex3 = False

    @pyqtSlot(str)        
    def update_plot4(self, name='default'):
        """Update the chart component"""        
        self._mutex4 = True
        dti = time.time() * 1000
        data = np.array(self._data4, dtype=np.float64).transpose()
        self.image_item["4"].setImage(image=data, name="4")  
        self.label["4"].setText("[ "+self.names[3]+" ]   recv: "+str(int(self._update_time4-self._t4)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t4 = self._update_time4                           
        self._mutex4 = False

    @pyqtSlot(str)        
    def update_plot5(self, name='default'):
        """Update the chart component"""        
        self._mutex5 = True
        dti = time.time() * 1000
        data = np.array(self._data5, dtype=np.float64).transpose()
        self.image_item["5"].setImage(image=data, name="5")        
        self.label["5"].setText("[ "+self.names[4]+" ]   recv: "+str(int(self._update_time5-self._t5)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t5 = self._update_time5        
        self._mutex5 = False

    @pyqtSlot(str)        
    def update_plot6(self, name='default'):
        """Update the chart component"""        
        self._mutex6 = True
        dti = time.time() * 1000
        data = np.array(self._data6, dtype=np.float64).transpose()
        self.image_item["6"].setImage(image=data, name="6")                
        self.label["6"].setText("[ "+self.names[5]+" ]   recv: "+str(int(self._update_time6-self._t6)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t6 = self._update_time6        
        self._mutex6 = False

    @pyqtSlot(str)        
    def update_plot7(self, name='default'):
        """Update the chart component"""        
        self._mutex7 = True
        dti = time.time() * 1000
        data = np.array(self._data7, dtype=np.float64).transpose()
        self.image_item["7"].setImage(image=data, name="7")                
        self.label["7"].setText("[ "+self.names[6]+" ]   recv: "+str(int(self._update_time7-self._t7)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t7 = self._update_time7        
        self._mutex7 = False

    @pyqtSlot(str)        
    def update_plot8(self, name='default'):
        """Update the chart component"""        
        self._mutex8 = True
        dti = time.time() * 1000
        data = np.array(self._data8, dtype=np.float64).transpose()
        self.image_item["8"].setImage(image=data, name="8")                
        self.label["8"].setText("[ "+self.names[7]+" ]   recv: "+str(int(self._update_time8-self._t8)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t8 = self._update_time8        
        self._mutex8 = False

    @pyqtSlot(str)        
    def update_plot9(self, name='default'):
        """Update the chart component"""        
        self._mutex9 = True
        dti = time.time() * 1000
        data = np.array(self._data9, dtype=np.float64).transpose()
        self.image_item["9"].setImage(image=data, name="9")                
        self.label["9"].setText("[ "+self.names[8]+" ]   recv: "+str(int(self._update_time9-self._t9)) + "    paint: "+ str(int((time.time() * 1000)-dti)))
        self._t9 = self._update_time9        
        self._mutex9 = False


class _Example(QMainWindow):
    
    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        
        self.im_widget = ImageWidget(self, name=["Image1","Image2","Image3","Image4"],roi=False)
        # self.im_widget = ImageWidget(self)
        
        self.im_widget.add_plot(title="MyTitleA", name_parent="Image1", name="PlotItem1")
        self.im_widget.add_plot(title="MyTitleB", name_parent="Image2", name="PlotItem2")
        self.im_widget.add_plot(title="MyTitleC", name_parent="Image3", name="PlotItem3")
        self.im_widget.add_plot(title="MyTitleD", name_parent="Image4", name="PlotItem4")

        self.setCentralWidget(self.im_widget)
        self.resize(800,600)
        self.move(100,300)

        size1 = 500
        size2 = 500
        sigma = 1.
        muu = 0.
        x, y = np.meshgrid(np.linspace(-2, 2, size1), np.linspace(-2, 2, size2))
        dst = np.sqrt(x**2+y**2) 
        
        gauss = np.exp(-((dst-muu)**2 / (2.0 * sigma**2)))
        gauss1 = np.exp(-((dst-muu)**2 / (4.0 * sigma**2)))
        gauss2 = np.exp(-((dst-muu)**2 / (6.0 * sigma**2)))
        gauss3 = np.exp(-((dst-muu)**2 / (0.2 * sigma**2)))

        self.im_widget.set_data(
                gauss,
                name="PlotItem1", 
                name_parent="Image1")

        self.im_widget.set_data(
                gauss1,
                name="PlotItem2", 
                name_parent="Image2")

        self.im_widget.set_data(
                gauss2,
                name="PlotItem3", 
                name_parent="Image3")

        self.im_widget.set_data(
                gauss3,
                name="PlotItem4", 
                name_parent="Image4")

        self.im_widget.dark_()

        self.im_widget.set_colormap(name_parent="Image4",
                                    colormap="dyngray")

        self.im_widget.show_lut(name_parent="Image2")

        self.im_widget.show_roi(name_parent="Image3")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())