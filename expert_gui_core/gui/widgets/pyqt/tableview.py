from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from functools import partial
from natsort import natsort_keygen
import qtawesome as qta
import numpy as np
import pandas as pd
import sys
import sip

from expert_gui_core.tools.formatting import format_value_to_string
from expert_gui_core.gui.common.colors import Colors

# some buggy versions
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)

def getPixelWidthFromQLabel(string, offset=30):
    """Convert to qlabel in order to get the width"""
    w = QLabel(string).fontMetrics().boundingRect(QLabel(string).text()).width() + offset
    return w


class DataFrameModel(QAbstractTableModel):
    """
    DataFrameModel
    """

    def __init__(self, 
                df = pd.DataFrame(), 
                status_list = np.array([]), 
                include_status_list = True, 
                enable_column_options = True, 
                cells_editable = False, 
                parent = None,
                status_colors=None):

        # inherit and init attributes

        super(DataFrameModel, self).__init__()

        # own attributes

        self.parent = parent

        # status row

        self.include_status_list = include_status_list
        self.status_colors=status_colors
        if status_colors is None:
            self.include_status_list = False
        
        # status2d row

        self.enable_column_options = enable_column_options
        self.cells_editable = cells_editable

        # init states

        self.transform_states = {}

        # set df variables

        self.setDataFrame(dataFrame=df, status_list=status_list)
    
    @property
    def df(self):
        """
        Internal df getter.
        """
        return self._df

    @df.setter
    def df(self, dataFrame):
        """
        Internal df setter.
        """        
        self.modelAboutToBeReset.emit()
        self._df = dataFrame
        self.modelReset.emit()

    def setDataFrame(self, dataFrame, status_list = np.array([])):
        """
        Set dafa frame.
        """
        # make an invisible status column in the last column of the dataframe

        if self.include_status_list:
            if status_list.any():
                dataFrame["status_list"] = int(status_list)
            else:
                dataFrame["status_list"] = np.zeros(dataFrame.shape[0],dtype=np.int32)
                    
        # set the dataframe into the model

        self.df = dataFrame
        self._cols = dataFrame.columns
        
        self._pre_dyn_filter_df = None
        self._resort = lambda : None

        # disable filtering to improve the performance

        if self.enable_column_options:
            self._orig_df = None #dataFrame.copy()
        else:
            self._orig_df = None

        # transform columns

        if self.transform_states:
            for col in self.transform_states.keys():
                try:
                    col_ix = self.df.columns.get_loc(col)
                except:
                    continue
                self.transform_type(col_ix = col_ix, conversion_type=self.transform_states[col])

    @pyqtSlot()
    def beginDynamicFilter(self):
        """
        Start the filtering.
        """
        if self._pre_dyn_filter_df is None:
            self._pre_dyn_filter_df = self.df.copy()
        
    @pyqtSlot()
    def endDynamicFilter(self):
        """
        Finish the filtering.
        """
        self._pre_dyn_filter_df = None        

    @pyqtSlot()
    def cancelDynamicFilter(self):
        """
        Cancel the filtering.
        """
        self.df = self._pre_dyn_filter_df.copy()
        self._pre_dyn_filter_df = None
        
    def headerData(self, p_int, orientation, role):
        """
        Show horizontal and vertical headers.
        """
        if not self._df.empty:
            if role == Qt.DisplayRole:
                if orientation == Qt.Horizontal:
                    if self.include_status_list:
                        if p_int < len(self._cols) - 1:
                            return self._cols[p_int]
                    else:
                        if p_int < len(self._cols):
                            return self._cols[p_int]
                elif orientation == Qt.Vertical:
                    return str(self.df.index[p_int])
        return None

    def flags(self, index):
        """
        Override flags function to edit cells.
        """

        # only if cells are editable

        if self.cells_editable:

            # set offset

            if self.include_status_list:
                off = 1
            else:
                off = 0

            # set flags

            if index.column() < len(self._cols) - off:
                return super(DataFrameModel, self).flags(index) | Qt.ItemIsEditable

        return super(DataFrameModel, self).flags(index)

    def setData(self, index, value, role=Qt.EditRole):
        """
        Override setData function to edit cells.
        """

        # only if cells are editable

        if self.cells_editable:

            # only if it is valid

            if index.isValid() and role == Qt.EditRole:

                # get row and col

                row = index.row()
                col = index.column()

                # set offset

                if self.include_status_list:
                    off = 1
                else:
                    off = 0

                # avoid last column (status list)

                if col < len(self._cols) - off:
                    try:
                        value = pd.Series(value).astype(self._orig_df[self.df.columns[col]].dtype).iloc[0]
                    except Exception as xcp:
                        print("Exception when editing cell (type error): {}".format(xcp))
                        return False
                    self.df.iloc[row, col] = value
                    self._orig_df.iloc[row, col] = value
                    self.dataChanged.emit(index, index)
                    return True

        return False

    def copy_cells(self, orientation, indices):
        """
        Copy cells function to copy whole rows or columns.
        """
        if orientation == Qt.Horizontal:
            return [self.df.iloc[:, i].tolist() for i in indices]
        elif orientation == Qt.Vertical:
            return [self.df.iloc[i].tolist() for i in indices]

    def paste_cells(self, orientation, indices, values):
        """
        Paste cells function to paste whole rows or columns.
        """
        if len(values) != len(indices):
            values = values * (len(indices) // len(values)) + values[:len(indices) % len(values)]
        if orientation == Qt.Horizontal:
            for i, index in enumerate(indices):
                if self.df.shape[0] != len(values[i]):
                    continue
                self.df.iloc[:, index] = values[i]
                self._orig_df.iloc[:, index] = values[i]
        elif orientation == Qt.Vertical:
            for i, index in enumerate(indices):
                if self.df.shape[1] != len(values[i]):
                    continue
                self.df.iloc[index] = values[i]
                self._orig_df.iloc[index] = values[i]
        self.dataChanged.emit(QModelIndex(), QModelIndex())        

    def data(self, index, role=Qt.DisplayRole):
        """
        Get data only if index makes sense.
        """

        # only if index makes sense
        
        if index.isValid():

            # get row and col

            row = index.row()
            col = index.column()

            # set off

            if self.include_status_list:
                off = 1
            else:
                off = 0

            # avoid last column (status list)

            if col < len(self._cols) - off:

                # coloring

                if role == Qt.BackgroundRole:
                    
                    try:
                        
                        # row coloring
                        
                        if self.df.iloc[row,col] == np.nan:
                            return QBrush(Colors.COLOR_TRANSPARENT)
                        else:
                            
                            # row bg coloring
                            
                            if self.include_status_list:
                                if self.df["status_list"].iloc[row] != 0:
                                    if str(self.df["status_list"].iloc[row]) in self.status_colors:                                
                                        color_cell = self.status_colors[str(self.df["status_list"].iloc[row])][1]
                                        return QBrush(color_cell)
                                                        
                    except:
                        pass

                # coloring

                elif role == Qt.ForegroundRole:
                    try:

                        if self.df.iloc[row,col] == np.nan:
                            return QBrush(Colors.COLOR_TRANSPARENT)
                        else:

                            # row fg color

                            if self.include_status_list:                            
                                if self.df["status_list"].iloc[row] != 0:
                                    if str(self.df["status_list"].iloc[row]) in self.status_colors:                                
                                        color_cell = self.status_colors[str(self.df["status_list"].iloc[row])][0]
                                        return QBrush(color_cell)                            
                           
                    except:
                        pass

                # display data

                elif role == Qt.DisplayRole:
                    if str(self.df.iloc[row, col]) != "nan":
                        return str(self.df.iloc[row, col])
                    else:
                        return ""

                # center the text of the cells

                elif role == Qt.TextAlignmentRole:
                    return Qt.AlignCenter

                # font size

                elif role == Qt.FontRole:
                    # if col == 0:
                    #     font = QFont()
                    #     font.setBold(True)
                    #     return font
                    # else:
                    return QFont()

                # icon show
                # ex : https://www.pythonguis.com/tutorials/pyside-qtableview-modelviews-numpy-pandas/
                # elif role == Qt.DecorationRole:
                #     value = self._data[index.row()][index.column()]
                #     if isinstance(value, datetime):
                #         return QIcon('calendar.png')

        return None

    def rowCount(self, index=QModelIndex()):
        """
        Get row count.
        """
        return self.df.shape[0]

    def columnCount(self, index=QModelIndex()):
        """
        Get column count.
        """
        if self.include_status_list:
            return self.df.shape[1] - 1
        else:
            return self.df.shape[1]

    def transform_type(self, col_ix, conversion_type):
        """
        This function transforms the type of the column (e.g. to hexadecimal).
        """

        # column out of bounds

        if col_ix >= self.df.shape[1]:
            return

        # get column

        col = self.df.columns[col_ix]

        # all type conversions

        self.layoutAboutToBeChanged.emit()
        self._orig_df=self.df.copy()
        try:
            if conversion_type == "hexadecimal":
                self.df[col] = self._orig_df[col].apply(lambda x: format_value_to_string(x, type_format="hex"))
            elif conversion_type == "scientific":
                self.df[col] = self._orig_df[col].apply(lambda x: format_value_to_string(x, type_format="sci"))
            elif conversion_type == "binary":
                self.df[col] = self._orig_df[col].apply(lambda x: format_value_to_string(x, type_format="bin"))
            elif conversion_type == "time":
                self.df[col] = self._orig_df[col].apply(lambda x: format_value_to_string(x, type_format="date"))
            elif conversion_type == "original":
                self.df[col] = self._orig_df[col]
        except Exception as xcp:
            print("Cannot convert column {} into {} due to: {}".format(col, conversion_type, xcp))
       
        self.layoutChanged.emit()

        # resort in case we changed the sorting previously

        self._resort()

        # save state

        self.transform_states[col] = conversion_type

        return

    def sort(self, col_ix, order = Qt.AscendingOrder, use_natsort = False):
        """Sort table"""

        # column out of bounds

        if col_ix >= self.df.shape[1]:
            return

        # sort the dataframe
        
        self.layoutAboutToBeChanged.emit()
        ascending = True if order == Qt.AscendingOrder else False
        if use_natsort:
            self._orig_df=self.df.copy()
            self.df = self.df.sort_values(self.df.columns[col_ix], ascending=ascending, key=natsort_keygen())
        else:
            self._orig_df=self.df.copy()
            self.df = self.df.sort_values(self.df.columns[col_ix], ascending=ascending)
        self.layoutChanged.emit()

        # set sorter to current sort (for future filtering)

        self._resort = partial(self.sort, col_ix, order)

    def filter(self, col_ix, needle, method="list_comprehension"):
        """
        Filter data.
        """

        # get latest filtered dataframe

        if self._pre_dyn_filter_df is not None:
            df = self._pre_dyn_filter_df.copy()
        else:
            df = self.df

        # get column

        col = df.columns[col_ix]

        # convert needle to lowercase

        needle = str(needle).lower()

        # do the filtering (list comprehension works faster than pandas)

        if method=="list_comprehension":
            filtered_vals = [needle in str(s).lower() for s in df[col]]
        else:
            filtered_vals = df[col].str.contains(needle, case=False, regex=False)

        # update dataframe
        
        self._orig_df=self.df.copy()
        self.df = df[filtered_vals]

        # resort in case we changed the sorting previously

        self._resort()

    def reset(self):
        """
        Reset everything.
        """
        if self._orig_df is not None:
            self.df = self._orig_df.copy()
        self._resort = lambda: None
        self._pre_dyn_filter_df = None
        
    def refresh_table(self):
        """
        Refresh table.
        """
        self.layoutChanged.emit()


class DynamicFilterMenuAction(QWidgetAction):
    """
    Class DynamicFilterMenuAction.
    """

    def __init__(self, parent, menu, col_ix):

        # inherit from parent

        super(DynamicFilterMenuAction, self).__init__(parent)

        # save parent menu

        self.parent_menu = menu

        # build the widgets and bind the signals

        widget = QWidget()
        layout = QHBoxLayout()
        self.label = QLabel('Filter')
        self.text_box = DynamicFilterLineEdit()
        self.text_box.bind_dataframewidget(self.parent(), col_ix)
        self.text_box.returnPressed.connect(self._close_menu)
        layout.addWidget(self.label)
        layout.addWidget(self.text_box)
        widget.setLayout(layout)
        self.setDefaultWidget(widget)

    def _close_menu(self):
        """
        Close the menu.
        """
        self.parent_menu.close()
        

class DynamicFilterLineEdit(QLineEdit):
    """
    Class DynamicFilterLineEdit.
    """

    def __init__(self, *args, **kwargs):

        # attributes

        self._always_dynamic = kwargs.pop('always_dynamic', False)
        super(DynamicFilterLineEdit, self).__init__(*args, **kwargs)
        self.col_to_filter = None
        self._orig_df = None
        self._host = None

    def bind_dataframewidget(self, host, col_ix):
        """
        Bind this class to the dataframe table column.
        """
        self.host = host
        self.col_to_filter = col_ix
        self.textChanged.connect(self._update_filter)

    @property
    def host(self):
        """
        Internal host getter.
        """
        if self._host is None:
            raise RuntimeError("Must call bind_dataframewidget() before use.")
        else:
            return self._host
        
    @host.setter
    def host(self, value):
        """
        Internal host setter
        """
        if not isinstance(value, DataFrameWidget):
            raise ValueError("Must bind to a DataFrameWidget, not %s" % value)
        else:
            self._host = value
        if not self._always_dynamic:
            self.editingFinished.connect(self._host._data_model.endDynamicFilter)
        
    def focusInEvent(self, QFocusEvent):
        """
        Focus in event
        """
        self._host._data_model.beginDynamicFilter()
        
    def _update_filter(self, text):
        """
        Update the data filter.
        """
        col_ix = self.col_to_filter
        self.host.filter(col_ix, text)


class DataFrameWidget(QTableView):
    """
    Class DataFrameWidget.
    """

    # signals

    dataFrameChanged = pyqtSignal()

    def __init__(self, 
                parent=None, 
                df=pd.DataFrame(), 
                status_list = np.array([]), 
                external_data_model = None, 
                show_vertical_index=True, 
                show_horizontal_headers=True, 
                always_show_vertical_scroll_bar=False, 
                resize_offsets=[20], 
                include_status_list = True, 
                enable_column_options = True, 
                auto_resizing = True, 
                selection_mode = "items", 
                cells_editable = False,
                status_colors=None,
                include_status2d_list = False, 
                status2d_list = np.array([]), 
            ):

        # inherit from qtableview

        super(DataFrameWidget, self).__init__(parent)

        # main attributes

        self.parent = parent

        # color row

        self.status_list = status_list
        self.status_colors = status_colors

        # color cell

        self.status2d_list = status2d_list
        
        # own attributes

        self._data_model = None
        self.last_stored_width = 0
        self.header_labels = []
        self.header_labels_widths = []
        self.show_vertical_index = show_vertical_index
        self.show_horizontal_headers = show_horizontal_headers
        self.always_show_vertical_scroll_bar = always_show_vertical_scroll_bar
        self.resize_offsets = resize_offsets

        self.include_status_list = include_status_list
        if status_colors is None:
            self.include_status_list = False
        self.status_colors = status_colors
        
        self.include_status2d_list = include_status2d_list
        if status_colors is None:
            self.include_status2d_list = False
        
        self.enable_column_options = enable_column_options
        self.auto_resizing = auto_resizing
        self.selection_mode = selection_mode
        self.cells_editable = cells_editable

        # for copying rows and columns

        self.clipboard_orientation = None
        self.clipboard = None

        # load new attributes

        self.initHeaders(df)

        # set up model

        self.setDataModel(external_data_model)

        # tableview typical configuration

        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Plain)
        self.setDragEnabled(False)
        self.setAlternatingRowColors(False)
        self.setFocusPolicy(Qt.ClickFocus)
        self.setShowGrid(True)
        self.setGridStyle(Qt.SolidLine)
        self.setObjectName("DataFrameWidget")
        self.horizontalHeader().setHighlightSections(False)
        self.horizontalHeader().setDefaultAlignment(Qt.AlignCenter)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Interactive)
        self.horizontalHeader().setStretchLastSection(True)
        self.horizontalHeader().setVisible(show_horizontal_headers)
        self.horizontalHeader().setFixedHeight(30)
        self.verticalHeader().setHighlightSections(False)
        self.verticalHeader().setMinimumSectionSize(25)
        self.verticalHeader().setVisible(show_vertical_index)
        self.verticalHeader().setDefaultSectionSize(25)
        self.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # to edit or not

        if not self.cells_editable:
            self.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # set selection mode

        if self.selection_mode == "rows":
            self.setSelectionMode(QAbstractItemView.SingleSelection)
            self.setSelectionBehavior(QAbstractItemView.SelectRows)
        elif self.selection_mode == "items":
            self.setSelectionMode(QAbstractItemView.ExtendedSelection)
            self.setSelectionBehavior(QAbstractItemView.SelectItems)

        # always show vertical scroll bar to avoid resize problems

        if self.always_show_vertical_scroll_bar:
            self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        # resize columns

        self.resizeColumnsCustom()

        # bind widgets

        self.bindWidgets()

    def initHeaders(self, df):
        """
        Initialize header.
        """

        # get labels and widths

        self.header_labels = df.columns.tolist()
        self.header_labels = [str(i) for i in self.header_labels]
        self.header_labels_widths = [getPixelWidthFromQLabel(i, offset=20) for i in self.header_labels]

    @property
    def df(self):
        """
        Internal df getter.
        """
        
        return self._data_model.df

    def setDataFrame(self, df, status_list = np.array([]), status2d_list=np.array([])):
        """
        Set dataframe.
        """

        # load new attributes

        self.initHeaders(df)

        # set new data

        self._data_model.setDataFrame(df, status_list = status_list)

        # resize columns

        self.resizeColumnsCustom()

    def setDataModel(self, external_data_model = None):
        """
        Set data model.
        """

        # set up model

        if external_data_model:
            self._data_model = external_data_model
        else:
            self._data_model = DataFrameModel(df=pd.DataFrame(), 
                                              status_list=self.status_list, 
                                              include_status_list = self.include_status_list, 
                                              enable_column_options = self.enable_column_options, 
                                              cells_editable = self.cells_editable,
                                              status_colors=self.status_colors
                                            )
        self.setModel(self._data_model)

    def bindWidgets(self):
        """
        Bind widget.
        """

        # binding at model reset

        self._data_model.modelReset.connect(self.dataFrameChanged)

        # menu bindings

        if self.enable_column_options:
            self.horizontalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
            self.horizontalHeader().customContextMenuRequested.connect(self._header_menu)

    def _header_menu(self, pos):
        """
        Header menu creation.
        """

        # init menu

        menu = QMenu(self)

        # get column header index

        col_ix = self.horizontalHeader().logicalIndexAt(pos)

        # out of bounds

        if col_ix == -1:
            return

        # menu actions

        if self.show_horizontal_headers:
            menu.addAction(DynamicFilterMenuAction(self, menu, col_ix))
            menu.addSeparator()
            menu.addAction(qta.icon("mdi.sort-ascending"), "Sort - Ascending", partial(self._data_model.sort, col_ix=col_ix, order=Qt.AscendingOrder))
            menu.addAction(qta.icon("mdi.sort-ascending"), "NatSort - Ascending [SLOWER]", partial(self._data_model.sort, col_ix=col_ix, order=Qt.AscendingOrder, use_natsort=True))
            menu.addAction(qta.icon("mdi.sort-descending"), "Sort - Descending", partial(self._data_model.sort, col_ix=col_ix, order=Qt.DescendingOrder))
            menu.addAction(qta.icon("mdi.sort-descending"), "NatSort - Descending [SLOWER]", partial(self._data_model.sort, col_ix=col_ix, order=Qt.DescendingOrder, use_natsort=True))
            menu.addSeparator()
            menu.addAction(qta.icon("mdi.format-text"), "Convert to hexadecimal", partial(self._data_model.transform_type, col_ix=col_ix, conversion_type="hexadecimal"))
            menu.addAction(qta.icon("mdi.format-text"), "Convert to scientific", partial(self._data_model.transform_type, col_ix=col_ix, conversion_type="scientific"))
            menu.addAction(qta.icon("mdi.format-text"), "Convert to binary", partial(self._data_model.transform_type, col_ix=col_ix, conversion_type="binary"))
            menu.addAction(qta.icon("mdi.format-text"), "Convert to time", partial(self._data_model.transform_type, col_ix=col_ix, conversion_type="time"))
            menu.addAction(qta.icon("mdi.format-text"), "Convert to original", partial(self._data_model.transform_type, col_ix=col_ix, conversion_type="original"))
            menu.addSeparator()
            menu.addAction(qta.icon("mdi.autorenew"), "Reset", self._data_model.reset)

        # show the menu

        menu.exec_(self.mapToGlobal(pos))

    def contextMenuEvent(self, event):

        # get row and column

        row_ix = self.rowAt(event.y())
        col_ix = self.columnAt(event.x())

        # out of bounds

        if row_ix < 0 or col_ix < 0:
            return

        # menu that appears when clicking a cell of the table

        menu = QMenu(self)
        menu = self.cellTableMenuHandler(menu, row_ix, col_ix)
        menu.exec_(self.mapToGlobal(event.pos()))

    def cellTableMenuHandler(self, menu, row_ix, col_ix):
        """
        Create menu per cell.
        """

        # init dict

        self.cell_menu_right_click_dict = {}

        # actions of the cell menu

        self.cell_menu_right_click_dict["export_to_csv"] = menu.addAction(qta.icon("fa5s.file-csv"), self.tr("Export table to CSV"))
        self.cell_menu_right_click_dict["export_to_csv"].triggered.connect(lambda: self.exportTable(export_type="csv", qta_icon="fa5s.file-csv"))
        self.cell_menu_right_click_dict["copy_to_clipboard"] = menu.addAction(qta.icon("fa5.clipboard"), self.tr("Copy to clipboard"))
        self.cell_menu_right_click_dict["copy_to_clipboard"].triggered.connect(lambda: self.copySelectionToClipboard())

        return menu

    def exportTable(self, export_type = "csv", qta_icon = "fa5s.file-csv", verbose = True):
        """
        Export table to something (ex:csv).
        """

        # typical try except workflow

        try:

            # csv case

            if export_type == "csv":

                # get name

                name, _ = QFileDialog.getSaveFileName(self, "Save file as CSV", filter="CSV(*.csv)")

                # return and exit if user did not introduce any name

                if not name:
                    return

                # add .csv in case user did not add it

                if name[-4:].lower() != ".csv":
                    name = name + ".csv"

                # for debugging

                if verbose:
                    print("Exporting table to CSV...")

                # save dataframe to csv

                self.df.to_csv(name, index=False)

            # show success message

            message_title = "Successful export!"
            message_text = ("Table has been successfully exported to the following path: {}".format(name))
            message_box = QMessageBox(QMessageBox.Information, message_title, message_text, parent=self)
            message_box.setWindowIcon(qta.icon(qta_icon))
            message_box.exec_()

        # throw some error

        except Exception as xcp:

            # show error message

            message_title = "Error"
            message_text = ("Unable to export table due to: {}".format(xcp))
            message_box = QMessageBox(QMessageBox.Critical, message_title, message_text, parent=self)
            message_box.setWindowIcon(qta.icon(qta_icon))
            message_box.exec_()

    def resizeColumnsCustom(self):
        """
        Resize column table.
        """

        # do not show grey header if there is no data

        if self.df.empty or not self.show_horizontal_headers:
            self.horizontalHeader().setVisible(False)
        else:
            self.horizontalHeader().setVisible(True)

        # resize columns based on the header labels

        if self.header_labels and self.header_labels != ["status_list"]:
            if "status_list" in self.header_labels:
                length_headers = len(self.header_labels) - 1
            else:
                length_headers = len(self.header_labels)
            if self.show_vertical_index:
                minimum_width = int((self.frameGeometry().width() - self.verticalHeader().width() - self.resize_offsets[0]) / length_headers)
            else:
                minimum_width = int((self.frameGeometry().width() - self.resize_offsets[0]) / length_headers)
            for c in range(0, len(self.header_labels)):
                if self.header_labels_widths[c] > minimum_width:
                    width_to_set = self.header_labels_widths[c]
                else:
                    width_to_set = minimum_width
                self.setColumnWidth(c, width_to_set)

    def copySelectionToClipboard(self):
        """
        Function to copy the selection text to the clipboard.
        """

        # get selection

        selection = self.selectedIndexes()
        if not selection:
            return

        # get rows and cols

        selected_rows = set(index.row() for index in selection)
        selected_columns = set(index.column() for index in selection)

        # get all data

        table_data = []
        for row in sorted(selected_rows):
            row_data = []
            for column in sorted(selected_columns):
                index = self.model().index(row, column)
                row_data.append(self.model().data(index))
            table_data.append(row_data)

        # copy to clipboard

        clipboard = QGuiApplication.clipboard()
        text = ' '.join(' '.join(str(cell) for cell in row) for row in table_data)
        clipboard.setText(text)

    def resizeEvent(self, event):
        """
        Resize table when resizing the widget.
        """
        if self.auto_resizing:
            if self._data_model:
                stored_width = self.frameGeometry().width()
                if np.abs(stored_width - self.last_stored_width) > 0:
                    self.resizeColumnsCustom()
                    self.show()
                    self.last_stored_width = stored_width

        # resize event

        QTableView.resizeEvent(self, event)

    def copy_cells(self):
        """
        Copy cells function to copy whole rows or columns.
        """
        index = self.currentIndex()
        if not index.isValid():
            return
        if self.selectionModel().hasSelection():
            selected = self.selectionModel().selectedColumns() or self.selectionModel().selectedRows()
            if len(selected) >= 1:
                self.clipboard_orientation = self.selectionModel().selectedColumns() and Qt.Horizontal or Qt.Vertical
                indices = [i.column() if self.clipboard_orientation == Qt.Horizontal else i.row() for i in
                           selected]
                self.clipboard = self.model().copy_cells(self.clipboard_orientation, indices)
        
    def paste_cells(self):
        """
        Paste cells function to paste whole rows or columns.
        """
        if self.clipboard:
            if not self.clipboard:
                return
            if self.selectionModel().hasSelection():
                selected = self.selectionModel().selectedColumns() or self.selectionModel().selectedRows()
                if len(selected) >= 1:
                    self.clipboard_orientation = self.selectionModel().selectedColumns() and Qt.Horizontal or Qt.Vertical
                    indices = [i.column() if self.clipboard_orientation == Qt.Horizontal else i.row() for i in
                               selected]
                    self.model().paste_cells(self.clipboard_orientation, indices, self.clipboard)

    def keyPressEvent(self, event):
        """
        Handle key press events.
        """
        if event.matches(QKeySequence.Copy):
            self.copy_cells()
        elif event.matches(QKeySequence.Paste):
            self.paste_cells()
        else:
            super().keyPressEvent(event)

    def filter(self, col_ix, needle):
        """
        filter data.
        """
        return self._data_model.filter(col_ix, needle)