from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import pyqtgraph as pg
import numpy as np
import qtawesome as qta
import sys
import statistics as stat
import time

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.widgets.pyqt import chartwidget


class StatWidget(QWidget):

    """
    
    Statistic widget
    Histogram plot + standard stat data (avg stddev min max ntps)
    
    """

    update_signal = pyqtSignal()

    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)

        qta.icon("fa5.clipboard")
                
        self._data = {}
        self._values = {
            "npts":0.,
            "min":0.,
            "max":0.,
            "avg":0.,
            "std":0.,
            "std(%)":0.,
            "delta":1000000000,
            "sum":0.
        }
        self._units = {
            "npts":None,
            "min":None,
            "max":None,
            "avg":None,
            "std":None,
            "std(%)":"%",
            "delta":None,
            "sum":None
        }
        self._minHisto = {}
        self._maxHisto = {}
        self._sizeHisto = {}
        self._updatetime = 0
        self.layout = QGridLayout(self)        
        
        self.font_textbig = QFont()
        # qta.icon("fa5.clipboard")
        
        self._enum_value_name = QComboBox()        
        self._list_value_name = QListView(self._enum_value_name)
        self._enum_value_name.setView(self._list_value_name)
        self._enum_value_name.setEditable(True)
        self._enum_value_name.lineEdit().setAlignment(Qt.AlignLeft)   
        self._enum_value_name.lineEdit().setDisabled(True)
        self._enum_value_name.lineEdit().setReadOnly(True)
        self.layout.addWidget(self._enum_value_name, 0, 0,1,2)

        self._chart_widget = chartwidget.ChartWidget(self)
        self._chart_widget.add_plot(title="Histogram",  dyn_cursor=True, show_grid=True,add_region=False)        
        self.layout.addWidget(self._chart_widget, 1, 0)
        self._statinfopanel = _StatInfoPanel(self,self._values,self._units)
        self.layout.addWidget(self._statinfopanel, 1, 1)

        self._statinfopanel.setMinimumWidth(320)
        self._statinfopanel.setMaximumWidth(400)
        self._statinfopanel.set_minimum_width_name(60)

        self.update_signal.connect(self.update_data)

        self.resize(1000,370)

        self._enum_value_name.currentTextChanged.connect(self.change)

        self.set_font_size(9)
        
    def set_font_size(self, size=8):
        """Change font size"""
        pass
        
    def set_data(self, data, name="default", x=None, min=None, max=None, size=100):
        """Set data to calculate statistics"""
        if name not in self._data:
            self._enum_value_name.addItem(name)
        self._data[name]=data       
        self._minHisto[name]=min
        self._maxHisto[name]=max
        self._sizeHisto[name]=size

        # Calculate evrything !
        dt = self.current_milli_time() - self._updatetime 
        if dt >= 1000:
            self.calculate()
            self._updatetime = self.current_milli_time()
       
    def current_milli_time(self):
        """Return time in millisecond"""
        return round(time.time() * 1000)

    def calculate(self):
        """Do the calculation and update widget"""

        # Only if visible (optimization)

        if self.isVisible(): 
            name = self._enum_value_name.currentText()
            if name == "":
                return     
            size = self._sizeHisto[name]
            try:
                data = self._data[name]

                # Number of points
                self._values["npts"] = len(data)

                # Case List

                # Min
                self._values["min"] = data.min()
                
                # Max
                self._values["max"] = data.max()
                
                 # Sum
                self._values["sum"] = sum(data)                

                # Min max used by histogram calculation
                if self._minHisto[name] == None:
                    self._minHisto[name]=self._values["min"]
                if self._maxHisto[name] == None:
                    self._maxHisto[name]=self._values["max"]
                
                # Average 
                self._values["avg"] = data.mean(axis=0)
                
                # Std dev
                self._values["std"] = data.std(axis=0)
                
                # Std dev / avg percent
                if self._values["avg"] != 0.:
                    self._values["std(%)"] = 100.*self._values["std"]/self._values["avg"]
                else:
                    self._values["std(%)"] = 0.                
            except:

                # Case ndarray 

                # Min
                self._values["min"] = min(data)

                # Max
                self._values["max"] = max(data)            
                
                # Min max used by histogram calculation
                if self._minHisto[name] == None:
                    self._minHisto[name]=self._values["min"]
                if self._maxHisto[name] == None:
                    self._maxHisto[name]=self._values["max"]
                
                if size > 0:

                    # Average
                    self._values["avg"] = sum(data) / len(data)
                    
                    # Std dev
                    self._values["std"] = stat.stdev(data)
                    
                    # Std dev / avg percent 
                    if self._values["avg"] != 0.:
                        self._values["std(%)"] = 100.*self._values["std"]/self._values["avg"]
                    else:
                        self._values["std(%)"] = 0.
                else:

                    # Case exception
                    self._values["avg"] = 0
                    self._values["std"] = 0
                    self._values["std(%)"] = 0

            # Delta X value (histogram bar width)
            if size > 1:
                self._values["delta"] = (self._maxHisto[name]-self._minHisto[name])/(size)                     
            else:
                self._values["delta"] = 1000000000
            
            # Update data !

            self.update_signal.emit()       

    @pyqtSlot()
    def update_data(self):
        """Calculate histogram and update info panel and plot"""
        name = self._enum_value_name.currentText()     
        self._statinfopanel.update_data()
        self.hist, self.bins = np.histogram(self._data[name], bins = np.arange(self._minHisto[name],self._maxHisto[name],self._values["delta"]))            
        try:
            self.bins=np.delete(self.bins,len(self.bins)-1)
        except:
            self.bins = self.bins.pop(len(self.bins)-1)
        self.update_plot(name)        
    
    def update_plot(self,name="default"):
        """Update plot chart + update plot marker"""
        self._chart_widget.set_data(self.hist, 
                                    datax=self.bins,
                                    update=True, 
                                    type_plotitem="h", 
                                    name="histo", 
                                    pen=pg.mkPen({'color': Colors.COLOR_LIGHTORANGE, 'width': 1}), 
                                    brush=Colors.COLOR_LIGHTORANGE, 
                                    skipFiniteCheck=True, 
                                    antialias=False, 
                                    autoDownSample=True)
        self._chart_widget.add_marker_line(name="avg",pos=self._values["avg"],pen=Colors.COLOR_LPINK,movable=False,label="avg",font_size=8)
        self._chart_widget.add_marker_line(name="avg+",pos=self._values["avg"]+self._values["std"],pen=Colors.COLOR_LPINKT,movable=False)
        self._chart_widget.add_marker_line(name="avg-",pos=self._values["avg"]-self._values["std"],pen=Colors.COLOR_LPINKT,movable=False)

    def dark_(self):
        """Set dark theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self._enum_value_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._list_value_name.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._chart_widget.dark_()
        self._statinfopanel.dark_()
        self.calculate()

    def light_(self):
        """Set light theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._enum_value_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";background-color:"+Colors.STR_COLOR_WHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_value_name.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")        
        self._chart_widget.light_()
        self._statinfopanel.light_()
        self.calculate()

    def closeEvent(self,event):
        """Close/hide frame"""
        self.hide()
        event.ignore()

    def show(self):
        """Overloaded show function to force re-calculation (optimization)"""
        super().show()
        self.calculate()
    
    def change(self,s):
        """New field selected"""
        self.calculate()

class _StatInfoPanel(QWidget):
    """
    
    Statistic panel info widget
    
    """
    def __init__(self, parent,values,unit):
        super(QWidget, self).__init__(parent)                
        self._values = values
        self.layout = QGridLayout(self)                
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)  
        i=0        
        self._valuespanel = {}
        for name in self._values:
            self._valuespanel[name] = numberpanelwidget.NumberPanelWidget(self,label_name=name,label_unit=unit[name],unit=True,history=False,type_format="sci",temp=False,statistics=False,fitting=False) 
            self.layout.addWidget(self._valuespanel[name], i, 1)    
            i=i+1
        
    def set_minimum_width_name(self,width=0):
        """Force label name width"""
        pass
        for name in self._valuespanel:
            self._valuespanel[name].set_minimum_width_name(width)

    def update_data(self):
        """Update all numberpanelwidget"""
        for name in self._valuespanel:
            self._valuespanel[name].set_data(self._values[name])

    def dark_(self):
        """Set dark theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        for name in self._valuespanel:
            self._valuespanel[name].dark_()

    def light_(self):
        """Set light theme"""
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        for name in self._valuespanel:
            self._valuespanel[name].light_()


class _Example(QMainWindow):

    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        
        """Init user interface"""
        self.scalar = StatWidget()
        
        self.setWindowTitle('Scalar')
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self.scalar, 0, 0)
        
        data = np.random.randint(1000, size=10000)
        self.scalar.set_data(data,size=100)
        self.setCentralWidget(self.scalar)
        self.resize(1000,400)
        
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self.scalar.dark_()

    def show(self):
        super().show()
        self.scalar.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())