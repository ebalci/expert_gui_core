from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import pyqtgraph as pg
import numpy as np
import qtawesome as qta
import sys
from scipy.optimize import curve_fit
from scipy import stats as st

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.datapanels import numberpanelwidget
from expert_gui_core.gui.widgets.datapanels import boolpanelwidget
from expert_gui_core.gui.widgets.pyqt import chartwidget
from expert_gui_core.tools import formatting


class FitWidget(QWidget):

    """
    
    Statistic widget
    Histogram plot + standard stat data (avg stddev min max ntps)
    
    """

    update_signal = pyqtSignal()

    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)        

        qta.icon("fa5.clipboard")
        
        self._mutex = False
        self._data = {}
        self._datax = {}
        self._datafit = {}
        self._valid = {
            "fit":True
        }
        self._values = {
            "npts":0.,
            "amp":0.,
            "mu":0.,
            "sigma":0.,
            "offset":0.,
            "b":0.,
            "c":0.
        }
        self._units = {
            "npts":None,
            "amp":None,
            "mu":None,
            "sigma":None,
            "offset":None,
            "b":None,
            "c":"%"
        }

        self._updatetime = 0
        self._force_recalculate = False

        self.layout = QGridLayout(self)        
        
        self._enum_value_name = QComboBox()        
        self._list_value_name = QListView(self._enum_value_name)
        self._enum_value_name.setView(self._list_value_name)
        self._enum_value_name.setEditable(True)
        self._enum_value_name.lineEdit().setAlignment(Qt.AlignLeft)   
        self._enum_value_name.lineEdit().setDisabled(True)
        self._enum_value_name.lineEdit().setReadOnly(True)
        self.layout.addWidget(self._enum_value_name, 0, 0,1,1)

        self._enum_type_name = QComboBox()        
        self._list_type_name = QListView(self._enum_type_name)
        self._enum_type_name.setView(self._list_type_name)
        self._enum_type_name.setEditable(True)
        self._enum_type_name.lineEdit().setAlignment(Qt.AlignLeft)   
        self._enum_type_name.lineEdit().setDisabled(True)
        self._enum_type_name.lineEdit().setReadOnly(True)        
        self._enum_type_name.addItem("Fast Gauss")
        self._enum_type_name.addItem("Gauss")
        self._enum_type_name.addItem("Fast Linear")
        self._enum_type_name.addItem("Linear")

        self._chart_widget = chartwidget.ChartWidget(self)
        self._chart_widget.add_plot(title="Fitting",  dyn_cursor=True, show_grid=True,add_region=False)        
        self.layout.addWidget(self._chart_widget, 1, 0)
        
        self._fitinfopanel = _FitInfoPanel(self,self._values,self._units,self._valid)
        self.layout.addWidget(self._fitinfopanel, 0, 1,2,1)
        self._fitinfopanel.setMinimumWidth(320)
        self._fitinfopanel.setMaximumWidth(400)
        self._fitinfopanel.set_minimum_width_name(60)

        self.update_signal.connect(self.update_data)

        self.resize(1000,350)

        self._enum_value_name.currentTextChanged.connect(self.change)
        self._enum_type_name.currentTextChanged.connect(self.changetype)

        self.set_font_size(9)
        
    def set_font_size(self, size=8):
        """Change font size"""
        pass

    def set_data(self, data, name="default", x=None, xmin=None, xmax=None, size=100):
        """
        Set data to calculate fitting.
        """
        
        if name not in self._data:
            self._enum_value_name.addItem(name)
        
        # start end
        
        istart=0
        iend=len(data)

        # get istart
        
        if xmin is not None:
            if x is not None:
                for i in range(len(x)):
                    if x[i] <= xmin:
                        istart=i
            else:
                istart = xmin
        
        if istart > len(data):
            return

        # get iend
        
        if xmax is not None:
            if x is not None:
                for i in range(len(x)):
                    if x[i] <= xmax:
                        iend=i
            else:
                iend = xmax
        
        if iend > len(data):
            if iend <= istart:
                return

        # set data

        self._data[name] = data[istart:iend] 

        if x is None:
            self._datax[name] = range(len(self._data[name]))   
        else:
            self._datax[name] = x[istart:iend]        

        # Calculate everything !

        dt = formatting.current_milli_time() - self._updatetime 
        
        if dt >= 1000:
            self.calculate()
            self._updatetime = formatting.current_milli_time()
       
    def fo_gauss(self,x,a,mu,sig,b,c):
        """
        Gauss formula.
        """
        gauss = a*np.exp(-(x-mu)**2/(2*sig**2)) + c
        # line = b*x+c
        # return gauss + line
        return gauss
    
    def fo_linear(self,x,b,c):
        """
        Linear formula.
        """
        line = b*x+c
        return line
        
    def fit(self, fo, x, y):
        """
        Function fit linear or gaussian.
        """
        if self._enum_type_name.currentText() == "Gauss" or self._enum_type_name.currentText() == "Fast Gauss":
            p_val = st.pearsonr(x, y)
            if p_val.pvalue < 0.03:
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"] = False
                return None

        sigma0 = 0
        max0 = 0
        mu0 = 0
        
        # gauss

        if self._enum_type_name.currentText() == "Gauss" or self._enum_type_name.currentText() == "Fast Gauss":

            if self._enum_type_name.currentText() == "Fast Gauss" and len(y) > 1000:
                size = int(len(y)/10)
                y_used = np.array(list(y[::size]),dtype=float) 
                x_used = np.array(list(x[::size]),dtype=float) 
            else:
                y_used = np.array(list(y),dtype=float)
                x_used = np.array(list(x),dtype=float)

            try:
                imax = y_used.argmax()
            except:
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=False
                return None
            max0 = y_used[imax]
            mu0 = x_used[imax]
            y2 = y_used[y_used<max0/2]
            if y2 is None or len(y2) == 0:
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=False
                return None 
            imax2 = y2.argmax()
            sigma0=x_used[imax]-x_used[imax2]
            if sigma0 < 0:
                sigma0 = -sigma0        
            if sigma0 == 0:
                sigma0 = 0.1
            c0=y_used[:3].mean()
            if x_used[-1] != x_used[0]:
                b0=(y_used[-1]-y_used[0])/(x_used[-1]-x_used[0])
            else:
                b0 = 0 

            # fit!

            try:
                popt,pcov = curve_fit(f=fo,xdata=x_used,ydata=y_used,p0=[float(max0),float(mu0),float(sigma0),float(b0),float(c0)],maxfev=500)
            except Exception as e:
                # print(e)
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=False
                return None
            
            if popt[2] > 0:
                self._values["npts"] = len(x)
                self._values["amp"] = popt[0]
                self._values["mu"] = popt[1]
                self._values["sigma"] = popt[2]
                self._values["offset"] = popt[4]
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=True
            else:
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=False

            return self.fo_gauss(x,*popt)
            
        # linear

        else:

            if self._enum_type_name.currentText() == "Fast Linear" and len(y) > 1000:
                size = int(len(y)/10)
                y_used = np.array(list(y[::size]),dtype=float) 
                x_used = np.array(list(x[::size]),dtype=float) 
            else:
                y_used = np.array(list(y),dtype=float)
                x_used = np.array(list(x),dtype=float)

            if x_used[-1] != x_used[0]:
                b0=(y_used[-1]-y_used[0])/(x_used[-1]-x_used[0])
            else:
                b0 = 0        
            c0=y_used[0]-b0*x_used[0]

            # fit!
            try:
                popt,pcov = curve_fit(f=fo,xdata=x_used,ydata=y_used,p0=[float(b0),float(c0)],maxfev=500)
            except Exception as e:
                self._values["npts"] = len(x)
                self._values["amp"] = 0
                self._values["mu"] = 0
                self._values["sigma"] = 0
                self._values["offset"] = 0
                self._values["b"] = 0
                self._values["c"] = 0
                self._valid["fit"]=False
                return None
            
            self._values["npts"] = len(x)
            self._values["amp"] = 0
            self._values["mu"] = 0
            self._values["sigma"] = 0
            self._values["offset"] = 0
            self._values["b"] = popt[0]
            self._values["c"] = popt[1]
            self._valid["fit"]=True

            return self.fo_linear(x,*popt)
            
        return None 

    def set_force_recalculate(self,force):
        """
        Set force calculation flag at every update.
        """
        self._force_recalculate = force

    def calculate(self):
        """
        Do the calculation and update widget.
        """

        # Only if visible (optimization)

        if self.isVisible() or self._force_recalculate==True: 
            if self._mutex:
                return
            self._mutex = True
            try:
                name = self._enum_value_name.currentText()                
                if name == "":
                    self._mutex = False
                    return     
                if self._enum_type_name.currentText() == "Gauss" or self._enum_type_name.currentText() == "Fast Gauss":
                    self._datafit[name] = self.fit(self.fo_gauss,self._datax[name],self._data[name])
                else:
                    self._datafit[name] = self.fit(self.fo_linear,self._datax[name],self._data[name])                
            except:
                self._mutex = False
                return None

            # Update data !        
            
            self.update_signal.emit()
            return self._datafit[name]

    @pyqtSlot()
    def update_data(self):
        """
        Calculate histogram and update info panel and plot.
        """

        name = self._enum_value_name.currentText() 
           
        self._fitinfopanel.update_data()
        self.update_plot(name)     
        self._mutex = False       
    
    def update_plot(self,name="default"):
        """
        Update plot chart + update plot marker.
        """        
        self._chart_widget.set_data(self._data[name], 
                                    datax=self._datax[name],
                                    update=True, 
                                    name="raw", 
                                    pen=pg.mkPen({'color': Colors.COLOR_LBLUE, 'width': 1}))
        if self._enum_type_name.currentText() == "Gauss" or self._enum_type_name.currentText() == "Fast Gauss":
            if self._datafit[name] is not None and self._fitinfopanel._values["sigma"] > 0:
                self._chart_widget.set_data(self._datafit[name], 
                                        datax=self._datax[name],
                                        update=True, 
                                        name="fit", 
                                        pen=pg.mkPen({'color': Colors.COLOR_LIGHTORANGE, 'width': 1}))                               
            else:
                self._chart_widget.set_data([0,0],
                                        update=True, 
                                        name="fit", 
                                        pen=pg.mkPen({'color': Colors.COLOR_LIGHT3GRAY, 'width': 0.1}))                                         
        else:
            self._chart_widget.set_data(self._datafit[name], 
                                        datax=self._datax[name],
                                        update=True, 
                                        name="fit", 
                                        pen=pg.mkPen({'color': Colors.COLOR_LIGHTORANGE, 'width': 1}))                               

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self._enum_value_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._list_value_name.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._enum_type_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT1GRAY+";background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._list_type_name.setStyleSheet("background-color:"+Colors.STR_COLOR_L2BLACK+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_WHITE+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")
        self._chart_widget.dark_()
        self._fitinfopanel.dark_()
        self.calculate()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._enum_value_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";background-color:"+Colors.STR_COLOR_WHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_value_name.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")        
        self._enum_type_name.setStyleSheet("border:1px solid "+Colors.STR_COLOR_LIGHT4GRAY+";background-color:"+Colors.STR_COLOR_WHITE+";selection-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._list_type_name.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";selection-color:"+Colors.STR_COLOR_WHITE+";color:"+Colors.STR_COLOR_BLACK+";selection-background-color:"+Colors.STR_COLOR_LBLUE+";")        
        self._chart_widget.light_()
        self._fitinfopanel.light_()
        self.calculate()

    def closeEvent(self,event):
        """
        Close/hide frame.
        """
        self.hide()
        event.ignore()

    def show(self):
        """
        Overloaded show function to force re-calculation (optimization).
        """
        super().show()
        self.calculate()
    
    def change(self,s):
        """
        New field selected.
        """
        self.calculate()
    
    def changetype(self,s):
        """
        New type selected.
        """
        self.calculate()


class _FitInfoPanel(QWidget):
    """
    
    Fitting panel info widget
    
    """

    def __init__(self, parent,values,unit,valid):
        super(QWidget, self).__init__(parent)                
        
        self._parent = parent
        self._values = values
        self._valid=valid
        self._valuespanel = {}
        self._oldtype = ""

        self.layout = QGridLayout(self)             
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)     

        # Type

        self.layout.addWidget(parent._enum_type_name, 0, 1)
        
        # LED

        i=1        
        
        self._ledpanel = boolpanelwidget.BoolPanelWidget(self,name=False,type_boolean="led",history=False,statistics=False,fitting=False) 
        self._ledpanel.set_color_led(true_color=Colors.COLOR_LIGHTGREEN,false_color=Colors.COLOR_LIGHTRED)
        self.layout.addWidget(self._ledpanel, i, 1)    
        
        # Formula

        i=2

        self._formula = QLabel("")
        font_small = QFont()
        font_small.setPointSize(8)
        self._formula.setFont(font_small)
        self.layout.addWidget(self._formula, i, 1)    

        # Number data widgets

        i=3
                
        for name in self._values:
            if i == 3:
                self._valuespanel[name] = numberpanelwidget.NumberPanelWidget(self,label_name=name,label_unit=unit[name],unit=True,type_format="str",temp=False,statistics=False,fitting=False,history=False) 
            else:
                self._valuespanel[name] = numberpanelwidget.NumberPanelWidget(self,label_name=name,label_unit=unit[name],unit=True,type_format="sci",temp=False,statistics=False,fitting=False,history=False) 
            self.layout.addWidget(self._valuespanel[name], i, 1)    
            i=i+1

        # Stretch and move * to the top
        
        self.layout.addWidget(QLabel(""), i, 1)    
        self.layout.setRowStretch(i,1)
        
    def set_type(self):
        """
        Change type fitting -> display/hide numberwidget.
        """
        if self._oldtype != self._parent._enum_type_name.currentText():
            self._oldtype = self._parent._enum_type_name.currentText()
            if self._oldtype == "Gauss" or self._oldtype == "Fast Gauss":
                self._formula.setText("FIT = amp * \u212F ** ( -(x - \u03BC)\u00B2 / ( 2 \u03C3\u00B2 ) ) + offset")
                self._valuespanel["amp"].show()
                self._valuespanel["mu"].show()
                self._valuespanel["sigma"].show()
                self._valuespanel["offset"].show()
                self._valuespanel["b"].hide()
                self._valuespanel["c"].hide()
            else:
                self._formula.setText("FIT = b * x + c")
                self._valuespanel["amp"].hide()
                self._valuespanel["mu"].hide()
                self._valuespanel["sigma"].hide()
                self._valuespanel["offset"].hide()
                self._valuespanel["b"].show()
                self._valuespanel["c"].show()

    def set_minimum_width_name(self,width=0):
        """
        Force label name width.
        """
        for name in self._valuespanel:
            self._valuespanel[name].set_minimum_width_name(width)

    def update_data(self):
        """
        Update all numberpanelwidget.
        """
        self.set_type()
        for name in self._valuespanel:
            self._valuespanel[name].set_data(self._values[name])
        self._ledpanel.set_data(self._valid["fit"])

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self._formula.setStyleSheet("color:"+Colors.STR_COLOR_BLUE)
        for name in self._valuespanel:
            self._valuespanel[name].dark_()
        self._ledpanel.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self._formula.setStyleSheet("color:"+Colors.STR_COLOR_BLUE)
        for name in self._valuespanel:
            self._valuespanel[name].light_()
        self._ledpanel.light_()


class _Example(QMainWindow):

    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
        self._update_time = 0
        
    def init_ui(self):        
        
        """Init user interface"""
        
        self.scalar = FitWidget()
        
        self.setWindowTitle('Scalar')
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self.scalar, 0, 0)

        # data = np.random.randint(1000, size=10000)
        x = np.linspace(0, 10, 100)
        # fo_gauss(self,x,a,mu,sig,m,c):
        y = self.scalar.fo_gauss(x=x, 
                                 a=100, 
                                 mu=5, 
                                 sig=1,
                                 b=1,
                                 c=1)
        self.scalar.set_data(y,x=x,size=100)
        self.setCentralWidget(self.scalar)
        self.resize(1000,400)
        
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self.scalar.dark_()

    def show(self):
        super().show()
        self.scalar.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())