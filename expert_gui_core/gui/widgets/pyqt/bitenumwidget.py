from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import math
from functools import partial

from PyQt5.QtCore import pyqtSlot, pyqtSignal

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors


class BitEnumWidget(QWidget):

    """    
    BitEnum widget.
    It can handle maximum 64 bit value.
    It can  display or not text labels, use a modulo value to define the maximum of cells per line and it can be read only.

    :param parent: Parent object.
    :type param: object
    :param size_bit: number of bit used and activated (1 to 64, default=64).
    :type size_bit: int, optional
    :param title: Top center title.
    :type title: str, optional
    :param enabled: Read-only or modifiable (default=True).
    :type enabled: bool, optional
    :param labels: List of labels to be used in bit enum.
    :type labels: list, optional
    :param modulo: Maximum number of cells per line (default=8)
    :type modulo: int, optional    
    """  

    update_signal = pyqtSignal()
    dataChanged = pyqtSignal(str)

    def __init__(self,
                 parent=None,
                 size_bit=32,
                 title="",
                 enabled=True,
                 labels=None,
                 modulo=8,
                 border=True):
        super(BitEnumWidget, self).__init__(parent)

        # init labels

        self._labels = ["","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","","",
                        "","","","","","","",""]                        

        if labels is not None:
            for label in labels:
                ind = int(label)                
                self._labels[ind] = labels[label]

        if size_bit > 64:
            size_bit = 64

        # parameters

        self._size_bit = size_bit
        self._title = title
        self._module = modulo
        self.border = border
        self._light = False
        self._key_handler = None

        # layout

        self._layout = QGridLayout()
        self._bitEnumCell = []
        self._layout.setContentsMargins(3, 2, 3, 1)
        self._layout.setSpacing(0)        
        self.setLayout(self._layout)

        self.font_text = QFont()
        
        if self._title != "":
            self._label_title = QLabel("  " + self._title)
            self._label_title.setFont(self.font_text)
            self._label_title.setAlignment(Qt.AlignCenter)        
            self._layout.addWidget(self._label_title,0,0,1,modulo)

        # n lines

        nlines = int(self._size_bit / modulo)
        if self._size_bit%modulo != 0:
            nlines = nlines + 1  
                  
        # create cells

        for i in range(nlines*modulo):
            nl = int(i/modulo)
            nloffset = i%modulo
            if i < self._size_bit:
                bitEnumCell = _BitEnumCell(
                    self._labels[i], active=True)
                self._bitEnumCell.append(bitEnumCell)  
                if enabled:   
                    bitEnumCell.clicked.connect(partial(self.push, bitcell=i))
            else:
                bitEnumCell = _BitEnumCell(
                    "", active=False)
                self._bitEnumCell.append(bitEnumCell)                    
            self._layout.addWidget(self._bitEnumCell[i],nl+1,nloffset,1,1)

        # init bit array

        self.col = -1
        self.row = -1

        self._bitarray = []
        for i in range(64):
            self._bitarray.append(int(math.pow(2,i)))

        # colors

        self.color_bg = Colors.COLOR_LBLACK
        self.color_mainbg = None
        self.color_mainbglight = Colors.COLOR_WHITE
       
        self.color_mainbgdark = Colors.COLOR_LBLACK
        self.color_border = Colors.COLOR_LIGHT1GRAY
        self.color_bglight = Colors.COLOR_WHITE
        self.color_bgdark = Colors.COLOR_LBLACK
        self.change_color = True

        # paint mutex

        self._mutex = False

        # value main

        self._value = 0

        # signals

        self.update_signal.connect(self.update_data)
        self.update_signal.emit()

        # self.set_font_size(9)

    def set_handler(self, handler, key_handler):
        """
        Set/connect handler to provide change event.

        :param handler: Signal, external value changed listener
        :type handler: PyQtSignal
        """
        self._key_handler = key_handler
        self.dataChanged.connect(handler)

    def set_font_size(self, size=8):
        """
        Change component font size.

        :param size: Font size (default=8).
        :type size: int, optional
        """
        self.font_text.setPointSize(size)
      
    def push(self, bitcell=0):
        """
        Push bit cell button.

        :param bitcell: Cell number(default = 0)
        :type bitcell: int, optional
        """
        if self._bitEnumCell[bitcell]._value > 0:
            self._value = self._value - math.pow(2,bitcell)
        else:
            self._value = self._value + math.pow(2, bitcell)
        self.update_signal.emit()
        if self._key_handler is not None:
            self.dataChanged.emit(self._key_handler)

    
    def abs (self, val):
        """
        Absolute value.

        :param val: Value to process.
        :type val: float
        :return: Abs value.
        :rtype: float
        """
        if val < 0:
            return -val
        return val
    
    def compare_bit(self, value, index):
        """
        Comparison each bit value.

        :param value: Value to compare.
        :type value: float
        :param index: Which index.
        :type index: int
        """
        val = int(value)
        if self.abs(val&self._bitarray[index]) == self.abs(self._bitarray[index]):
            return True
        return False  

    def dark_(self):
        """
        Set dark theme.
        """
        self.color_bg = self.color_bgdark  
        self.color_mainbg = self.color_mainbgdark
        self.color_border = Colors.COLOR_L3BLACK
        if self._title != "":
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_WHITE+";font-weight:bold;")
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            self._bitEnumCell[i].dark_()
        self.update()
        self._light = False

    def light_(self):
        """
        Set light theme.
        """
        self.color_bg = self.color_bglight 
        self.color_mainbg = self.color_mainbglight
        self.color_border = Colors.COLOR_LIGHT6GRAY
        if self._title != "":
            self._label_title.setStyleSheet("color:"+Colors.STR_COLOR_BLACK+";font-weight:bold;")
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            self._bitEnumCell[i].light_()
        self.update()
        self._light = True

    def get_data(self):
        """
        Get data.

        :return: Return the corresponding value
        :rtype: float
        """
        return self._value

    @pyqtSlot()
    def update_data(self):
        """
        Update data component.
        """    
        if self._mutex:
            return
        self._mutex = True    
        if self._value > math.pow(2, self._size_bit):
            self._value = math.pow(2, self._size_bit) - 1
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            nl = int(i/8)
            nloffset = i%8
            if i < self._size_bit:
                if self.compare_bit(self._value, i):
                    self._bitEnumCell[i].set_value(True)
                else:
                    self._bitEnumCell[i].set_value(False)
        self.update()
        self._mutex = False

    def set_data(self, data):
        """
        Set data.

        :param data: Set data.
        :type data: float
        """
        self._value = data
        self.update_signal.emit()

    def sel_bgColors(self,bglight,bgdark):
        """
        Change bg colors.

        :param bglight: Background color light theme.
        :type bglight: QColor
        :param bgdark: Background color dark theme.
        :type bgdark: QColor        
        """
        self.color_mainbglight = bglight
        self.color_mainbgdark = bgdark
        self.color_bglight = bglight
        self.color_bgdark  = bgdark
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            self._bitEnumCell[i].sel_bgColors(bglight,bgdark)        
        if self._light:
            self.light_()
        else:
            self.dark_()
        self.update_signal.emit()

    def sel_valColors(self,vallight,valdark):
        """
        Change value color (default is green).

        :param vallight: Background cell color light theme.
        :type vallight: QColor
        :param valdark: Background cell color dark theme.
        :type valdark: QColor        
        """
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            self._bitEnumCell[i].sel_valColors(vallight,valdark)        
        if self._light:
            self.light_()
        else:
            self.dark_()
        self.update_signal.emit()

    def set_circle_color(self, val):
        """
        Change circle color.
        
        :param val: Circle color.
        :type val: QColor
        """
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            self._bitEnumCell[i].sel_circle_color(val)        
        if self._light:
            self.light_()
        else:
            self.dark_()
        self.update_signal.emit()

    def set_cell_colors(self,index,vallight,valdark):
        """
        Set cell colors.
        """
        self._bitEnumCell[index].sel_valColors(vallight,valdark)

    @pyqtSlot()
    def update_data(self):
        """
        Set data.
        """        
        if self._mutex:
            return
        self._mutex = True
        if self._value > math.pow(2,self._size_bit):            
            self._value = math.pow(2,self._size_bit) - 1        
        nlines = int(self._size_bit / 8)
        if self._size_bit%8 != 0:
            nlines = nlines + 1
        for i in range(nlines*8):
            nl = int(i/8)
            nloffset = i%8
            if i < self._size_bit:
                if self.compare_bit(self._value,i):
                    self._bitEnumCell[i].set_value(1)
                else:
                    self._bitEnumCell[i].set_value(0)
        if self._title != "":
            self._label_title.setToolTip(str(self._value))
        self.update()
        self._mutex = False

    def resizeEvent(self,e):
        """
        Resize event.
        """
        self.setMaximumWidth(int(3*self.height()))
        width = self.width()
        if width > int(4*self.height()):
            width = int(4*self.height())
        nlines = int(self._size_bit / self._module)
        if self._size_bit%self._module != 0:
            nlines = nlines + 1
        w = int((width) / self._module)
        if self._title != "":
            h = int((self.height()-16) / nlines)
        else:
            h = int((self.height()-0) / nlines)
        for i in range(nlines*self._module):
            self._bitEnumCell[i].setMaximumWidth(w)
            self._bitEnumCell[i].set_wanted_dim(w,h)       

    def paintEvent(self,e): 
        """
        Paint event handling.
        """    
        if self._mutex:
            return      
        self._mutex = True
        painter = QPainter()
        try:
            painter.begin(self)
        except:
            painter.end()
            painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        self.w_rect = self.frameGeometry().width()
        self.h_rect = self.frameGeometry().height()
        if self.color_mainbg is not None:            
            painter.fillRect(self.rect(), self.color_mainbg)
        if self.border:
            painter.setPen(self.color_border)
            painter.drawRect(self.rect())
        painter.end()
        self._mutex = False


class _BitEnumCell(QPushButton):
    
    """    
    Single bit enum cell component widget
    
    :param text: Tooltip text.
    :type text: str, optional
    :param active: Activate or not the cell (default=False).
    :type active: bool, optional
    """

    def __init__(self, text="", active=False):

        QPushButton.__init__(self)
        
        self.setToolTip(text)
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

        self._light = True

        self.wanted_w = 0
        self.wanted_h = 0

        self.color_bglight = Colors.copy_color(Colors.COLOR_WHITE)
        self.color_bglight.setAlpha(40)
        self.color_bgdark = Colors.copy_color(Colors.COLOR_LBLACK)
        self.color_bgdark.setAlpha(40)

        self.color_bg = Colors.COLOR_WHITE
        
        self.color_title = Colors.COLOR_BLUE
        
        self.color_inactive = Colors.copy_color(Colors.COLOR_L2BLACK)
        self.color_inactive.setAlpha(40)
        self.color_active = Colors.copy_color(Colors.COLOR_LIGHT2GRAY)
        self.color_active.setAlpha(40)
    
        self.color_orangelight = Colors.COLOR_LIGHTORANGE
        self.color_orangedark = Colors.COLOR_LIGHTORANGE
        self.color_orange = Colors.COLOR_LIGHTORANGE

        self.color_redlight = Colors.COLOR_LIGHTRED
        self.color_reddark = Colors.COLOR_LIGHTRED
        self.color_red = Colors.COLOR_LIGHTRED

        self.color_greenlight = Colors.COLOR_LIGHT2GREEN
        self.color_greendark  = Colors.COLOR_LIGHTGREEN
        self.color_green= Colors.COLOR_BLACK

        self.color_circlelight = Colors.copy_color(Colors.COLOR_LIGHT0GRAY)
        self.color_circledark = Colors.copy_color(Colors.COLOR_LIGHT4GRAY)
        self.color_circle = Colors.copy_color(self.color_greenlight.darker())

        self.color_circlelight.setAlpha(40)
        self.color_circledark.setAlpha(40)
        self.color_circle.setAlpha(40)

        self.font_text = QFont()
        self.font_text.setFamily("Courier New")
        self.font_text.setBold(True)
        self.font_text.setPointSize(7)

        self._active = active
        self._value = False

    def set_value(self, val):
        """
        Set value.
        
        :param val: Value to set.
        :type val: float
        """
        self._value = val
        self.update()

    def set_active(self, val):
        """
        Set active.

        :param val: Active or not.
        :type val: bool
        """
        self._active = val
        self.update()
    
    def set_wanted_dim(self, w, h):
        """
        Define wanted/expected dimension.

        :param w: Desired width.
        :type w: int
        :param h: Desired height.
        :type h: int        
        """
        self.wanted_w = w
        self.wanted_h = h

    def paintEvent(self, e):
        """
        Paint event handling.
        """
        painter = QPainter()
        try:
            painter.begin(self)            
        except:
            painter.end()
            painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        if self.color_bg is not None:
            painter.setBrush(self.color_bg)
            painter.setPen(self.color_bg)
        if self.wanted_w == 0:
            self.w_rect = 0.9*self.frameGeometry().width()
        else:
            self.w_rect = 0.9*self.wanted_w
        if self.wanted_h == 0:
            self.h_rect = 0.9*self.frameGeometry().height()
        else:
            self.h_rect = 0.9*self.wanted_h  

        #graphical parameters
        gap = 2
        factor = 0.7
        if self.h_rect < 10 or self.w_rect < 20:
            gap = 1
            factor = 0.5
        self.h_line = self.h_rect - 2*gap
        self.w_cell = self.w_rect - 2*gap
        
        #fill
        rcircle = self.w_cell/2
        if self.h_line < self.w_cell:
            rcircle = self.h_line/2
        if rcircle < 2:
            rcircle = 2
        center = QPoint(
            int(gap + self.w_cell/2),
            int(gap + self.h_line/2)
            )   
        if self._active == False:
            rect = self.rect()
            grad1 = QLinearGradient(rect.topLeft(), rect.topRight())
            grad1.setColorAt(0, self.color_inactive.darker(140))
            grad1.setColorAt(0.8, self.color_inactive.lighter(140))
            painter.setPen(self.color_inactive.darker(90))
            painter.setBrush(QBrush(grad1))
        else:
            if self._value == 0:
                rect = self.rect()
                grad1 = QLinearGradient(rect.topLeft(), rect.topRight())
                grad1.setColorAt(0, self.color_active.darker(140))
                grad1.setColorAt(0.8, self.color_active.lighter(140))
                painter.setBrush(QBrush(grad1))
                if self.color_bg is not None:
                    painter.setPen(self.color_bg)
                else:
                    painter.setPen(self.color_inactive)
            elif self._value == 1:
                if rcircle < 5:
                    rect = self.rect()
                    grad1 = QLinearGradient(rect.topLeft(), rect.topRight())
                    grad1.setColorAt(0, self.color_green.darker(140))
                    grad1.setColorAt(0.8, self.color_green.lighter(140))
                    painter.setBrush(QBrush(grad1))
                    if self._light:
                        painter.setPen(self.color_green.darker(factor=120))
                    else:
                        painter.setPen(self.color_green)
                else:
                    rect = self.rect()
                    grad1 = QLinearGradient(rect.topLeft(), rect.topRight())
                    grad1.setColorAt(0, self.color_green.darker(140))
                    grad1.setColorAt(0.8, self.color_green.lighter(140))
                    painter.setBrush(QBrush(grad1))
                    if self._light:
                        painter.setPen(self.color_green.darker(factor=120))
                    else:
                        painter.setPen(self.color_green)
        
        painter.drawEllipse(center,int(rcircle*1.001),int(rcircle*1.001))    

        painter.end()
 
    def sel_bgColors(self, bglight=None, bgdark=None):
        """
        Change bg colors.

        :param bglight: Background color light theme.
        :type bglight: QColor
        :param bgdark: Background color dark theme.
        :type bgdark: QColor 
        """
        self.color_bglight = bglight
        self.color_bgdark  = bgdark

    def sel_valColors(self, vallight, valdark):
        """
        Change value color (default is green).

        :param vallight: Background cell color light theme.
        :type vallight: QColor
        :param valdark: Background cell color dark theme.
        :type valdark: QColor
        """
        self.color_greenlight = vallight
        self.color_greendark  = valdark

    def sel_circle_color(self, val):
        """
        Change circle color.
        
        :param val: Circle color.
        :type val: QColor
        """
        self.color_circlelight  = val

    def dark_(self):
        """
        Set dark theme.
        """
        self._light = False
        self.color_bg = self.color_bgdark 
        self.color_title = Colors.COLOR_BLUE
        self.color_green = self.color_greendark
        self.color_red = self.color_reddark
        self.color_orange = self.color_orangedark
        self.color_circle = self.color_circledark

    def light_(self):
        """
        Set light theme.
        """
        self._light = True
        self.color_bg = self.color_bglight 
        self.color_title = Colors.COLOR_BLUE
        self.color_green = self.color_greenlight
        self.color_red = self.color_redlight
        self.color_orange = self.color_orangelight
        self.color_circle = self.color_circlelight 


class _Example(QMainWindow):

    """
    
    Example class to test
    
    """
    
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):        
        """Init user interface"""
        w = QWidget()
        labels = [  "1", "2", "3", "4", "5", "6", "7", "8",
                    "9", "10", "11", "12", "13", "14", "15", "16",
                    "","","","","","","","",
                    "","","","","","","",""]
        self.bitenum = BitEnumWidget(w,labels=labels,size_bit=30)
        self.bitenum.set_data(20)
        self.bitenum.set_circle_color(QColor(30,100,120))
        self.setWindowTitle('BitEnum')
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self.bitenum, 1, 0)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(500,100)
        
        # self.bitenum.light_()
        self.bitenum.dark_()
      
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())

