from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys
import pandas as pd
import numpy as np
import threading

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.pyqt.tableview import DataFrameWidget

class TableWidget(DataFrameWidget):
    
    """

    TableWidget is a custom table that works with pandas dataframes. It allows the user to sort, filter and convert (e.g. hexadecimal) the different columns.
    
    Attributes:

        - status_list: list containing int values that serves as an auxiliar invisible column to display colored data.
        - include_status_list: whether or not to include the status list as an invisible column (disable to increase performance).
        - enable_column_options: whether or not to enable the options menu (e.g. filtering) (disable to increase performance).
        - auto_resizing: whether or not to automatically resize at expanding (disable to increase performance).
        - cells_editable: whether or not to make cells editable.

    """
    
    # connections and signals

    update_signal = pyqtSignal(object)
    update_col_signal = pyqtSignal(str,object)
    refresh_table_signal = pyqtSignal()

    mutex = threading.Lock()
    
    def __init__(self, 
                parent, 
                status_list = np.array([]), 
                include_status_list = True, 
                enable_column_options = True, 
                auto_resizing = True, 
                cells_editable = True,
                status_colors=None,
                ):

        super(TableWidget, self).__init__(parent=parent, 
                                          status_list=status_list, 
                                          include_status_list=include_status_list, 
                                          enable_column_options=enable_column_options, 
                                          auto_resizing=auto_resizing, 
                                          cells_editable=cells_editable, 
                                          status_colors=status_colors,
                                        )
        self.parent = parent
        self.update_signal.connect(self.update)
        self.update_col_signal.connect(self.update_col)
        self.refresh_table_signal.connect(self.refresh_table_view)

    def get_data(self, convert_to = "pandas"):
        """
        Get data table (use numpy for converting to numpy arrays).
        """
        data = self.df        
        if 'status_list' in data.columns:
            data = data.drop('status_list', axis=1)
        for l in data.columns:
            data = data.drop(l, axis=1)
        if convert_to == "numpy":
            if len(data.columns) == 1:
                data = np.ravel(data)
            else:
                data = data.to_numpy().T        
        return data
        
    def set_data(self, df):
        """
        Set data in table.
        """
        self.update_signal.emit(df)

    def set_data_col(self,name,data):
        """
        Set data in table column.
        """
        self.update_col_signal.emit(name,data)

    @pyqtSlot(object)
    def update(self, df):
        """
        Update data in a table.
        """
        self.setDataFrame(df, status_list=self.status_list)
    
    @pyqtSlot(str,object)
    def update_col(self,name,data):
        """
        Update df column.
        """        
        self._data_model.df[name]=data        

    def refresh_table(self):
        """
        Refresh table signal.
        """
        # self.mutex.acquire()
        self.refresh_table_signal.emit()

    @pyqtSlot()
    def refresh_table_view(self):
        """
        Refresh table.
        """
        self._data_model.refresh_table()
        # self.mutex.release()

    def dark_(self):
        """
        Set dark theme.
        """
        num_col = 0
        if self._data_model.df is not None:
            num_col = len(self._data_model.df.columns)
        # self.setStyleSheet("selection-color:rgb(0,0,0);selection-background-color:rgb(84,190,230);gridline-color:"+Colors.STR_COLOR_L2BLACK+";")
        
    def light_(self):
        """
        Set light theme.
        """
        num_col = 0
        if self._data_model.df is not None:
            num_col = len(self._data_model.df.columns)
        # self.setStyleSheet("selection-color:rgb(0,0,0);selection-background-color:rgb(84,190,230);gridline-color:"+Colors.STR_COLOR_LIGHT5GRAY+";")


class _Example(QMainWindow):
    """
    Example PyQt window with a frame that handles the TableWidget and TableView
    """

    def __init__(self, app):

        # inheritance

        super(_Example, self).__init__()

        # main attributes

        self.app = app

        # build and bind widgets

        self.buildLayout()

        # create the table

        NROW = 6
        data = {'A': np.arange(1, NROW + 1),
                'B': np.random.random(NROW),
                'C': np.random.choice(['Apple', 'Banana', 'Cherry', 'Date'], NROW),
                'D': np.random.choice([''], NROW),
                'E': np.random.choice([''], NROW),
                'F': np.random.choice([''], NROW),
                'G': np.random.choice([''], NROW),
                'H': np.random.choice([''], NROW)
                }
        self.df = pd.DataFrame(data)

        status_list = np.full(
                            shape=6,
                            fill_value=0,
                            dtype=np.int32
                        )
        status_list[3] = 1
        status_list[4] = 2
        status_list[5] = 3

        status_colors = {
            "1":[QColor(255,0,0),QColor(255,0,0,20)],
            "2":[QColor(0,0,255),QColor(0,0,255,20)],
            "3":[QColor(255,106,0),QColor(255,106,0,20)]
        }

        self.table = TableWidget(parent=self.frame_table,status_colors=status_colors)

        self.table.set_data(self.df)

        self.verticalLayout_frame_table.addWidget(self.table)

        # labels

        labels_global_status = {
            "General power fail",
            "Or 8X power fail",
            "Or 8X step busy",
            "Or 8X ADC busy",
            "Presence ext. clock",
            "Presence ext. trig",
            "Presence cable con64",
            "Presence cable P2_0",
            "Presence cable P2_1"
        }
        labels = {}
        ind = 0
        for lab in labels_global_status:
            labels[str(ind)] = str(lab)
            ind = ind + 1

        from expert_gui_core.gui.widgets.datapanels.bitenumpanelwidget import BitEnumPanelWidget

        for i in range(0,NROW):
            number_widget = BitEnumPanelWidget(self, size_bit=16, labels=labels,border=False)
            number_widget.set_bitenum_bg_colors()
            number_widget.dark_()

            nIndex = self.table._data_model.index(i,3)
            self.table.setIndexWidget(nIndex, number_widget)

        # enum

        labelsenum = {
            "1": "A",
            "5": "B",
            "6": "C",
        }

        from expert_gui_core.gui.widgets.datapanels.enumpanelwidget import EnumPanelWidget

        for i in range(0,NROW):
            number_widget = EnumPanelWidget(self, labels=labelsenum)
            # self.layout.addWidget(self.number_widget, i, 0)
            number_widget.set_data(5)
            number_widget.dark_()
            nIndex = self.table._data_model.index(i, 4)
            self.table.setIndexWidget(nIndex, number_widget)

        # bool

        from expert_gui_core.gui.widgets.datapanels.boolpanelwidget import BoolPanelWidget

        type_format = [
            "checkbox",
            "led",
            "toggle",
            "checkboxcol"
        ]

        b = True
        for i in range(0,NROW):
            bool_widget = BoolPanelWidget(self, type_boolean=type_format[i % 4],edit=True,history=False)
            # self.layout.addWidget(bool_widget, i, 0)
            b = not b
            bool_widget.set_data(b)
            bool_widget.dark_()
            nIndex = self.table._data_model.index(i, 5)
            self.table.setIndexWidget(nIndex, bool_widget)


        header = self.table.horizontalHeader()
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QHeaderView.ResizeToContents)


        # self.filter_proxy_model = QSortFilterProxyModel()
        # self.filter_proxy_model.setSourceModel(self.table._data_model)
        # self.filter_proxy_model.setFilterKeyColumn(-1)
        # self.filter_proxy_model.setFilterCaseSensitivity(Qt.CaseInsensitive)
        # self.edit.textChanged.connect(self.filter_proxy_model.setFilterRegExp)
        # self.table.setModel(self.filter_proxy_model)




        # self.proxyModel = QSortFilterProxyModel(self.table)
        # self.proxyModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        # self.proxyModel.setSourceModel(self.table._data_model)
        # self.table.setModel(self.proxyModel)

        # filter proxy model


        self.df["status_list"] = status_list

        self.table.dark_()

    # def filter(self, filter_text):
    #     # self.proxyModel.setFilterFixedString(filter_text)
    #
    #     for i in range(self.table._data_model.rowCount()):
    #         for j in range(self.table._data_model.columnCount()):
    #             item = self.table._data_model.item(i, j)
    #             match = filter_text.lower() not in item.text().lower()
    #             self.table._data_model.setRowHidden(i, match)
    #             if not match:
    #                 break


    def buildLayout(self):

        # create central widget
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)

        # main layout
        self.verticalLayout_frame_table = QVBoxLayout(self.central_widget)
        self.verticalLayout_frame_table.setContentsMargins(20, 20, 20, 20)
        self.verticalLayout_frame_table.setObjectName("verticalLayout_frame_table")

        # create frame for table
        self.frame_table = QFrame(self.central_widget)
        self.frame_table.setFrameShape(QFrame.NoFrame)
        self.frame_table.setFrameShadow(QFrame.Plain)
        self.frame_table.setObjectName("frame_table")

        self.edit = QLineEdit()

        self.verticalLayout_frame_table.addWidget(self.edit)

        self.verticalLayout_frame_table.addWidget(self.frame_table)


        # layout of the frame for the table
        # self.verticalLayout_frame_table = QVBoxLayout(self.frame_table)
        # self.verticalLayout_frame_table.setObjectName("verticalLayout_frame_table")
        # self.verticalLayout_frame_table.setContentsMargins(15, 15, 15, 15)


        # some resizing
        self.setMinimumHeight(600)
        self.setMinimumWidth(1200)


def main():
    app = QApplication(sys.argv)
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    window = _Example(app)
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
