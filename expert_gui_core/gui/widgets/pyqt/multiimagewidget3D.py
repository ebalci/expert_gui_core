import os
import sys
import time
import traceback
import typing
from enum import Enum
from typing import List

import numpy as np
import pyqtgraph as pg
from PIL import Image
from PyQt5.QtCore import *
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from pyqtgraph import makeRGBA, colormap
from pyqtgraph.opengl import GLImageItem, GLViewWidget, GLGridItem, GLAxisItem
from qtpy import QtGui

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

import expert_gui_core.comm.data
from expert_gui_core.gui.common.colors import Colors
import expert_gui_core.gui.widgets.pyqt.imagewidget


class _MultiImageIdType(Enum):
    ImageIndex = 1


class _CameraPosition(Enum):
    Center = 1
    Corner = 2


class _VignetteUpdate:
    def __init__(self, vignette, grab=False, clear_previous=True, clear_selected=False):
        self._grab = grab
        self._clear_previous = clear_previous
        self._vignette = vignette
        self._clear_selected = clear_selected

    def clear_selected(self):
        return self._clear_selected

    def vignette(self):
        return self._vignette

    def grab(self):
        return self._grab

    def clear_previous(self):
        return self._clear_previous


class _Vignette(QWidget):
    signal_clicked = pyqtSignal(_VignetteUpdate)
    signal_mouse_enter = pyqtSignal()
    signal_mouse_leave = pyqtSignal()
    signal_selected_changed = pyqtSignal()

    def __init__(self, parent, object_id: str, pixmap: QPixmap, max_width=250):
        super(QWidget, self).__init__(parent)
        self._parent = parent
        self._object_id = object_id
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(1, 1, 1, 1)

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        self._pixmap_label = QLabel()
        self.layout().addWidget(self._pixmap_label)
        self._pixmap_scaled = pixmap.scaledToWidth(max_width)

        self._pixmap = pixmap
        self._pixmap_label.setPixmap(self._pixmap_scaled)
        self._selection_active = False

    def get_pixmap(self):
        return self._pixmap

    def mouseReleaseEvent(self, a0: QtGui.QMouseEvent) -> None:

        grab = bool((QApplication.keyboardModifiers() & Qt.ShiftModifier))
        clear_previous = not grab and a0.button() != Qt.MiddleButton

        self.signal_clicked.emit(
            _VignetteUpdate(self, clear_selected=self._selection_active, clear_previous=clear_previous, grab=grab)
        )

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        if event.key() == Qt.ShiftModifier:
            pass

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        self._selection_active = True

    def mouseMoveEvent(self, a0: QMouseEvent) -> None:
        if self._selection_active:
            pass

    def set_active_styles(self):
        self.setStyleSheet('border: 2px solid red;')

    def set_inactive_styles(self):
        self.setStyleSheet('border: none;')

    def object_id(self):
        return self._object_id


class _Vignettes(QWidget):
    selection_changed = pyqtSignal(object)

    def __init__(self, parent, orientation, max_width=250):
        super(QWidget, self).__init__()
        self._parent = parent
        self.setFocusPolicy(Qt.StrongFocus)

        self._active_vignettes = []
        if orientation == Qt.Horizontal:
            self.setLayout(QHBoxLayout())
        else:
            self.setLayout(QVBoxLayout())

        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self._max_vignette_width = max_width
        self._spacer = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self._vignettes = {}
        self._vignettes_ordered = []

        self.layout().addItem(self._spacer)

    def clear_data(self):
        self._vignettes = {}
        self._vignettes_ordered = []
        self._active_vignettes = []

    def add(self, object_id: str, rgba_images):
        if not isinstance(rgba_images, List):
            rgba_images = [rgba_images]

        for image in rgba_images:
            pixmap = QPixmap(QImage(image, len(rgba_images[0][0]), len(rgba_images[0]), QImage.Format_RGBA8888))
            vignette = _Vignette(self, object_id, pixmap, max_width=100)
            self.layout().addWidget(vignette)

            vignette.signal_clicked.connect(self.update_selection)
            self._vignettes[object_id] = vignette
            self._vignettes_ordered.append(object_id)

    def dark_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_LBLACK + ";")

    def light_(self):
        self.setStyleSheet("background-color:" + Colors.STR_COLOR_WHITE + ";")

    def update_selection(self, update: _VignetteUpdate):

        if update.grab() and len(self._active_vignettes) == 1:

            index_1 = self._vignettes_ordered.index(self._active_vignettes[0].object_id())
            index_2 = self._vignettes_ordered.index(update.vignette().object_id())

            start = index_1 if index_1 < index_2 else index_2
            end = index_2 if index_2 > index_1 else index_1

            new_active_vignettes = []
            for object_id in self._vignettes_ordered[start:end + 1]:
                new_active_vignettes.append(self._vignettes[object_id])

            self.set_active_vignettes(new_active_vignettes)

        elif update.clear_previous():
            self.set_active_vignettes([update.vignette()])
        elif update.clear_selected():

            if update.vignette() in self._active_vignettes:
                self._active_vignettes.remove(update.vignette())
            else:
                self._active_vignettes.append(update.vignette())

            self.set_active_vignettes(self._active_vignettes)

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        key = event.key()
        if key == Qt.Key_Up or key == Qt.Key_Left or key == Qt.Key_Backtab:
            self.set_previous_vignette_active()
        elif key == Qt.Key_Down or key == Qt.Key_Right or key == Qt.Key_Tab:
            self.set_next_vignette_active()
        elif key == Qt.Key_Escape:
            self.set_none_active()

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        if event.angleDelta().y() > 0:
            self.set_previous_vignette_active()
        elif event.angleDelta().y() < 0:
            self.set_next_vignette_active()

    def set_previous_vignette_active(self):

        if len(self._active_vignettes) == 1:
            index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) - 1
            if index < 0:
                index = len(self._vignettes_ordered) - 1

            self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])

    def set_next_vignette_active(self):

        if len(self._active_vignettes) == 1:
            index = self._vignettes_ordered.index(self._active_vignettes[0].object_id()) + 1

            if index > len(self._vignettes_ordered) - 1:
                index = 0
            self.set_active_vignettes([self._vignettes[self._vignettes_ordered[index]]])

    def set_none_active(self):
        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()

        self._active_vignettes = []
        self.selection_changed.emit(self._vignettes_ordered)

    def get_object_ids(self, vignettes: List[_Vignette]):
        object_ids = []
        for object_id, vignette in self._vignettes.items():
            if vignette in vignettes:
                object_ids.append(object_id)
        return object_ids

    def set_active_vignettes(self, vignettes: List[_Vignette]):

        for key, vignette in self._vignettes.items():
            vignette.set_inactive_styles()

        for vignette in vignettes:
            # print(vignette.object_id())
            self._vignettes[vignette.object_id()].set_active_styles()

        self._active_vignettes = vignettes
        self.selection_changed.emit(self.get_object_ids(vignettes))


class _MultiImage3D(QWidget):
    update_signal = pyqtSignal()

    _selection_active = False
    _gl_view: GLViewWidget = None
    _gl_images: typing.Dict[str, List[GLImageItem]] = {}

    _translation: int = 0

    _np_images: List[np.ndarray] = []
    _data: typing.Dict[str, List[np.ndarray]] = {}

    _last_shown_ids = set([])

    def __init__(self, max_width=300, image_spacing=50, id_type=_MultiImageIdType.ImageIndex):

        super().__init__()

        self._max_width = max_width

        self.init_layout()

        self._position_center = QVector3D(max_width, max_width, 300)
        self._position_center_azi = -90
        self._distance = 1400

        self._gl_view = self.create_gl_view(max_width, max_width)

        self.set_camera(_CameraPosition.Center)

        self.layout().addWidget(self._gl_view)

        self._translation = image_spacing
        self._id_type = id_type

        self._gl_images = {}
        self._gl_images_ordered = []

    def clear_data(self):
        self._gl_images = {}
        self._gl_images_ordered = []
        _gl_images: typing.Dict[str, List[GLImageItem]] = {}
        _translation: int = 0
        _np_images: List[np.ndarray] = []
        _data: typing.Dict[str, List[np.ndarray]] = {}

    _last_shown_ids = set([])

    def to_grayscale(self, rgb):
        r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
        gray = 0.2989 * r + 0.5870 * g + 0.1140 * b

        return gray

    # TODO
    def show_roi_border(self, image: GLImageItem):
        pass
        # gray = self.to_grayscale(image.data)
        # print(gray.max())
        # indices = np.where(gray == gray.max())
        # from PIL import Image
        # im = Image.fromarray(gray)
        # im.save("/tmp/image.png")
        # print(image.data.shape)
        # print(image.transform())
        # print(image.data.max())

    def trace(self):
        for image_id in self._gl_images_ordered:
            image = self._gl_images[image_id]
            self.show_roi_border(image)

    def width(self):
        return self._max_width

    def images(self) -> List[GLImageItem]:
        return self._gl_images

    def add(self, object_id: str, rgba_image):

        if not self._selection_active:
            self._last_shown_ids.add(object_id)

        gl_image = GLImageItem(rgba_image.transpose(1, 0, 2))
        gl_image.translate(0, 0, len(self._gl_images) * self._translation)

        self._gl_images[object_id] = gl_image
        self._gl_view.addItem(gl_image)

        self._gl_images_ordered.append(object_id)

    def remove(self, object_ids=None):

        if object_ids:
            for object_id in object_ids:
                self._gl_view.removeItem((self._gl_images[object_id]))
                del self._gl_images[object_id]
        else:
            self.layout().removeWidget(self._gl_view)
            self._gl_view = self.create_gl_view(self._max_width, self._max_width)
            params = self._gl_view.cameraParams()
            self._gl_view.setCameraPosition(pos=self._gl_view.cameraPosition(), azimuth=params['azimuth'],
                                            elevation=params['elevation'], distance=params['distance'])
            self.layout().addWidget(self._gl_view)
            self._gl_images = {}

    def opacity(self, value):

        for key, gl_image in self._gl_images.items():
            data = gl_image.data
            image_copy = np.array(data)
            image_copy[:, :, 3] = 256 * (value / 100) - 1

            gl_image.setData(image_copy)

    def set_gl_image(self, indices: List[str]):

        to_remove = [i for i in self._last_shown_ids if i not in indices]
        to_add = [i for i in indices if i not in self._last_shown_ids]

        for obj_id in to_remove:
            self._gl_view.removeItem(self._gl_images[obj_id])
        for obj_id in to_add:
            self._gl_view.addItem(self._gl_images[obj_id])

        self._last_shown_ids = set(indices)

    def init_layout(self):
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

    def create_gl_view(self, base_width: int, base_height: int) -> GLViewWidget:

        gl_view = GLViewWidget()
        xy_plane = GLGridItem()
        base_length = base_height * 2
        xy_plane.setSize(base_width, base_length)
        xy_plane.translate(base_width / 2, base_length / 2, 0)

        yz_plane = GLGridItem()
        yz_plane.setSize(base_height, base_length)
        yz_plane.rotate(90, 0, 1, 0)
        yz_plane.translate(0, base_length / 2, base_height / 2)

        xz_plane = GLGridItem()
        xz_plane.setSize(base_width, base_height)
        xz_plane.rotate(90, 1, 0, 0)
        xz_plane.translate(base_width / 2, 0, base_height / 2)
        axes = GLAxisItem()
        axes.setSize(base_width, base_height, base_width)
        gl_view.addItem(axes)

        return gl_view

    def gl_view(self):
        return self._gl_view

    def image_width(self):
        return self.width

    def image_height(self):
        return self.height

    def light_(self):
        self._gl_view.setBackgroundColor(Colors.COLOR_WHITE)

    def dark_(self):
        self._gl_view.setBackgroundColor(Colors.COLOR_LBLACK)

    def set_camera(self, position: _CameraPosition):

        if position == _CameraPosition.Center:
            azimuth = self._position_center_azi
            elevation = azimuth * -1.2
            self._gl_view.setCameraPosition(pos=self._position_center, elevation=elevation, azimuth=azimuth,
                                            distance=self._distance)

    def set_z_spacing(self, value: int):
        for object_id, image in self._gl_images.items():
            image.resetTransform()
            image.translate(0, 0, self._gl_images_ordered.index(object_id) * value)


class MultiImageWidget3D(QWidget):
    update_signal = pyqtSignal(str, bool, tuple, dict)

    _update_time = 0
    _min_refresh_time = 0
    _mutex = False
    _transpose = False
    _filter = None

    _cmap = None
    _lut = None

    _rgba_images = {}
    _image_items = {}
    _image_nparrays = []

    def multi_image(self) -> _MultiImage3D:
        return self._multi_image

    def __init__(self, parent, bar_position=Qt.BottomEdge, transpose=False):
        super(QWidget, self).__init__(parent)

        orientation = Qt.Horizontal if bar_position == Qt.TopEdge or bar_position == Qt.BottomEdge else Qt.Vertical
        self._transpose = transpose
        self._multi_image = _MultiImage3D(image_spacing=150)
        self._vignettes = _Vignettes(parent, orientation, max_width=100)

        self.init_layout()
        self._container.layout().addWidget(self._multi_image)
        self._container.layout().addWidget(self._imagewidget)
        self._imagewidget.hide()

        self.init_splitter(orientation)
        self._data = expert_gui_core.comm.data.DataObject(history=True)

        self._cmap = pg.colormap.getFromMatplotlib('jet')
        self._lut = self._cmap.getLookupTable(alpha=True)

        if bar_position == Qt.TopEdge or bar_position == Qt.LeftEdge:
            self._splitter.addWidget(self._vignettes)
            self._splitter.addWidget(self._container)
            self._splitter.setStretchFactor(0, 0)
            self._splitter.setStretchFactor(1, 1)
        else:
            self._splitter.addWidget(self._container)
            self._splitter.addWidget(self._vignettes)
            self._splitter.setStretchFactor(0, 1)
            self._splitter.setStretchFactor(1, 0)

        self.update_signal.connect(self.update)

        self._vignettes.selection_changed.connect(self.handle_selection_changed)

        self.dark_()

    def set_z_spacing(self, value: int):
        self._multi_image.set_z_spacing(value)

    def set_data(self, data: List[List[int]], update=True, name="default", *args, **kwargs):
        
        self._data.clear_data()
        _rgba_images = {}
        _image_items = {}
        _image_nparrays = []
        self._multi_image.clear_data()
        self._vignettes.clear_data()

        # try:
        #     print(self._data.get())
        # except:
        #     pass

        if np.array(data).ndim == 2:
            data = [data]

        for raw_image in data:
            self._data.add(raw_image)

        if update and self._mutex == False:
            self.update_signal.emit(name, update, args, kwargs)

    def np_to_rgba(self, np_image):
        levels = [np_image.min(), np_image.max()]
        lut = self._lut
        return pg.makeRGBA(np_image, levels=levels, lut=lut)[0]

    def init_layout(self):
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

        self._imagewidget = expert_gui_core.gui.widgets.pyqt.imagewidget.ImageWidget(self)
        self._imagewidget.add_plot()

        self._imagewidget.setContentsMargins(0, 0, 0, 0)
        self._container = QWidget()
        self._container.setContentsMargins(0, 0, 0, 0)
        self._container.setLayout(QVBoxLayout())
        self._container.layout().setContentsMargins(0, 0, 0, 0)
        self._container.layout().setSpacing(0)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def init_splitter(self, bar_position: Qt.Edge):

        self._splitter = QSplitter()
        self._splitter.setHandleWidth(5)
        self.layout().addWidget(self._splitter)

        if bar_position == Qt.BottomEdge or bar_position == Qt.TopEdge:
            self._splitter.setOrientation(Qt.Vertical)
        else:
            self._splitter.setOrientation(Qt.Horizontal)

    _np_images = {}

    def trace(self):
        width = self._multi_image.width()
        self._multi_image._gl_view.setCameraPosition(pos=QVector3D(0, 0, width), azimuth=30, elevation=5,
                                                     distance=width * 7)
        self._multi_image.trace()

    @pyqtSlot(str, bool, tuple, dict)
    def update(self, name='default', update=True, args=None, kwargs=None):
        """ Updates the component, with the current contents of the data object.
            The data object contains all the images to be rendered.

            We don't know if we need the last 3, the last one, or if all the images are already drawn.
        """

        self._mutex = True
        try:
            for raw_image in self._data.get(name):
                object_id = hex(id(raw_image))
                if object_id not in self._np_images:
                    np_image = np.array(raw_image)
                    self._np_images[hex(id(raw_image))] = np_image
                    self._vignettes.add(object_id, self.np_to_rgba(np_image))
                    self._multi_image.add(object_id, self.np_to_rgba(np_image))


        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)
            self._mutex = False
            return

        self._mutex = False

    def handle_selection_changed(self, object_ids: List[str]):
        if len(object_ids) == 1:
            return self.show_imagewidget(self._np_images[object_ids[0]])

        self._multi_image.set_gl_image(object_ids)
        self.show_multi_images()

    def show_imagewidget(self, np_image):
        np_image = np.flipud(np_image)
        self._multi_image.hide()
        self._imagewidget.show()
        self._imagewidget.set_data(np_image)

    def set_opacity(self, value):
        """
        Image operation. As the internal data must be modified, the image must be redrawn.
        :param value:
        :return:
        """
        np_images = []

        self._multi_image.clear()

        for image_rgba in self._multi_image.images_rgba():
            np_image = np.array(image_rgba)
            np_image[:, :, 3] = int(256 * value / 100) - 1
        #     np_images.append(np_image)
        # for np_image in self._data.get():

        #
        # self._data.clear()
        # self._data.add(np_images)
        #
        # if self._mutex == False:
        #     self.update_signal.emit()

    def center_camera(self, distance=None):
        self._multi_image._gl_view.setCameraPosition(azimuth=90, elevation=0, distance=(
                self._multi_image.image_width() * 1.5) if not distance else distance)

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_WHITE + ";color:" + Colors.STR_COLOR_BLACK + "; }"
                                                                                                           "QSplitter::handle { background-color: " + Colors.STR_COLOR_DWHITE + "; }"
        )
        self._multi_image.light_()
        self._vignettes.light_()
        self._imagewidget.light_()

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet(
            "QWidget { background-color: " + Colors.STR_COLOR_LBLACK + ";color:" + Colors.STR_COLOR_WHITE + "; }"
                                                                                                            "QSplitter::handle { background-color: " + Colors.STR_COLOR_L2BLACK + "; }")
        self._multi_image.dark_()
        self._vignettes.dark_()
        self._imagewidget.dark_()

    def show_multi_images(self):
        self._multi_image.show()
        self._imagewidget.hide()

    def file_to_image_array(self, filename: str):

        image = Image.open(filename)
        if image.mode == "RGB":
            image.putalpha(Image.new('L', image.size, 255))
        return np.asarray(image)

    def data_object(self):
        return self._data

    def images(self, name="default"):
        return self._data.get(name)

    def get_image_arrays(self, images) -> List[np.ndarray]:
        np_arrays = []
        for image in images:
            if isinstance(image, str) and os.path.isfile(image):
                np_arrays.append(self.file_to_image_array(image))
            elif isinstance(image, np.ndarray):
                if image.shape[2] == 3:
                    image = np.insert(image, 3, 255, axis=2)
                np_arrays.append(image)
            else:
                raise Exception("Invalid image format supplied to MultiImage3dsWidget.")
        return np_arrays

    def get_pixmaps(self, images: List[np.ndarray]) -> List[QPixmap]:

        pixmaps = []
        for image in images:
            if image.shape[2] != 4:
                raise Exception("The image supplied to get_vignettes must be an NP array with"
                                "3 colour channels and an alpha channel")
            pixmaps.append(QPixmap(QImage(image.data, image.shape[1], image.shape[0], QImage.Format_RGBA8888)))

        return pixmaps


class _UpdateThread(QThread):
    new_image = pyqtSignal(np.ndarray)

    @staticmethod
    def raw_to_rgba(shift, raw_image):
        np_image = np.array(raw_image)
        levels = [np_image.min(), np_image.max()]
        lut = colormap.getFromMatplotlib('jet').getLookupTable(alpha=True)
        np_image = np.roll(raw_image, shift, axis=1)
        shift += 80
        return makeRGBA(np_image, levels=levels, lut=lut)[0]

    @staticmethod
    def bar():
        rows = 5
        cols = 10
        a = np.zeros((rows, cols))
        a[0:3, :] = np.ones((1, cols))
        return a

    @staticmethod
    def gaussian(sigma=1):
        x_grid = 750
        y_grid = 500
        muu = 0

        x, y = np.meshgrid(np.linspace(-5, 5, x_grid), np.linspace(-5, 5, y_grid))
        dst = np.sqrt(x ** 2 + y ** 2)
        return np.exp(-((dst - muu) ** 2 / (2.0 * sigma ** 2)))

    def run(self):
        count = 0
        while count < 5:
            time.sleep(0.2)
            count += 1
            self.new_image.emit(_UpdateThread.gaussian())


class _Example(QMainWindow):
    _x_roll = 250
    _y_roll = 150
    _image_count = 0

    def __init__(self):
        super().__init__()
        self._image_ids = []

        w = QWidget()
        w.setLayout(QVBoxLayout())

        self._multi_image_widget = MultiImageWidget3D(None, bar_position=Qt.LeftEdge)
        self._multi_image = self._multi_image_widget.multi_image()

        updater = _UpdateThread()
        updater.new_image.connect(self.add_image)
        updater.start()

        w.layout().addWidget(self._multi_image_widget)

        controls = QWidget()
        controls.setLayout(QHBoxLayout())

        col1 = QWidget()
        col1.setLayout(QVBoxLayout())

        set_opacity_50 = QPushButton("Opacity 50%")
        set_opacity_50.clicked.connect(lambda: self._multi_image.opacity(50))
        col1.layout().addWidget(set_opacity_50)

        set_opacity_100 = QPushButton("Opacity 100%")
        set_opacity_100.clicked.connect(lambda: self._multi_image.opacity(100))
        col1.layout().addWidget(set_opacity_100)
        controls.layout().addWidget(col1)

        col2 = QWidget()
        col2.setLayout(QVBoxLayout())
        dark = QPushButton("Dark")
        dark.clicked.connect(self._multi_image_widget.dark_)
        col2.layout().addWidget(dark)

        light = QPushButton("Light")
        col2.layout().addWidget(light)
        light.clicked.connect(self._multi_image_widget.light_)
        controls.layout().addWidget(col2)

        col3 = QWidget()
        col3.setLayout(QVBoxLayout())
        z_space_small = QPushButton("Small Z Spacing (10)")
        z_space_small.clicked.connect(lambda: self._multi_image_widget.set_z_spacing(10))
        z_space_medium = QPushButton("Medium Z Spacing (50)")
        z_space_medium.clicked.connect(lambda: self._multi_image_widget.set_z_spacing(50))
        col3.layout().addWidget(z_space_small)
        col3.layout().addWidget(z_space_medium)
        controls.layout().addWidget(col3)

        w.layout().addWidget(controls)

        self.setCentralWidget(w)
        self.show()
        self.resize(1100, 600)
        self.move(100, 300)

    @pyqtSlot(np.ndarray)
    def add_image(self, raw_image):
        self._image_count += 1
        np_image = np.array(raw_image)
        np_image = np.roll(np_image, self._x_roll, axis=1)
        np_image = np.roll(np_image, self._y_roll, axis=0)

        self._x_roll -= 80
        self._y_roll -= 30

        self._image_ids.append(self._multi_image_widget.set_data(np_image.tolist()))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.resize(600, 600)
    ex.show()

    sys.exit(app.exec_())
