import sys
import numpy as np
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget


class MultiAxisPlot(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.setGeometry(100, 100, 800, 600)

        # Create a central widget
        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        # Create a layout for the central widget
        layout = QVBoxLayout()
        central_widget.setLayout(layout)

        # Create a PlotWidget
        self.plot_widget = pg.PlotWidget()
        layout.addWidget(self.plot_widget)

        # Create two y-axes
        self.plot_widget.addLegend()
        self.plot_widget.showGrid(x=True, y=True)
        self.plot_item1 = self.plot_widget.plotItem
        self.plot_item1.setLabel('left', 'Primary Y Axis')
        self.plot_item2 = pg.ViewBox()
        self.plot_item1.showAxis('right')
        self.plot_item1.scene().addItem(self.plot_item2)
        self.plot_item1.getAxis('right').linkToView(self.plot_item2)
        self.plot_item2.setXLink(self.plot_item1)
        self.plot_item1.getAxis('right').setLabel('Secondary Y Axis', color='#FF0')

        # Generate some data
        x = np.linspace(0, 10, 100)
        y1 = np.sin(x)
        y2 = np.cos(x) * 5

        # Plot data on the primary y-axis
        self.plot_primary = self.plot_item1.plot(x, y1, pen='b', name='sin(x)')

        # Plot data on the secondary y-axis
        self.plot_secondary = pg.PlotDataItem(x=x, y=y2, pen='r')
        self.plot_item2.addItem(self.plot_secondary)

        # Connect sigResized signal
        # self.plot_item1.sigResized.connect(self.onPlotResized)

        self.show()

    def onPlotResized(self, event):
        geometry = self.plot_widget.geometry()  # Get PlotWidget's geometry
        self.setGeometry(geometry)  # Resize the main window to match the PlotWidget's size


def main():
    app = QApplication(sys.argv)
    ex = MultiAxisPlot()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
