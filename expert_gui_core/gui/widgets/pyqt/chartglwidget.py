
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import pyqtgraph.opengl as gl
import OpenGL.GL as ogl
import pyqtgraph as pg
import numpy as np
import sys
import time
import traceback
import fontawesome as fa  
import qtawesome as qta
from functools import partial
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm

import sys
import math

import threading

import scipy.ndimage as ndi

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.comm.data import DataObject
from expert_gui_core.gui.common.colors import Colors


class ChartGLWidget(QWidget):

    """    
    Low-level chart widget displaying multi-arrays.
    
    :param parent: Parent object.
    :type param: object
    :param type_plot: Type plot element shown (lines, surface, points, default="lines")
    :type type_plot: str, optional    
    :param spatial: Force 2D view in 3D space (default=True)    
    :type spatial: bool, optional
    """

    def __init__(self, parent, type_plot="lines", spatial=True, palette_line="stroke",color_amplitude=True,low_res=True):
        super(QWidget, self).__init__(parent)
        
        qta.icon("fa5.clipboard")
        
        # layout

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)                
        
        # chart

        self._chart_gl_view_widget = _ChartGLViewWidget(
                                                type_plot=type_plot,
                                                spatial=spatial,
                                                width=self.width(),
                                                height=self.height(),
                                                palette_line=palette_line,
                                                color_amplitude=color_amplitude,
                                                low_res=low_res)
        
        self.layout.addWidget(self._chart_gl_view_widget,0,1)
        
        # skin theme

        self.light_()
        self.initial_size = QSize(self.width(),self.height())
        self.sizeHint()
        self.listColors = {
            "default":["default","default"],
            "seismic":["seismic","CET-D1A"],
            "bwr":["bwr","CET-D1"],
            "gray":["gray","CET-L1"],
            "dyngray":["dyngray","dyngray"],
            "dbgray":["dbgray","dbgray"],
            "expgray":["expgray","expgray"],
            "jet":["jet","CET-R4"],
            "hot":["hot","CET-L3"],
            "cividis":["cividis","CET-CBL2"],
            "viridis":["viridis","viridis"],
            "inferno":["inferno","inferno"],
            "plasma":["plasma","plasma"],
            "blues":["Blues","CET-L6"],
            "greens":["Greens","CET-L5"],
            "greys":["Greys","CET-L2"],
            "ylorrd":["YlOrRd","CET-L18"],
        }

        # data
        
        self._data = None

        # color map
        
        if type_plot == "surface":
            self.set_colormap()

    def set_colormap(self,name=None,colormap='default'):
        """
        Set colormap for image.

        :param name: Name of the plot.
        :type name: str
        :param colormap: Palette name ("seismic","bwr","gray","jet","hot","cividis","viridis","inferno","plasma","blues","greens","greys","ylorrd", default="seismic")
        :type colormap: str, optional
        """

        if self.listColors[colormap][0] == "dbgray":      
            self._chart_gl_view_widget._cmap = Colors.CMAP_DBGRAY
        elif self.listColors[colormap][0] == "expgray":                  
            self._chart_gl_view_widget._cmap = Colors.CMAP_EXPGRAY
        elif self.listColors[colormap][0] == "default":                         
            self._chart_gl_view_widget._cmap = Colors.CMAP_DEFAULT
        elif self.listColors[colormap][0] == "dyngray":      
            self._chart_gl_view_widget._cmap = Colors.CMAP_DYNGRAY
        else:
           self._chart_gl_view_widget._cmap = matplotlib.cm.get_cmap(self.listColors[colormap][0],1000) 
                          
        if self._data is not None:
            self.set_data(self._data)

    def set_type_plot(self,type_plot="surface"):
        """
        Set/define type plot.

        :param type_plot: Type plot ("lines","points","surface",default="surface")
        :type type_plot: str, optional        
        """
        self._chart_gl_view_widget.clear()
        self._chart_gl_view_widget.create_axis()
        self._chart_gl_view_widget._surface = None
        self._chart_gl_view_widget._type_plot = type_plot
        self._chart_gl_view_widget._grid = False
        self._chart_gl_view_widget._traces = dict() 
        if self._chart_gl_view_widget.opts['bgcolor'][0] == (20./255.):
            self._chart_gl_view_widget.dark_()
        else:
            self._chart_gl_view_widget.light_()
        if self._data is not None:
            self.set_data(self._data)
        self.sizeHint()

    def dark_(self):
        """
        Set dark theme.
        """
        self._chart_gl_view_widget.dark_()

    def light_(self):
        """
        Set light theme.
        """
        self._chart_gl_view_widget.light_()
        
    def set_data(self, data, update=True, name="default", name_parent="default", max=-1, *args, **kwargs):
        """
        Set data in a plot item.

        :param data: Data object
        :type data: expert_gui_core.comm.data.DataObject
        :param update: Force update (default=True).
        :type update: bool, optional
        :param name: Name of the plot (default="default")
        :type name: str, optional
        :param name_parent: Name of the parent plot (default="default")
        :type name_parent: str, optional        
        """
        self._data = data
        self._chart_gl_view_widget.set_data(data,update,name,name_parent,max,*args,**kwargs)        
        
    def get_mutex(self):
        """
        Get the current mutex value.

        :rtype: bool
        """
        return self._chart_gl_view_widget._mutex
    
    def sizeHint(self):
        """
        Resize event.
        """
        self._chart_gl_view_widget.sizeHintCustom(self.width(),self.height())
        return QSize(self.width(),self.height())
        
    def contextMenuEvent(self, event):
        """
        Menu event.
        """
        cmenu = QMenu(self)
        cmenu.addAction("Show surface", partial(self.set_type_plot, type_plot="surface"))
        cmenu.addAction("Show lines", partial(self.set_type_plot, type_plot="lines"))
        cmenu.addAction("Show points", partial(self.set_type_plot, type_plot="points"))
        palettes_menu = cmenu.addMenu('&Palettes')
        for pal in self.listColors.keys():
            palettes_menu.addAction(pal, partial(self.set_colormap, colormap=pal))                
        action = cmenu.exec_(self.mapToGlobal(event.pos()))


class _ChartGLViewWidget(gl.GLViewWidget):

    
    """    
    Class low-level chart widget layout displaying multi-arrays
    
    :param type_plot: Type plot element shown (lines, surface, points, default="lines").
    :type type_plot: str, optional    
    :param spatial: Force 2D view in 3D space (default=True)    .
    :type spatial: bool, optional
    :param width: Specific width (default=10).
    :type width: int, optional
    :param height: Specific height (default=10).
    :type height: int, optional
    :param min_refresh_time: Control minimum time between refresh event (default=10ms).
    :type min_refresh_time: int, optional    
    """

    update_signal = pyqtSignal(str,str,int,tuple,dict)

    mutex = threading.Lock()

    def __init__(self,type_plot="lines",spatial=True,width=10,height=10,min_refresh_time=50,palette_line="stroke",color_amplitude=True,low_res=False):
        
        super().__init__()
        
        self._data = {}        
        self._traces = dict()
        self._surface = None
        self._type_plot = type_plot
        self._min_refresh_time = min_refresh_time
        self._max = -1
        self._color_amplitude = color_amplitude
        self._low_res = low_res

        # timing update security
        self._update_time = 0
        
        # 3D or not ?
        if spatial:
            self._factor = 1.
        else:
            self._factor = 0.
        
        # palette lines
        self._palette_line = palette_line

        # Grids 3D
        self._grid = False
        self._xgrid = gl.GLGridItem()
        self._ygrid = gl.GLGridItem()
        self._zgrid = gl.GLGridItem()  
        self._zmax = {}
        self._zmin = {}
        self._xmin = {}
        self._xmax = {}
        self._ymin = {}
        self._ymax = {}
        self.create_axis()
        
        # Width & Height
        if height == 0:
            height = 1
        self._height = height
        self._width = width        
        self._h_on_w = width / height

        self.config_camera()

        self._cmap = None
        
        self.update_signal.connect(self.update_plot)

    def config_camera(self):
        # Config camera
        if self._h_on_w > 0.99:
            distance = self._factor * 18 + 18*self._h_on_w
        else:
            if self._h_on_w < 0.6:
                distance = self._factor * 18 + 18./self._h_on_w
            else:
                distance = self._factor * 18 + 25./self._h_on_w
        
        elevation = 25 * self._factor
        if self._factor == 0:
            azimuth = -90
        else:
            azimuth = 260
        self.setCameraPosition(distance=distance, azimuth=azimuth, elevation=elevation)
        
    def set_data(self, data, update=True, name="default", name_parent="default", max=-1, *args, **kwargs):
        """
        Set data in a plot item.

        :param data: Data object
        :type data: expert_gui_core.comm.data.DataObject
        :param update: Force update (default=True).
        :type update: bool, optional
        :param name: Name of the plot (default="default")
        :type name: str, optional
        :param name_parent: Name of the parent plot (default="default")
        :type name_parent: str, optional        
        """        
        self._data[name] = np.array(data)
        if time.time() * 1000 - self._update_time < self._min_refresh_time:
            return
        self._update_time = time.time() * 1000
        if update:
            self.update_signal.emit(name,name_parent,max,args,kwargs)
    
    @pyqtSlot(str, str, int, tuple, dict)
    def update_plot(self,name='default', name_parent="default", max=-1, args=None, kwargs=None):
        """
        Update plot.

        :param name: Name of the plot (default="default")
        :type name: str, optional
        :param name_parent: Name of the parent plot (default="default")
        :type name_parent: str, optional                
        """

        # mutex check
        self.mutex.acquire()

        # data

        try:
            if len(self._data[name]) == 0:
                self.mutex.release()
                return
        except: 
            pass

        data = self._data[name].copy()

        # filtering

        # data = ndi.gaussian_filter(data, sigma=3)

        nlines = len(data)
        nlines_real = nlines
        self.npoints = len(data[0])
        npoints_real = self.npoints

        # data reduction

        if self._low_res:

            maxline = 100
            inc = 1
            inc2 = 1
            
            if nlines > maxline:
                inc = int(nlines/maxline)            
            if len(data[0]) > maxline:
                inc2 = int(len(data[0]) /maxline)            
            if inc != 1 or inc2 != 1:
                try:
                    data = data[::inc,::inc2]
                except:
                    pass

            self.npoints = len(data[0])  
            nlines = len(data)

        # define min max delta

        try:
            self._zmax[name] = np.amax(data)
            self._zmin[name] = np.amin(data)
        except:
            self.mutex.release()
            return

        self._xmin[name] = 0
        self._xmax[name] = npoints_real
        self._ymin[name] = 0
        self._ymax[name] = nlines_real

        delta = self._zmax[name] - self._zmin[name]        

        if delta == 0.:
            delta += 0.00000001

        # grids        
                        
        if self._grid == False:

            self._grid = True
            if self._factor != 0:
                self._xgrid.rotate(90, 0, 1, 0)
                self._xgrid.translate(-10, 0, 0)
                self._xgrid.setDepthValue(10000)
                self._xgrid.setGLOptions('translucent')
                self.addItem(self._xgrid)
                self._zgrid.translate(0, 0, -10)
                self._zgrid.setDepthValue(10001)
                self._zgrid.setGLOptions('translucent')
                self.addItem(self._zgrid)
                self._ygrid.rotate(90, 1, 0, 0)
                self._ygrid.translate(0, -10, 0)
                self._ygrid.setSize(20.,20.,0)
                self._ygrid.setSpacing(2,2,0)
                self._ygrid.setDepthValue(10002)
                self._ygrid.setGLOptions('translucent')
                self.addItem(self._ygrid)
            else:
                self._ygrid.rotate(90, 1, 0, 0)
                self._ygrid.translate(0, 0, 0)
                self._ygrid.setSize(10,10,3)
                self._ygrid.setSpacing(2,2,0)
                self._ygrid.setDepthValue(10002)
                self._ygrid.setGLOptions('translucent')
                self.addItem(self._ygrid)

            self.sizeHintCustom(self._width-1,self._height-1,update=False)

        # X Y axis calibrated for [-10,10] scale
        
        y = np.linspace(-10*self._factor, 10*self._factor, nlines)[::-1]
        x = np.linspace(-10*self._h_on_w , 10*self._h_on_w, self.npoints)
       
        # axis
        self.update_axis()

        # if new data length is less than old data length, delete old PlotItems

        traces_len = len(self._traces)
        if traces_len > nlines:
            for i in range(traces_len - nlines, traces_len):
                self.removeItem(self._traces[i])
                del self._traces[i]

        # surface

        if self._type_plot == "surface":
            
            pts = np.vstack((19.8/delta)*(data-self._zmin[name])-9.9).transpose()
            pts = np.flip(pts, axis=None)
            
            minZ = np.amin(pts)
            maxZ = np.amax(pts)
            deltaZ = maxZ-minZ            
            if deltaZ == 0.:
                deltaZ = 0.0001
            
            if self._cmap is None:
                self._cmap = matplotlib.cm.get_cmap('jet')
            
            try:
                colors_rgb = self._cmap((pts-minZ)/deltaZ)                                
            except Exception as e:
                self.mutex.release()
                return

            if self._surface is None:            
                self._surface = gl.GLSurfacePlotItem(
                        x=np.flip(x),
                        y=y,
                        z=pts,
                        drawEdges=False,
                        colors=colors_rgb,
                        computeNormals=False)

                self.addItem(self._surface)   
                self._surface.setGLOptions('translucent')
                self._surface.setDepthValue(100)
            else:
                self._surface.setData(
                        x=np.flip(x),
                        y=y,
                        z=pts,
                        colors=colors_rgb)
        
        # lines

        elif self._type_plot == "lines":

            if self._factor == 0:
                xl = x[::]
            else:
                xl = x[::]
            
            pts = np.vstack((20./delta)*(data-self._zmin[name])-10.).transpose()
            
            minZ = np.amin(pts)
            maxZ = np.amax(pts)
            deltaZ = maxZ-minZ
            if deltaZ == 0.:
                deltaZ = 0.0001
            
            if self._cmap is None:
                self._cmap = matplotlib.cm.get_cmap('jet')
            
            for i in range(nlines):

                yi = np.array([y[nlines-i-1]] * self.npoints)            
                pts = np.vstack([xl, yi, (20./delta)*(data[i]-self._zmin[name])-10.])

                if self._color_amplitude:
                    colors_rgb = self._cmap((pts[2].transpose()-minZ)/deltaZ) 
                else:
                    colors_rgb = Colors.getColor((i/nlines),
                                                 max=nlines,
                                                 palette=self._palette_line)
                
                if i in self._traces.keys():
                    self._traces[i].setData(
                        pos=pts.transpose(), 
                        color=colors_rgb)
                else:                            
                    self._traces[i] = gl.GLLinePlotItem(
                        pos=pts.transpose(),
                        color=colors_rgb,
                        width=1,
                        antialias=False)
                    self._traces[i].setDepthValue(100+i)
                    self._traces[i].setGLOptions('translucent')
                    self.addItem(self._traces[i])    
        
        # points

        elif self._type_plot == "points":

            if self._factor == 0:
                xl = x[::]
            else:
                xl = x[::]

            ptsm = np.vstack((20./delta)*(data-self._zmin[name])-10.).transpose()
            
            minZ = np.amin(ptsm)
            maxZ = np.amax(ptsm)
            deltaZ = maxZ-minZ
            if deltaZ == 0.:
                deltaZ = 0.0001

            for i in range(nlines):   

                yi = np.array([y[nlines-i-1]] * self.npoints)            
                pts = np.vstack([xl, yi, (20./delta)*(data[i]-self._zmin[name])-10.])

                if self._color_amplitude:
                    colors_rgb = self._cmap((pts[2].transpose()-minZ)/deltaZ) 
                else:
                    colors_rgb = Colors.getColor((i/nlines),
                                                 max=nlines,
                                                 palette=self._palette_line)

                if i in self._traces.keys():
                    self._traces[i].setData(
                        pos=pts.transpose(), 
                        color=colors_rgb,   
                    )

                else:             
                      
                    self._traces[i] = gl.GLScatterPlotItem(
                        pos=pts.transpose(),
                        color=colors_rgb,                        
                        size=5
                    )
                    
                    self._traces[i].setDepthValue(5000-i)
                    
                    self._traces[i].setGLOptions("translucent")

                    self.addItem(self._traces[i])

        self.mutex.release()

    def create_axis(self):
        """
        Creation of the 3 axis witb 6 items.
        """
        self._axis = [None for x in range(18)]
        pos = (0,0,0)         
        text = str(0)
        font = QFont('Helvetica', 6)    
        for i in range(18):
            self._axis[i] = gl.GLTextItem(pos=pos, text=text, font=font)          
            self.addItem(self._axis[i])

    def update_axis(self):
        """
        Update of all axis items.
        """
        visible = 5
        if self._width < 600:
            visible = 1
        if self._width < 350:
            visible = -1          
        if self._factor == 0:
            number = 12
        else:
            number = 18 
        for name in self._ymin.keys():
            if visible != -1:
                deltax = (self._xmax[name]-self._xmin[name])/5
                deltaz = (self._zmax[name]-self._zmin[name])/5
                if self._factor != 0:                    
                    deltay = (self._ymax[name]-self._ymin[name])/5
            for i in range(number): 
                if visible != -1:
                    if visible == 1:
                        if i==0 or i==5 or i==6 or i==11 or i==12 or i==17:
                            self._axis[i].setVisible(True)
                        else:
                            self._axis[i].setVisible(False)
                    else:
                        self._axis[i].setVisible(True)
                else:
                    self._axis[i].setVisible(False)
            # y
            if self._factor != 0:                
                for i in range(visible+1):
                    if visible == 1:
                        if i == 1:
                            i = 5
                    pos=(
                        self._h_on_w*(-13.),
                        (-10+2*2*i),
                        -10)         
                    text = self.auto_scientific(self._ymin[name] + i*deltay, deltay)
                    self._axis[i+12].pos = pos
                    self._axis[i+12].text = text
                    self._axis[i+12].update()
            else:
                for i in range(6): 
                    self._axis[i+12].setVisible(False)
            
            # x
            for i in range(visible+1):
                if visible == 1:
                    if i == 1:
                        i = 5
                pos=(self._h_on_w*(-10+2*(1.9+0.1*(1.-self._factor))*i),
                     -12.*self._factor,
                     -12.5 - (1-self._factor)*1.2)  
                
                text = self.auto_scientific(self._xmin[name] + i*deltax, deltax)
                self._axis[i].pos = pos
                self._axis[i].text = text
                self._axis[i].update()
            
            # z
            gap = 0
            for i in range(visible+1):
                if visible == 1:
                    gap = 1.
                    if self._h_on_w <= 0.99:
                        gap = 5.
                    if i == 1:
                        i = 5
                else:
                    if self._h_on_w <= 0.99:
                        gap = 2. / self._h_on_w
                pos=(self._h_on_w*(10.5)-(1-self._factor)*(21.5+gap)*self._h_on_w,
                     -11*self._factor,
                     (-10+2*2*i))         
                text = self.auto_scientific(self._zmin[name] + i*deltaz, deltaz)
                self._axis[i+6].pos = pos
                self._axis[i+6].text = text
                self._axis[i+6].update()
                                
    def auto_scientific(self,value,delta=1):
        """
        Automatic scientific notation dependeing on delta.

        :param value: Input value.
        :type value: float 
        :param delta: Check delta resolution (default=1)
        :type: float, optional
        :rtype: str
        """
        if delta > 10000:
            return str("{:.2e}".format(value))
        if delta < 0.01:
            return str("{:.2e}".format(value))
        return str(int(value*1000)/1000)

    def scientific(self,value):
        """
        Standard scientifc notation.

        :param value: Input value.
        :type: float
        :rtype: str
        """
        return str("{:.2e}".format(value))

    def sizeHintCustom(self, width, height, update=True):
        """
        Widget is being resized.

        :param width: Specific width.
        :type width: int
        :param height: Specific height.
        :type height: int        
        """
        
        if width == self._width and height == self._height:
            return

        self._height = height
        self._width = width    

        # Calculate h on w then distance of camera
        self._h_on_w = width / height
        
        if self._h_on_w > 0.99:
            if self._h_on_w < 1.5:
                distance = self._factor * 18 + 19*self._h_on_w
            else:
                distance = self._factor * 18 + 30*self._h_on_w
        else:
            if self._h_on_w < 0.6:
                distance = self._factor * 18 + 19./self._h_on_w
            else:
                distance = self._factor * 18 + 27./self._h_on_w
        if self._factor == 0:
            distance = distance + 3
        distance = distance + 10
        
        # # Update axis item
        self.update_axis()

        # # Adjust camera position
        self.setCameraPosition(distance=distance,
                               azimuth=self.cameraParams()["azimuth"],
                               elevation=self.cameraParams()["elevation"])

        # y grid position
        if self._ygrid is not None:
            if self._factor != 0:
                self._ygrid.resetTransform()
                self._ygrid.rotate(90, 1, 0, 0)
                self._ygrid.translate(0, 10, 0)
                self._ygrid.setSize(20.*self._h_on_w,20.,0)
                self._ygrid.setSpacing(2.*self._h_on_w,2,0)                
            else:
                self._ygrid.resetTransform()
                self._ygrid.rotate(90, 1, 0, 0)
                self._ygrid.translate(0, 0, 0)
                self._ygrid.setSize(20.*self._h_on_w,20.,3)
                self._ygrid.setSpacing(2.*self._h_on_w,2,0)

        # x grid position         
        if self._xgrid is not None:
            self._xgrid.resetTransform()
            self._xgrid.setSize(20.,20.,0)
            self._xgrid.setSpacing(2.,2.,0)
            self._xgrid.translate(0,0,10.*self._h_on_w)
            self._xgrid.rotate(90, 0, 1, 0)      
               
        # z grid position   
        if self._zgrid is not None:
            self._zgrid.resetTransform()
            self._zgrid.setSize(20.*self._h_on_w,20.,20.)
            self._zgrid.translate(0,0,-10.)
            self._zgrid.setSpacing(2.*self._h_on_w, 2.,2.)     

        if update:
            for name in self._data:
                self.update_plot(name=name)

    def dark_(self):
        """
        Set dark theme.
        """ 

        self.setBackgroundColor((20,20,20))
        if self._ygrid is not None:
            self._ygrid.setColor((60,60,60))
            if self._factor != 0:
                for i in range(12,18):
                    self._axis[i].color = QColor(90,90,90)
                    self._axis[i].update()
        if self._xgrid is not None:
            self._xgrid.setColor((60,60,60))
            for i in range(0,6):
                self._axis[i].color = QColor(90,90,90)
                self._axis[i].update()
        if self._zgrid is not None:
            for i in range(6,12):
                self._axis[i].color = QColor(90,90,90)
                self._axis[i].update()
            self._zgrid.setColor((60,60,60))

    def light_(self):

        """
        Set light theme.
        """

        self.setBackgroundColor((250,250,250))    
        if self._ygrid is not None:
            self._ygrid.setColor((230,230,230))
            if self._factor != 0:
                for i in range(12,18):
                    self._axis[i].color = QColor(190,190,190)
                    self._axis[i].update()
        if self._xgrid is not None:
            self._xgrid.setColor((230,230,230))
            for i in range(0,6):
                self._axis[i].color = QColor(190,190,190)
                self._axis[i].update()
        if self._zgrid is not None:
            self._zgrid.setColor((230,230,230))
            for i in range(6,12):
                self._axis[i].color = QColor(190,190,190)
                self._axis[i].update()


class _Example(QMainWindow):

    """
    
    Example class to test
    
    """

    def __init__(self):
        super().__init__()
        self.init_ui()
                
    def init_ui(self):        
        
        """Init user interface"""

        # self._type_plot_3d = "lines" 
        self._type_plot_3d = "surface" 
        # self._type_plot_3d = "points"  
        self.show_chartgl_3d = True
        self._chart_gl = ChartGLWidget(self, type_plot=self._type_plot_3d, spatial=self.show_chartgl_3d)  
        
        self.setWindowTitle('3D')
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self._chart_gl, 0, 0)
        self.setCentralWidget(self._chart_gl)
        self.resize(1000,800)        
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_LBLACK+";")
        self._chart_gl.light_()

    def show(self):
        # super().show()
        size = 20
        sigma = 1.
        muu = 0.
        x, y = np.meshgrid(np.linspace(0, 2, size), np.linspace(0, 2, size))
        dst = np.sqrt(x**2+y**2) 
        gauss = np.exp(-((dst-muu)**2 / (2.0 * sigma**2)))
        self._chart_gl.set_data(gauss, name="TEST", update=True)        
        super().show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()    
    sys.exit(app.exec_())
