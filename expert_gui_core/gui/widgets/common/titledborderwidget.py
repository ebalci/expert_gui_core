from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors


class TitledBorderWidget(QWidget):    

    """
    Titled border panel.
    Panel surrounded with line and text string shown on top left corner.

    :param title: Title.
    :type title: str
    """

    def __init__(self,title):
        """
        Initialize class.
        """
        QWidget.__init__(self)
        self._title = title        

    def paintEvent(self,e): 
        """
        Paint event handling.
        """    
        painter = QPainter()
        try:
            painter.begin(self)            
        except:
            painter.end()
            painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        
        size_font = self.font().pointSize()
        half_size = int(size_font/2)-1
        if half_size < 0:
            half_size = 0

        wstring = int(len(self._title)*size_font)

        if len(self._title) < 6:
            wstring = wstring + 6

        self.w_rect = self.frameGeometry().width()-4-4
        self.h_rect = self.frameGeometry().height()-3-5-half_size
        
        painter.fillRect(0,0, self.frameGeometry().width(), self.frameGeometry().height(),self.palette().color(self.backgroundRole()))

        painter.setPen(Colors.COLOR_LIGHT4GRAY)
        painter.drawRect(4,5+half_size,self.w_rect,self.h_rect)        
        
        painter.fillRect(10,2+half_size,wstring,12,self.palette().color(self.backgroundRole()))
        
        painter.setPen(Colors.COLOR_LIGHT3GRAY)        
        painter.drawText(QPoint(12,12+half_size+0)," "+self._title+" ")

        painter.end()

    def dark_(self):
        """Set dark theme"""
        self.setStyleSheet("background-color: "+Colors.STR_COLOR_L2BLACK+";color:"+Colors.STR_COLOR_WHITE+";")

    def light_(self):
        """Set light theme"""
        self.setStyleSheet("background-color: "+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK+";")

if __name__ == "__main__":
    app = QApplication(sys.argv)

    darkpalette = QPalette()   
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)
    
    MainWindow = QMainWindow()

    MainWindow.setStyleSheet("background-color: "+Colors.STR_COLOR_LBLACK+";color:"+Colors.STR_COLOR_WHITE+";")


    central_widget = QWidget() 
    layout = QGridLayout(central_widget)
    
    sw = TitledBorderWidget("TEST")
    
    tblayout = QGridLayout(sw)        
    tblayout.setContentsMargins(20,20,20,20)
    
    l = QLabel("This is a test")
    l.setStyleSheet("background-color:red;")
    tblayout.addWidget(l,0,0)

    font_text_user = app.font()
    font_text_user.setPointSize(11)
    app.setFont(font_text_user)  

    layout.addWidget(sw,0,0)
    MainWindow.setCentralWidget(central_widget)
    MainWindow.resize(400,400)
    MainWindow.show()

    app.exec()