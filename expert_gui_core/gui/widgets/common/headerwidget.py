from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import time
import fontawesome as fa  
import qtawesome as qta

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.widgets.common.timingtickwidget import _TimingTickWidget
from expert_gui_core.gui.common.colors import Colors


class _ExpandFrame(QWidget):

    """    
    Frame to expand component.
    ex : only used by header widget.
    
    :param parent: Parent object
    :type parent: object
    :param layout: Input external layout
    :type layout: GridLayout, optional
    :param header: Show or not the header bar (default=True).
    :type header: bool, optional
    :param datawidget: Data widget to be shown inside
    :type datawidget: class:expert_gui_core.gui.widgets.pyqt.datawidget, optional
    """

    def __init__(self, parent = None, layout=None, header=None, datawidget=None): 
        """
        Initialize class.
        """  
        super().__init__()

        qta.icon("fa5.clipboard")

        self._parent = parent
        if datawidget is None:
            try:
                self._datawidget = self._parent._data_widget
            except:
                self._datawidget = None
        else:
            self._datawidget = datawidget
        if header is None:
            try:
                self._header = self._parent._headerWidget
            except:
                self._header = None                
        else:
            self._header = header
        if layout is None:
            try:
                self._layoutparent = self._parent.layout
            except:
                self._layoutparent = None                
        else:
            self._layoutparent = layout
        self.setWindowTitle("QGridLayout Example")
        self._layout = QGridLayout()
        self.setLayout(self._layout)
        self.resize(600, 300)
        
    def show(self):
        """
        Show window (outside).
        """
        if self._datawidget is not None:
            self._width = self._datawidget.width()
            self._height = self._datawidget.height()
        if self._header is not None:
            self._layout.addWidget(self._header)
        if self._datawidget is not None:
            self._layout.addWidget(self._datawidget)
        if Colors.COLOR_LIGHT:
            self.light_()
        else:
            self.dark_()
        super().show()
    
    def closeEvent(self,event):
        """
        Close window.
        """
        self.showNormal()
        if self._datawidget is not None:
            self._datawidget.showNormal()
            self._datawidget.resize(self._width,self._height)
        if self._header is not None:
            self._layoutparent.addWidget(self._header,0,0)
        if self._datawidget is not None:
            self._layoutparent.addWidget(self._datawidget,1,0)
        self.hide()
        event.ignore()

    def dark_(self):
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_BLACK+";color:"+Colors.STR_COLOR_WHITE)
        if self._datawidget is not None:
            try:
                self._datawidget.dark_()
            except:
                pass
        
    def light_(self):
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";color:"+Colors.STR_COLOR_BLACK)
        if self._datawidget is not None:
            try:
                self._datawidget.light_()
            except:
                pass
        
class HeaderWidget(QWidget):

    """    
    Header bar for all GUI plot/display widgets.

    :param parent: Parent object
    :type parent: object
    :param layout: Input external layout
    :type layout: GridLayout
    :param datawidget: Data widget to be shown inside
    :type datawidget: class:expert_gui_core.gui.widgets.pyqt.datawidget, optional
    :param palette: Color palette (rainbow ocean summer autumn spring winter bbq winter hawai dyngray dbgray expgray purple stroke, default=stroke).
    :type palette: str, optional
    :param tick: Show ticking events panel (default=True)
    :type tick: bool, optional
    """    
    
    update_led_signal = pyqtSignal()

    def __init__(self, parent = None, layout=None, datawidget=None, palette="stroke", pause=True, tick=True):
        """
        Initialize class.
        """
        QWidget.__init__(self)   

        qta.icon("fa5.clipboard")

        # init param

        self._parent = parent
        if parent is not None:
            self._frame = _ExpandFrame(parent,header=self,layout=layout,datawidget=datawidget)
        else:
            self._frame = None
        self._device = ""
        self._cycle = ""
        self._time = -1  
        self._cycle = ""
        self._palette = palette
        self._name = None
        self._tick = tick

        self.time_now = QDateTime()
        
        # size and background

        self.setAutoFillBackground(True)

        # layout

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)        

        # toolbar

        self._toolBar = QToolBar()
        self._toolBar.setOrientation(Qt.Horizontal)
        self._toolBar.setContentsMargins(0, 0, 0, 0)
        self._toolBar.layout().setSpacing(0)
        self._toolBar.layout().setContentsMargins(0, 0, 0, 0)
  
        self.font_size = 8
        self.font_icon_size = 7

        font_cell = QFont()
        font_cell.setFamily("FontAwesome")

        # led

        self.toolButton_led = QToolButton()
        self.toolButton_led.setFont(font_cell)
        self.toolButton_led.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHTORANGE+";")
        self.toolButton_led.setText(fa.icons["circle"])
        self._toolBar.addWidget(self.toolButton_led)
        self._led = False
        self.update_led_signal.connect(self.update_led)

        # time

        self._time_label = QLabel("")
        self._time_label.setStyleSheet("padding:0 10 0 0;margin:0;border:none;color:#777;")
        self._toolBar.addWidget(self._time_label)

        # comment label

        self._comment_label = QLabel("")
        self._comment_label.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHTORANGE+";")
        self._toolBar.addWidget(self._comment_label)

        # cycle label

        self._cycle_label = QLabel("")
        self._cycle_label.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_L2BLUE+";")
        self._toolBar.addWidget(self._cycle_label)
        
        # timing tick widget

        self._header_timing = _TimingTickWidget(palette=self._palette)
        self._header_timing.setMaximumWidth(200)
        self._header_timing.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)

        # dt label

        self._dt_label = QLabel("")
        self._dt_label.setMinimumWidth(100)
        self._dt_label.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHT3GRAY+";")

        if self._tick:
            self._toolBar.addWidget(self._header_timing)
            self._toolBar.addWidget(self._dt_label)

        # separator

        separator = QWidget()
        separator.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Expanding)
        self._toolBar.addWidget(separator)

        # pause

        self._pause = False
        self.toolButton_pause = QToolButton()
        self.toolButton_pause.setFont(font_cell)
        self.toolButton_pause.setStyleSheet("padding:0;margin:0;border:none;color:#777;")
        self.toolButton_pause.setText(fa.icons["pause"])
        self.toolButton_pause.clicked.connect(self.pause)
        if pause:
            self._toolBar.addWidget(self.toolButton_pause)

        # expand

        self.toolButton_expand = QToolButton()
        self.toolButton_expand.setFont(font_cell)
        self.toolButton_expand.setStyleSheet("padding:0;margin:0;border:none;color:#777;")
        self.toolButton_expand.setText(fa.icons["expand"])
        self.toolButton_expand.clicked.connect(self.expand)
        if self._frame is not None:
            self._toolBar.addWidget(self.toolButton_expand)

        # info

        # self.toolButton_info = QToolButton()
        # self.toolButton_info.setStyleSheet("padding:0;margin:0;border:none;color:"+Colors.STR_COLOR_BLUE+"")
        # self.toolButton_info.setText(fa.icons["info-circle"])
        # self.toolButton_info.clicked.connect(self.info)
        # self._toolBar.addWidget(self.toolButton_info)

        # add toolbar

        self.layout.addWidget(self._toolBar, 0, 0)
        
        self._time = 0

    def expand(self):
        """
        Expand the frame outside its parent component.
        """
        self._frame.show()
        
    def is_pause(self):
        """
        Check whether it is paused or not.
        """
        return self._pause

    def pause(self):
        """
        Pause receiving of notifications.
        """
        self._pause = not self._pause
        if self._pause:
            self.toolButton_pause.setText(fa.icons["play"])
        else:
            self.toolButton_pause.setText(fa.icons["pause"])

    def set_comment(self,comment):
        """
        Add comment text to the bar.

        :param comment: Comments
        :type comment: str
        """
        self._comment_label.setText(comment)

    def set_name(self,name):
        """
        Add name used for name filtering.

        :param name: Name/label to be shown (ex: cycle name)
        :type name: str
        """
        self._name = name

    def get_name(self):
        """
        Return active name for filtering.
        """
        return self._name

    def set_cycle(self,cycle):
        """
        Add cycle to the bar.

        :param cycle: Cycle name.
        :type cycle: str
        """
        if cycle is not None:
            self._cycle_label.setText("  "+cycle)

    def info(self):
        """
        Info/help option (not used).
        """
        pass
    
    def recv(self,cycle="",name=None):
        """
        Notification received.

        :param cycle: Cycle name.
        :type cycle: str
        :param name: Name to identify, if name is not the good one then no update
        """
        self._cycle = cycle
        if self._name is not None and name is not None and self._name != name:
            return
        self.update_led_signal.emit()

    @pyqtSlot()
    def update_led(self):
        """
        Update graphical components.
        """
        self._led = not self._led
        self.set_cycle(self._cycle)        
        ti = QDateTime.currentMSecsSinceEpoch()
        self.time_now.setMSecsSinceEpoch(ti)
        strnow = self.time_now.toString('yyyy-MM-dd hh:mm:ss.zzz')
        dti = time.time() * 1000
        self._time_label.setText(strnow)
        if dti != self._time:
            freq = str(int(10000./ (dti-self._time))/10.)
        else:
            freq = ""
        self._dt_label.setText(" " + str(int(dti-self._time)) + "ms " + freq + "Hz ")
        self._time = dti
        if self._tick:
            self._header_timing.recv(self._time)
        if self._led:
            self.toolButton_led.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHTORANGE+";")
        else:
            self.toolButton_led.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHT0GRAY+";")
        
    def dark_(self):
        """
        Set dark theme.
        """
        Colors.COLOR_LIGHT = False
        self._toolBar.setStyleSheet("QToolBar{border:0px solid "+Colors.STR_COLOR_BLACK+";border-radius:0px;background-color:"+Colors.STR_COLOR_BLACK+";}")
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_BLACK+";")
        self.color_bg = Colors.COLOR_LBLACK
        self._dt_label.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHT3GRAY+";")
        self._header_timing.dark_()
        if self._frame is not None:
            self._frame.dark_()
       
    def light_(self):
        """
        Set light theme.
        """
        Colors.COLOR_LIGHT = True
        self._toolBar.setStyleSheet("QToolBar{border:0px solid "+Colors.STR_COLOR_DWHITE+";border-radius:0px;background-color:"+Colors.STR_COLOR_DWHITE+";}")
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_DWHITE+";")
        self.color_bg = Colors.COLOR_DWHITE    
        self._dt_label.setStyleSheet("padding:0;margin:0;border:none;color: "+Colors.STR_COLOR_LIGHT4GRAY+";")
        self._header_timing.light_()
        if self._frame is not None:
            self._frame.light_()


class _Example(QMainWindow):

    """    
    Example class to test    
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()

    def init_ui(self):       
        """
        Init user interface.
        """ 
        w = QWidget()
        self._header = HeaderWidget()
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self._header, 1, 0)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(1200,310)
        self._header.recv()
        self.setStyleSheet("background-color:black;")
        self._header.dark_()
        # self._header.light_()
        self.show()  
        
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())