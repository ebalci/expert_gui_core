from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors


class LedWidget(QPushButton):

    """    
    Basic Led Button.    
    """    
    
    def __init__(self):
        """
        Initialize class.
        """
        QPushButton.__init__(self)
        self.color_inside = Colors.COLOR_INACTIVE
        self._show = True
        
    def set_color_inside(self,color):
        """
        Define color inside LED.

        :param color: Color of the LED.
        :type color: QColor
        """
        self.color_inside = color
        self.update()
    
    def blink(self):
        """
        hide/show.
        """
        if self._show:
            self._show = False
        else:
            self._show = True
        self.update()

    def paintEvent(self,e): 
        """
        Paint event handling.
        """    
        painter = QPainter()
        try:
            painter.begin(self)            
        except:
            painter.end()
            painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        self.w_rect = self.frameGeometry().width()
        self.h_rect = self.frameGeometry().height()
        gap = 2
        factor = 0.7
        if self.h_rect < 10 or self.w_rect < 20:
            gap = 1
            factor = 0.5
        self.h_line = self.h_rect - 2*gap
        self.w_cell = self.w_rect - 2*gap

        rcircle = self.w_cell/2
        if self.h_line < self.w_cell:
            rcircle = self.h_line/2
        if rcircle < 2:
            rcircle = 2
        center = QPoint(
            int(gap + self.w_cell/2),
            int(gap + self.h_line/2)
            )        
        rect = self.rect()
        grad1 = QLinearGradient(rect.topLeft(), rect.topRight())
        grad1.setColorAt(0, self.color_inside.darker(130))
        grad1.setColorAt(0.6, self.color_inside.lighter(130))
        painter.setPen(self.color_inside.darker(90))
        painter.setBrush(QBrush(grad1))
        if self._show:
            painter.drawEllipse(center,int(rcircle),int(rcircle))     
        painter.end()

    def light_(self):
        pass

    def dark_(self):
        pass


class _Example(QMainWindow):

    """    
    Example class to test    
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()

    def init_ui(self):       
        """
        Init user interface.
        """ 
        w = QWidget()
        self._led = LedWidget()
        self._led.set_color_inside(Colors.COLOR_LIGHTRED)            
        self._led2 = LedWidget()
        self._led2.set_color_inside(Colors.COLOR_LIGHTORANGE)            
        self._led3 = LedWidget()
        self._led2.setMinimumWidth(50)
        self._led2.setMinimumHeight(50)
        self._led3.set_color_inside(Colors.COLOR_BLUE)            
        mainLayout = QGridLayout() 
        mainLayout.addWidget(self._led, 0, 0)
        mainLayout.addWidget(self._led3, 0, 1)
        mainLayout.addWidget(self._led2, 0, 2)
        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(1200,310)
        self._led.blink()
        self._led.blink()
        self.setStyleSheet("background-color:"+Colors.STR_COLOR_BLACK+";")
        self.show()  
        
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())