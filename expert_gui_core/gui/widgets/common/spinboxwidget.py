from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from enum import Enum
import fontawesome as fa
import qtawesome as qta
import sys
import numpy as np

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.gui.common.colors import Colors
from expert_gui_core.gui.widgets.common.thermometer import _UpdateState


class _Arrow(QPushButton):
    
    """
    Arrow class object. 
    Base class icon arrow using font awesome lib.

    :param font_size: Font size (default=9).
    :type font_size: int, optional
    """

    def __init__(self, font_size=9):
        """
        Initialize class.
        """
        super().__init__()
        qta.icon("fa5.clipboard")
        self.setFocusPolicy(Qt.NoFocus)
        self._size_policy = self.sizePolicy()
        self._size_policy.setRetainSizeWhenHidden(True)
        self.setSizePolicy(self.sizePolicy())
        font_fa = QFont()
        font_fa.setFamily("FontAwesome")
        font_fa.setPointSize(9)
        self.setFont(font_fa)


class _RightArrow(_Arrow):

    """
    Right Arrow class object. 
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.setText(fa.icons["caret-right"])


class _LeftArrow(_Arrow):

    """
    Left Arrow class object. 
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.setText(fa.icons["caret-left"])


class _UpArrow(_Arrow):

    """
    Up Arrow class object. 
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.setText(fa.icons["caret-up"])


class _DownArrow(_Arrow):

    """
    Down Arrow class object.
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.setText(fa.icons["caret-down"])


class _Operation(Enum):

    """
    Operation list definition.
    """

    Decrement = 0
    Increment = 1
    ShiftRight = 2
    ShiftLeft = 3
    NumKeyPressed = 4


class _SpinField(QWidget):

    """
    SpinField widget class.

    :param value: Initial value
    :type value: float
    :param font_size: Font size (default=10)
    :type font_size: int, optional
    :param prev_field: Initial previous value. 
    :type prev_field: float, optional
    :param next_field: Initial next value
    :type next_field: float, optional
    :param shift_arrows: Use shift arrow (left right, default=True).
    :type shift_arrows: bool, optional    
    """

    dataChanged = pyqtSignal(object)

    _up_arrow = None
    _down_arrow = None

    def __init__(self, value, font_size=10, prev_field=None, next_field=None, shift_arrows=True):
        """
        Initialize class.
        """
        super().__init__()

        self.setFocusPolicy(Qt.WheelFocus)

        self._next_value = str(0)
        self.setLayout(QVBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(1, 0, 1, 0)
        self.setContentsMargins(0, 0, 0, 0)

        self._prev_field = prev_field
        self._next_field = next_field

        self._value_label = QLabel(self)

        self._up_arrow = _UpArrow()

        self._up_arrow.clicked.connect(self.try_increment)
        self._up_arrow.hide()

        self._down_arrow = _DownArrow()

        self._down_arrow.clicked.connect(self.try_decrement)
        self._down_arrow.hide()

        self._shift_arrows = shift_arrows

        if shift_arrows:
            self._left_arrow = _LeftArrow()
            self._left_arrow.clicked.connect(self.swap_left)
            self._left_arrow.hide()
            self._left_shift_enabled = True

            self._right_arrow = _RightArrow()
            self._right_arrow.clicked.connect(self.swap_right)
            self._right_arrow.hide()
            self._right_shift_enabled = True

        self.set_text(value)

        self.layout().addWidget(self._up_arrow)

        if shift_arrows:
            self.layout().addWidget(self._left_arrow)

        self.layout().addWidget(self._value_label)
        self.layout().addWidget(self._down_arrow)

        if shift_arrows:
            self.layout().addWidget(self._right_arrow)

        self._value_label.setAlignment(Qt.AlignCenter)

    def set_handler(self, handler):
        """
        Set/connect handler to provide change event.

        :param handler: Signal, external value changed listener
        :type handler: PyQtSignal
        """
        self.dataChanged.connect(handler)

    def set_prev(self, field):
        """
        Set/replace by previous value.

        :param field: Previous value.
        :type field: float
        """
        self._prev_field = field
        if not field:
            self._value_label.setObjectName('firstChild')

    def set_next(self, field):
        """
        Set/replace  next value.

        :param field: Next value.
        :type field: float
        """
        self._next_field = field
        if not field:
            self._value_label.setObjectName('lastChild')

    def get_previous(self):
        """
        Replace value by prev value.
        """
        return self._prev_field

    def get_next(self):
        """
        Get next value.
        """
        return self._next_field

    def keyPressEvent(self, event: QKeyEvent) -> None:
        """
        Keyboard press event.
        """
        if event.key() == Qt.Key_Up:
            self.try_increment()
        elif event.key() == Qt.Key_Left or event.key() == Qt.Key_Backtab:
            self.set_prev_focus()
        elif event.key() == Qt.Key_Right or event.key() == Qt.Key_Tab:
            self.set_next_focus()
        elif event.key() == Qt.Key_Down:
            self.try_decrement()
        else:
            try:
                value = int(event.text())
                self.try_set(value)
            except ValueError:
                pass

    def set_prev_focus(self):
        """
        Set the focus to the previous spinfield, and wrap-around if the current field is the first.
        """
        
        if self._value_label.objectName() != 'firstChild':
            self.parent().focusPreviousChild()
        else:
            children = self.parent().findChildren(_SpinField)
            children[len(children) - 1].setFocus()

    def set_next_focus(self):
        """
        Set the focus to the next spinfield, and wrap-around if the current field is the last.
        """
        if self._value_label.objectName() != 'lastChild':
            self.parent().focusNextChild()
        else:
            first_child = self.parent().findChild(_SpinField)
            first_child.setFocus()

    def try_set(self, new_value: int) -> None:
        """
        Try a set using the number pad.

        :param new_value: The new value for the field, an int from 0 to 9
        :type new_value: int
        """

        self._prev_value = self._value_label.text()
        self._next_value = str(new_value)
        self._current_op = _Operation.NumKeyPressed

        self.set_next_focus()
        self.dataChanged.emit(self)

    def focusInEvent(self, event: QFocusEvent) -> None:
        """
        Focus in component.
        """
        if self.is_point():
            if self._shift_arrows:
                if self._left_shift_enabled:
                    self._left_arrow.show()
                    
                if self._right_shift_enabled:
                    self._right_arrow.show()
                    
            elif event.reason() != Qt.MouseFocusReason:
                if event.reason() == Qt.TabFocus and self.get_next():
                     self.get_next().setFocus()
                elif event.reason() == Qt.BacktabFocusReason and self.get_previous():
                     self.get_previous().setFocus()

        else:

            self._up_arrow.show()
            self._down_arrow.show()

    def focusNextPrevChild(self, x):
        return False

    def focusOutEvent(self, _: QFocusEvent) -> None:
        """
        Focus out of the component.
        """
        self._up_arrow.hide()
        self._down_arrow.hide()

        if self._shift_arrows:
            self._left_arrow.hide()
            self._right_arrow.hide()

    def enterEvent(self, _: QEvent) -> None:
        """
        Enter event.
        """
        if self.is_point():
            if self._shift_arrows and self._left_shift_enabled:
                self._left_arrow.show()
            if self._shift_arrows and self._right_shift_enabled:
                self._right_arrow.show()
            elif self._shift_arrows:
                self._right_arrow.hide()
        else:
            self._up_arrow.show()
            self._down_arrow.show()

        self.setFocus()

    def leaveEvent(self, _: QEvent) -> None:
        """
        Leave component event.
        """
        self._up_arrow.hide()
        self._down_arrow.hide()
        if self._shift_arrows:
            self._left_arrow.hide()
            self._right_arrow.hide()

    def rolled_over(self):
        """
        Check start/end of the range limits.
        """
        return self._next_value == '0' and self._value_label.text() == '9' or self._next_value == '9' and self._value_label.text() == '0'

    def get_text(self) -> str:
        """
        Return value as text.

        :rtype: str
        """
        return self._value_label.text()

    def set_text(self, char: str):
        """
        Set text.

        :param char: Text.
        :type text: str
        """
        self._value_label.setText(char)
        self._next_value = char
        if self._shift_arrows and char == '.':
            policy = QSizePolicy()
            policy.setRetainSizeWhenHidden(True)
            self._left_arrow.setSizePolicy(policy)
            self._right_arrow.setSizePolicy(policy)
            self._up_arrow.hide()
            self._down_arrow.hide()
        elif self._shift_arrows:
            policy = QSizePolicy()
            policy.setRetainSizeWhenHidden(False)
            self._left_arrow.setSizePolicy(policy)
            self._right_arrow.setSizePolicy(policy)

    def wheelEvent(self, event: QWheelEvent) -> None:
        """
        Mouse wheel event.
        """
        if self._value_label.text() == '.':
            return
        if event.angleDelta().y() > 0:
            self.try_increment()
        else:
            self.try_decrement()

        event.accept()

    def set(self):
        """
        Event set.
        """
        self.set_text(self._next_value)

        if self._shift_arrows:
            self._left_arrow.hide()
            self._right_arrow.hide()

        if self.hasFocus():
            self._up_arrow.show()
            self._down_arrow.show()

    def is_point(self):
        """
        Is point or not ?
        """
        return self._value_label.text() == '.'

    def swap_left(self):
        """
        Swap left event.
        """
        if self.get_previous().get_previous():
            self._next_value = self._prev_field.get_text()
            self._prev_field.set_next_value(self.get_text())
            self._current_op = _Operation.ShiftLeft
            self.dataChanged.emit(self)

    def set_right_shift_enabled(self, enable_disable: bool):
        """
        Set flag to shift right.

        :param enable_disable: Enable or not the right shift
        :type enable_disable: bool
        """
        self._right_shift_enabled = enable_disable

    def set_left_shift_enabled(self, enable_disable: bool):
        """
        Set flag to shift left.

        :param enable_disable: Enable or not the left shift
        :type enable_disable: bool
        """
        self._left_shift_enabled = enable_disable

    def swap_right(self):
        """
        Swap right.
        """
        if self.get_next().get_next():
            self._next_value = self._next_field.get_text()
            self._next_field.set_next_value(self.get_text())
            self._current_op = _Operation.ShiftRight
            self.dataChanged.emit(self)

    def try_increment(self):
        """Increment!"""
        self.set_active_style(self._up_arrow)
        self._prev_value = self._value_label.text()
        self._next_value = str((int(self._value_label.text()) + 1) % 10)
        self._current_op = _Operation.Increment
        self.dataChanged.emit(self)

    def get_operation(self) -> _Operation:
        """Get current operation type"""
        return self._current_op

    def try_decrement(self):
        """
        Decrement !
        """
        self.set_active_style(self._down_arrow)
        self._prev_value = self._value_label.text()
        self._next_value = str((int(self._value_label.text()) - 1) % 10)
        self._current_op = _Operation.Decrement
        self.dataChanged.emit(self)

    def set_active_style(self, arrow):
        """
        Set active style.

        :param arrow: Arrow to style.
        :type arrow: QArrow
        """
        arrow.setStyleSheet('_Arrow {color:' + Colors.STR_COLOR_LIGHT2GREEN + ';}')
        QTimer.singleShot(250, lambda: arrow.setStyleSheet(''))

    def roll_back(self):
        """
        Roll back.
        """
        self._next_value = self._value_label.text()

    def set_next_value(self, next_value):
        """
        Set next value.

        :param next_value: Change next value
        :type next_value: str
        """
        self._next_value = next_value

    def get_next_value(self) -> str:
        """
        Return next value.

        :rtype: float
        """
        return self._next_value

    def get_value_int(self) -> int:
        """
        Cast data to int value.

        :rtype: int
        """
        return int(self.value_)


class SpinboxWidget(QWidget):

    """
    Spinbox widget 

    :param parent: Parent object.
    :type parent: object
    :param display_format: Specify the format to display (default="5.5").
    :type display_format: str, optional
    :param min_val: Minimum value.
    :type min_val: float, optional
    :param max_val: Maximum value.
    :type max_val: float, optional
    :param fixed_format: Fixed the format (default=False).
    :type fixed_format: bool, optional    
    :param shift_arrows: Show or not shift arrows (default=True).
    :type fixed_format: bool, optional
    :param font_size: Font size (default=8).
    :type font_size: int, optional    
    """

    dataChanged = pyqtSignal(object)

    def __init__(self, parent=None, display_format='5.5', min_val=None, max_val=None,
                 fixed_format=False, shift_arrows=True, font_size=8):
        """
        Initialize class.
        """
        super().__init__()

        self._formatter = _SpinFormat()
        self._format = display_format
        self._hard_max = max_val
        self._hard_min = min_val

        if self._hard_max is None:
            self._hard_max = sys.float_info.max

        if self._hard_min is None:
            self._hard_min = -sys.float_info.max

        self._fixed_format = fixed_format
        self._shift_arrows = shift_arrows and not fixed_format

        self.setLayout(QHBoxLayout())
        self.layout().setAlignment(Qt.AlignRight)
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        
        self._light_styles = ""
        self._dark_styles = ""

        self._base_styles = '''                
            SpinboxWidget > _SpinField > QLabel { padding: 0; margin: 0; border: 0; margin: 0px;}
            SpinboxWidget > _SpinField  { padding: 0; margin: 0; border: 0; }
            SpinboxWidget > _SpinField > _Arrow { border: none; padding:0 0 2 0;margin:0;height:6;color:'''+Colors.STR_COLOR_LIGHT2GRAY+'''}
            SpinboxWidget > _SpinField > _LeftArrow { padding-left: 3px; }
            SpinboxWidget > _SpinField > _RightArrow { padding-left: 3px; }
            SpinboxWidget > _SpinField > _Arrow { color:'''+Colors.STR_COLOR_LIGHT2GRAY+'''}           
        '''

        value, _ = self._formatter.get_format(str(min_val) if min_val else str(0), display_format)

        spinfields = [_SpinField(value=digit, shift_arrows=shift_arrows)
                  for digit in value]

        for i, field in enumerate(spinfields):
            field.set_prev(spinfields[i - 1] if i != 0 else None)
            field.set_next(spinfields[i + 1] if i != len(spinfields) - 1 else None)
            field.set_handler(self.field_value_change)
            self.layout().addWidget(field)

    def light_(self):
        """
        Set light theme.
        """
        self.setStyleSheet(self._base_styles + self._light_styles)

    def dark_(self):
        """
        Set dark theme.
        """
        self.setStyleSheet(self._base_styles + self._dark_styles)

    def set_handler(self, handler):
        """
        Set event handler.

        :param handler: Signal, external listener
        :type handler: PyQtSignal
        """
        self.dataChanged.connect(handler)

    def field_value_change(self, field):
        """
        Click arrow event.

        :param field: _SpinField arrow click
        :type field: _SpinField
        """
        if field.get_operation() == _Operation.ShiftRight:
            _next = field.get_next()
            _next.set_right_shift_enabled(
                not _next.get_next().get_next() is None)
        elif field.get_operation() == _Operation.ShiftLeft:
            _prev = field.get_previous()
            _prev.set_left_shift_enabled(
                not _prev.get_previous().get_previous() is None)
        elif field.get_operation() == _Operation.Increment or field.get_operation() == _Operation.Decrement:
             if field.rolled_over() and field.get_previous():
                if field.get_operation() == _Operation.Decrement:
                    if field.get_previous().is_point():
                        field.get_previous().get_previous().try_decrement()
                    else:
                        field.get_previous().try_decrement()
                elif field.get_previous().is_point():
                        field.get_previous().get_previous().try_increment()
                else:
                    field.get_previous().try_increment()

        value = self._get_sum()
        valid = self.is_valid_value(value)

        if field.get_operation() == _Operation.NumKeyPressed and not valid:
            if self._hard_max is not None and value > self._hard_max:
                value_str, display_format = self._formatter.get_format(str(value), self._format, fixed_format=self._fixed_format)
                format_max = self._formatter.get_format_max(display_format if display_format else self._format)
                display_max = self._hard_max if self._hard_max and self._hard_max < format_max else format_max
                self._set(self._formatter.get_format(str(display_max), self._format)[0])
            elif self._hard_min is not None and value < self._hard_min:
                value_str, display_format = self._formatter.get_format(str(value), self._format, fixed_format=self._fixed_format)
                format_min = self._formatter.get_format_min(display_format if display_format else self._format)
                display_min = self._hard_min if self._hard_min and self._hard_min > format_min else format_min
                self._set(self._formatter.get_format(str(display_min), self._format)[0])
        else:
            for child in self.findChildren(_SpinField):
                if valid:
                    child.set()
                else:
                    child.roll_back()

        self.dataChanged.emit(_UpdateState.VALID if valid else _UpdateState.OUT_OF_RANGE)

    def set_data(self, value):
        """
        Set data.

        :param value: Value to set.
        :type value: float
        """
        try:
            new_value = np.format_float_positional(value, trim='-')
        except:
            update_state = _UpdateState.INVALID
            return

        value_str, display_format = self._formatter.get_format(
            new_value, self._format, fixed_format=self._fixed_format)   

        format_max = self._formatter.get_format_max(
            display_format if display_format else self._format)
        display_max = self._hard_max if self._hard_max and self._hard_max < format_max else format_max

        format_min = self._formatter.get_format_min(
            display_format if display_format else self._format)
        display_min = self._hard_min if self._hard_min and self._hard_min > format_min else format_min

        update_state = _UpdateState.VALID
        if value_str:
            if float(new_value) > display_max:
                self._set(self._formatter.get_format(str(display_max), self._format)[0])
                update_state = _UpdateState.OUT_OF_RANGE
            elif float(new_value) < display_min:
                self._set(self._formatter.get_format(str(display_min), self._format)[0])
                update_state = _UpdateState.OUT_OF_RANGE
            else:
                self._set(value_str)
        else:
            update_state = _UpdateState.INVALID

    def get_data(self):
        """
        Get data as str.

        :rtype: str
        """
        return self.text()

    def _set(self, new_value: str):
        """
        Set data in component (private).

        :param new_value: Value changed.
        :type new_value: str    
        """
        spinfields = self.findChildren(_SpinField)
        for i, v in enumerate(new_value):
            spinfields[i].set_text(v)
        self._value = self._get_sum()

    def text(self) -> str:
        """
        Get text.

        :rtype: str
        """
        value_str = ''
        for field in self.findChildren(_SpinField):
            value_str += field.get_next_value()
        return value_str

    def _get_sum(self) -> float:
        """
        Get text value as float.

        :rtype: float
        """
        return float(self.text())

    def is_valid_value(self, total: float):
        """
        Check if value is valid vs. min/max.

        :rtype: bool
        """
        return self._hard_max is not None and total <= self._hard_max and self._hard_min is not None and total >= self._hard_min

    def set_next(self, field, next_value):
        """
        Set next value.
        
        :param field: Specify which field
        :type field: _SpinField
        """
        field.set_next_value()
        self._value = next_value


class _SpinFormat():

    """    
    Hangling/functions for data manipulation/processing.
    """

    def get_format_min(self, display_format):
        """
        Get min format.

        :param display_format: Display format.
        :type display_format: str
        :return: Get the min value
        :rtype: float
        """
        return self.get_limit(display_format, '0')

    def get_format_max(self, display_format):
        """
        Get max format.

        :param display_format: Display format.
        :type display_format: str
        :return: Get the max value
        :rtype: float
        """
        return self.get_limit(display_format, '9')

    def get_limit(self, display_format, limit_char):
        """
        Get limit.

        :param display_format: Display format.
        :type display_format: str
        :param limit_char: Limit char.
        :type limit_char: int       
        :return: Get the limit.
        :rtype: float
        """
        counts = [n for n in display_format.split('.') if n]
        return float('.'.join([int(float(count)) * limit_char for count in counts]))

    def _get_float_as_int_pair(self, float_val: float):
        """
        Get value float as pair int.

        :param float_val: Input value
        :type float_val: float
        :return: List of int (left,right)
        :rtype: list
        """
        res = [int(float(f)) for f in self.float_(float_val).split('.')]
        # res2 = [int(float(f)) for f in str(float_val).split('.')]
        # return [int(float(f)) for f in str(float_val).split('.')]
        return res

    def float_(self,val):
        """
        Format float number.
        """
        str_float = np.format_float_positional(val, trim='-')
        if "." not in str_float:
            str_float = str_float + ".0"
        return str_float

    def get_format(self, value: str, display_format: str, fixed_format: bool = False):
        """
        Get format.

        :param value: Input value.
        :type value: str
        :param display_format: Format display.
        :type display_format: str
        :param fixed_format: Fixed format or not (default=False).
        :type fixed_format: bool, optional
        :return: Format number.
        :rtype: str
        """
        n_int_digits, n_float_digits = self._get_float_as_int_pair(float(display_format))
        value_int, value_fraction = self._get_float_as_int_pair(float(value))
       
        if '.' in value:
            value_fraction = value.split('.')[1]
        else:
            value_fraction = '0'
        
        total_available = n_int_digits + n_float_digits

        # we can use the decimal field for an integer

        if float(value_fraction) == 0:
            total_available += 1

        required_digit_n = len(str(value_int))
        if float(value_fraction) > 0:
            required_digit_n += len(str(value_fraction))

        if total_available < required_digit_n:
            return None, None

        changed_format = True
        if n_int_digits < len(str(value_int)):
            n_int_digits = len(str(value_int))
            n_float_digits = total_available - n_int_digits - 1
        elif int(float(value_fraction)) > 0 and n_float_digits < len(value_fraction):
            n_float_digits = len(value_fraction)
            n_int_digits = len(value) - n_float_digits - 1
        else:
            changed_format = False

        integer_part = f'{"0" * (n_int_digits - len(str(value_int)))}{value_int}'
        fraction_part = f'{value_fraction}{"0" * (n_float_digits - len(value_fraction))}'
        final_format = f'{n_int_digits}.{n_float_digits}' if changed_format else display_format
        format_none = changed_format and fixed_format

        if n_int_digits == total_available:
            if format_none:
                return integer_part, None
            return integer_part, str(n_int_digits)

        if n_float_digits == 0:
            n_int_digits = n_int_digits + 1 if '.' in display_format else n_int_digits
            integer_part = f'{"0" * (n_int_digits - len(str(value_int)))}{value_int}'
            if format_none:
                return integer_part, None
            return integer_part, str(n_int_digits)

        if format_none:
            return f'{integer_part}.{fraction_part}', None
        
        return f'{integer_part}.{fraction_part}', final_format


class _Example(QMainWindow):

    """    
    Example class to test    
    """

    def __init__(self):
        """
        Initialize class.
        """
        super().__init__()
        self.init_ui()

    def init_ui(self):       
        """
        Init user interface.
        """ 
        w = QWidget()
        
        spin_opts = {
            # 'min_val': 0,
            # 'max_val': 1000,
            'display_format': "5.5",
            'fixed_format': False,
            'shift_arrows': False
        }
        
        self._spinbox = SpinboxWidget(self, **spin_opts)       
        self._spinbox.dark_()
        # self._spinbox.setMinimumWidth(50)
        # self._spinbox.setMinimumHeight(50)
        
        mainLayout = QGridLayout() 

        mainLayout.addWidget(self._spinbox, 0, 0)

        w.setLayout(mainLayout)
        self.setCentralWidget(w)
        self.resize(300,100)
        # self.setStyleSheet("background-color:"+Colors.STR_COLOR_BLACK+";")
        self.show()  

        self._spinbox.set_data(26)
        
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    font_text_user = app.font()
    font_text_user.setPointSize(13)
    app.setFont(font_text_user) 
    darkpalette = QPalette()
    darkpalette.setColor(QPalette.Window, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.WindowText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Base, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.AlternateBase, Colors.COLOR_LBLACK)
    darkpalette.setColor(QPalette.ToolTipBase, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.ToolTipText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Text, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Button, Colors.COLOR_L2BLACK)
    darkpalette.setColor(QPalette.ButtonText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.BrightText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Highlight, Colors.COLOR_LBLUE)
    darkpalette.setColor(QPalette.HighlightedText, Colors.COLOR_WHITE)
    darkpalette.setColor(QPalette.Background, Colors.COLOR_LBLACK)
    app.setPalette(darkpalette)    
    ex = _Example()
    ex.show()
    sys.exit(app.exec_())