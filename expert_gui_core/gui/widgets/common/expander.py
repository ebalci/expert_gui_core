from PyQt5.QtWidgets import *

import qtawesome as qta


class _DataWidgetExpander(QWidget):

    """    
    Frame to expand components.
    ex: used by data panel widgets.
    
    :param parent: Parent object
    :type parent: object
    :param name: Title
    :type name: str, optional
    """
    
    def __init__(self, parent=None, name=""):   
        """
        Initialize class.
        """
        super().__init__()

        qta.icon("fa5.clipboard")

        self._parent = parent
        self.setWindowTitle(name)
        self._layout = QGridLayout()
        self.setLayout(self._layout)
        self.resize(600, 300)
        
    def show(self):
        """
        Show frame.
        """
        self._width = self._parent._data_widget.width()
        self._height = self._parent._data_widget.height()
        self._layout.addWidget(self._parent._data_widget)
        super().show()
        
    def closeEvent(self,event):
        """
        Close/hide frame.
        """
        self.showNormal()
        self._parent._data_widget.showNormal()
        self._parent._data_widget.resize(self._width,self._height)
        self.hide()
        event.ignore()