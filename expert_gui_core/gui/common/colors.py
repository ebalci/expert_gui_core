from PyQt5.QtGui import QColor, QFont

import pyqtgraph as pg

import time
import math
import matplotlib
import numpy as np

import fontawesome as fa
import qtawesome as qta

import warnings

warnings.filterwarnings("ignore")


class Colors:
    """    
    Color object containing all colors used by expert_gui_core.    
    """

    FONT_AWESOME = None

    # REFERENCE COLORS

    COLOR_LIGHT = True

    COLOR_TRANSPARENT = QColor(128, 128, 128, 0)
    STR_COLOR_TRANSPARENT = "rgb(128,128,128,0)"

    COLOR_INACTIVE = QColor(128, 128, 128, 40)
    STR_COLOR_INACTIVE = "rgb(128,128,128,40)"

    COLOR_WHITE = QColor(255, 255, 255)
    STR_COLOR_WHITE = "rgb(255,255,255)"
    COLOR_DWHITE = QColor(240, 240, 240)
    STR_COLOR_DWHITE = "rgb(240,240,240)"

    COLOR_BLACK = QColor(0, 0, 0)
    STR_COLOR_BLACK = "rgb(0,0,0)"
    COLOR_LBLACK = QColor(30, 30, 30)
    STR_COLOR_LBLACK = "rgb(30,30,30)"
    COLOR_L1BLACK = QColor(40, 40, 40)
    STR_COLOR_L1BLACK = "rgb(40,40,40)"
    COLOR_L2BLACK = QColor(50, 50, 50)
    STR_COLOR_L2BLACK = "rgb(50,50,50)"
    COLOR_L3BLACK = QColor(60, 60, 60)
    STR_COLOR_L3BLACK = "rgb(60,60,60)"

    COLOR_LIGHT6GRAY = QColor(230, 230, 230)
    STR_COLOR_LIGHT6GRAY = "rgb(230,230,230)"
    COLOR_LIGHT5GRAY = QColor(220, 220, 220)
    STR_COLOR_LIGHT5GRAY = "rgb(220,220,220)"
    COLOR_LIGHT4GRAY = QColor(200, 200, 200)
    STR_COLOR_LIGHT4GRAY = "rgb(200,200,200)"
    COLOR_LIGHT3GRAY = QColor(160, 160, 160)
    STR_COLOR_LIGHT3GRAY = "rgb(160,160,160)"
    COLOR_LIGHT2GRAY = QColor(110, 110, 110)
    STR_COLOR_LIGHT2GRAY = "rgb(110,110,110)"
    COLOR_LIGHT1GRAY = QColor(100, 100, 100)
    STR_COLOR_LIGHT1GRAY = "rgb(100,100,100)"
    COLOR_LIGHT0GRAY = QColor(80, 80, 80)
    STR_COLOR_LIGHT0GRAY = "rgb(80,80,80)"

    COLOR_LIGHT0GREEN = QColor(40, 215, 40)
    STR_COLOR_LIGHT0GREEN = "rgb(40,215,40)"
    COLOR_LIGHTGREEN = QColor(60, 235, 60)
    STR_COLOR_LIGHTGREEN = "rgb(60,235,60)"
    COLOR_LIGHT2GREEN = QColor(124, 252, 0)
    STR_COLOR_LIGHT2GREEN = "rgb(124,252,0)"

    COLOR_LIGHTORANGE = QColor(255, 180, 0)
    STR_COLOR_LIGHTORANGE = "rgb(255,180,0)"
    COLOR_ORANGE = QColor(255, 128, 0)
    STR_COLOR_ORANGE = "rgb(255,128,0)"
    COLOR_DORANGE = QColor(255, 128, 60)
    STR_COLOR_DORANGE = "rgb(255,128,60)"

    COLOR_YELLOW = QColor(255, 255, 0)
    STR_COLOR_YELLOW = "rgb(255,255,0)"
    COLOR_LYELLOW = QColor(255, 255, 100)
    STR_COLOR_LYELLOW = "rgb(255,220,100)"
    COLOR_DYELLOW = QColor(220, 255, 150)
    STR_COLOR_DYELLOW = "rgb(220,220,150)"

    COLOR_LIGHTRED = QColor(255, 40, 40)
    STR_COLOR_LIGHTRED = "rgb(255,40,40)"

    COLOR_RED = QColor(255, 0, 0)
    STR_COLOR_RED = "rgb(255,0,0)"
    COLOR_L2RED = QColor(255, 60, 60)
    STR_COLOR_L2RED = "rgb(255,60,60)"

    COLOR_PINK = QColor(200, 100, 100)
    STR_COLOR_PINK = "rgb(200,100,100)"
    COLOR_LPINK = QColor(220, 0, 220)
    STR_COLOR_LPINK = "rgb(220,0,220)"
    COLOR_LPINKT = QColor(220, 0, 220, 150)
    COLOR_L2PINK = QColor(253, 232, 230)
    STR_COLOR_L2PINK = "rgb(255,246,246)"
    COLOR_L3PINK = QColor(255, 160, 160)
    STR_COLOR_L3PINK = "rgb(255,160,160)"

    COLOR_BLUE = QColor(84, 190, 230)
    STR_COLOR_BLUE = "rgb(84,190,230)"
    COLOR_DBLUE = QColor(30, 100, 120)
    STR_COLOR_DBLUE = "rgb(30,100,120)"
    COLOR_FWBLUE = QColor(4, 24, 74)
    STR_COLOR_FWBLUE = "rgb(4,24,74)"
    COLOR_LBLUE = QColor(86, 147, 226)
    STR_COLOR_LBLUE = "rgb(86,147,226)"
    COLOR_L2BLUE = QColor(117, 197, 255)
    STR_COLOR_L2BLUE = "rgb(117,197,255)"
    COLOR_L3BLUE = QColor(174, 206, 229)
    STR_COLOR_L3BLUE = "rgb(174,206,229)"
    COLOR_L4BLUE = QColor(84, 190, 230)
    STR_COLOR_L4BLUE = "rgb(84,190,230)"
    COLOR_L5BLUE = QColor(174, 211, 255)
    STR_COLOR_L5BLUE = "rgb(174,211,255)"

    COLOR_BLUEGRAY = QColor(86, 147, 226)
    COLOR_BLUEGRAYT1 = QColor(86, 147, 226, 50)
    COLOR_BLUEGRAYT2 = QColor(86, 147, 226, 30)

    COLOR_PURPLE = QColor(87, 0, 127)
    STR_COLOR_PURPLE = "rgb(87,0,127)"
    COLOR_DPURPLE = QColor(40, 0, 80)
    STR_COLOR_DPURPLE = "rgb(40,0,80)"
    COLOR_D2PURPLE = QColor(10, 0, 50)
    STR_COLOR_D2PURPLE = "rgb(10,0,40)"
    COLOR_LPURPLE = QColor(100, 0, 140)
    STR_COLOR_LPURPLE = "rgb(120,0,160)"
    COLOR_L2PURPLE = QColor(120, 0, 160)
    STR_COLOR_L2PURPLE = "rgb(160,0,200)"
    COLOR_L3PURPLE = QColor(220, 200, 255)
    STR_COLOR_L3PURPLE = "rgb(230,200,255)"

    _strokes = [
        "#00FFFF", '#d95319', "#77ac30", "#A0522D",
        "#9933ff", "#9ACD32", "#648177", "#0d5ac1",
        "#A9A9A9", "#1c0365", "#14a9ad", "#4ca2f9", "#a4e43f", "#d298e2", "#6119d0",
        "#d2737d", "#c0a43c", "#f2510e", "#651be6", "#79806e", "#61da5e", "#cd2f00",
        "#9348af", "#01ac53", "#c5a4fb", "#996635", "#b11573", "#4bb473", "#75d89e",
        "#2f3f94", "#2f7b99", "#da967d", "#34891f", "#b0d87b", "#ca4751", "#7e50a8",
        "#c4d647", "#e0eeb8", "#11dec1", "#289812", "#566ca0", "#ffdbe1", "#2f1179",
        "#935b6d", "#916988", "#513d98", "#aead3a", "#9e6d71", "#4b5bdc", "#0cd36d",
        "#250662", "#cb5bea", "#228916", "#ac3e1b", "#df514a", "#539397", "#880977",
        "#f697c1", "#ba96ce", "#679c9d", "#c6c42c", "#5d2c52", "#48b41b", "#e1cf3b",
        "#5be4f0", "#57c4d8", "#a4d17a", "#225b8", "#be608b", "#96b00c", "#088baf",
        "#f158bf", "#e145ba", "#ee91e3", "#05d371", "#5426e0", "#4834d0", "#802234",
        "#6749e8", "#0971f0", "#8fb413", "#b2b4f0", "#c3c89d", "#c9a941", "#41d158",
        "#fb21a3", "#51aed9", "#5bb32d", "#807fb", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#21538e", "#89d534", "#d36647",
        "#7fb411", "#0023b8", "#3b8c2a", "#986b53", "#f50422", "#983f7a", "#ea24a3",
        "#79352c", "#521250", "#c79ed2", "#d6dd92", "#e33e52", "#b2be57", "#fa06ec",
        "#1bb699", "#6b2e5f", "#64820f", "#1c271", "#9cb64a", "#996c48", "#9ab9b7",
        "#06e052", "#e3a481", "#0eb621", "#fc458e", "#b2db15", "#aa226d", "#792ed8",
        "#73872a", "#520d3a", "#cefcb8", "#a5b3d9", "#7d1d85", "#c4fd57", "#f1ae16",
        "#8fe22a", "#ef6e3c", "#243eeb", "#1dc18", "#dd93fd", "#3f8473", "#e7dbce",
        "#421f79", "#7a3d93", "#635f6d", "#93f2d7", "#9b5c2a", "#15b9ee", "#0f5997",
        "#409188", "#911e20", "#1350ce", "#10e5b1", "#fff4d7", "#cb2582", "#ce00be",
        "#32d5d6", "#17232", "#608572", "#c79bc2", "#00f87c", "#77772a", "#6995ba",
        "#fc6b57", "#f07815", "#8fd883", "#060e27", "#96e591", "#21d52e", "#d00043",
        "#b47162", "#1ec227", "#4f0f6f", "#1d1d58", "#947002", "#bde052", "#e08c56",
        "#28fcfd", "#bb09b", "#36486a", "#d02e29", "#1ae6db", "#3e464c", "#a84a8f",
        "#911e7e", "#3f16d9", "#0f525f", "#ac7c0a", "#b4c086", "#c9d730", "#30cc49",
        "#3d6751", "#fb4c03", "#640fc1", "#62c03e", "#d3493a", "#88aa0b", "#406df9",
        "#615af0", "#4be47", "#2a3434", "#4a543f", "#79bca0", "#a8b8d4", "#00efd4",
        "#7ad236", "#7260d8", "#1deaa7", "#06f43a", "#823c59", "#e3d94c", "#dc1c06",
        "#f53b2a", "#b46238", "#2dfff6", "#a82b89", "#1a8011", "#436a9f", "#1a806a",
        "#4cf09d", "#c188a2", "#67eb4b", "#b308d3", "#fc7e41", "#af3101", "#ff065",
        "#71b1f4", "#a2f8a5", "#e23dd0", "#d3486d", "#00f7f9", "#474893", "#3cec35",
        "#1c65cb", "#5d1d0c", "#2d7d2a", "#ff3420", "#5cdd87", "#a259a4", "#e4ac44",
        "#1bede6", "#8798a4", "#d7790f", "#b2c24f", "#de73c2", "#d70a9c", "#25b67",
        "#88e9b8", "#c2b0e2", "#86e98f", "#ae90e2", "#1a806b", "#436a9e", "#0ec0ff",
        "#f812b3", "#b17fc9", "#8d6c2f", "#d3277a", "#2ca1ae", "#9685eb", "#8a96c6",
        "#dba2e6", "#76fc1b", "#608fa4", "#20f6ba", "#07d7f6", "#dce77a", "#77ecca"
    ]

    LIST_COLORS_MENU_IMAGE = {
        "default": ["default", "default"],
        "purple": ["purple", "purple"],
        "hawai": ["hawai", "hawai"],
        "dyngray": ["dyngray", "dyngray"],
        "dbgray": ["dbgray", "dbgray"],
        "expgray": ["expgray", "expgray"],
        "seismic": ["seismic", "CET-D1A"],
        "bwr": ["bwr", "CET-D1"],
        "gray": ["gray", "CET-L1"],
        "jet": ["jet", "CET-R4"],
        "hot": ["hot", "CET-L3"],
        "cividis": ["cividis", "CET-CBL2"],
        "viridis": ["viridis", "viridis"],
        "inferno": ["inferno", "inferno"],
        "plasma": ["plasma", "plasma"],
        "blues": ["Blues", "CET-L6"],
        "greens": ["Greens", "CET-L5"],
        "greys": ["Greys", "CET-L2"],
        "ylorrd": ["YlOrRd", "CET-L18"],
    }

    CMAP_DEFAULT = None
    CMAP_EXPGRAY = None
    CMAP_DBGRAY = None
    CMAP_DYNGRAY = None

    MAT_EXPGRAY = None
    MAT_DBGRAY = None

    CMAP_IM_DEFAULT = None
    CMAP_IM_DBGRAY = None
    CMAP_IM_EXPGRAY = None
    CMAP_IM_DYNGRAY = None
    CMAP_IM_HAWAI = None
    CMAP_IM_PURPLE = None

    @staticmethod
    def init_font():
        """
        Init font awesome.
        """

        if not Colors.COLOR_LIGHT:
            Colors._strokes[0] = "#00FFFF"
        else:
            Colors._strokes[0] = "#412EF0"

        # init cmap for 3d

        if Colors.FONT_AWESOME is None:
            print("init!")
            Colors.FONT_AWESOME = []
            Colors.init_color_gl()
            Colors.init_mat_palette()
            Colors.init_color_im()



    def init_color_im():

        # default

        colors = [
            [10, 50, 80],
            [15, 75, 150],
            [30, 110, 200],
            [60, 160, 240],
            [80, 180, 250],
            [130, 210, 255],
            [160, 230, 230],
            [255, 232, 120],
            [255, 192, 60],
            [255, 160, 0],
            [255, 96, 0],
            [255, 50, 0],
            [225, 20, 0],
            [170, 0, 0],
            [80, 0, 0],
            [180, 60, 255]
        ]
        Colors.CMAP_IM_DEFAULT = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors)), color=colors)

        # dbgray

        colors1 = Colors.MAT_DBGRAY
        Colors.CMAP_IM_DBGRAY = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors1)), color=colors1)

        # expgray

        colors2 = Colors.MAT_EXPGRAY
        Colors.CMAP_IM_EXPGRAY = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors2)), color=colors2)

        # dyngray

        colors3 = [
            [20, 20, 20],
            [200, 200, 200],
            [255, 255, 255]
        ]
        Colors.CMAP_IM_DYNGRAY = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors3)), color=colors3)

        # hawai

        colors4 = [
            [74, 141, 230],
            [219, 207, 99],
            [232, 97, 77]
        ]
        Colors.CMAP_IM_HAWAI = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors4)), color=colors4)

        # purple

        colors5 = [
            [95, 193, 220],
            [0, 129, 200],
            [235, 115, 197]
        ]
        Colors.CMAP_IM_PURPLE = pg.ColorMap(pos=np.linspace(0.0, 1.0, len(colors5)), color=colors5)

    def init_color_gl():

        # dbgray

        mat1 = []
        val = 0
        for i in range(0, 1000):
            col = (math.log(val + 0.1) - math.log(0.1)) / (math.log(1.1) - math.log(0.1))
            mat1.append([col, col, col, 1])
            val = val + 0.001
        Colors.CMAP_DBGRAY = matplotlib.colors.ListedColormap(np.array(mat1))

        # expgray

        mat2b = []
        val = 0
        for i in range(0, 1000):
            col = (math.exp(val) - 1) / (math.exp(1) - 1)
            mat2b.append([col, col, col, 1])
            val = val + 0.001
        Colors.CMAP_EXPGRAY = matplotlib.colors.ListedColormap(np.array(mat2b))

        # default

        matb = [
            [10 / 255, 50 / 255, 80 / 255, 1],
            [15 / 255, 75 / 255, 150 / 255, 1],
            [30 / 255, 110 / 255, 200 / 255, 1],
            [60 / 255, 160 / 255, 240 / 255, 1],
            [80 / 255, 180 / 255, 250 / 255, 1],
            [130 / 255, 210 / 255, 255 / 255, 1],
            [160 / 255, 230 / 255, 230 / 255, 1],
            [255 / 255, 232 / 255, 120 / 255, 1],
            [255 / 255, 192 / 255, 60 / 255, 1],
            [255 / 255, 160 / 255, 0 / 255, 1],
            [255 / 255, 96 / 255, 0 / 255, 1],
            [255 / 255, 50 / 255, 0 / 255, 1],
            [225 / 255, 20 / 255, 0 / 255, 1],
            [170 / 255, 0 / 255, 0 / 255, 1],
            [80 / 255, 0 / 255, 0 / 255, 1],
            [180 / 255, 60 / 255, 255 / 255, 1]
        ]
        mat2 = np.array(matb).transpose()
        mR, mG, mB = [], [], []
        mat3 = []
        size = 100
        for i in range(len(mat2[0]) - 1):
            R = np.linspace(
                mat2[0][i],
                mat2[0][i + 1],
                size
            )
            G = np.linspace(
                mat2[1][i],
                mat2[1][i + 1],
                size
            )
            B = np.linspace(
                mat2[2][i],
                mat2[2][i + 1],
                size
            )
            for j in range(size):
                mat3.append([R[j], G[j], B[j], 1.])
        Colors.CMAP_DEFAULT = matplotlib.colors.ListedColormap(np.array(mat3))

        # dyngray

        mat4 = []
        val = 0
        for i in range(0, 1000):
            col = 20 / 255 + (255 - 20) * val / 255
            mat4.append([col, col, col, 1])
            val = val + 0.001
        Colors.CMAP_DYNGRAY = matplotlib.colors.ListedColormap(np.array(mat4))

    def hsvColor(hue, sat=1.0, val=1.0, alpha=1.0, bright=0.7):
        """
        Generate a QColor from HSVa values. (all arguments are float 0.0-1.0).

        :param hue: hue value from 0 to 1
        :type hue: float
        :param sat: Saturation from 0 to 1
        :type sat: float, optional
        :param alpha: Alpha from 0 to 1
        :type alpha: float, optional
        :param bright: Bright from 0 to 1
        :type bright: float, optional
        :return: Color corresponding to Hue level
        :rtype: QColor
        """
        return QColor.fromHsvF(hue, sat, val * bright, alpha)

    def strokeColor(ind):
        """
        Get color index from the predefined strokes array. 
        
        "#1E90FF", '#d95319', "#77ac30", "#A0522D", 
        "#9933ff", "#9ACD32", "#648177", "#0d5ac1",
        ...

        :param ind: Index of stroke
        :type ind: int
        :return: Stroke color
        :rtype: QColor
        """
        return QColor(Colors._strokes[ind % len(Colors._strokes)])

    def init_mat_palette():
        """
        Init matrix pqlettes
        """

        mat1 = []
        val = 0
        for i in range(0, 1000):
            valexp = (math.exp(val) - 1) / (math.exp(1) - 1)
            col = int(255 * valexp)
            mat1.append([col, col, col])
            val = val + 0.001

        Colors.MAT_EXPGRAY = mat1

        mat2 = []
        val = 0
        for i in range(0, 1000):
            valdb = (math.log(val + 0.1) - math.log(0.1)) / (math.log(1.1) - math.log(0.1))
            col = int(255 * valdb)
            mat2.append([col, col, col])
            val = val + 0.001

        Colors.MAT_DBGRAY = mat2

    def copy_color(col):
        """
        Make a copy of a color
        """
        return QColor(
            col.red(),
            col.green(),
            col.blue(),
            255)

    def qColor(r,g,b):
        return QColor(int(r),int(g),int(b))

    def getColor(val=1.0, max=120, palette="rainbow"):
        """
        Get color from percent 0-1 and palette definition.

        :param max: Maximum number of colors (default=120).
        :type max: int, optional
        :param val: Index of the color using value between 0 and 1 (n=value*max, default=1).
        :type val: float, optional
        :param palette: Color palette (rainbow ocean summer autumn spring winter bbq winter hawai purple stroke, default=rainbow).
        :type palette: str, optional
        :return: Color from the requested index
        :rtype: QColor
        """
        if "#" in palette:
            return QColor(palette)
        if palette == "rainbow":
            return (val * max, max * 1.3)
        elif palette == "bbq":
            return Colors.qColor(245 - 90 * val, 190 - 160 * val, 190 - 170 * val)
        elif palette == "ocean":
            return Colors.qColor(100 - 40 * val, 200 - 80 * val, 230 - 50 * val)
        elif palette == "summer":
            return Colors.qColor(245 - 25 * val, 190 - 90 * val, 66 - 26 * val)
        elif palette == "spring":
            return Colors.qColor(220 - 100 * val, 230 - 80 * val, 190 - 90 * val)
        elif palette == "autumn":
            return Colors.qColor(240 - 107 * val, 195 - 125 * val, 100 - 65 * val)
        elif palette == "dyngray":
            st = 20
            return Colors.qColor(st + (255 - st) * val, st + (255 - st) * val, st + (255 - st) * val)
        elif palette == "dbgray":
            if val < 0.01:
                col = 20 * (0.99 + val)
                return Colors.qColor(col, col, col)
            elif val < 0.2:
                col = 20 + 200 * (0.8 + val)
                return Colors.qColor(col, col, col)
            else:
                col = 220 + 35 * (0.2 + val)
                return Colors.qColor(col, col, col)
        elif palette == "hawai":
            if val < 0.5:
                val = val * 2
                return Colors.qColor(230 + 10 * val, 100 + 100 * val, 50 + 40 * val)
            val = 2 * (val - 0.5)
            return Colors.qColor(240 - 180 * val, 200 - 60 * val, 90 + 150 * val)
        elif palette == "purple":
            if val < 0.5:
                val = val * 2
                return Colors.qColor(75 - 75 * val, 173 - 44 * val, 200)
            val = 2 * (val - 0.5)
            return Colors.qColor(0 + 235 * val, 129 - 14 * val, 200 - 3 * val)
        elif palette == "winter":
            return Colors.qColor(80 + 90 * val, 80 + 90 * val, 80 + 90 * val)
        elif palette == "stroke":
            if not Colors.COLOR_LIGHT and val == 0:
                Colors._strokes[0] = "#00FFFF"
            else:
                Colors._strokes[0] = "#412EF0"
            return Colors.strokeColor(int(val * max))
        return None


# init static

Colors.init_font()
