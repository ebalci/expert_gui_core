from expert_gui_core.comm import fesacomm
from expert_gui_core.comm import fesalistener


class FesaListener(fesacomm.FesaCommListener):
    """
    FESA listener object.
    Execute a given function (func) after each FESA notification.
    Time difference (security) between 2 consecutive events cannot be lower than min_time_ms.

    :param parent: Parent component (not used).
    :type parent: object
    :param func: Function to execute every notification.
    :type func: object
    :param min_time_ms: Minimum time between 2 consecutive notifcations (default: 40ms).
    :type min_time_ms: int, optional
    """

    def __init__(self, parent, func, min_time_ms=40):
        """
        Initialize class.
        """
        self.func = func
        self._parent = parent
        self._update_time = 0
        self.min_time_ms = min_time_ms

    def handle_event(self, name, value):
        """
        Notification received.
        
        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.            
        :type value: dict
        """
        ti = self._update_time
        if value['_time'] - ti > self.min_time_ms:
            self.func(name, value)
            self._update_time = value['_time']
