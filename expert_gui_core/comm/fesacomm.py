import time
import logging
import threading

from abc import abstractmethod
from pyjapc import PyJapc

# import os,pathlib
# PACKAGE_PARENT = pathlib.Path(__file__).parent.parent.parent
# print(PACKAGE_PARENT)
# sys.path.append(str(PACKAGE_PARENT))

from expert_gui_core.tools import formatting


class FesaCommListener():
    """
    Listener to FESA notifiction events.    
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict        
        """
        pass


class FesaComm:
    """
    FESA communication wrapper.

    :param dev: Device name.
    :type param: str
    :param prop: Property name.
    :type prop: str
    :param listener: FesaListener object.
    :type listener: object, optional
    :param field: Field name.     
    :type field: str, optional
    :param inca: Inca or not JAPC communication (not used).
    :type inca: bool, optional
    """

    JAPC = None
    mutex = threading.Lock()

    def __init__(self, dev, prop, listener=None, field=None, min_time_between_event=20, inca=None):
        """
        Initialize class.
        """
        self._device = dev
        self._property = prop
        self._field = field
        self._sub = None
        if FesaComm.JAPC is None:
            FesaComm.JAPC = PyJapc(incaAcceleratorName=None)
            FesaComm.JAPC.enableInThisThread()
        self._lock = False
        self._time_now = 0
        self.it = 0
        self._listener = listener
        self._min_time_between_event = min_time_between_event

    def get(self, cycle):
        """
        Get FESA property.
            
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Datamap object
        :rtype: dict
        """
        FesaComm.JAPC.setSelector(cycle)
        logging.info("Get : " + self._device + "/" + self._property)
        try:
            result = FesaComm.JAPC.getParam(self._device + "/" + self._property)
        except:
            try:
                FesaComm.JAPC.setSelector("")
                result = FesaComm.JAPC.getParam(self._device + "/" + self._property)
            except:
                return None
        return result

    def get_action(self, cycle, event=True, extra=True):
        """
        Get FESA property and trig handle event.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :param event: Trigger or not the event function.
        :type event: bool
        :return: Datamap object
        :rtype: dict
        """
        result = self.get(cycle)
        if result is None:
            logging.error("Get result none for : " + self._device + "/" + self._property)
            return
        if extra:
            result['_time'] = formatting.current_milli_time()
            result['_cycle'] = cycle
        if event:
            self.handle_event(self._device + "/" + self._property, result)
        return result

    def set(self, result, cycle):
        """
        Set a FESA property.

        :param result: Datamap to set
        :type result: dict
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        """
        FesaComm.JAPC.setSelector(cycle)
        msg = "Set : " + self._device + "/" + self._property
        logging.info(msg)
        try:
            FesaComm.JAPC.setParam(self._device + "/" + self._property, result, checkDims=False)
        except Exception as e:
            try:
                FesaComm.JAPC.setSelector("")
                FesaComm.JAPC.setParam(self._device + "/" + self._property, result, checkDims=False)
            except Exception as e2:
                msg = "Error set : " + self._device + "/" + self._property + " " + str(e2)
                logging.error(msg)
                return {"err": 1, "msg": msg}
        return {"err": 0, "msg": msg}

    def subscribe(self, cycle):
        """
        Subscribe to a FESA device property.

        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Status object {err value, msg}
        :rtype: dict
        """
        if self._sub is not None:
            self.unsubscribe()

        if cycle is not None:
            logging.info(cycle + " " + self._device + "/" + self._property)
        else:
            logging.info(self._device + "/" + self._property)

        FesaComm.JAPC.setSelector(cycle)
        try:
            self._sub = FesaComm.JAPC.subscribeParam(self._device + "/" + self._property, self.handle_event, getHeader=True)
            self._sub.startMonitoring()
            if cycle is not None:
                msg = "subscribe ok to cycle " + cycle
            else:
                msg = "subscribe ok"

            logging.info(msg)
            return {"err": 0, "msg": msg}
        except:
            logging.warning("problem cycle" + cycle)
            try:
                FesaComm.JAPC.setSelector("")
                self._sub = FesaComm.JAPC.subscribeParam(self._device + "/" + self._property, self.handle_event, getHeader=True)
                self._sub.startMonitoring()
            except:
                msg = "subscribe not ok! : empty cycle : " + self._device + "/" + self._property
                logging.error(msg)
                return {"err": 1, "msg": msg}

    def unsubscribe(self):
        """
        Unsubscribe to a FESA device property.
        """
        if self._sub is not None:
            self._sub.stopMonitoring()
            self._sub = None

    def handle_event(self, name, value, header=None):
        """
        Notification received.
            
        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...).
        """

        # check if already locked
        # self.mutex.acquire()

        # check 2 consecutive tstamp
        # print(formatting.current_milli_time() - self._time_now)
        # print("#", (value["imageTimeStamp"] - self.it)/1000000)

        if formatting.current_milli_time() - self._time_now < self._min_time_between_event:
            # self.mutex.release()
            return

        self._time_now = formatting.current_milli_time()

        # add time and cycle in data map

        value['_time'] = self._time_now

        try:
            if header is not None:
                value['_cycle'] = header['selector']
            else:
                value['_cycle'] = ""
        except:
            value['_cycle'] = ""

        # notification

        if self._listener is not None:
            self._listener.handle_event(name, value)

        # release lock

        # self.mutex.release()


class _Example(FesaCommListener):
    """
    Example to test.
    """

    _update_time = 0

    def get(self):
        """
        Get FESA property.

        :rtype: dict
        """
        fesa_comm = FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)
        return fesa_comm.get("")

    def subscribe(self):
        """
        Subscribe to a FESA device property.
        """

        # fesa_comm = FesaComm("BT.BLM", "Acquisition",listener=self)
        # fesa_comm.subscribe("PSB.USER.ZERO")       
        # fesa_comm = FesaComm("SPS.BCSPILLSPS.DEV", "RawDataSlow",listener=self)
        # fesa_comm.subscribe(None)
        # time.sleep(20000)
        # fesa_comm.unsubscribe()

        # FesaComm.JAPC = PyJapc(incaAcceleratorName=None)
        #
        # FesaComm.JAPC2 = PyJapc(incaAcceleratorName=None)
        #
        # FesaComm.JAPC.setSelector("CPS.USER.ALL")
        # FesaComm.JAPC2.setSelector("PSB.USER.ALL")
        #
        # _sub = FesaComm.JAPC.subscribeParam("PR.BPM/Status", self.handle_event, getHeader=True)
        # _sub.startMonitoring()
        #
        # _sub2 = FesaComm.JAPC2.subscribeParam("BR1.BPM/Status", self.handle_event, getHeader=True)
        # _sub2.startMonitoring()
        #
        # time.sleep(20000)
        #
        # _sub.stopMonitoring()

        fesa_comm = FesaComm("BTVDEV.DigiCam.CAM2", "LastImage", listener=self)
        fesa_comm.subscribe("")
        time.sleep(20000)

    def handle_event(self, name, value, header=None):
        """
        Notification received.
        
        :param header:
        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict
        """
        pass
        # if value['_time'] - self._update_time > 150:
        #     print("# " + str(value['_time'] - self._update_time))
        # # self.profile.set_data(value['image2D'])
        # self._update_time = value['_time']



if __name__ == "__main__":
    ex = _Example()
    ex.subscribe()

    # t1 = Worker()
    # t1.start()
    # t1.put("first event",[])
    # t1.stop()