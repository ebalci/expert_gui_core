import os
from typing import List

import pyccda

import json
import time
import urllib.request
import xmltodict

sync_api = pyccda.SyncAPI()
DATA_CACHE = {}


def get_all_devices(key=""):
    """
    Get class/version from a device name.
   
    :param key: search key (device name).
    :type key: str, optional
    :return: List of device ccda objects
    :rtype: List
    """

    key_cache = "get_all_devices#" + key
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    devices = []
    for device_def in sync_api.Device.search("name=='*" + key + "*'"):
        if device_def is not None:
            devices.append(device_def.name)

    DATA_CACHE[key_cache] = devices

    return devices


def get_fesa_design_txt(class_name, version=None):
    """
    Get the xml FESA design file content.
    """

    key_cache = "get_fesa_design_txt#" + class_name
    if version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    content = None
    dir_fesa = "/user/sbartped/fesa-class/fesa-class/" + class_name
    if version is None:
        newest_dir = max([os.path.join(dir_fesa, d) for d in os.listdir(dir_fesa)], key=os.path.getmtime)
        dir_fesa = newest_dir + "/" + class_name + ".design"
    else:
        dir_fesa = dir_fesa + "/" + version + "/" + class_name + ".design"
    try:
        f = open(dir_fesa)
        content = f.read()
        f.close()
    except:
        pass

    if version is None:
        DATA_CACHE[key_cache] = content

    return content


def xml_to_dict(xml_data):
    """
    Converting xml to dictionary.
    """
    dict_data = xmltodict.parse(xml_data, attr_prefix='')
    return dict_data


def get_fesa_design(class_name, version=None):
    """
    Get all scheduling units defined in FESA class design.
    """

    key_cache = "get_fesa_design#" + class_name
    if version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    dictionary = None
    content_design = get_fesa_design_txt(class_name, version=version)
    if content_design is not None:
        try:
            dictionary = xml_to_dict(content_design)
        except Exception as e:
            return None

    if version is None:
        DATA_CACHE[key_cache] = dictionary

    return dictionary


def get_all_classes(operational=True):
    """
    Get all available class with or without tst.

    :param operational: all domains except tst.
    :type operational: bool, optional
    :return: List of class names
    :rtype: List
    """

    key_cache = "get_all_classes#" + str(operational)
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    dir_fesa = "/user/sbartped/fesa-class/fesa-class"
    if operational:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "sps", "ctf"}
    else:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "ctb", "tst", "sps", "ctf"}
    os.chdir(dir_fesa)
    classes = []
    for dom in dir_dom:
        dir_fec_dom = "/acc/dsc/" + dom
        try:
            os.chdir(dir_fec_dom)
            list_dir_fec_dom = os.listdir()
            for fec_found in list_dir_fec_dom:
                try:
                    os.chdir(dir_fec_dom + "/" + fec_found + "/bin")
                except:
                    continue
                list_dir_fec_bin = os.listdir()
                for class_found in list_dir_fec_bin:
                    index_fesa_M = class_found.find('_DU_M')
                    index_fesa_S = class_found.find('_DU_S')
                    index_fesa_R = class_found.find('_DU_R')
                    index_fesa_point = class_found.find('.')
                    index_fesa_minus = class_found.find('-')
                    if index_fesa_M >= 0 and index_fesa_point < 0 and index_fesa_minus < 0:
                        class_found = class_found.replace("_DU_M", "")
                        if class_found not in classes:
                            classes.append(class_found)
                    elif index_fesa_R >= 0 and index_fesa_point < 0 and index_fesa_minus < 0:
                        class_found = class_found.replace("_DU_R", "")
                        if class_found not in classes:
                            classes.append(class_found)
                    elif index_fesa_S >= 0 and index_fesa_point < 0 and index_fesa_minus < 0:
                        class_found = class_found.replace("_DU_S", "")
                        if class_found not in classes:
                            classes.append(class_found)
        except:
            pass

    DATA_CACHE[key_cache] = classes

    return classes


def get_class_from_device(device):
    """
    Get class/version from a device name.
        
    :param device: FESA device name.
    :type device: str
    :return: (class,version) dictionary
    :rtype: dict
    """

    key_cache = "get_class_from_device#" + str(device)
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    info = {}
    for device_def in sync_api.Device.search('name==' + device):
        info["class"] = device_def.device_class_info.name
        info["version"] = device_def.device_class_info.version

    DATA_CACHE[key_cache] = info

    return info


def get_fecs_from_class(classes_to_find=[], operational=True):
    """
    Get all fec names running specific FESA class using binary name _DU_M.

    :param classes_to_find: List of class names.
    :type classes_to_find: str
    :param operational: if true then all domains except tst.
    :type operational: bool, optional
    :return: {class_name, fec[]}
    :rtype: dict
    """
    str_class = ""
    for class_ in classes_to_find:
        str_class = str_class + "#" + class_
    key_cache = "get_fecs_from_class#" + str(operational) + "#" + str_class
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    class_fec = {}
    for fesa_class_name in classes_to_find:
        fec_class = get_devices_from_class_per_fec(fesa_class_name)
        for fec in fec_class:
            if fesa_class_name not in class_fec:
                class_fec[fesa_class_name] = []
            class_fec[fesa_class_name].append(fec)

    DATA_CACHE[key_cache] = class_fec

    return class_fec


def get_fecs_from_class2(classes_to_find=[], operational=True):
    """
    Get all fec names running specific FESA class using binary name _DU_M.

    :param classes_to_find: List of class names.
    :type classes_to_find: str
    :param operational: if true then all domains except tst.
    :type operational: bool, optional
    :return: {class_name, fec[]}
    :rtype: dict
    """
    dir_fesa = "/user/sbartped/fesa-class/fesa-class"

    if operational:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "sps", "ctf"}
    else:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "ctb", "tst", "sps", "ctf"}

    os.chdir(dir_fesa)

    class_fec = {}

    for dom in dir_dom:
        dir_fec_dom = "/acc/dsc/" + dom
        try:
            os.chdir(dir_fec_dom)
            list_dir_fec_dom = os.listdir()
            for fec_found in list_dir_fec_dom:
                try:
                    os.chdir(dir_fec_dom + "/" + fec_found + "/bin")
                except:
                    continue
                list_dir_fec_bin = os.listdir()
                for fec_prog_found in list_dir_fec_bin:
                    index_fesa_bin = fec_prog_found.find('_DU_M')
                    index_fesa_specific_class = 0
                    if len(classes_to_find) > 0:
                        index_fesa_specific_class = -1
                        for class_to_find in classes_to_find:
                            if index_fesa_specific_class == -1:
                                index_fesa_specific_class = fec_prog_found.find(class_to_find + "_DU_M")
                    if index_fesa_bin != -1 and index_fesa_specific_class != -1:
                        fesa_class_name = fec_prog_found[:index_fesa_bin]
                        if fesa_class_name not in class_fec:
                            class_fec[fesa_class_name] = []
                        try:
                            class_fec[fesa_class_name].index(fec_found)
                        except:
                            class_fec[fesa_class_name] = fec_found
        except:
            pass
    return class_fec


def get_classes_from_fec(class_to_find=[], operational=True):
    """
    Get all FESA class names running on a specific fec using binary name _DU_M.

    :param class_to_find:
    :param classes_to_find: List of class names.
    :type classes_to_find: list of str
    :param operational: if true then all domains except tst.
    :type operational: bool, optional
    :return: {fec_name, class[]} 
    :rtype: dict
    """

    str_class = ""
    for class_ in class_to_find:
        str_class = str_class + "#" + class_
    key_cache = "get_classes_from_fec#" + str(operational) + "#" + str_class
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    fec_classes = {}
    for fesa_class_name in class_to_find:
        fec_class = get_devices_from_class_per_fec(fesa_class_name)
        for fec in fec_class:
            if fec not in fec_classes:
                fec_classes[fec] = []
            fec_classes[fec].append(fesa_class_name)

    DATA_CACHE[key_cache] = fec_classes

    return fec_classes


def get_classes_from_fec2(class_to_find=[], operational=True):
    """
    Get all FESA class names running on a specific fec using binary name _DU_M.

    :param class_to_find:
    :param classes_to_find: List of class names.
    :type classes_to_find: list of str
    :param operational: if true then all domains except tst.
    :type operational: bool, optional
    :return: {fec_name, class[]} 
    :rtype: dict
    """
    dir_fesa = "/user/sbartped/fesa-class/fesa-class"

    if operational:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "sps", "ctf"}
    else:
        dir_dom = {"ade", "cps", "iso", "lei", "lhc", "ln3", "ln4", "psb", "ctb", "tst", "sps", "ctf"}

    os.chdir(dir_fesa)

    fec_class = {}

    for dom in dir_dom:
        dir_fec_dom = "/acc/dsc/" + dom
        try:
            os.chdir(dir_fec_dom)
            list_dir_fec_dom = os.listdir()
            for fec_found in list_dir_fec_dom:
                fec_class[fec_found] = []
                try:
                    os.chdir(dir_fec_dom + "/" + fec_found + "/bin")
                except:
                    continue
                list_dir_fec_bin = os.listdir()
                for fec_prog_found in list_dir_fec_bin:
                    index_fesa_bin = fec_prog_found.find('_DU_M')
                    index_fesa_specific_class = 0
                    if len(classes_to_find) > 0:
                        index_fesa_specific_class = -1
                        for class_to_find in classes_to_find:
                            if index_fesa_specific_class == -1:
                                index_fesa_specific_class = fec_prog_found.find(class_to_find + "_DU_M")
                    if index_fesa_bin != -1 and index_fesa_specific_class != -1:
                        fesa_class_name = fec_prog_found[:index_fesa_bin]
                        try:
                            fec_class[fec_found].index(fesa_class_name)
                        except:
                            fec_class[fec_found] = fesa_class_name
        except:
            pass
    return fec_class


def get_devices_from_class(class_to_find):
    """
    Get all device names for a given class name.

    :param class_to_find: List of class names.
    :type class_to_find: str
    :return: List of device names
    :rtype: List
    """

    key_cache = "get_devices_from_class#" + class_to_find
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    devices = []
    devices_ccda = sync_api.Device.search('deviceClassInfo.name==' + class_to_find)
    for device in devices_ccda:
        if device is not None:
            if device.name.find("GD") != 0:
                devices.append(device.name)

    DATA_CACHE[key_cache] = devices

    return devices


def get_timingdomain_from_device(device_to_find):
    """
    Get timing domain for a given device name.
    
    :param device_to_find: Device name
    :type device_to_find: str
    :return: Name of timing domain
    :rtype: str 
    """

    key_cache = "get_timingdomain_from_device#" + device_to_find
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    timing_domain = None
    for device in sync_api.Device.search('name==' + device_to_find):
        timing_domain = device.timing_domain

    DATA_CACHE[key_cache] = timing_domain

    return timing_domain


def get_devices_from_class_per_fec(class_to_find):
    """
    Get all device names for a given class name and organised per fec.

    :param class_to_find: FESA class name
    :type class_to_find: str
    :return: {fec_name,device_name[]}
    :rtype: dict
    """

    key_cache = "get_devices_from_class_per_fec#" + class_to_find
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    devices = get_devices_from_class(class_to_find)
    devices_per_fec = {}
    for device in devices:
        if device is not None:
            for device_def in sync_api.Device.search('name==' + device + " and deviceClassInfo.name==" + class_to_find):
                if device_def.fec_name is not None:
                    if device_def.fec_name not in devices_per_fec.keys():
                        devices_per_fec[device_def.fec_name] = []
                    devices_per_fec[device_def.fec_name].append(device)

    DATA_CACHE[key_cache] = devices_per_fec

    return devices_per_fec


def get_global_devices_per_fec(fec: str) -> List[str]:
    """
    Get the list of global devices for the giver FEC.

    :param fec: the FEC.
    :type fec: str.
    :return: List of global devices for the given FEC.
    :rtype: List[str].
    """

    try:
        return [device.name for device in sync_api.Device.search(f'fecName=={fec}; global==true')]
    except Exception as e:
        print(f"Error: {e}")
        return []


def get_properties_from_class(class_to_find, class_version=None):
    """
    Get all device names for a given class name.

    :param class_to_find: FESA class name.
    :type class_to_find: str
    :param class_version: FESA class version.
    :type class_version: str, optional
    :return: List of properties
    :rtype: list
    """

    key_cache = "get_properties_from_class#" + class_to_find
    if class_version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    properties = []
    fesa_class_version = ""
    if class_version is None:
        for deviceclass in sync_api.Device.search('deviceClassInfo.name==' + class_to_find):
            fesa_class_version = deviceclass.device_class_info.version
    else:
        fesa_class_version = class_version
    for deviceclass in sync_api.DeviceClass.search('version==' + fesa_class_version + ' and name==' + class_to_find):
        for properti in deviceclass.device_class_properties:
            if properti is not None:
                properties.append(properti)

    DATA_CACHE[key_cache] = properties

    return properties


def get_properties_from_device(device):
    """
    Get all properties for a given device.

    :param device: Device name.
    :type device: str
    :return: List of properties
    :rtype: list
    """

    key_cache = "get_properties_from_device#" + device
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    info = get_class_from_device(device)
    if len(info.keys()) == 0:
        return None
    names = []
    properties = get_properties_from_class(info["class"], info["version"])
    if len(properties) > 0:
        for propertyfesa in properties:
            if propertyfesa is not None:
                names.append(propertyfesa.name)

    DATA_CACHE[key_cache] = names

    return names


def get_property_fields_from_class(class_name, property_name, class_version=None):
    """
    Get all fields in a given property.

    :param class_name: FESA class name.
    :type class_name: str
    :param property_name: FESA property name.
    :type property_name: str
    :param class_version: FESA class version.
    :type class_version: str, optional
    :return: List of property field names
    :rtype: list
    """

    key_cache = "get_property_fields_from_class#" + class_name + "#" + property_name
    if class_version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    properties = get_properties_from_class(class_name, class_version)
    property_fields = []
    field_names = []
    for propertyFesa in properties:
        if propertyFesa is not None:
            if propertyFesa.name == property_name:
                for propertyField in propertyFesa.data_fields:
                    field_names.append(propertyField.name)
    field_names.sort()
    for propertyFesa in properties:
        if propertyFesa.name == property_name:
            for name in field_names:
                for propertyField in propertyFesa.data_fields:
                    if name == propertyField.name:
                        property_fields.append(propertyField)

    DATA_CACHE[key_cache] = property_fields

    return property_fields


def is_property_writable(class_name=None, property_name=None, class_version=None):
    """
    Check if the property is writable.

    :param class_name: FESA class name.
    :type class_name: str
    :param property_name: FESA property name.
    :type property_name: str
    :param class_version: FESA class version.
    :type class_version: str, optional
    :return: Check if property is writable
    :rtype: bool
    """

    key_cache = "is_property_writable#" + class_name + "#" + property_name
    if class_version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    properties = get_properties_from_class(class_name, class_version)
    for propertyFesa in properties:
        if propertyFesa.name == property_name:
            DATA_CACHE[key_cache] = propertyFesa.is_writable
            return propertyFesa.is_writable


def get_property_definition(class_name=None, property_name=None, class_version=None):
    """
    Get all fields in a given property.

    :param class_name: FESA class name.
    :type class_name: str
    :param property_name: FESA property name.
    :type property_name: str
    :param class_version: FESA class version.
    :type class_version: str, optional
    :return: Dict of definitions (visibility, writable, cycle_bound, is_multiplexed, readable, transactional, monitorable)
    :rtype: dict
    """

    key_cache = "get_property_definition#" + class_name + "#" + property_name
    if class_version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    properties = get_properties_from_class(class_name, class_version)

    definition = {}
    for propertyFesa in properties:
        if propertyFesa.name == property_name:
            definition["visibility"] = propertyFesa.visibility
            definition["writable"] = propertyFesa.is_writable
            definition["cycle_bound"] = propertyFesa.is_cycle_bound
            definition["is_multiplexed"] = propertyFesa.is_multiplexed
            definition["readable"] = propertyFesa.is_readable
            definition["transactional"] = propertyFesa.is_transactional
            definition["monitorable"] = propertyFesa.is_monitorable

    DATA_CACHE[key_cache] = definition

    return definition


def get_property_field_from_class(class_name=None, property_name=None, field_name=None, class_version=None):
    """
    Get field description in a given property.

    :param class_name: FESA class name.
    :type class_name: str
    :param property_name: FESA property name.
    :type property_name: str
    :param field_name: FESA field name.
    :type field_name: str
    :param class_version: FESA class version.
    :type class_version: str, optional
    :return: CCDA Property field definition
    :rtype: list
    """

    key_cache = "get_property_definition#" + class_name + "#" + property_name + "#" + field_name
    if class_version is None:
        if key_cache in DATA_CACHE:
            return DATA_CACHE[key_cache]

    properties = get_properties_from_class(class_name, class_version)
    for propertyFesa in properties:
        if propertyFesa.name == property_name:
            for propertyField in propertyFesa.data_fields:
                if propertyField.name == field_name:
                    DATA_CACHE[key_cache] = propertyField
                    return propertyField

    return []


def get_members_egroup(egroup="sy-dep-bi-sw"):
    """
    Get list all user belonging to a egroup.

    :param egroup: name of the egroup to consult (default sy-dep-bi-sw).
    :type egroup: str, optional
    :return: List of members
    :rtype: list
    """
    url = "https://ccda.cern.ch:8900/api/egroups/" + egroup
    try:
        response = json.loads(urllib.request.urlopen(url).read().decode())
        accounts = response['accounts']
        members = []
        for account in response['accounts']:
            members.append(account['userName'])
        return members
    except:
        return None


def get_instance_fields(key=""):
    key_cache = "get_instance_fields#" + key
    if key_cache in DATA_CACHE:
        return DATA_CACHE[key_cache]

    dev_info_fields = {}

    for device_def in sync_api.Device.search("name=='" + key + "'"):
        if device_def is not None:
            try:
                myFesaDev = sync_api.FesaDevice.from_device(device_def)
                info_field_values = {}
                fvs = myFesaDev.field_values
                for fv in fvs:
                    ff = fv.field_name
                    vv = fv.field_value
                    info_field_values[ff] = vv
                dev_info_fields[device_def.name] = info_field_values
            except:
                pass

    return dev_info_fields


if __name__ == "__main__":
    # property_fields = get_property_fields_from_class("BSRALHC","Acquisition")
    # for propertyField in property_fields:
    #     print(propertyField)
    # properties_fesa = get_properties_from_class("BCTDCPS")
    # print(properties_fesa)
    # property_fesa = get_property_definition("BSRALHC","Acquisition")
    # print(property_fesa)
    # property_field = get_property_field_from_class("BSRALHC","Acquisition","status_verification_process_with_led")
    # print(property_field.property_field_enums)
    # for propertyFieldEnum in property_field.property_field_enums:
    #     print(propertyFieldEnum.value)    
    # print(get_members_egroup())

    # class_names = ["BTVDC"]

    # get_pffc_opts = {
    #         'class_name': class_names[0],
    #         'property_name': "ExpertSetting",
    #         'class_version': None
    #     }

    # print(get_all_devices(key="BCSPILLSPS"))

    # print(get_properties_from_class("BCSPILLSPS"))

    t1 = round(time.time() * 1000)

    # print(get_devices_from_class(class_names[0]))

    # print(get_property_fields_from_class(**get_pffc_opts))

    # print(get_properties_from_class(class_names[0]))

    # print(get_fesa_design("BSISO",version="0.9.1")['equipment-model']["scheduling-units"])

    t2 = round(time.time() * 1000)

    # print(get_devices_from_class_per_fec("BCSPILLSPS"))

    # print(get_fecs_from_class(class_names))

    # print(get_classes_from_fec(class_names))

    # print(get_all_classes2())

    # fesa_db_fec_class_info = get_fecs_from_class(class_names, operational=False)
    # for fesa_class_name, fesa_class_fec_names in fesa_db_fec_class_info.items():
    #     dict_fesa_fec = get_devices_from_class_per_fec(fesa_class_name)
    #     class_menu_item = class_menu.addMenu(fesa_class_name)
    #     for fec_name in dict_fesa_fec.keys():
    #         devices = dict_fesa_fec[fec_name]
    #         fec_menu_item = class_menu_item.addMenu(fec_name)
    # for device in devices:
    #     fec_menu_item.addAction(device, partial(self.open_tab_panel, device))

    # print(get_instance_fields2("PR.BPM"))
    print(get_instance_fields("BR*.BPM"))

    print("done " + str(t2 - t1))
