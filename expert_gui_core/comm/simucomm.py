import time
import logging
from abc import abstractmethod

from PyQt5.QtCore import QTimer


class SimuCommListener:
    """
    Listener to FESA Simulated notifiction events.    
    """

    @abstractmethod
    def handle_event(self, name, value):
        """
        Notification received.

        :param name: Name of the notification key (device/property).
        :type name: str
        :param value: Datamap received.
        :type value: dict        
        """
        pass


class SimuDataObject:
    """
    Simulated communication data storage.

    :param data: Initial data.
    :type data: map, optional, can be set later
    """

    def __init__(self, data=None):
        """
        Initialize the SimuDataObject.
        """
        self.data = data

    def get(self, condition=None, cycle=None):
        """
        Get the data from the SimuDataObject.

        :param condition: Specific condition for the data.
        :type condition: function that returns a bool, optional
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Data stored in the SimuDataObject.
        :rtype: dict
        """
        if self.data is None:
            return {}
        return self.data

    def set_data(self, data_map):
        """
        Set the data in the SimuDataObject.

        :param data_map: Data to be set.
        :type data_map: dict
        """
        self.data = data_map


class _SimuTimerObject:
    """
    Timer Object for Simulated data.

    :param _simucomm: Simucomm object.
    :type _simucomm: object
    :param condition: Specific condition for the data
    :type condition: function that returns a bool, optional
    :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
    :type cycle: str
    """

    def __init__(self, simucomm, condition=None, cycle=None):
        """
        Initialize the _SimuTimerObject.
        """
        self.timer = QTimer()
        self.timer.timeout.connect(self.handleTimerTimeout)
        self._simucomm = simucomm
        self._condition = condition
        self._cycle = cycle

    def startMonitoring(self, timer_period=1000):
        """
        Start monitoring with a specified timer period.

        :param timer_period: Timer period in milliseconds.
        :type timer_period: int
        """
        if not self.timer.isActive():
            self.timer.start(timer_period)

    def stopMonitoring(self):
        """
        Stop monitoring.
        """
        if self.timer is not None:
            self.timer.stop()
            self.timer = None

    def handleTimerTimeout(self):
        """
        Handle timer timeout event.
        Retrieve data from simudataobject and send it to simucomm.handle_event.

        """

        # retrieve data from simucomm._simudataobject

        data = self._simucomm._simudataobject.get(condition=self._condition, cycle=self._cycle)

        # send data to simucomm.handle_event

        self._simucomm.handle_event(self._simucomm._device + "/" + self._simucomm._property, data)


class SimuComm:
    """
    FESA Simulated communication wrapper.

    :param dev: Device name.
    :type dev: str
    :param prop: Property name.
    :type prop: str
    :param simudataobject: SimuDataObject object for simulation.
    :type simudataobject: object
    :param listener: Simuistener object.
    :type listener: object, optional
    :param field: Field name.     
    :type field: str, optional
    """

    def __init__(self, dev, prop, simudataobject, listener=None, timer_period=1000):
        """
        Initialize class.
        """

        self._device = dev
        self._property = prop
        self._simudataobject = simudataobject
        self._sub = None
        self._lock = False
        self.timer_period = timer_period
        self.timer = None
        self._time_now = 0
        self._listener = listener

    def get(self, condition=None, cycle=None):
        """
        Get FESA Simulated property.

        :param condition: Specific condition for the data
        :type condition: function that returns a bool, optional
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Datamap object
        :rtype: dict
        """
        
        logging.info("Get : " + self._device + "/" + self._property)

        try:
            result = self._simudataobject.get(condition, cycle)
        except:
            return None
        return result

    def subscribe(self, condition=None, cycle=None):
        """
        Subscribe to a FESA Simulated device property.

        :param condition: Specific condition for the data
        :type condition: function that returns a bool, optional
        :param cycle: Specific timing cycle name (ex: CPS.USER.TOF).
        :type cycle: str
        :return: Status object {err value, msg}
        :rtype: dict
        """
        if self._sub is not None:
            self.unsubscribe()

        logging.info(str(cycle) + " " + self._device + "/" + self._property)

        try:
            self._sub = _SimuTimerObject(self, condition=condition, cycle=cycle)

            self._sub.startMonitoring(timer_period=self.timer_period)

            msg = "subscribe ok to cycle " + str(cycle)
            logging.info(msg)
            return {"err": 0, "msg": msg}
        except:
            logging.warning("problem cycle" + str(cycle))

    def unsubscribe(self):
        """
        Unsubscribe to a FESA Simulated device property.
        """
        if self._sub is not None:
            self._sub.stopMonitoring()
            self._sub = None

    def handle_event(self, name, value, header=None):
        """
        Notification received.
            
        :param name: Name of the notification key (device/property).

        :param value: Datamap received.
        :param header: Include header parameter (cyclename cycletstamp...), optional
        """

        # check if already locked

        # if self._lock:
        #     logging.debug(name + " event lost!")
        #     return
        #
        # self.lock = True

        # check 2 consecutive timestamp
        if round(time.time() * 1000) - self._time_now < 10:
            # self.lock = False
            return
        else:
            pass

        self._time_now = round(time.time() * 1000)

        # add time and cycle in data map

        value['_time'] = self._time_now

        try:
            if header is not None:
                value['_cycle'] = header['selector']
            else:
                value['_cycle'] = ""
        except:
            value['_cycle'] = ""

        # notification

        if self._listener is not None:
            self._listener.handle_event(name, value)

        # release lock

        # self.lock = False
