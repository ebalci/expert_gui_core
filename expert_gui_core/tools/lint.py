import sys
from pathlib import Path
from pylint import run_pylint

def main():
    run_pylint(argv=["--rcfile", str(Path(__file__).parent.parent.absolute() / "pylintrc"), sys.argv[1]])

if __name__ == '__main__':
    main()