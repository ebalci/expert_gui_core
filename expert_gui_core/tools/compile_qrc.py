import sys
import subprocess
from os import path, walk

compile_resources = 'pyrcc5 {target_qrc} -o {outpath}'

def main():

    widgets_root = sys.argv[2]

    for root, dirs, files in walk(widgets_root, topdown=False):

        for file in files:
            if file.endswith('.qrc'):

                outpath = '{}.py'.format(path.splitext(path.join(root, 'resource'))[0])

                command = compile_resources.format(
                    target_qrc=path.join(root, file),
                    outpath=outpath
                )
                subprocess.run(command.split(' '), stdout=subprocess.PIPE)


if __name__ == '__main__':
    main()