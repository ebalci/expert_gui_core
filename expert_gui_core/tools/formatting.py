import time

from datetime import datetime
from dateutil.parser import parse
from fxpmath import Fxp 
from jpype.types import *

def current_milli_time():
    """Time now in ms"""
    return (time.time() * 1000)

def float_bin(num):
    """Convertion float to binary"""
    return bin(num)

def format_value_to_string(data,type_format="str", format_sci="{:.8e}"):
    """Format value to string in data/bin/hex/sci/fxp131/fun or none"""
    if type_format == "fun":
        type_format = "str"
    if type_format == "str" or str(type(data)) == "str":
        return str(data)    
    elif type_format == "date":
        return datetime.fromtimestamp(int(float(data))).isoformat(sep=' ', timespec='milliseconds')
    elif type_format == "idate":
        return str(parse(data).timestamp())
    elif type_format == "bin":
        return str(float_bin(int(data)))        
    elif type_format == "ibin":
        return str(int(data,2))
    elif type_format == "fxp131":
        fxp = Fxp(data, signed=True, raw=True, n_word=32, n_frac=31)
        return str(fxp.astype(float))
    elif type_format == "ifxp131":
        fdata = float(data)
        return str(round(fdata*(2**31)))
    elif type_format == "hex":
        try:
            if (str(type(data))).index('float') >= 0:
                return float.hex(data).upper()            
        except:
            return hex(int(float(data))).upper()
    elif type_format == "ihex":
        return str(int(data,0))
    elif type_format == "sci":
        fdata = float(data)
        return str(format_sci.format(fdata))
    elif type_format == "isci":
        return str(float(data))

def str_find(str,val):
    try:
        ind = str.index(val)
        return ind
    except:
        return -1
    return -1

def type_value(value,type):
    stype = str(type)
    if value == '' or value == None:
        return 0
    if str_find(stype,"nt") >= 0 or str_find(stype,"hort") >= 0 or str_find(stype,"yte") >= 0 or str_find(stype,"ong") >= 0 or str_find(stype,"har") >= 0:
        return int(float(value))
    elif str_find(stype,"loat") >= 0 or str_find(stype,"ouble") >= 0:
        return float(value)
    elif str_find(stype,"ool") >= 0:
        return bool(value)
    elif str_find(stype,"tr") >= 0:
        return str(value)
    return value

def type_value_to_java(value,type):
    stype = str(type)
    le=0
    try:
        le=len(value)
    except:
        le=0
    if le==0:
        if str_find(stype,"JShort") >= 0:
            return JShort(float(value))        
        elif str_find(stype,"JInt") >= 0:
            return JInt(float(value))        
        elif str_find(stype,"JLong") >= 0:
            return JLong(float(value))        
        elif str_find(stype,"JChar") >= 0:
            return JChar(float(value))        
        elif str_find(stype,"JByte") >= 0:
            return JByte(float(value))        
        elif str_find(stype,"JBoolean") >= 0:
            return JBoolean(float(value))        
        elif str_find(stype,"JChar") >= 0:
            return JChar(float(value))        
        elif str_find(stype,"JDouble") >= 0:
            return JDouble(float(value))        
        elif str_find(stype,"JFloat") >= 0:
            return JFloat(float(value))    
        else:
            return type_value(value,type)    
    else:
        for i in range(le):
            if str_find(stype,"JShort") >= 0:
                value[i]  = JShort(float(value[i]))        
            elif str_find(stype,"JInt") >= 0:
                value[i]  =  JInt(float(value[i]))        
            elif str_find(stype,"JLong") >= 0:
                value[i]  =  JLong(float(value[i]))        
            elif str_find(stype,"JChar") >= 0:
                value[i]  =  JChar(float(value[i]))        
            elif str_find(stype,"JByte") >= 0:
                value[i]  =  JByte(float(value[i]))        
            elif str_find(stype,"JBoolean") >= 0:
                value[i]  =  JBoolean(float(value[i]))        
            elif str_find(stype,"JChar") >= 0:
                value[i]  =  JChar(float(value[i]))        
            elif str_find(stype,"JDouble") >= 0:
                value[i]  =  JDouble(float(value[i]))        
            elif str_find(stype,"JFloat") >= 0:
                value[i]  =  JFloat(float(value[i]))    
            else:
                value[i]  = type_value(value[i],type)    
    return value